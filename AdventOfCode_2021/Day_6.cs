﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public unsafe static class Day_6
    {
        public static void PartOne()
        {
            Console.WriteLine(FindNumberOfFish(Data, 80));
            // 359999
        }

        public static void PartTwo()
        {
            Console.WriteLine(FindNumberOfFish(Data, 256));
            // 1631647919273
        }

        private static ulong FindNumberOfFish(int[] data, int numberOfDaysToSimulateFor)
        {
            var numberOfDaysForNewFish = 9;
            var numberOfDaysForOldFish = 7;

            var daysToNumberOfFishLength = 9;
            var daysToNumberOfFish = stackalloc ulong[daysToNumberOfFishLength];
            var day0Index = 0;

            // Populate the initial fish counts
            for (int i = 0; i < data.Length; i++)
            {
                daysToNumberOfFish[data[i]]++;
            }

            // Keep a running count of the number of fish
            ulong totalNumberOfFish = (ulong)data.Length;

            // Simulate for the number of days
            for (int i = 0; i < numberOfDaysToSimulateFor; i++)
            {
                // Get the number of fish for today
                var numberOfFishGivingBirth = daysToNumberOfFish[day0Index];

                // Add the old fish
                daysToNumberOfFish[(day0Index + numberOfDaysForOldFish) % daysToNumberOfFishLength] += numberOfFishGivingBirth;

                // Increment the day 0 index
                day0Index = (day0Index + 1) % daysToNumberOfFishLength;

                // Add to the total number of fish
                totalNumberOfFish += numberOfFishGivingBirth;
            }

            return totalNumberOfFish;
        }

        private static readonly int[] Data = new int[]
        {
            5,3,2,2,1,1,4,1,5,5,1,3,1,5,1,2,1,4,1,2,1,2,1,4,2,4,1,5,1,3,5,4,3,3,1,4,1,3,4,4,1,5,4,3,3,2,5,1,1,3,1,4,3,2,2,3,1,3,1,3,1,5,3,5,1,3,1,4,2,1,4,1,5,5,5,2,4,2,1,4,1,3,5,5,1,4,1,1,4,2,2,1,3,1,1,1,1,3,4,1,4,1,1,1,4,4,4,1,3,1,3,4,1,4,1,2,2,2,5,4,1,3,1,2,1,4,1,4,5,2,4,5,4,1,2,1,4,2,2,2,1,3,5,2,5,1,1,4,5,4,3,2,4,1,5,2,2,5,1,4,1,5,1,3,5,1,2,1,1,1,5,4,4,5,1,1,1,4,1,3,3,5,5,1,5,2,1,1,3,1,1,3,2,3,4,4,1,5,5,3,2,1,1,1,4,3,1,3,3,1,1,2,2,1,2,2,2,1,1,5,1,2,2,5,2,4,1,1,2,4,1,2,3,4,1,2,1,2,4,2,1,1,5,3,1,4,4,4,1,5,2,3,4,4,1,5,1,2,2,4,1,1,2,1,1,1,1,5,1,3,3,1,1,1,1,4,1,2,2,5,1,2,1,3,4,1,3,4,3,3,1,1,5,5,5,2,4,3,1,4
        };

        private static readonly int[] DataTest = new int[]
        {
            3,4,3,1,2
        };
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(16)]
[SolutionInput("Inputs/Day_16_Test.txt", Problem1Solution = "46", Problem2Solution = "51")]
[SolutionInput("Inputs/Day_16.txt", Problem1Solution = "7199", Problem2Solution = "7438", Benchmark = true)]
public unsafe class Day_16 : Solution
{
    private readonly record struct LightBeam(Position Position, Direction Direction);
    private readonly record struct Position(int X, int Y)
    {
        public static Position operator +(Position a, Position b) => new Position(a.X + b.X, a.Y + b.Y);
    }
    private enum Direction : byte
    {
        Up,
        Down,
        Left,
        Right
    }

    private readonly int width;
    private readonly int height;
    private readonly Queue<LightBeam> lightBeamsToProcess;
    private readonly HashSet<LightBeam> lightBeamsProcessed;
    private readonly HashSet<Position> cellsTouched;

    public Day_16(Input input)
        : base(input)
    {
        width = Input.Lines[0].Length;
        height = Input.Lines.Length;

        lightBeamsToProcess = new Queue<LightBeam>();
        lightBeamsProcessed = new HashSet<LightBeam>(width * height);
        cellsTouched = new HashSet<Position>(width * height);
    }

    protected override string? Problem1()
    {
        return RunFromStartingLightBeam(new LightBeam(new Position(0, 0), Direction.Right)).ToString();
    }

    protected override string? Problem2()
    {
        // Get each starting cell
        var startingLightBeams = new List<LightBeam>((width * 2) + (height * 2));
        for (int x = 0; x < width; x++)
        {
            startingLightBeams.Add(new LightBeam(new Position(x, 0), Direction.Down));
            startingLightBeams.Add(new LightBeam(new Position(x, height - 1), Direction.Up));
        }
        for (int y = 0; y < height; y++)
        {
            startingLightBeams.Add(new LightBeam(new Position(0, y), Direction.Right));
            startingLightBeams.Add(new LightBeam(new Position(width - 1, y), Direction.Left));
        }

        // Run for each starting cell, getting the maximum cells touched
        var maximumCellsTouched = 0;
        foreach (var startingLightBeam in startingLightBeams)
        {
            maximumCellsTouched = int.Max(
                maximumCellsTouched,
                RunFromStartingLightBeam(startingLightBeam));
        }

        return maximumCellsTouched.ToString();
    }

    private int RunFromStartingLightBeam(LightBeam startingLightBeam)
    {
        lightBeamsToProcess.Clear();
        lightBeamsProcessed.Clear();
        cellsTouched.Clear();

        // Start with the top left moving right
        lightBeamsToProcess.Enqueue(startingLightBeam);

        // Process the light beams
        while (lightBeamsToProcess.TryDequeue(out var lightBeam))
        {
            // Process this light beam while it's still within bounds and we haven't processed one
            // with the same position and direction (protects agains infinite loops
            while ((lightBeam.Position.X >= 0 & lightBeam.Position.X < width &
                lightBeam.Position.Y >= 0 & lightBeam.Position.Y < height) &&
                lightBeamsProcessed.Add(lightBeam))
            {
                // Set this cell as touched
                cellsTouched.Add(lightBeam.Position);

                // Check the tile at this cell
                var tile = Input.Lines[lightBeam.Position.Y][lightBeam.Position.X];
                if (tile == '\\' | tile == '/')
                {
                    // Get the new direction
                    Direction directionNew;
                    var isLeftLeaningMirror = tile == '\\';
                    switch (lightBeam.Direction)
                    {
                        case Direction.Up:
                            directionNew = isLeftLeaningMirror ? Direction.Left : Direction.Right;
                            break;
                        case Direction.Down:
                            directionNew = isLeftLeaningMirror ? Direction.Right : Direction.Left;
                            break;
                        case Direction.Left:
                            directionNew = isLeftLeaningMirror ? Direction.Up : Direction.Down;
                            break;
                        case Direction.Right:
                            directionNew = isLeftLeaningMirror ? Direction.Down : Direction.Up;
                            break;
                        default:
                            throw new Exception("Unrecognised direction.");
                    }

                    // Get the new offset
                    var positionOffset = GetDirectionPositionOffset(directionNew);

                    // Update the light beam
                    lightBeam = lightBeam with
                    {
                        Position = lightBeam.Position + positionOffset,
                        Direction = directionNew
                    };
                }
                else if (tile == '-')
                {
                    // Split the beam in left and right directions
                    lightBeamsToProcess.Enqueue(
                        lightBeam with
                        {
                            Position = lightBeam.Position + GetDirectionPositionOffset(Direction.Left),
                            Direction = Direction.Left
                        });
                    lightBeamsToProcess.Enqueue(
                        lightBeam with
                        {
                            Position = lightBeam.Position + GetDirectionPositionOffset(Direction.Right),
                            Direction = Direction.Right
                        });

                    // Stop processing this beam
                    break;
                }
                else if (tile == '|')
                {
                    // Split the beam in up and down directions
                    lightBeamsToProcess.Enqueue(
                        lightBeam with
                        {
                            Position = lightBeam.Position + GetDirectionPositionOffset(Direction.Up),
                            Direction = Direction.Up
                        });
                    lightBeamsToProcess.Enqueue(
                        lightBeam with
                        {
                            Position = lightBeam.Position + GetDirectionPositionOffset(Direction.Down),
                            Direction = Direction.Down
                        });

                    // Stop processing this beam
                    break;
                }
                else
                {
                    // Continue in the current direction
                    lightBeam = lightBeam with
                    {
                        Position = lightBeam.Position + GetDirectionPositionOffset(lightBeam.Direction)
                    };
                }
            }
        }

        return cellsTouched.Count;
    }

    private Position GetDirectionPositionOffset(Direction direction) => direction switch
    {
        Direction.Up => new Position(0, -1),
        Direction.Down => new Position(0, 1),
        Direction.Left => new Position(-1, 0),
        Direction.Right => new Position(1, 0),
        _ => throw new Exception("Unrecognised direction."),
    };
}

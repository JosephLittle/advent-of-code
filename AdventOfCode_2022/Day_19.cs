﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(19)]
[SolutionInput("Inputs/Day_19_Test.txt", Problem1Solution = "33", Problem2Solution = "3472")]
//[SolutionInput("Inputs/Day_19_Test_2.txt", Problem1Solution = "", Problem2Solution = "")]
[SolutionInput("Inputs/Day_19.txt", Problem1Solution = "1958", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_19 : Solution
{
    private readonly record struct Blueprint(
        ushort Id,
        ushort OreRobotOreCost,
        ushort ClayRobotOreCost,
        ushort ObsidianRobotOreCost,
        ushort ObsidianRobotClayCost,
        ushort GeodeRobotOreCost,
        ushort GeodeRobotObsidianCost)
    {
        public readonly ushort MaximumOreCost = Math.Max(OreRobotOreCost, Math.Max(ClayRobotOreCost, Math.Max(ObsidianRobotOreCost, GeodeRobotOreCost)));
        public readonly ushort MaximumClayCost = ObsidianRobotClayCost;
        public readonly ushort MaximumObsidianCost = GeodeRobotObsidianCost;

        public bool HasMaximumResources(WorldState state) => 
            state.OreCount >= MaximumOreCost &
            state.ClayCount >= MaximumClayCost &
            state.ObsidianCount >= MaximumObsidianCost;

        public bool IsBetterState(WorldState lhs, WorldState rhs)
        {
            return
                // Check whether we have more resources or the maximum resources
                (lhs.OreCount >= rhs.OreCount | lhs.OreCount >= MaximumOreCost) &
                (lhs.ClayCount >= rhs.ClayCount | lhs.ClayCount >= MaximumClayCost) &
                (lhs.ObsidianCount >= rhs.ObsidianCount | lhs.ObsidianCount >= MaximumObsidianCost) &
                (lhs.GeodeCount >= rhs.GeodeCount) &
                // Check whether we have more robots or the maximum useful robots
                (lhs.OreRobotCount >= rhs.OreRobotCount | lhs.OreRobotCount >= MaximumOreCost) &
                (lhs.ClayCount >= rhs.ClayRobotCount | lhs.ClayRobotCount >= MaximumClayCost) &
                (lhs.ObsidianRobotCount >= rhs.ObsidianRobotCount | lhs.ObsidianRobotCount >= MaximumObsidianCost) &
                (lhs.GeodeRobotCount >= rhs.GeodeRobotCount);
        }
    }

    private readonly record struct WorldState(
        ushort OreRobotCount,
        ushort ClayRobotCount,
        ushort ObsidianRobotCount,
        ushort GeodeRobotCount,
        ushort OreCount,
        ushort ClayCount,
        ushort ObsidianCount,
        ushort GeodeCount);

    public Day_19(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var blueprints = ParseBlueprints(Input.Lines, int.MaxValue);
        var geodeCounts = GetHighestGeodeCountsForBlueprints(blueprints, 24);

        var qualityLevel = 0;
        for (int i = 0; i < geodeCounts.Length; i++)
        {
            qualityLevel += (i + 1) * geodeCounts[i];
        }

        return qualityLevel.ToString();
    }

    protected override string? Problem2()
    {
        var blueprints = ParseBlueprints(Input.Lines, 3);
        var geodeCounts = GetHighestGeodeCountsForBlueprints(blueprints, 32);

        var geodeCountProduct = geodeCounts[0];
        for (int i = 1; i < geodeCounts.Length; i++)
        {
            geodeCountProduct *= geodeCounts[i];
        }
        return geodeCountProduct.ToString();
    }

    private Blueprint[] ParseBlueprints(string[] inputLines, int maximumNumberOfBlueprints)
    {
        var blueprints = new Blueprint[Math.Min(maximumNumberOfBlueprints, Input.Lines.Length)];

        // Read the input into blueprints
        for (int i = 0; i < Input.Lines.Length & i < maximumNumberOfBlueprints; i++)
        {
            var line = Input.Lines[i];

            // Parse the ore robot cost
            var characterIndex = i >= 9 ? 1 : 0;
            characterIndex += 34;
            var oreRobotOreCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');

            // Parse the clay robot cost
            characterIndex += 28;
            var clayRobotOreCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');

            // Parse the obsidian robot cost
            characterIndex += 32;
            var obsidianRobotOreCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');
            characterIndex += 9;
            var obsidianRobotClayCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');

            // Parse the geode robot cost
            characterIndex += 30;
            var geodeRobotOreCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');
            characterIndex += 9;
            var geodeRobotObsidianCost = (byte)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(
                line, ref characterIndex, ' ');

            // Create the blueprints
            blueprints[i] = new Blueprint(
                (byte)(i + 1),
                oreRobotOreCost,
                clayRobotOreCost,
                obsidianRobotOreCost,
                obsidianRobotClayCost,
                geodeRobotOreCost,
                geodeRobotObsidianCost);
        }

        return blueprints;
    }

    private ushort[] GetHighestGeodeCountsForBlueprints(Blueprint[] blueprints, int numberOfMinutesToRunFor)
    {
        // Set up the world state tracking 
        var statesForCurrentMinute = new HashSet<WorldState>();
        var statesForNextMinute = new HashSet<WorldState>();

        var highestGeodeCounts = new ushort[blueprints.Length];

        // Run for each blueprint
        foreach (var blueprint in blueprints)
        {
            Console.WriteLine($"Starting blueprint: {blueprint}");

            // Track the best state
            var bestState = new WorldState();

            // Set the initial state
            statesForNextMinute.Clear();
            statesForNextMinute.Add(new WorldState(
                1, 0, 0, 0,
                0, 0, 0, 0));

            // Run each minute
            for (int currentMinute = 0; currentMinute < numberOfMinutesToRunFor; currentMinute++)
            {
                // Swap the states map
                (statesForCurrentMinute, statesForNextMinute) = (statesForNextMinute, statesForCurrentMinute);
                statesForNextMinute.Clear();

                // Process each possible state for this minute
                foreach (var currentState in statesForCurrentMinute)
                {
                    // Finalise without building anything
                    // (only do this if we have less than the maximum costs)
                    if ((currentState.OreCount < blueprint.MaximumOreCost) |
                        (currentState.ClayCount < blueprint.MaximumClayCost & currentState.ClayRobotCount > 0) |
                        (currentState.ObsidianCount < blueprint.MaximumObsidianCost & currentState.ObsidianRobotCount > 0) |
                        currentState.GeodeRobotCount > 0)
                    {
                        FinaliseState(currentState);
                    }

                    // Attempt to build an ore robot (only if we have something to gain from it)
                    if (currentState.OreCount >= blueprint.OreRobotOreCost &
                        currentState.OreRobotCount < blueprint.MaximumOreCost)
                    {
                        FinaliseState(currentState with
                        {
                            OreCount = (byte)(currentState.OreCount - blueprint.OreRobotOreCost),
                            OreRobotCount = (byte)(currentState.OreRobotCount + 1)
                        });
                    }

                    // Attempt to build a clay robot (only if we have something to gain from it)
                    if (currentState.OreCount >= blueprint.ClayRobotOreCost &
                        currentState.ClayRobotCount < blueprint.MaximumClayCost)
                    {
                        FinaliseState(currentState with
                        {
                            OreCount = (byte)(currentState.OreCount - blueprint.ClayRobotOreCost),
                            ClayRobotCount = (byte)(currentState.ClayRobotCount + 1)
                        });
                    }

                    // Attempt to build an obsidian robot (only if we have something to gain from it)
                    if (currentState.OreCount >= blueprint.ObsidianRobotOreCost &
                        currentState.ClayCount >= blueprint.ObsidianRobotClayCost &
                        currentState.ObsidianRobotCount < blueprint.MaximumObsidianCost)
                    {
                        FinaliseState(currentState with
                        {
                            OreCount = (byte)(currentState.OreCount - blueprint.ObsidianRobotOreCost),
                            ClayCount = (byte)(currentState.ClayCount - blueprint.ObsidianRobotClayCost),
                            ObsidianRobotCount = (byte)(currentState.ObsidianRobotCount + 1)
                        });
                    }

                    // Attempt to build a geode robot
                    if (currentState.OreCount >= blueprint.GeodeRobotOreCost &
                        currentState.ObsidianCount >= blueprint.GeodeRobotObsidianCost)
                    {
                        FinaliseState(currentState with
                        {
                            OreCount = (byte)(currentState.OreCount - blueprint.GeodeRobotOreCost),
                            ObsidianCount = (byte)(currentState.ObsidianCount - blueprint.GeodeRobotObsidianCost),
                            GeodeRobotCount = (byte)(currentState.GeodeRobotCount + 1)
                        });
                    }

                    // Finalise the current state by doing production and adding to the next states
                    void FinaliseState(WorldState stateToFinalise)
                    {
                        // Do production
                        var finalState = stateToFinalise with
                        {
                            OreCount = (byte)(stateToFinalise.OreCount + currentState.OreRobotCount),
                            ClayCount = (byte)(stateToFinalise.ClayCount + currentState.ClayRobotCount),
                            ObsidianCount = (byte)(stateToFinalise.ObsidianCount + currentState.ObsidianRobotCount),
                            GeodeCount = (byte)(stateToFinalise.GeodeCount + currentState.GeodeRobotCount)
                        };

                        var minutesRemaining = (numberOfMinutesToRunFor - currentMinute) - 1;
                        if (minutesRemaining < 10 & finalState.GeodeRobotCount > 0 &
                            (finalState.GeodeCount + (finalState.GeodeRobotCount * minutesRemaining) + (((minutesRemaining * minutesRemaining) + minutesRemaining) / 2)) <
                            (bestState.GeodeCount + (bestState.GeodeRobotCount * minutesRemaining)))
                        {

                        }
                        else
                        {
                            if (statesForNextMinute.Add(finalState))
                            {
                                if (blueprint.IsBetterState(finalState, bestState))
                                {
                                    bestState = finalState;
                                }
                            }
                        }
                    }
                }

                //Console.WriteLine($"Blueprint {blueprint.Id} minute {currentMinute} done... (next states: {statesForNextMinute.Count})");
            }

            // Get the highest geode count for this blueprint
            var highestGeodeCount = ushort.MinValue;
            foreach (var state in statesForNextMinute)
            {
                if (state.GeodeCount > highestGeodeCount)
                {
                    highestGeodeCount = state.GeodeCount;
                }
            }
            highestGeodeCounts[blueprint.Id - 1] = highestGeodeCount;

            //Console.WriteLine($"Blueprint {blueprint.Id} highest geode count: {highestGeodeCount}");
        }

        return highestGeodeCounts;
    }
}

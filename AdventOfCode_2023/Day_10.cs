﻿using AdventOfCode.Framework;
using System.Runtime.CompilerServices;

namespace AdventOfCode_2023;

[Solution(10)]
[SolutionInput("Inputs/Day_10_Test_01.txt", Problem1Solution = "8", Problem2Solution = "1")]
[SolutionInput("Inputs/Day_10_Test_02.txt", Problem1Solution = "23", Problem2Solution = "4")]
[SolutionInput("Inputs/Day_10.txt", Problem1Solution = "6820", Problem2Solution = "337", Benchmark = true)]
public unsafe class Day_10 : Solution
{
    public Day_10(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        const char StartingCharacter = 'S';

        // Find the starting position
        var startingX = -1;
        var startingY = -1;
        for (int y = 0; y < Input.Lines.Length; y++)
        {
            var line = Input.Lines[y];
            for (int x = 0; x < line.Length; x++)
            {
                if (line[x] == StartingCharacter)
                {
                    startingX = x;
                    startingY = y;
                    goto EndNestedLoop;
                }
            }
        }
    EndNestedLoop:

        // Track the current and previous position
        var previousX = startingX;
        var previousY = startingY;
        var currentX = startingX;
        var currentY = startingY;
        var numberOfSteps = 0;

        // Make the first step from the starting pipe
        char pipe;
        if ((pipe = GetCharAt(startingX, currentY - 1)) == '|' | pipe == 'F' | pipe == '7')
        {
            currentY -= 1;
        }
        else if ((pipe = GetCharAt(startingX, currentY + 1)) == '|' | pipe == 'L' | pipe == 'J')
        {
            currentY += 1;
        }
        else if ((pipe = GetCharAt(startingX - 1, currentY)) == '-' | pipe == 'L' | pipe == 'F')
        {
            currentX -= 1;
        }
        else if ((pipe = GetCharAt(startingX + 1, currentY)) == '-' | pipe == '7' | pipe == 'J')
        {
            currentX += 1;
        }
        numberOfSteps++;

        // Traverse the pipes
        do
        {
            var previousXTemp = currentX;
            var previousYTemp = currentY;

            switch (pipe)
            {
                case '|':
                    // Move up or down
                    currentY += (currentY - previousY);
                    break;
                case '-':
                    // Move left or right
                    currentX += (currentX - previousX);
                    break;
                case 'F':
                    // Move right or down 
                    if (previousX != currentX)
                    {
                        currentY += 1;
                    }
                    else
                    {
                        currentX += 1;
                    }
                    break;
                case '7':
                    // Move left or down
                    if (previousX != currentX)
                    {
                        currentY += 1;
                    }
                    else
                    {
                        currentX -= 1;
                    }
                    break;
                case 'L':
                    // Move up or right
                    if (previousX != currentX)
                    {
                        currentY -= 1;
                    }
                    else
                    {
                        currentX += 1;
                    }
                    break;
                case 'J':
                    // Move up or left
                    if (previousX != currentX)
                    {
                        currentY -= 1;
                    }
                    else
                    {
                        currentX -= 1;
                    }
                    break;
            }

            // Update previous
            previousX = previousXTemp;
            previousY = previousYTemp;

            // Update pipe character
            pipe = GetCharAt(currentX, currentY);

            // Update steps
            numberOfSteps++;
        }
        while (pipe != StartingCharacter);

        // Return half number of steps
        return (numberOfSteps / 2).ToString();

        // Local method to get the character at the given position
        char GetCharAt(int x, int y) => Input.Lines[y][x];
    }

    public enum Direction
    {
        None,

        Up,
        Down,
        Left,
        Right
    }

    protected override string? Problem2()
    {
        const char StartingCharacter = 'S';

        // Find the starting position
        var startingX = -1;
        var startingY = -1;
        for (int y = 0; y < Input.Lines.Length; y++)
        {
            var line = Input.Lines[y];
            for (int x = 0; x < line.Length; x++)
            {
                if (line[x] == StartingCharacter)
                {
                    startingX = x;
                    startingY = y;
                    goto EndNestedLoop;
                }
            }
        }
    EndNestedLoop:

        // Create a map to write the path to
        var mapWidth = Input.Lines[0].Length;
        var mapHeight = Input.Lines.Length;
        Span<char> mapMemory = stackalloc char[mapWidth * mapHeight];
        ref char GetMapCell(Span<char> map, int x, int y) => ref map[(mapWidth * y) + x];

        mapMemory.Fill(' ');

        // Track the current and previous position
        var currentX = startingX;
        var currentY = startingY;

        var numberOfRightTurns = 0;
        var numberOfLeftTurns = 0;

        var currentDirection = Direction.None;

        // Get the starting pipe
        var pipeUp    = startingY - 1 >= 0 ? GetCharAt(startingX, startingY - 1) : '.';
        var pipeDown  = startingY + 1 < mapHeight ? GetCharAt(startingX, startingY + 1) : '.';
        var pipeLeft  = startingX - 1 >= 0 ? GetCharAt(startingX - 1, startingY) : '.';
        var pipeRight = startingX + 1 < mapWidth ? GetCharAt(startingX + 1, startingY) : '.';
        var isPipeUpAccessible    = pipeUp == '|' | pipeUp == 'F' | pipeUp == '7';
        var isPipeDownAccessible  = pipeDown == '|' | pipeDown == 'L' | pipeDown == 'J';
        var isPipeLeftAccessible  = pipeLeft == '-' | pipeLeft == 'L' | pipeLeft == 'F';
        var isPipeRightAccessible = pipeRight == '-' | pipeRight == '7' | pipeRight == 'J';
        if (isPipeUpAccessible & isPipeDownAccessible)
        {
            // '|'
            currentY -= 1;
            currentDirection = Direction.Up;
        }
        else if (isPipeLeftAccessible & isPipeRightAccessible)
        {
            // '-'
            currentX += 1;
            currentDirection = Direction.Right;
        }
        else if (isPipeDownAccessible & isPipeRightAccessible)
        {
            // 'F'
            currentX += 1;
            numberOfRightTurns += 1;
            currentDirection = Direction.Right;
        }
        else if (isPipeDownAccessible & isPipeLeftAccessible)
        {
            // '7'
            currentY += 1;
            numberOfRightTurns += 1;
            currentDirection = Direction.Down;
        }
        else if (isPipeUpAccessible & isPipeRightAccessible)
        {
            // 'L'
            currentY -= 1;
            numberOfRightTurns += 1;
            currentDirection = Direction.Up;
        }
        else if (isPipeUpAccessible & isPipeLeftAccessible)
        {
            // 'J'
            currentX -= 1;
            numberOfRightTurns += 1;
            currentDirection = Direction.Left;
        }

        // Get the current pipe
        char pipe = GetCharAt(currentX, currentY);

        // Set this map cell to filled
        ref char cell = ref GetMapCell(mapMemory, currentX, currentY);
        cell = pipe;

        var positionsList = new List<(int X, int Y, Direction Direction, char Pipe)>();
        positionsList.Add((currentX, currentY, currentDirection, pipe));

        // Traverse the pipes
        do
        {
            switch (pipe)
            {
                case '|':
                    // Move up or down
                    if (currentDirection == Direction.Up)
                    {
                        currentY -= 1;
                    }
                    else if (currentDirection == Direction.Down)
                    {
                        currentY += 1;
                    }
                    break;
                case '-':
                    // Move left or right
                    if (currentDirection == Direction.Left)
                    {
                        currentX -= 1;
                    }
                    else if (currentDirection == Direction.Right)
                    {
                        currentX += 1;
                    }
                    break;
                case 'F':
                    // Move right or down 
                    if (currentDirection == Direction.Up)
                    {
                        currentX += 1;
                        currentDirection = Direction.Right;
                        numberOfRightTurns += 1;
                    }
                    else if (currentDirection == Direction.Left)
                    {
                        currentY += 1;
                        currentDirection = Direction.Down;
                        numberOfLeftTurns += 1;
                    }
                    break;
                case '7':
                    // Move left or down
                    if (currentDirection == Direction.Up)
                    {
                        currentX -= 1;
                        currentDirection = Direction.Left;
                        numberOfLeftTurns += 1;
                    }
                    else if (currentDirection == Direction.Right)
                    {
                        currentY += 1;
                        currentDirection = Direction.Down;
                        numberOfRightTurns += 1;
                    }
                    break;
                case 'L':
                    // Move up or right
                    if (currentDirection == Direction.Left)
                    {
                        currentY -= 1;
                        currentDirection = Direction.Up;
                        numberOfRightTurns += 1;
                    }
                    else if (currentDirection == Direction.Down)
                    {
                        currentX += 1;
                        currentDirection = Direction.Right;
                        numberOfLeftTurns += 1;
                    }
                    break;
                case 'J':
                    // Move up or left
                    if (currentDirection == Direction.Right)
                    {
                        currentY -= 1;
                        currentDirection = Direction.Up;
                        numberOfLeftTurns += 1;
                    }
                    else if (currentDirection == Direction.Down)
                    {
                        currentX -= 1;
                        currentDirection = Direction.Left;
                        numberOfRightTurns += 1;
                    }
                    break;
            }

            // Update pipe character
            pipe = GetCharAt(currentX, currentY);

            // Set this cell to filled
            cell = ref GetMapCell(mapMemory, currentX, currentY);
            cell = pipe;

            positionsList.Add((currentX, currentY, currentDirection, pipe));
        }
        while (pipe != StartingCharacter);

        // Get whether we're filling on the right side of left side
        var isFillingRight = numberOfRightTurns < numberOfLeftTurns;

        foreach (var positionAndDirection in positionsList)
        {
            var posX = positionAndDirection.X;
            var posY = positionAndDirection.Y;
            var dir = positionAndDirection.Direction;
            switch (positionAndDirection.Pipe)
            {
                case '|':
                    if ((dir == Direction.Up & isFillingRight) |
                        (dir == Direction.Down & !isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX + 1, posY);
                    }
                    else
                    {
                        FillFromPosition(mapMemory, posX - 1, posY);
                    }
                    break;
                case '-':
                    if ((dir == Direction.Right & isFillingRight) |
                        (dir == Direction.Left & !isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX, posY + 1);
                    }
                    else
                    {
                        FillFromPosition(mapMemory, posX, posY - 1);
                    }
                    break;
                case 'F':
                    if ((dir == Direction.Up & !isFillingRight) |
                        (dir == Direction.Left & isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX - 1, posY);
                        FillFromPosition(mapMemory, posX, posY - 1);
                    }
                    break;
                case '7':
                    if ((dir == Direction.Up & isFillingRight) |
                        (dir == Direction.Right & !isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX + 1, posY);
                        FillFromPosition(mapMemory, posX, posY - 1);
                    }
                    break;
                case 'L':
                    if ((dir == Direction.Down & isFillingRight) |
                        (dir == Direction.Left & !isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX - 1, posY);
                        FillFromPosition(mapMemory, posX, posY + 1);
                    }
                    break;
                case 'J':
                    if ((dir == Direction.Down & !isFillingRight) |
                        (dir == Direction.Right & isFillingRight))
                    {
                        FillFromPosition(mapMemory, posX + 1, posY);
                        FillFromPosition(mapMemory, posX, posY + 1);
                    }
                    break;
            }
        }

        void FillFromPosition(Span<char> map, int posX, int posY)
        {
            if (posX < 0 | posX >= mapWidth |
                posY < 0 | posY >= mapHeight)
            {
                return;
            }

            if (posX == 8 & posY == 6)
            {

            }

            ref char cellAtPos = ref GetMapCell(map, posX, posY);
            if (cellAtPos != ' ')
            {
                return;
            }

            // Fill
            cellAtPos = 'X';


            //for (int y = 0; y < mapHeight; y++)
            //{
            //    for (int x = 0; x < mapWidth; x++)
            //    {
            //        Console.Write(GetMapCell(map, x, y));
            //    }
            //    Console.WriteLine();
            //}
            //Console.WriteLine();

            // Fill neighbours
            FillFromPosition(map, posX + 1, posY);
            FillFromPosition(map, posX - 1, posY);
            FillFromPosition(map, posX, posY + 1);
            FillFromPosition(map, posX, posY - 1);

        }

        //for (int y = 0; y < mapHeight; y++)
        //{
        //    for (int x = 0; x < mapWidth; x++)
        //    {
        //        Console.Write(GetMapCell(mapMemory, x, y));
        //    }
        //    Console.WriteLine();
        //}

        // Count the unfilled tiles
        var unfilledTiles = 0;
        foreach (var tile in mapMemory)
        {
            if (tile == ' ')
            {
                unfilledTiles++;
            }
        }

        return unfilledTiles.ToString();

        // Local method to get the character at the given position
        char GetCharAt(int x, int y) => Input.Lines[y][x];
    }
}

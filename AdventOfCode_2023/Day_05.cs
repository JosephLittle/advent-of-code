﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(5)]
[SolutionInput("Inputs/Day_05_Test.txt", Problem1Solution = "35", Problem2Solution = "46")]
[SolutionInput("Inputs/Day_05.txt", Problem1Solution = "825516882", Problem2Solution = "136096660", Benchmark = true)]
public unsafe class Day_05 : Solution
{
    private readonly record struct MapRange(ulong Destination, ulong Source, ulong Length)
    {
        public bool IsInRange(ulong value) => value >= Source & value <= Source + Length;

        public ulong Map(ulong value) => value + (Destination - Source);
    }

    public Day_05(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        Span<MapRange> mapMemory = stackalloc MapRange[Input.Lines.Length];

        // Create the maps
        var mapLineIndex = 3;
        var map1 = ProcessMap(mapMemory, mapLineIndex);
        var map2 = ProcessMap(mapMemory = mapMemory[map1.Length..], (mapLineIndex += (map1.Length + 2)));
        var map3 = ProcessMap(mapMemory = mapMemory[map2.Length..], (mapLineIndex += (map2.Length + 2)));
        var map4 = ProcessMap(mapMemory = mapMemory[map3.Length..], (mapLineIndex += (map3.Length + 2)));
        var map5 = ProcessMap(mapMemory = mapMemory[map4.Length..], (mapLineIndex += (map4.Length + 2)));
        var map6 = ProcessMap(mapMemory = mapMemory[map5.Length..], (mapLineIndex += (map5.Length + 2)));
        var map7 = ProcessMap(mapMemory = mapMemory[map6.Length..], (mapLineIndex += (map6.Length + 2)));

        // Local method for processing maps
        Span<MapRange> ProcessMap(Span<MapRange> memory, int mapStartLineIndex)
        {
            // Parse the map until we reach an empty line
            string line;
            int lineCount = -1;
            int lineIndex;
            while (
                (lineIndex = mapStartLineIndex + (++lineCount)) < Input.Lines.Length &&
                (line = Input.Lines[lineIndex]).Length > 0)
            {
                // Split into destination, source, and length
                var lineParseIndex = 0;
                var destination = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                lineParseIndex++;
                var source = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                lineParseIndex++;
                var length = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                // Add to the map memory
                memory[lineCount] = new MapRange(destination, source, length);
            }

            // Return the slice
            return memory[..(lineCount)];
        }

        // Parse and process the seeds
        var seedsLine = Input.Lines[0];
        var characterIndex = "seeds: ".Length;
        ulong lowestLocation = ulong.MaxValue;
        while (characterIndex < seedsLine.Length)
        {
            var seed = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(seedsLine, ref characterIndex, ' ');
            characterIndex++;

            // Process this seed
            var soil        = Map(map1, seed);
            var fertilizer  = Map(map2, soil);
            var water       = Map(map3, fertilizer);
            var light       = Map(map4, water);
            var temperature = Map(map5, light);
            var humidity    = Map(map6, temperature);
            var location    = Map(map7, humidity);

            // Track the lowest location
            lowestLocation = Math.Min(lowestLocation, location);
        }

        // Local method to map
        ulong Map(Span<MapRange> map, ulong value)
        {
            for (int i = 0; i < map.Length; i++)
            {
                var mapRange = map[i];
                if (mapRange.IsInRange(value))
                {
                    return mapRange.Map(value);
                }
            }

            return value;
        }

        return lowestLocation.ToString();
    }

    protected override string? Problem2()
    {
        Span<MapRange> mapMemory = stackalloc MapRange[Input.Lines.Length];

        // Create the maps
        var mapLineIndex = 3;
        var map1 = ProcessMap(mapMemory, mapLineIndex);
        var map2 = ProcessMap(mapMemory = mapMemory[map1.Length..], (mapLineIndex += (map1.Length + 2)));
        var map3 = ProcessMap(mapMemory = mapMemory[map2.Length..], (mapLineIndex += (map2.Length + 2)));
        var map4 = ProcessMap(mapMemory = mapMemory[map3.Length..], (mapLineIndex += (map3.Length + 2)));
        var map5 = ProcessMap(mapMemory = mapMemory[map4.Length..], (mapLineIndex += (map4.Length + 2)));
        var map6 = ProcessMap(mapMemory = mapMemory[map5.Length..], (mapLineIndex += (map5.Length + 2)));
        var map7 = ProcessMap(mapMemory = mapMemory[map6.Length..], (mapLineIndex += (map6.Length + 2)));

        // Local method for processing maps
        Span<MapRange> ProcessMap(Span<MapRange> memory, int mapStartLineIndex)
        {
            // Parse the map until we reach an empty line
            string line;
            int lineCount = -1;
            int lineIndex;
            while (
                (lineIndex = mapStartLineIndex + (++lineCount)) < Input.Lines.Length &&
                (line = Input.Lines[lineIndex]).Length > 0)
            {
                // Split into destination, source, and length
                var lineParseIndex = 0;
                var destination = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                lineParseIndex++;
                var source = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                lineParseIndex++;
                var length = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref lineParseIndex, ' ');

                // Add to the map memory
                memory[lineCount] = new MapRange(destination, source, length);
            }

            // Return the slice
            return memory[..(lineCount)];
        }

        // Parse and process the seeds
        var seedsLine = Input.Lines[0];
        var characterIndex = "seeds: ".Length;
        ulong lowestLocation = ulong.MaxValue;
        while (characterIndex < seedsLine.Length)
        {
            var seed = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(seedsLine, ref characterIndex, ' ');
            characterIndex++;

            var seedLength = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(seedsLine, ref characterIndex, ' ');
            characterIndex++;

            // Process each seed in the range
            while (seedLength-- > 0)
            {
                // Process this seed
                var soil = Map(map1, seed);
                var fertilizer = Map(map2, soil);
                var water = Map(map3, fertilizer);
                var light = Map(map4, water);
                var temperature = Map(map5, light);
                var humidity = Map(map6, temperature);
                var location = Map(map7, humidity);

                // Track the lowest location
                lowestLocation = Math.Min(lowestLocation, location);

                seed++;
            }
        }

        // Local method to map
        ulong Map(Span<MapRange> map, ulong value)
        {
            for (int i = 0; i < map.Length; i++)
            {
                var mapRange = map[i];
                if (mapRange.IsInRange(value))
                {
                    return mapRange.Map(value);
                }
            }

            return value;
        }

        return lowestLocation.ToString();
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(24)]
[SolutionInput("Inputs/Day_24_Test.txt", Problem1Solution = "18", Problem2Solution = "54")]
[SolutionInput("Inputs/Day_24.txt", Problem1Solution = "286", Problem2Solution = "820", Benchmark = true)]
public unsafe class Day_24 : Solution
{
    private enum WorldCell : byte
    {
        Empty = 0,
        BlizzardEast = 2,
        BlizzardWest = 4,
        BlizzardNorth = 8,
        BlizzardSouth = 16,
    }

    private readonly record struct NodeKey(
        int StateIndex, 
        int X, 
        int Y);

    private readonly record struct World(
        Dictionary<NodeKey, Node> Nodes,
        int StartX,
        int StartY,
        int EndX,
        int EndY,
        int NumberOfStates);

    private class Node
    {
        public readonly NodeKey Key;

        public int NumberOfSteps;

        public bool IsVisited;
        public bool IsInUnvisitedList;

        public Node(NodeKey key)
        {
            Key = key;
            NumberOfSteps = int.MaxValue;
        }
    }

    public Day_24(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Create the world
        var world = CreateWorld();

        // Do the search
        return PerformSearch(world, 0, world.StartX, world.StartY, world.EndX, world.EndY).ToString();
    }

    protected override string? Problem2()
    {
        // Create the world
        var world = CreateWorld();

        // Do the initial search from start to end
        var numberOfSteps = PerformSearch(
            world, 
            0, 
            world.StartX, world.StartY, 
            world.EndX, world.EndY);
        ResetNodes();

        // Now search from the end to start from the state we last finished at
        numberOfSteps += PerformSearch(
            world, 
            numberOfSteps % world.NumberOfStates, 
            world.EndX, world.EndY, 
            world.StartX, world.StartY);
        ResetNodes();

        // Now search from the start to the end again
        numberOfSteps += PerformSearch(
            world,
            numberOfSteps % world.NumberOfStates,
            world.StartX, world.StartY,
            world.EndX, world.EndY);

        // Local method to reset nodes between searches
        void ResetNodes()
        {
            foreach (var node in world.Nodes)
            {
                node.Value.NumberOfSteps = int.MaxValue;
                node.Value.IsVisited = false;
                node.Value.IsInUnvisitedList = false;
            }
        }

        // Return the total number of steps
        return numberOfSteps.ToString();
    }

    private World CreateWorld()
    {
        // Get the dimensions inside of the walls
        var width = Input.Lines[0].Length;
        var height = Input.Lines.Length;
        var innerWidth = width - 2;
        var innerHeight = height - 2;

        // Create the start and end coordinates
        var startX = -1;
        var startY = 0;
        var endX = -1;
        var endY = height - 1;
        var startRow = Input.Lines[0];
        var endRow = Input.Lines[height - 1];
        for (int x = 0; x < width; x++)
        {
            if (startRow[x] == '.')
            {
                startX = x;
            }
            if (endRow[x] == '.')
            {
                endX = x;
            }
        }

        // Create the map with each blizzard
        var currentWorld = new WorldCell[innerHeight][];
        var nextWorld = new WorldCell[innerHeight][];
        for (int y = 0; y < innerHeight; y++)
        {
            var inputLine = Input.Lines[y + 1];
            var currentWorldRow = new WorldCell[width];
            currentWorld[y] = currentWorldRow;
            nextWorld[y] = new WorldCell[width];

            for (int x = 0; x < innerWidth; x++)
            {
                var blizzard = WorldCell.Empty;
                switch (inputLine[x + 1])
                {
                    case '>':
                        blizzard = WorldCell.BlizzardEast;
                        break;
                    case '<':
                        blizzard = WorldCell.BlizzardWest;
                        break;
                    case '^':
                        blizzard = WorldCell.BlizzardNorth;
                        break;
                    case 'v':
                        blizzard = WorldCell.BlizzardSouth;
                        break;
                }
                currentWorldRow[x] = blizzard;
            }
        }

        // Get the number of minutes until the states are reset
        // Each blizzard will only move in its own direction until its position is reset to
        // the other side of the world. So the LMC of the width and height of the world will
        // give us the number of minutes until both north/south and east/west blizzards are
        // back in their original positions.
        var numberOfMinutesUntilStateReset = Helpers.LeastCommonMultiple(innerWidth, innerHeight);

        // Populate the empty coordinates for each state, while populating the blizzards for the next state
        var nodes = new Dictionary<NodeKey, Node>(innerWidth * innerHeight);
        for (int i = 0; i < numberOfMinutesUntilStateReset; i++)
        {
            // Add the start and end coordinates
            var startNodeKey = new NodeKey(i, startX, startY);
            var endNodeKey = new NodeKey(i, endX, endY);
            nodes.Add(startNodeKey, new Node(startNodeKey));
            nodes.Add(endNodeKey, new Node(endNodeKey));

            // Scan the current world, adding empty coordinates for the current state and blizzard
            // positions for the next world
            for (int y = 0; y < innerHeight; y++)
            {
                var currentWorldRow = currentWorld[y];
                var nextWorldRow = nextWorld[y];

                for (int x = 0; x < innerWidth; x++)
                {
                    var worldCell = currentWorldRow[x];

                    // Check for empty
                    if (worldCell == WorldCell.Empty)
                    {
                        var nodeKey = new NodeKey(i, x + 1, y + 1);
                        nodes.Add(nodeKey, new Node(nodeKey));
                    }
                    else
                    {
                        // Check for east blizzard
                        if ((worldCell & WorldCell.BlizzardEast) == WorldCell.BlizzardEast)
                        {
                            var nextX = (x + 1) % innerWidth;
                            nextWorldRow[nextX] |= WorldCell.BlizzardEast;
                        }

                        // Check for west blizzard
                        if ((worldCell & WorldCell.BlizzardWest) == WorldCell.BlizzardWest)
                        {
                            var nextX = x == 0 ? innerWidth - 1 : x - 1;
                            nextWorldRow[nextX] |= WorldCell.BlizzardWest;
                        }

                        // Check for north blizzard
                        if ((worldCell & WorldCell.BlizzardNorth) == WorldCell.BlizzardNorth)
                        {
                            var nextY = y == 0 ? innerHeight - 1 : y - 1;
                            nextWorld[nextY][x] |= WorldCell.BlizzardNorth;
                        }

                        // Check for south blizzard
                        if ((worldCell & WorldCell.BlizzardSouth) == WorldCell.BlizzardSouth)
                        {
                            var nextY = (y + 1) % innerHeight;
                            nextWorld[nextY][x] |= WorldCell.BlizzardSouth;
                        }
                    }
                }
            }

            // Swap the current and next worlds
            (currentWorld, nextWorld) = (nextWorld, currentWorld);

            // Zero the next world
            for (int y = 0; y < innerHeight; y++)
            {
                Array.Fill(nextWorld[y], WorldCell.Empty);
            }
        }

        return new World(
            nodes,
            startX, startY,
            endX, endY,
            numberOfMinutesUntilStateReset);
    }

    private int PerformSearch(World world, int startState, int startX, int startY, int endX, int endY)
    {
        var nodes = world.Nodes;

        // Perform dijkstra
        var unvisitedNodes = new List<Node>();
        var startNode = nodes[new NodeKey(startState, startX, startY)];
        startNode.IsInUnvisitedList = true;
        startNode.NumberOfSteps = 0;
        unvisitedNodes.Add(startNode);
        while (unvisitedNodes.Count > 0)
        {
            // Get the unvisited node with the shortest path
            var currentNode = unvisitedNodes[0];
            var currentNodeIndex = 0;
            for (int i = 1; i < unvisitedNodes.Count; i++)
            {
                var unvisitedNode = unvisitedNodes[i];
                if (unvisitedNode.NumberOfSteps < currentNode.NumberOfSteps)
                {
                    currentNode = unvisitedNode;
                    currentNodeIndex = i;
                }
            }

            // Check for the end node
            if (currentNode.Key.X == endX &
                currentNode.Key.Y == endY)
            {
                return currentNode.NumberOfSteps;
            }

            // Set it as visited
            unvisitedNodes.RemoveAt(currentNodeIndex);
            currentNode.IsVisited = true;

            // Get the next state index to check
            var nextStateIndex = (currentNode.Key.StateIndex + 1) % world.NumberOfStates;

            // Get the next step count
            var nextNumberOfSteps = currentNode.NumberOfSteps + 1;

            // Check each valid move
            CheckNodeAtOffset(0, 0);
            CheckNodeAtOffset(1, 0);
            CheckNodeAtOffset(-1, 0);
            CheckNodeAtOffset(0, 1);
            CheckNodeAtOffset(0, -1);
            void CheckNodeAtOffset(int xOffset, int yOffset)
            {
                // Check for a valid node at the offset
                var x = currentNode.Key.X + xOffset;
                var y = currentNode.Key.Y + yOffset;
                if (!nodes.TryGetValue(new NodeKey(nextStateIndex, x, y), out var nextNode))
                {
                    // No valid node
                    return;
                }

                // Check number of steps
                if (nextNode.NumberOfSteps > nextNumberOfSteps)
                {
                    nextNode.NumberOfSteps = nextNumberOfSteps;
                }

                // Add to the unvisited list if not already
                if (!nextNode.IsInUnvisitedList & !nextNode.IsVisited)
                {
                    nextNode.IsInUnvisitedList = true;
                    unvisitedNodes.Add(nextNode);
                }
            }
        }

        return -1;
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

internal class Program
{
    static void Main(string[] args)
    {
        var runner = new SolutionRunner();

        runner.SolveLatest();
        //runner.SolveAll();

        //runner.BenchmarkAll();

        //var day = 12;
        //runner.Solve(day);
        //runner.Benchmark(day);
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(23)]
[SolutionInput("Inputs/Day_23_Test_2.txt", Problem1Solution = "25", Problem2Solution = "4")]
[SolutionInput("Inputs/Day_23_Test.txt", Problem1Solution = "110", Problem2Solution = "20")]
[SolutionInput("Inputs/Day_23.txt", Problem1Solution = "4218", Problem2Solution = "976", Benchmark = true)]
public unsafe class Day_23 : Solution
{
    private readonly record struct Coordinates(int X, int Y)
    {
        public Coordinates Add(Coordinates other) => 
            new Coordinates(X + other.X, Y + other.Y);
    };

    private readonly record struct DirectionOffsets(
        Coordinates Offset1,
        Coordinates Offset2,
        Coordinates Offset3,
        Coordinates Offset4,
        Coordinates Offset5,
        Coordinates Offset6,
        Coordinates Offset7,
        Coordinates Offset8);
    private enum Direction : byte
    {
        North = 0,
        South = 1,
        West = 2,
        East = 3,
    }

    public Day_23(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Process for 10 rounds
        (var finalElves, _) = ProcessInput(10);

        // Get the bounds of the area the elves occupy
        var northMost = int.MinValue;
        var southMost = int.MaxValue;
        var westMost = int.MaxValue;
        var eastMost = int.MinValue;
        foreach (var elf in finalElves)
        {
            if (elf.X > eastMost)
            {
                eastMost = elf.X;
            }
            else if (elf.X < westMost)
            {
                westMost = elf.X;
            }

            if (elf.Y > northMost)
            {
                northMost = elf.Y;
            }
            else if (elf.Y < southMost)
            {
                southMost = elf.Y;
            }
        }
        var width = (eastMost - westMost) + 1;
        var height = (northMost - southMost) + 1;

        // Get the number of empty ground tiles in the bounds
        var area = width * height;
        var numberOfEmptyGroundTiles = area - finalElves.Count;
        return numberOfEmptyGroundTiles.ToString();
    }

    protected override string? Problem2()
    {
        // Process until all elves are dispersed
        (_, var numberOfRoundsProcessed) = ProcessInput(int.MaxValue);

        // Return the number of rounds it took
        return numberOfRoundsProcessed.ToString();
    }

    private (HashSet<Coordinates> FinalElves, int NumberOfRounds) ProcessInput(int maximumNumberOfRounds)
    {
        // Get the coordinates of all the elves
        var allElves = new HashSet<Coordinates>(Input.Lines[0].Length * Input.Lines.Length);
        for (int y = 0; y < Input.Lines.Length; y++)
        {
            var inputLine = Input.Lines[y];
            for (int x = 0; x < inputLine.Length; x++)
            {
                if (inputLine[x] == '#')
                {
                    allElves.Add(new Coordinates(x, Input.Lines.Length - y));
                }
            }
        }
        var numberOfElves = allElves.Count;

        // Set up the direction to offsets map
        var directionIndexToOffsets = new DirectionOffsets[4];
        directionIndexToOffsets[(int)Direction.North] = new DirectionOffsets(
            new Coordinates(-1, 1), new Coordinates(0, 1), new Coordinates(1, 1),
            new Coordinates(1, 0), new Coordinates(1, -1),
            new Coordinates(0, -1), new Coordinates(-1, -1), new Coordinates(-1, 0));
        directionIndexToOffsets[(int)Direction.South] = new DirectionOffsets(
            new Coordinates(1, -1), new Coordinates(0, -1), new Coordinates(-1, -1),
            new Coordinates(-1, 0), new Coordinates(-1, 1),
            new Coordinates(0, 1), new Coordinates(1, 1), new Coordinates(1, 0));
        directionIndexToOffsets[(int)Direction.West] = new DirectionOffsets(
            new Coordinates(-1, -1), new Coordinates(-1, 0), new Coordinates(-1, 1),
            new Coordinates(0, 1), new Coordinates(1, 1),
            new Coordinates(1, 0), new Coordinates(1, -1), new Coordinates(0, -1));
        directionIndexToOffsets[(int)Direction.East] = new DirectionOffsets(
            new Coordinates(1, 1), new Coordinates(1, 0), new Coordinates(1, -1),
            new Coordinates(0, -1), new Coordinates(-1, -1),
            new Coordinates(-1, 0), new Coordinates(-1, 1), new Coordinates(0, 1));

        // Set the starting direction
        var startingDirection = Direction.North;
        var startingDirectionOffsets = directionIndexToOffsets[(int)startingDirection];

        // Set up the collections
        var dispersedElves = new HashSet<Coordinates>(numberOfElves);
        var destinationsToElves = new Dictionary<Coordinates, Coordinates>(numberOfElves);
        var currentElves = new HashSet<Coordinates>(numberOfElves);
        var nextElves = allElves;

        // Process rounds until all elves are fully dispersed or we reach the maximum
        var numberOfRoundsProcessed = 0;
        while (++numberOfRoundsProcessed <= maximumNumberOfRounds & dispersedElves.Count != numberOfElves)
        {
            // Swap the current elves
            (currentElves, nextElves) = (nextElves, currentElves);

            // Clear the collections
            dispersedElves.Clear();
            destinationsToElves.Clear();
            nextElves.Clear();

            // Check every elf for moves
            foreach (var elf in currentElves)
            {
                // Check whether we can move in our starting direction
                var canMove1 = !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset1));
                var canMove2 = !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset2));
                var canMove3 = !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset3));
                if (canMove1 & canMove2 & canMove3)
                {
                    // Check whether this elf is fully dispersed
                    if (!currentElves.Contains(elf.Add(startingDirectionOffsets.Offset4)) &
                        !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset5)) &
                        !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset6)) &
                        !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset7)) &
                        !currentElves.Contains(elf.Add(startingDirectionOffsets.Offset8)))
                    {
                        // Elf is fully dispersed
                        dispersedElves.Add(elf);
                        nextElves.Add(elf);
                    }
                    else
                    {
                        // Can move in our starting direction
                        var destination = elf.Add(startingDirectionOffsets.Offset2);
                        AddDestinationForElfIfPossible(elf, destination);
                    }
                }
                else
                {
                    // Check each other direction in turn
                    var couldMove = false;
                    for (int i = 1; i < 4; i++)
                    {
                        var directionIndex = ((int)startingDirection + i) % 4;
                        var directionOffsets = directionIndexToOffsets[directionIndex];

                        // Check whether we can move in this direction
                        if (!currentElves.Contains(elf.Add(directionOffsets.Offset1)) &
                            !currentElves.Contains(elf.Add(directionOffsets.Offset2)) &
                            !currentElves.Contains(elf.Add(directionOffsets.Offset3)))
                        {
                            // We can move in this direction
                            var destination = elf.Add(directionOffsets.Offset2);
                            AddDestinationForElfIfPossible(elf, destination);
                            couldMove = true;
                            break;
                        }
                    }

                    // If the elf couldn't move then leave him in the same place
                    if (!couldMove)
                    {
                        nextElves.Add(elf);
                    }
                }

                // Local method to add the destination for the given elf
                void AddDestinationForElfIfPossible(Coordinates elf, Coordinates destination)
                {
                    if (destinationsToElves.TryGetValue(destination, out var elfAlreadyMovingToDestination))
                    {
                        // Already an elf moving to that destination, so remove the destination from the next elves
                        // and add him back to his old place
                        nextElves.Remove(destination);
                        nextElves.Add(elfAlreadyMovingToDestination);

                        // Add our current elf to the next elves since we know he won't be moving
                        nextElves.Add(elf);
                    }
                    else
                    {
                        // Destination is free, so add our elf for this destination and add the destination to the next elves
                        destinationsToElves.Add(destination, elf);
                        nextElves.Add(destination);
                    }
                }
            }

            // Get the next starting direction
            startingDirection = (Direction)(((int)startingDirection + 1) % 4);
            startingDirectionOffsets = directionIndexToOffsets[(int)startingDirection];
        }

        // Return the final positions of the elves and the number of rounds
        return (nextElves, numberOfRoundsProcessed - 1);
    }

    private void PrintState(int stateIndex, HashSet<Coordinates> elves)
    {
        var northMost = int.MinValue;
        var southMost = int.MaxValue;
        var westMost = int.MaxValue;
        var eastMost = int.MinValue;
        foreach (var elf in elves)
        {
            if (elf.X > eastMost)
            {
                eastMost = elf.X;
            }
            else if (elf.X < westMost)
            {
                westMost = elf.X;
            }

            if (elf.Y > northMost)
            {
                northMost = elf.Y;
            }
            else if (elf.Y < southMost)
            {
                southMost = elf.Y;
            }
        }

        Console.WriteLine($"State: {stateIndex} (Northmost: {northMost}, Westmost: {westMost}");
        for (int y = northMost; y >= southMost; y--)
        {
            for (int x = westMost; x <= eastMost; x++)
            {
                Console.Write(elves.Contains(new Coordinates(x, y)) ? '#' : '.');
            }
            Console.WriteLine();
        }
    }
}

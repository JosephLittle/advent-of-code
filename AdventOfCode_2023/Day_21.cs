﻿using AdventOfCode.Framework;
using System.Diagnostics;

namespace AdventOfCode_2023;

[Solution(21)]
[SolutionInput("Inputs/Day_21_Test.txt", Problem1Solution = "16", Problem2Solution = "16733044")]
[SolutionInput("Inputs/Day_21.txt", Problem1Solution = "3660", Problem2Solution = "605492675373144", Benchmark = true)]
public unsafe class Day_21 : Solution
{
    private readonly record struct Position(int X, int Y)
    {
        public static Position operator +(Position l, Position r)
        {
            return new Position(l.X + r.X, l.Y + r.Y);
        }
    }

    private readonly int width;
    private readonly int height;

    public Day_21(Input input)
        : base(input)
    {
        width = Input.Lines[0].Length;
        height = Input.Lines.Length;
    }

    protected override string? Problem1()
    {
        return null;

        // Set number of steps depending on input
        var numberOfSteps = Input.Raw.Length == 141 ? 6 : 64;
        return Simulate(numberOfSteps, false).Count.ToString();
    }

    protected override string? Problem2()
    {
        if (Input.Raw.Length == 141)
        {
            return null;
        }

        //var numberOfSteps = 65 + (131 * 4);
        var numberOfSteps = 26501365;
        var gridsOutwards = (numberOfSteps - (width / 2)) / width;

        // Simulate 
        var numberOfStepsToSimulate = 65 + (131 * 2);
        var positions = Simulate(numberOfStepsToSimulate, true);

        // 65 + (131 * 4) = 299976
        // 65 + (131 * 3) = 181533

        var gridsOutwardsSimulated = (numberOfStepsToSimulate - (width / 2)) / width;

        // Full
        var countFullEven = FilterByGrid(positions, 0, 0);
        var countFullOdd = FilterByGrid(positions, 1, 0);

        // Straight ends
        var countRight = FilterByGrid(positions, gridsOutwardsSimulated, 0);
        var countLeft = FilterByGrid(positions, -gridsOutwardsSimulated, 0);
        var countUp = FilterByGrid(positions, 0, -gridsOutwardsSimulated);
        var countDown = FilterByGrid(positions, 0, gridsOutwardsSimulated);

        // Up right
        var indexMore = gridsOutwardsSimulated - 1;
        var indexLess = gridsOutwardsSimulated;
        var countUpRightMore = FilterByGrid(positions, indexMore, -1);
        var countUpRightLess = FilterByGrid(positions, indexLess, -1);

        // Up left
        var countUpLeftMore = FilterByGrid(positions, -indexMore, -1);
        var countUpLeftLess = FilterByGrid(positions, -indexLess, -1);

        // Down right
        var countDownRightMore = FilterByGrid(positions, indexMore, 1);
        var countDownRightLess = FilterByGrid(positions, indexLess, 1);

        // Down left
        var countDownLeftMore = FilterByGrid(positions, -indexMore, 1);
        var countDownLeftLess = FilterByGrid(positions, -indexLess, 1);

        // Count odd and even full grids
        long fullGridsOddCount = 0;
        long fullGridsEvenCount = 0;
        for (int i = 1; i < gridsOutwards; i++)
        {
            if (i % 2 == 0)
            {
                fullGridsEvenCount += i;
            }
            else
            {
                fullGridsOddCount += i;
            }
        }
        fullGridsEvenCount *= 4;
        fullGridsOddCount *= 4;
        fullGridsEvenCount += 1;

        long gridsDiagonalMorePerDirection = gridsOutwards - 1;
        long gridsDiagonalLessPerDirection = gridsOutwards;

        // Count the actual positions
        long positionsCount = 0;

        positionsCount += fullGridsOddCount * countFullOdd;
        positionsCount += fullGridsEvenCount * countFullEven;

        positionsCount += countRight;
        positionsCount += countLeft;
        positionsCount += countUp;
        positionsCount += countDown;

        positionsCount += countUpRightMore * gridsDiagonalMorePerDirection;
        positionsCount += countUpRightLess * gridsDiagonalLessPerDirection;

        positionsCount += countUpLeftMore * gridsDiagonalMorePerDirection;
        positionsCount += countUpLeftLess * gridsDiagonalLessPerDirection;
        
        positionsCount += countDownRightMore * gridsDiagonalMorePerDirection;
        positionsCount += countDownRightLess * gridsDiagonalLessPerDirection;
        
        positionsCount += countDownLeftMore * gridsDiagonalMorePerDirection;
        positionsCount += countDownLeftLess * gridsDiagonalLessPerDirection;
        
        return positionsCount.ToString();
    }

    private long FilterByGrid(HashSet<Position> positions, int gridX, int gridY)
    {
        var xStart = gridX * width;
        var xEnd = xStart + width;
        var yStart = gridY * height;
        var yEnd = yStart + height;

        var filteredPositions = new HashSet<Position>();

        for (int y = yStart; y < yEnd; y++)
        {
            for (int x = xStart; x < xEnd; x++)
            {
                var pos = new Position(x, y);
                if (positions.Contains(pos))
                {
                    filteredPositions.Add(pos);
                }
            }
        }

        return filteredPositions.Count;
    }

    private HashSet<Position> Simulate(int numberOfSteps, bool useInfiniteGrid)
    {
        var rockPositions = new HashSet<Position>();
        var startingPosition = default(Position);

        // Process the map
        for (int y = 0; y < Input.Lines.Length; y++)
        {
            for (int x = 0; x < Input.Lines[y].Length; x++)
            {
                var cell = Input.Lines[y][x];

                if (cell == '#')
                {
                    rockPositions.Add(new Position(x, y));
                }
                else if (cell == 'S')
                {
                    startingPosition = new Position(x, y);
                }
            }
        }

        // Add borders (if not using infinite grid)
        if (!useInfiniteGrid)
        {
            for (int x = -1; x < width + 1; x++)
            {
                rockPositions.Add(new Position(x, -1));
                rockPositions.Add(new Position(x, height));
            }
            for (int y = -1; y < height + 1; y++)
            {
                rockPositions.Add(new Position(-1, y));
                rockPositions.Add(new Position(width, y));
            }
        }

        var positionsToVisitSet = new HashSet<Position>();
        var positionsVisiting = new List<Position>();

        positionsVisiting.Add(startingPosition);

        while (numberOfSteps-- > 0)
        {
            // Process each visiting position
            foreach (var position in positionsVisiting)
            {
                // Up
                AddIfPossible(position + new Position(0, -1));

                // Down
                AddIfPossible(position + new Position(0, 1));

                // Left
                AddIfPossible(position + new Position(-1, 0));

                // Right
                AddIfPossible(position + new Position(1, 0));

                void AddIfPossible(Position candidatePosition)
                {
                    // Normalise to rock positions
                    var x = candidatePosition.X < 0 ?
                        (width - (Math.Abs(candidatePosition.X) % width)) % width :
                        candidatePosition.X % width;
                    var y = candidatePosition.Y < 0 ?
                        (height - (Math.Abs(candidatePosition.Y) % height)) % height :
                        candidatePosition.Y % height;
                    var normalisedPosition = new Position(x, y);

                    if (!rockPositions.Contains(normalisedPosition))
                    {
                        positionsToVisitSet.Add(candidatePosition);
                    }
                }
            }

            // Clear visiting and copy to visit into it
            positionsVisiting.Clear();
            positionsVisiting.AddRange(positionsToVisitSet);
            positionsToVisitSet.Clear();
        }

        return positionsVisiting.ToHashSet();
    }
}
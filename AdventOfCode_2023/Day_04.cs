﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(4)]
[SolutionInput("Inputs/Day_04_Test.txt", Problem1Solution = "13", Problem2Solution = "30")]
[SolutionInput("Inputs/Day_04.txt", Problem1Solution = "25174", Problem2Solution = "6420979", Benchmark = true)]
public unsafe class Day_04 : Solution
{
    public Day_04(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Find the winning numbers start index and candidate numbers start index
        var firstLine = Input.Lines[0];
        var startIndexWinningNumbers = firstLine.IndexOf(':') + 2;
        var startIndexCandidateNumbers = firstLine.IndexOf('|', startIndexWinningNumbers) + 2;

        // Get the number of each set of numbers
        var numberOfWinningNumbers = (startIndexCandidateNumbers - startIndexWinningNumbers - 2) / 3;
        var numberOfCandidateNumbers = ((firstLine.Length + 1) - startIndexCandidateNumbers) / 3;

        // Create the lookup for each number
        var isWinningNumberLookup = stackalloc bool[100];

        // Keep a sum of the points
        var pointsSum = 0;

        // Process each line
        foreach (var line in Input.Lines)
        {
            // Clear the winning numbers
            new Span<bool>(isWinningNumberLookup, 100).Fill(false);

            // Start with 1 point (we'll shift this right at the end of this line so that no winning numbers is 0 points)
            var points = 1;

            // Parse the winning numbers
            for (int i = 0; i < numberOfWinningNumbers; i++)
            {
                var startIndex = startIndexWinningNumbers + (3 * i);

                var firstCharacter = line[startIndex];
                var secondCharacter = line[startIndex + 1];

                var winningNumber = firstCharacter == ' ' ? 0 : Helpers.CharToInt(firstCharacter) * 10;
                winningNumber += Helpers.CharToInt(secondCharacter);

                // Set in the lookup
                isWinningNumberLookup[winningNumber] = true;
            }

            // Parse the candidate numbers
            for (int i = 0; i < numberOfCandidateNumbers; i++)
            {
                var startIndex = startIndexCandidateNumbers + (3 * i);

                var firstCharacter = line[startIndex];
                var secondCharacter = line[startIndex + 1];

                var candidateNumber = firstCharacter == ' ' ? 0 : Helpers.CharToInt(firstCharacter) * 10;
                candidateNumber += Helpers.CharToInt(secondCharacter);

                // Shift points if this is a winning number (we double points for each win)
                points <<= Helpers.BoolToInt(isWinningNumberLookup[candidateNumber]);
            }

            // Shift points right so that no winning numbers means 0 points
            points >>= 1;

            // Sum
            pointsSum += points;
        }

        return pointsSum.ToString();
    }

    protected override string? Problem2()
    {
        // Find the winning numbers start index and candidate numbers start index
        var firstLine = Input.Lines[0];
        var startIndexWinningNumbers = firstLine.IndexOf(':') + 2;
        var startIndexCandidateNumbers = firstLine.IndexOf('|', startIndexWinningNumbers) + 2;

        // Get the number of each set of numbers
        var numberOfWinningNumbers = (startIndexCandidateNumbers - startIndexWinningNumbers - 2) / 3;
        var numberOfCandidateNumbers = ((firstLine.Length + 1) - startIndexCandidateNumbers) / 3;

        // Create the lookup for each number
        Span<bool> isWinningNumberLookup = stackalloc bool[100];

        // Create the card count lookup
        Span<uint> cardCountLookup = stackalloc uint[Input.Lines.Length];
        cardCountLookup.Fill(1);

        // Keep a sum of the cards
        uint cardsSum = 0;

        // Process each line (card)
        for (int lineIndex = 0; lineIndex < Input.Lines.Length; lineIndex++)
        {
            var line = Input.Lines[lineIndex];

            // Clear the winning numbers
            isWinningNumberLookup.Fill(false);

            // Get the multiplier for this card
            var numberOfInstancesOfThisCard = cardCountLookup[lineIndex];

            // Count the number of wins
            var numberOfWinsForThisCard = 0;

            // Parse the winning numbers
            for (int i = 0; i < numberOfWinningNumbers; i++)
            {
                var startIndex = startIndexWinningNumbers + (3 * i);

                var firstCharacter = line[startIndex];
                var secondCharacter = line[startIndex + 1];

                var winningNumber = firstCharacter == ' ' ? 0 : Helpers.CharToInt(firstCharacter) * 10;
                winningNumber += Helpers.CharToInt(secondCharacter);

                // Set in the lookup
                isWinningNumberLookup[winningNumber] = true;
            }

            // Parse the candidate numbers
            for (int i = 0; i < numberOfCandidateNumbers; i++)
            {
                var startIndex = startIndexCandidateNumbers + (3 * i);

                var firstCharacter = line[startIndex];
                var secondCharacter = line[startIndex + 1];

                var candidateNumber = firstCharacter == ' ' ? 0 : Helpers.CharToInt(firstCharacter) * 10;
                candidateNumber += Helpers.CharToInt(secondCharacter);

                // If this is a winning number, then add to multipliers
                if (isWinningNumberLookup[candidateNumber])
                {
                    numberOfWinsForThisCard++;

                    // Add the number of instances of this card to the next card along
                    cardCountLookup[lineIndex + numberOfWinsForThisCard] += numberOfInstancesOfThisCard;
                }
            }

            // Sum this card multipler
            cardsSum += numberOfInstancesOfThisCard;
        }

        return cardsSum.ToString();
    }
}

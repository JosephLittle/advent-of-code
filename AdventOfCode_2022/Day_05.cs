﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(5)]
[SolutionInput("Inputs/Day_05_Test.txt", Problem1Solution = "CMZ", Problem2Solution = "MCD")]
[SolutionInput("Inputs/Day_05.txt", Problem1Solution = "VWLCWGSDQ", Problem2Solution = "TCGLQSLPW", Benchmark = true)]
public class Day_05 : Solution
{
    public struct CharacterStack
    {
        public int Count;
        public char[] Items;

        public char Peek()
        {
            return Items[Count - 1];
        }

        public void PushItem(char item)
        {
            Items[Count++] = item;
        }

        public char PopItem()
        {
            return Items[--Count];
        }
    }

    public Day_05(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Find the empty line that splits the setup and the instructions
        var emptyLineIndex = -1;
        while (inputLines[++emptyLineIndex].Length != 0)
        { }

        // Get the number of stacks
        var stacksStartLine = inputLines[emptyLineIndex - 2];
        var numberOfStacks = (stacksStartLine.Length + 1) / 4;

        // Get the height of the tallest stack at the start
        var tallestStackHeightAtStart = emptyLineIndex - 1;

        // Create the stacks
        var maximumStackHeight = tallestStackHeightAtStart * numberOfStacks;
        var stacks = new CharacterStack[numberOfStacks];
        for (int i = 0; i < numberOfStacks; i++)
        {
            stacks[i] = new CharacterStack()
            {
                Count = 0,
                Items = new char[maximumStackHeight]
            };
        }

        // Add the starting items to each stack
        for (int lineIndex = emptyLineIndex - 2; lineIndex >= 0; lineIndex--)
        {
            var stackLine = inputLines[lineIndex];

            for (int stackItemIndex = 1; stackItemIndex < stackLine.Length; stackItemIndex += 4)
            {
                var item = stackLine[stackItemIndex];

                if (char.IsLetter(item))
                {
                    stacks[(stackItemIndex - 1) / 4].PushItem(item);
                }
            }
        }

        // Now parse the instructions
        for (int lineIndex = emptyLineIndex + 1; lineIndex < inputLines.Length; lineIndex++)
        {
            var instructionLine = inputLines[lineIndex];

            // Stack indices are always single digit and specific offset from the end.
            // Move count starts at specific offset from start and ends at offset from end.
            var toIndex = Helpers.CharToInt(instructionLine[instructionLine.Length - 1]) - 1;
            var fromIndex = Helpers.CharToInt(instructionLine[instructionLine.Length - 6]) - 1;
            var count = Helpers.SubstringToUnsignedInt(instructionLine, 5, (instructionLine.Length - 11) - 6);

            // Pop and push for the given count
            ref var fromStack = ref stacks[fromIndex];
            ref var toStack = ref stacks[toIndex];
            for (int i = 0; i < count; i++)
            {
                var item = stacks[fromIndex].PopItem();
                stacks[toIndex].PushItem(item);
            }
        }

        // Build the output
        var output = new char[stacks.Length];
        for (int i = 0; i < stacks.Length; i++)
        {
            output[i] = stacks[i].Peek();
        }
        return new String(output);
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        // Find the empty line that splits the setup and the instructions
        var emptyLineIndex = -1;
        while (inputLines[++emptyLineIndex].Length != 0)
        { }

        // Get the number of stacks
        var stacksStartLine = inputLines[emptyLineIndex - 2];
        var numberOfStacks = (stacksStartLine.Length + 1) / 4;

        // Get the height of the tallest stack at the start
        var tallestStackHeightAtStart = emptyLineIndex - 1;

        // Create the stacks
        var maximumStackHeight = tallestStackHeightAtStart * numberOfStacks;
        var stacks = new CharacterStack[numberOfStacks];
        for (int i = 0; i < numberOfStacks; i++)
        {
            stacks[i] = new CharacterStack()
            {
                Count = 0,
                Items = new char[maximumStackHeight]
            };
        }

        // Add the starting items to each stack
        for (int lineIndex = emptyLineIndex - 2; lineIndex >= 0; lineIndex--)
        {
            var stackLine = inputLines[lineIndex];

            for (int stackItemIndex = 1; stackItemIndex < stackLine.Length; stackItemIndex += 4)
            {
                var item = stackLine[stackItemIndex];

                if (char.IsLetter(item))
                {
                    stacks[(stackItemIndex - 1) / 4].PushItem(item);
                }
            }
        }

        // Now parse the instructions
        for (int lineIndex = emptyLineIndex + 1; lineIndex < inputLines.Length; lineIndex++)
        {
            var instructionLine = inputLines[lineIndex];

            // Stack indices are always single digit and specific offset from the end.
            // Move count starts at specific offset from start and ends at offset from end.
            var toIndex = Helpers.CharToInt(instructionLine[instructionLine.Length - 1]) - 1;
            var fromIndex = Helpers.CharToInt(instructionLine[instructionLine.Length - 6]) - 1;
            var count = Helpers.SubstringToUnsignedInt(instructionLine, 5, (instructionLine.Length - 11) - 6);

            // Transfer the given number of items
            ref var fromStack = ref stacks[fromIndex];
            ref var toStack = ref stacks[toIndex];
            Array.Copy(
                fromStack.Items, fromStack.Count - count,
                toStack.Items, toStack.Count,
                count);
            fromStack.Count -= count;
            toStack.Count += count;
        }

        // Build the output
        var output = new char[stacks.Length];
        for (int i = 0; i < stacks.Length; i++)
        {
            output[i] = stacks[i].Peek();
        }
        return new String(output);
    }
}

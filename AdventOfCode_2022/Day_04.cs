﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(4)]
[SolutionInput("Inputs/Day_04_Test.txt", Problem1Solution = "2", Problem2Solution = "4")]
[SolutionInput("Inputs/Day_04.txt", Problem1Solution = "509", Problem2Solution = "870", Benchmark = true)]
public unsafe class Day_04 : Solution
{
    public Day_04(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        var numberOfPairsWithFullyOverlappingRanges = 0;

        // Run for each line
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var line = inputLines[lineIndex];

            var charIndex = 0;
            var leftFrom = ParseIntegerUntilDelimiter(line, ref charIndex, '-');
            var leftTo = ParseIntegerUntilDelimiter(line, ref charIndex, ',');
            var rightFrom = ParseIntegerUntilDelimiter(line, ref charIndex, '-');
            var rightTo = ParseIntegerUntilDelimiter(line, ref charIndex, Environment.NewLine[0]);

            var leftFullyOverlapsRight =
                leftFrom <= rightFrom &
                leftTo >= rightTo;
            var rightFullyOverlapsLeft =
                rightFrom <= leftFrom &
                rightTo >= leftTo;

            var hasFullOverlap = leftFullyOverlapsRight | rightFullyOverlapsLeft;

            numberOfPairsWithFullyOverlappingRanges += *(int*)&hasFullOverlap;
        }

        return numberOfPairsWithFullyOverlappingRanges.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        var numberOfPairsWithFullyOverlappingRanges = 0;

        // Run for each line
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var line = inputLines[lineIndex];

            var charIndex = 0;
            var leftFrom = ParseIntegerUntilDelimiter(line, ref charIndex, '-');
            var leftTo = ParseIntegerUntilDelimiter(line, ref charIndex, ',');
            var rightFrom = ParseIntegerUntilDelimiter(line, ref charIndex, '-');
            var rightTo = ParseIntegerUntilDelimiter(line, ref charIndex, Environment.NewLine[0]);

            var rightOverlapsLeft =
                (leftFrom >= rightFrom & leftFrom <= rightTo) |
                (leftTo >= rightFrom & leftTo <= rightTo);
            var leftOverlapsRight =
                (rightFrom >= leftFrom & rightFrom <= leftTo) |
                (rightTo >= leftFrom & rightTo <= leftTo);

            var hasFullOverlap = rightOverlapsLeft | leftOverlapsRight;

            numberOfPairsWithFullyOverlappingRanges += *(int*)&hasFullOverlap;
        }

        return numberOfPairsWithFullyOverlappingRanges.ToString();
    }

    private static int ParseIntegerUntilDelimiter(string line, ref int charIndex, char delimiter)
    {
        var integer = 0;
        char currentChar = '0';

        do
        {
            currentChar = line[charIndex++];

            if (currentChar == delimiter)
            {
                return integer;
            }

            integer *= 10;
            integer += currentChar - '0';
        }
        while (charIndex < line.Length);

        return integer;
    }
}
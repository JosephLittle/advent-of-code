﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(12)]
[SolutionInput("Inputs/Day_12_Test.txt", Problem1Solution = "21", Problem2Solution = "525152")]
[SolutionInput("Inputs/Day_12.txt", Problem1Solution = "7407", Problem2Solution = "30568243604962", Benchmark = true)]
//[SolutionInput("Inputs/Day_12_Final_Part.txt", Problem1Solution = "7407", Problem2Solution = "", Benchmark = true)]
//[SolutionInput("Inputs/Day_12_Split_01.txt", Problem1Solution = "", Problem2Solution = "", Benchmark = true)]
//[SolutionInput("Inputs/Day_12_Split_02.txt", Problem1Solution = "", Problem2Solution = "", Benchmark = true)]
//[SolutionInput("Inputs/Day_12_Split_03.txt", Problem1Solution = "", Problem2Solution = "", Benchmark = true)]
//[SolutionInput("Inputs/Day_12_Split_04.txt", Problem1Solution = "", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_12 : Solution
{
    public Day_12(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        var groupStack = new Stack<int>(8);
        Span<char> springsBuffer = stackalloc char[100];
        ulong countSum = 0;

        // Process each line
        foreach (var line in Input.Lines)
        {
            var lineSpan = line.AsSpan();

            // Split by the space
            var splitIndex   = lineSpan.IndexOf(' ') + 1;
            var springs      = lineSpan[..splitIndex];
            var springGroups = lineSpan[(splitIndex)..];

            // Create a stack of the groups
            groupStack.Clear();
            var totalGroupSizeRemaining = 0;
            foreach (var group in springGroups.ToString().Split(',').Reverse())
            {
                var groupSize = int.Parse(group);
                groupStack.Push(groupSize);
                totalGroupSizeRemaining += groupSize;
            }

            springs.CopyTo(springsBuffer);
            keysToCounts.Clear();
            ulong count = CountPossibleSolutions(springsBuffer[..springs.Length], 0, 0, groupStack, totalGroupSizeRemaining);
            countSum += count;
        }

        return countSum.ToString();
    }

    protected override string? Problem2()
    {
        var groupStack = new Stack<int>(8);
        Span<char> springsBuffer = stackalloc char[1000];
        ulong countSum = 0;

        // Process each line
        for (int lineIndex = 0; lineIndex < Input.Lines.Length; lineIndex++)
        {
            var lineSpan = Input.Lines[lineIndex].AsSpan();

            // Split by the space
            var splitIndex = lineSpan.IndexOf(' ') + 1;
            var springs = lineSpan[..splitIndex];
            var springGroups = lineSpan[(splitIndex)..];

            // Create a stack of the groups
            groupStack.Clear();
            var totalGroupSizeRemaining = 0;
            var springsLength = 0;
            for (int i = 0; i < 5; i++)
            {
                foreach (var group in springGroups.ToString().Split(',').Reverse())
                {
                    var groupSize = int.Parse(group);
                    groupStack.Push(groupSize);
                    totalGroupSizeRemaining += groupSize;
                }

                // Add the springs
                springs.CopyTo(springsBuffer[springsLength..]);
                springsLength += springs.Length;
                springsBuffer[springsLength - 1] = '?';
            }

            // End the springs
            springsBuffer[springsLength - 1] = ' ';

            // Get the count
            keysToCounts.Clear();
            ulong count = CountPossibleSolutions(springsBuffer[..springsLength], 0, 0, groupStack, totalGroupSizeRemaining);

            countSum += count;

        }

        return countSum.ToString();
    }

    private readonly record struct Key(string Springs, int CurrentGroupSize, int GroupSizeRemaining);
    private static Dictionary<Key, ulong> keysToCounts = new Dictionary<Key, ulong>();

    private static ulong CountPossibleSolutions(Span<char> springs, int springIndex, int currentGroupSize, Stack<int> groupStack, int totalGroupSizeRemaining)
    {
        var lengthRemaining = springs.Length - springIndex;
        var requiredLength = totalGroupSizeRemaining + groupStack.Count;
        if (currentGroupSize + lengthRemaining < requiredLength)
        {
            return 0;
        }

        // Check the dictionary
        var key = new Key(springs[springIndex..].ToString(), currentGroupSize, totalGroupSizeRemaining);
        if (keysToCounts.TryGetValue(key, out var count))
        {
            return count;
        }

        var character = springs[springIndex];
        if (character == '.' | character == ' ')
        {
            // Check for correct group size
            var didPop = false;
            if (groupStack.TryPeek(out var currentGroup) &&
                currentGroup == currentGroupSize)
            {
                currentGroupSize = 0;
                groupStack.Pop();
                didPop = true;
                totalGroupSizeRemaining -= currentGroup;
            }
            else if (currentGroupSize > 0 & groupStack.Count != 0)
            {
                // We have a group but no more groups on the stack
                return 0;
            }

            // Check whether we're continuing
            ulong returnValue;
            if (character == ' ')
            {
                // We reached the end
                if (groupStack.Count == 0)
                {
                    returnValue = 1;
                }
                else
                {
                    returnValue = 0;
                }
            }
            else
            {
                // Continue
                returnValue = CountPossibleSolutions(springs, ++springIndex, currentGroupSize, groupStack, totalGroupSizeRemaining);

                // Add to the dictionary
                var key2 = new Key(springs[springIndex..].ToString(), currentGroupSize, totalGroupSizeRemaining);
                if (!keysToCounts.TryAdd(key2, returnValue))
                {
                    if (keysToCounts[key2] > returnValue)
                    {
                        keysToCounts[key2] = returnValue;
                    }
                }
            }

            // Push back onto stack
            if (didPop)
            {
                groupStack.Push(currentGroup);
                totalGroupSizeRemaining += currentGroup;
            }


            return returnValue;
        }
        else if (character == '#')
        {
            // Increase spring count
            currentGroupSize++;

            // Check if we're over
            groupStack.TryPeek(out var currentGroup);
            if (currentGroupSize > currentGroup)
            {
                // Over, so invalid route
                return 0;
            }
            else
            {
                // Not over, continue
                var returnValue =  CountPossibleSolutions(springs, ++springIndex, currentGroupSize, groupStack, totalGroupSizeRemaining);

                // Add to the dictionary
                var key2 = new Key(springs[springIndex..].ToString(), currentGroupSize, totalGroupSizeRemaining);
                if (!keysToCounts.TryAdd(key2, returnValue))
                {
                    if (keysToCounts[key2] > returnValue)
                    {
                        keysToCounts[key2] = returnValue;
                    }
                }

                return returnValue;
            }
        }
        else if (character == '?')
        {
            ulong numberOfSolutions = 0;

            // Without spring
            springs[springIndex] = '.';
            numberOfSolutions += CountPossibleSolutions(springs, springIndex, currentGroupSize, groupStack, totalGroupSizeRemaining);

            // With spring
            springs[springIndex] = '#';
            numberOfSolutions += CountPossibleSolutions(springs, springIndex, currentGroupSize, groupStack, totalGroupSizeRemaining);

            // Reset
            springs[springIndex] = '?';

            // Add to the dictionary
            keysToCounts.Add(key, numberOfSolutions);

            return numberOfSolutions;
        }
        else
        {
            throw new Exception("What.");
        }
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(07)]
[SolutionInput("Inputs/Day_07_Test.txt", Problem1Solution = "6440", Problem2Solution = "5905")]
[SolutionInput("Inputs/Day_07.txt", Problem1Solution = "246912307", Problem2Solution = "246894760", Benchmark = true)]
public unsafe class Day_07 : Solution
{
    public Day_07(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        Span<ulong> cardValueMap = stackalloc ulong['T' + 1];
        cardValueMap['2'] = 2;
        cardValueMap['3'] = 3;
        cardValueMap['4'] = 4;
        cardValueMap['5'] = 5;
        cardValueMap['6'] = 6;
        cardValueMap['7'] = 7;
        cardValueMap['8'] = 8;
        cardValueMap['9'] = 9;
        cardValueMap['T'] = 10;
        cardValueMap['J'] = 11;
        cardValueMap['Q'] = 12;
        cardValueMap['K'] = 13;
        cardValueMap['A'] = 14;

        Span<int> cardCounts = stackalloc int[15];
        Span<ulong> handScores = stackalloc ulong[Input.Lines.Length];
        Span<ulong> handBids = stackalloc ulong[Input.Lines.Length];

        // Process each hand and its bid
        for (int lineIndex = 0; lineIndex < Input.Lines.Length; lineIndex++)
        {
            var line = Input.Lines[lineIndex].AsSpan();

            // Count the cards in the hand
            cardCounts.Fill(0);
            cardCounts[(int)cardValueMap[line[0]]]++;
            cardCounts[(int)cardValueMap[line[1]]]++;
            cardCounts[(int)cardValueMap[line[2]]]++;
            cardCounts[(int)cardValueMap[line[3]]]++;
            cardCounts[(int)cardValueMap[line[4]]]++;

            // Get the basic score without any jokers
            var score = CalculateHandTypeScore(cardCounts);

            // Add the card scores
            score += cardValueMap[line[0]] * 1000000000000;
            score += cardValueMap[line[1]] * 1000000000;
            score += cardValueMap[line[2]] * 1000000;
            score += cardValueMap[line[3]] * 1000;
            score += cardValueMap[line[4]];

            // Parse the bid
            var bid = ulong.Parse(line[6..]);

            // Store for sorting later
            handScores[lineIndex] = score;
            handBids[lineIndex] = bid;
        }

        // Sort based on 
        handScores.Sort(handBids);

        // Sum the total winnings
        ulong totalWinnings = 0;
        for (int i = 0; i < handBids.Length; i++)
        {
            totalWinnings += handBids[i] * (ulong)(i + 1);
        }

        return totalWinnings.ToString();
    }

    protected override string? Problem2()
    {
        Span<ulong> cardValueMap = stackalloc ulong['T' + 1];
        cardValueMap['2'] = 2;
        cardValueMap['3'] = 3;
        cardValueMap['4'] = 4;
        cardValueMap['5'] = 5;
        cardValueMap['6'] = 6;
        cardValueMap['7'] = 7;
        cardValueMap['8'] = 8;
        cardValueMap['9'] = 9;
        cardValueMap['T'] = 10;
        cardValueMap['J'] = 1;
        cardValueMap['Q'] = 11;
        cardValueMap['K'] = 12;
        cardValueMap['A'] = 13;

        Span<int> cardCounts = stackalloc int[15];
        Span<ulong> handScores = stackalloc ulong[Input.Lines.Length];
        Span<ulong> handBids = stackalloc ulong[Input.Lines.Length];

        // Process each hand and its bid
        for (int lineIndex = 0; lineIndex < Input.Lines.Length; lineIndex++)
        {
            var line = Input.Lines[lineIndex].AsSpan();

            // Count the cards in the hand
            cardCounts.Fill(0);
            cardCounts[(int)cardValueMap[line[0]]]++;
            cardCounts[(int)cardValueMap[line[1]]]++;
            cardCounts[(int)cardValueMap[line[2]]]++;
            cardCounts[(int)cardValueMap[line[3]]]++;
            cardCounts[(int)cardValueMap[line[4]]]++;

            // Add joker counts to other cards
            var jokerCount = cardCounts[1];

            // Get the hand type score
            ulong score;
            if (jokerCount == 0)
            {
                // Get the basic score without any jokers
                score = CalculateHandTypeScore(cardCounts);
            }
            else
            {
                // Get the score applying the joker count to each card individually, taking
                // the maximum score
                score = 0;
                for (int i = 2; i < cardCounts.Length; i++)
                {
                    cardCounts[i] += jokerCount;

                    score = ulong.Max(score, CalculateHandTypeScore(cardCounts));

                    cardCounts[i] -= jokerCount;
                }
            }

            // Add the card scores
            score += cardValueMap[line[0]] * 1000000000000;
            score += cardValueMap[line[1]] * 1000000000;
            score += cardValueMap[line[2]] * 1000000;
            score += cardValueMap[line[3]] * 1000;
            score += cardValueMap[line[4]];

            // Parse the bid
            var bid = ulong.Parse(line[6..]);

            // Store for sorting later
            handScores[lineIndex] = score;
            handBids[lineIndex] = bid;
        }
        // Sort based on 
        handScores.Sort(handBids);

        // Sum the total winnings
        ulong totalWinnings = 0;
        for (int i = 0; i < handBids.Length; i++)
        {
            totalWinnings += handBids[i] * (ulong)(i + 1);
        }

        return totalWinnings.ToString();
    }

    /// <summary>
    /// Calculate the hand type score.
    /// </summary>
    static ulong CalculateHandTypeScore(Span<int> cardCounts)
    {
        // Check hand type
        var hasFiveOfAKind = false;
        var hasFourOfAKind = false;
        var hasThreeOfAKind = false;
        var numberOfPairs = 0;
        for (int i = 2; i < cardCounts.Length; i++)
        {
            var count = cardCounts[i];

            if (count >= 5)
            {
                hasFiveOfAKind |= true;
            }
            else if (count >= 4)
            {
                hasFourOfAKind |= true;
            }
            else if (count >= 3)
            {
                hasThreeOfAKind |= true;
            }
            else if (count >= 2)
            {
                numberOfPairs++;
            }
        }

        // Get the hand score
        if (hasFiveOfAKind)
        {
            return 600000000000000;
        }
        else if (hasFourOfAKind)
        {
            return 500000000000000;
        }
        else if (hasThreeOfAKind & numberOfPairs >= 1)
        {
            return 400000000000000;
        }
        else if (hasThreeOfAKind)
        {
            return 300000000000000;
        }
        else if (numberOfPairs >= 2)
        {
            return 200000000000000;
        }
        else if (numberOfPairs >= 1)
        {
            return 100000000000000;
        }
        else
        {
            return 0;
        }
    }
}

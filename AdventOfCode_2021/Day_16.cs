﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public class Day_16
    {
        private const ulong TYPE_LITERAL = 4;
        private const ulong TYPE_SUM = 0;
        private const ulong TYPE_PRODUCT = 1;
        private const ulong TYPE_MINIMUM = 2;
        private const ulong TYPE_MAXIMUM = 3;
        private const ulong TYPE_GREATER_THAN = 5;
        private const ulong TYPE_LESS_THAN = 6;
        private const ulong TYPE_EQUAL_TO = 7;

        private const ulong LENGTH_TYPE_TOTAL_LENGTH = 0;
        private const ulong LENGTH_TYPE_SUBPACKETS_COUNT = 1;

        public static void PartOneAndTwo()
        {
            var bitStream = GetBitStream(Data);
            var bitStreamIndex = 0;

            // Create a map of operation type to its function
            var operationMap = new Func<ulong, ulong, ulong>[8];

            // Fill invalid operation indices with invalid operations
            Func<ulong, ulong, ulong> invalidOperation = (l, r) => throw new Exception($"Invalid operation. Args: {l}, {r}");
            Array.Fill(operationMap, invalidOperation);

            // Set the function for each operation type
            operationMap[TYPE_SUM]          = (l, r) => l + r;
            operationMap[TYPE_PRODUCT]      = (l, r) => l * r;
            operationMap[TYPE_MINIMUM]      = (l, r) => l < r ? l : r;
            operationMap[TYPE_MAXIMUM]      = (l, r) => l > r ? l : r;
            operationMap[TYPE_GREATER_THAN] = (l, r) => (ulong)Helpers.BoolToInt(l > r);
            operationMap[TYPE_LESS_THAN]    = (l, r) => (ulong)Helpers.BoolToInt(l < r);
            operationMap[TYPE_EQUAL_TO]     = (l, r) => (ulong)Helpers.BoolToInt(l == r);

            // Parse the bit stream
            ParsePacketRecursive(out var answer);
            Console.WriteLine(answer);

            // Read the given number of bits from the bit stream
            ulong ReadBits(int numberOfBitsToRead)
            {
                ulong bits = 0;
                for (int i = 0; i < numberOfBitsToRead; i++)
                {
                    // Shift to make room for the new bit
                    bits <<= 1;

                    // OR in the new bit
                    bits |= bitStream[bitStreamIndex++];
                }
                return bits;
            }

            // Parse the packet at the current bit stream index
            int ParsePacketRecursive(out ulong value)
            {
                // Take the starting bit index
                var bitStreamIndexStarting = bitStreamIndex;

                // Read the version 
                var version = ReadBits(3);

                // Read the type
                var type = ReadBits(3);

                // Check the type
                if (type == TYPE_LITERAL)
                {
                    // Parse the variable length integer while the terminator bit is 1
                    value = 0;
                    ulong terminatorBit;
                    do
                    {
                        // Get the next 5 bit chunk
                        var chunk = ReadBits(5);

                        // Mask out the terminal bit to get the integer bits
                        var integerBits = chunk & 0b1111;

                        // Append the bits to our current integer
                        value <<= 4;
                        value |= integerBits;

                        // Get the terminator bit
                        terminatorBit = chunk >> 4;
                    }
                    while (terminatorBit == 1);
                }
                else
                {
                    // Get the operation function
                    var operationFunction = operationMap[type];

                    // Read the length type id
                    var lengthTypeId = ReadBits(1);
                    if (lengthTypeId == LENGTH_TYPE_TOTAL_LENGTH)
                    {
                        // Get the total length of the sub packets
                        var totalNumberOfBitsForSubPackets = (int)ReadBits(15);

                        // Parse the first packet for the initial value
                        var numberOfBitsParsedForSubPackets = ParsePacketRecursive(out value);

                        // Parse the remaining packets until we reach the total number of bits
                        while (numberOfBitsParsedForSubPackets < totalNumberOfBitsForSubPackets)
                        {
                            numberOfBitsParsedForSubPackets += ParsePacketRecursive(out var subpacketValue);

                            // Use the subpacket value in the operation
                            value = operationFunction(value, subpacketValue);
                        }

                        // Check that we didn't parse too much
                        if (numberOfBitsParsedForSubPackets > totalNumberOfBitsForSubPackets)
                        {
                            throw new Exception(string.Format(
                                "Parsed too many bits for sub packets. Number parsed: {0}, Number expected: {1}",
                                numberOfBitsParsedForSubPackets,
                                totalNumberOfBitsForSubPackets));
                        }
                    }
                    else if (lengthTypeId == LENGTH_TYPE_SUBPACKETS_COUNT)
                    {
                        // Get the number of sub packets
                        var numberOfSubPackets = ReadBits(11);

                        // Parse the first packet for the initial value
                        ParsePacketRecursive(out value);

                        // Parse each sub packet
                        for (ulong i = 1; i < numberOfSubPackets; i++)
                        {
                            ParsePacketRecursive(out var subpacketValue);

                            // Use the subpacket value in the operation
                            value = operationFunction(value, subpacketValue);
                        }
                    }
                    else
                    {
                        throw new Exception($"Unrecognised length type id: {lengthTypeId}");
                    }
                }

                // Return the number of bits parsed
                return (bitStreamIndex - bitStreamIndexStarting);
            }
        }

        private static byte[] GetBitStream(string hexString)
        {
            // Create a lookup for the hex bits
            var bitStream = new byte[hexString.Length * 4];
            var bitStreamIndex = 0;

            // Process each hex character
            for (int i = 0; i < hexString.Length; i++)
            {
                var hexCharacter = hexString[i];

                // Local method to add the given bits to the stream
                void LocalAddBits(byte bit3, byte bit2, byte bit1, byte bit0)
                {
                    bitStream[bitStreamIndex++] = bit3;
                    bitStream[bitStreamIndex++] = bit2;
                    bitStream[bitStreamIndex++] = bit1;
                    bitStream[bitStreamIndex++] = bit0;
                }

                // Add the bits for the given hex character
                switch (hexCharacter)
                {
                    case '0':
                        LocalAddBits(0, 0, 0, 0);
                        break;
                    case '1':
                        LocalAddBits(0, 0, 0, 1);
                        break;
                    case '2':
                        LocalAddBits(0, 0, 1, 0);
                        break;
                    case '3':
                        LocalAddBits(0, 0, 1, 1);
                        break;
                    case '4':
                        LocalAddBits(0, 1, 0, 0);
                        break;
                    case '5':
                        LocalAddBits(0, 1, 0, 1);
                        break;
                    case '6':
                        LocalAddBits(0, 1, 1, 0);
                        break;
                    case '7':
                        LocalAddBits(0, 1, 1, 1);
                        break;
                    case '8':
                        LocalAddBits(1, 0, 0, 0);
                        break;
                    case '9':
                        LocalAddBits(1, 0, 0, 1);
                        break;
                    case 'A':
                        LocalAddBits(1, 0, 1, 0);
                        break;
                    case 'B':
                        LocalAddBits(1, 0, 1, 1);
                        break;
                    case 'C':
                        LocalAddBits(1, 1, 0, 0);
                        break;
                    case 'D':
                        LocalAddBits(1, 1, 0, 1);
                        break;
                    case 'E':
                        LocalAddBits(1, 1, 1, 0);
                        break;
                    case 'F':
                        LocalAddBits(1, 1, 1, 1);
                        break;
                    default:
                        throw new Exception($"Unrecognized hex character: {hexCharacter}");
                }
            }

            return bitStream;
        }

        private static readonly string DataTest1 = @"8A004A801A8002F478";
        private static readonly string DataTest2 = @"620080001611562C8802118E34";
        private static readonly string DataTest3 = @"C0015000016115A2E0802F182340";
        private static readonly string DataTest4 = @"A0016C880162017C3686B18A3D4780";

        private static readonly string Data = @"A20D74AFC6C80CEA7002D4009202C7C00A6830029400F500218080C3002D006CC2018658056E7002DC00C600E75002ED6008EDC00D4003E24A13995080513FA309482649458A054C6E00E6008CEF204BA00B080311B21F4101006E1F414846401A55002F53E9525B845AA7A789F089402997AE3AFB1E6264D772D7345C6008D8026200E41D83B19C001088CB04A294ADD64C0129D818F802727FFF3500793FFF9A801A801539F42200DC3801A39C659ACD3FC6E97B4B1E7E94FC1F440219DAFB5BB1648E8821A4FF051801079C379F119AC58ECC011A005567A6572324D9AE6CCD003639ED7F8D33B8840A666B3C67B51388440193E003413A3733B85F2712DEBB59002B930F32A7D0688010096019375300565146801A194844826BB7132008024C8E4C1A69E66108000D39BAD950802B19839F005A56D9A554E74C08028992E95D802D2764D93B27900501340528A7301F2E0D326F274BCAB00F5009A737540916D9A9D1EA7BD849100425D9E3A9802B800D24F669E7691E19CFFE3AF280803440086C318230DCC01E8BF19E33980331D631C593005E80330919D718EFA0E3233AE31DF41C67F5CB5CAC002758D7355DD57277F6BF1864E9BED0F18031A95DDF99EB7CD64626EF54987AE007CCC3C4AE0174CDAD88E65F9094BC4025FB2B82C6295F04100109263E800FA41792BCED1CC3A233C86600B48FFF5E522D780120C9C3D89D8466EFEA019009C9600A880310BE0C47A100761345E85F2D7E4769240287E80272D3CEFF1C693A5A79DFE38D27CCCA75E5D00803039BFF11F401095F714657DC56300574010936491FBEC1D8A4402234E1E68026200CC5B8FF094401C89D12E14B803325DED2B6EA34CA248F2748834D0E18021339D4F962AB005E78AE75D08050E10066114368EE0008542684F0B40010B8AB10630180272E83C01998803104E14415100623E469821160";
    }
}

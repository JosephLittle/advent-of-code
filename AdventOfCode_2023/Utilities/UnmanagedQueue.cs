﻿using System.Runtime.InteropServices;

namespace AdventOfCode_2023;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public unsafe  struct UnmanagedQueue<T> where T : unmanaged
{
    private readonly T* array;
    private readonly int arrayLength;

    private int headIndex;
    private int count;

    public UnmanagedQueue(T* array, int arrayLength)
    {
        this.array = array;
        this.arrayLength = arrayLength;

        headIndex = 0;
        count = 0;
    }

    public void Enqueue(T item)
    {
        var enqueueIndex = (headIndex + count) % arrayLength;
        array[enqueueIndex] = item;
        count++;
    }

    public T Dequeue()
    {
        var dequeuedItem = array[headIndex];
        headIndex = (headIndex + 1) % arrayLength;
        count--;

        return dequeuedItem;
    }

    public bool TryDequeue(out T item)
    {
        if (count > 0)
        {
            item = Dequeue();
            return true;
        }
        else
        {
            item = default;
            return false;
        }
    }
}

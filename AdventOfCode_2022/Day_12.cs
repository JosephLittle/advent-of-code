﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2022;

[Solution(12)]
[SolutionInput("Inputs/Day_12_Test.txt", Problem1Solution = "31", Problem2Solution = "29")]
[SolutionInput("Inputs/Day_12.txt", Problem1Solution = "383", Problem2Solution = "377", Benchmark = true)]
public unsafe class Day_12 : Solution
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct WorldCell
    {
        public byte Height;
        public ushort ShortestPath;

        public bool IsVisited;
        public bool HasBeenAddedToUnvisitedSet;
    }

    public Day_12(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        var height = inputLines.Length;
        var width = inputLines[0].Length;

        // Create our own grid so that we can store traversal info and also
        // make it 2 cells wider and taller so we don't need to worry about
        // bounds checking
        var worldGridWidth = width + 2;
        var worldGridHeight = height + 2;
        var worldMemory = stackalloc WorldCell[worldGridWidth * worldGridHeight];
        var worldGrid = stackalloc WorldCell*[worldGridHeight];
        for (int y = 0; y < worldGridHeight; y++)
        {
            worldGrid[y] = worldMemory + (worldGridWidth * y);
        }

        // Set the border cells to the highest height so we don't attempt to traverse them
        var borderCell = new WorldCell() { Height = byte.MaxValue };
        new Span<WorldCell>(worldGrid[0], worldGridWidth).Fill(borderCell);
        new Span<WorldCell>(worldGrid[worldGridHeight - 1], worldGridWidth).Fill(borderCell);

        WorldCell* startCell = null;
        WorldCell* endCell = null;

        // Find the starting coords and the end coords
        for (int y = 0; y < height; y++)
        {
            var worldGridY = y + 1;

            var inputLine = inputLines[y];
            var worldGridLine = worldGrid[worldGridY];

            // Set border cells
            worldGridLine[0] = borderCell;
            worldGridLine[worldGridWidth - 1] = borderCell;

            // Iterate over the input cells, copying them into the world grid and checking
            // for the start and end
            for (int x = 0; x < width; x++)
            {
                var cell = inputLine[x];

                var worldGridX = x + 1;

                // Get the world grid cell memory
                var worldGridCell = worldGridLine + worldGridX;

                // Populate it
                *worldGridCell = new WorldCell()
                {
                    ShortestPath = ushort.MaxValue,
                };
                if (cell == 'S')
                {
                    startCell = worldGridCell;

                    worldGridCell->Height = (byte)'a';
                }
                else if (cell == 'E')
                {
                    endCell = worldGridCell;

                    worldGridCell->Height = (byte)'z';
                }
                else
                {
                    worldGridCell->Height = (byte)cell;
                }
            }
        }

        // Populate the shortest paths to each cell from the start cell
        PopulateShortestPathsWithDijkstra(endCell, worldGridWidth, worldGridHeight);

        return startCell->ShortestPath.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        var height = inputLines.Length;
        var width = inputLines[0].Length;

        // Create our own grid so that we can store traversal info and also
        // make it 2 cells wider and taller so we don't need to worry about
        // bounds checking
        var worldGridWidth = width + 2;
        var worldGridHeight = height + 2;
        var worldMemory = stackalloc WorldCell[worldGridWidth * worldGridHeight];
        var worldGrid = stackalloc WorldCell*[worldGridHeight];
        for (int y = 0; y < worldGridHeight; y++)
        {
            worldGrid[y] = worldMemory + (worldGridWidth * y);
        }

        // Set the border cells to the highest height so we don't attempt to traverse them
        var borderCell = new WorldCell() { Height = byte.MaxValue };
        new Span<WorldCell>(worldGrid[0], worldGridWidth).Fill(borderCell);
        new Span<WorldCell>(worldGrid[worldGridHeight - 1], worldGridWidth).Fill(borderCell);

        var endX = 0;
        var endY = 0;

        var lowestElevationCells = new List<IntPtr>();

        // Find the starting coords and the end coords
        for (int y = 0; y < height; y++)
        {
            var worldGridY = y + 1;

            var inputLine = inputLines[y];
            var worldGridLine = worldGrid[worldGridY];

            // Set border cells
            worldGridLine[0] = borderCell;
            worldGridLine[worldGridWidth - 1] = borderCell;

            // Iterate over the input cells, copying them into the world grid and checking
            // for the start and end
            for (int x = 0; x < width; x++)
            {
                var cell = inputLine[x];

                var worldGridX = x + 1;

                // Get the world grid cell memory
                var worldGridCell = worldGridLine + worldGridX;

                // Populate it
                *worldGridCell = new WorldCell()
                {
                    ShortestPath = ushort.MaxValue,
                };
                if (cell == 'S' | cell == 'a')
                {
                    worldGridCell->Height = (byte)'a';

                    // Track the cells with the lowest elevation
                    lowestElevationCells.Add((IntPtr)worldGridCell);
                }
                else if (cell == 'E')
                {
                    endX = worldGridX;
                    endY = worldGridY;

                    worldGridCell->Height = (byte)'z';
                }
                else
                {
                    worldGridCell->Height = (byte)cell;
                }
            }
        }

        // Populate the shortest paths to each cell from the end cell
        var endCell = worldGrid[endY] + endX;
        PopulateShortestPathsWithDijkstra(endCell, worldGridWidth, worldGridHeight);

        // Find the lowest elevant cell with the shortest path
        var shortestPath = ushort.MaxValue;
        for (int i = 0; i < lowestElevationCells.Count; i++)
        {
            var cell = (WorldCell*)lowestElevationCells[i];

            var candidateShortestPath = cell->ShortestPath;
            if (candidateShortestPath < shortestPath)
            {
                shortestPath = candidateShortestPath;
            }
        }

        return shortestPath.ToString();
    }

    private void PopulateShortestPathsWithDijkstra(
        WorldCell* endCell, int worldWidth, int worldHeight)
    {
        // Create the queue of cells to visit
        var unvisitedCells = new List<IntPtr>();

        // Start with the end cell
        unvisitedCells.Add((IntPtr)endCell);
        endCell->HasBeenAddedToUnvisitedSet = true;
        endCell->ShortestPath = 0;

        // Perform dijkstra's algorithm
        while (unvisitedCells.Count > 0)
        {
            // Get the next cell to check with the shortest path
            var currentCellIndex = 0;
            var currentCell = (WorldCell*)unvisitedCells[0];
            for (int i = 1; i < unvisitedCells.Count; i++)
            {
                var candidateCell = (WorldCell*)unvisitedCells[i];
                if (candidateCell->ShortestPath < currentCell->ShortestPath)
                {
                    currentCellIndex = i;
                    currentCell = candidateCell;
                }
            }
            unvisitedCells.RemoveAt(currentCellIndex);

            // Mark this cell as visited
            currentCell->IsVisited = true;

            // Process each neighbour
            ProcessNeighbour(currentCell - 1);
            ProcessNeighbour(currentCell + 1);
            ProcessNeighbour(currentCell - worldWidth);
            ProcessNeighbour(currentCell + worldWidth);

            // Local method to process each neighbour
            void ProcessNeighbour(WorldCell* neighbourCell)
            {
                // Ignore if we can't move to it or we've already visited it
                if (!(neighbourCell->Height >= currentCell->Height - 1) |
                    neighbourCell->IsVisited)
                {
                    return;
                }

                // Get the length of the path to this cell from our previous
                var pathLength = (ushort)(currentCell->ShortestPath + 1);

                // If the new path length is shorter then update it
                if (neighbourCell->ShortestPath > pathLength)
                {
                    neighbourCell->ShortestPath = pathLength;
                }

                // If it's not already in the unvisited list then add it now
                if (!neighbourCell->HasBeenAddedToUnvisitedSet)
                {
                    unvisitedCells.Add((IntPtr)neighbourCell);
                    neighbourCell->HasBeenAddedToUnvisitedSet = true;
                }
            }
        }
    }

    private void OutputWorldGrid(
        WorldCell** worldGrid, int worldGridWidth, int worldGridHeight,
        int startX, int startY, int endX, int endY)
    {
        for (int y = 0; y < worldGridHeight; y++)
        {
            for (int x = 0; x < worldGridWidth; x++)
            {
                if (x == startX & y == startY)
                {
                    Console.Write('S');
                }
                else if (x == endX & y == endY)
                {
                    Console.Write('E');
                }
                else
                {
                    Console.Write((char)worldGrid[y][x].Height);
                }
            }
            Console.WriteLine();
        }
    }
}

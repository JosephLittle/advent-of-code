﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(1)]
[SolutionInput("Inputs/Day_01_Test_01.txt", Problem1Solution = "142")]
[SolutionInput("Inputs/Day_01_Test_02.txt", Problem2Solution = "281")]
[SolutionInput("Inputs/Day_01.txt", Problem1Solution = "54597", Problem2Solution = "54504", Benchmark = true)]
public unsafe class Day_01 : Solution
{
    public Day_01(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        int sum = 0;

        foreach (var line in Input.Lines)
        {
            int value = 0;

            // Get the first digit from the front
            for (int i = 0; i < line.Length; i++)
            {
                var character = line[i];
                if (char.IsDigit(character))
                {
                    value = Helpers.CharToInt(character) * 10;
                    break;
                }
            }

            // Get the last digit from the back
            for (int i = line.Length - 1; i >= 0; i--)
            {
                var character = line[i];
                if (char.IsDigit(character))
                {
                    value += Helpers.CharToInt(character);
                    break;
                }
            }

            // Sum
            sum += value;
        }

        return sum.ToString();
    }

    protected override string? Problem2()
    {
        var digitStrings = new[] 
        { 
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
        };

        int sum = 0;

        foreach (var line in Input.Lines)
        {
            int value = 0;

            // Get the first digit from the front
            for (int i = 0; i < line.Length; i++)
            {
                var character = line[i];
                if (char.IsDigit(character))
                {
                    value = Helpers.CharToInt(character) * 10;
                    goto firstDigitFound;
                }
                else
                {
                    var lineSpan = line.AsSpan();
                    for (int j = 0; j < digitStrings.Length; j++)
                    {
                        var digitString = digitStrings[j];

                        var candidateStartIndex = i;
                        var candidateEndIndex = i + (digitString.Length);

                        if (candidateEndIndex <= line.Length &&
                            line[candidateStartIndex..candidateEndIndex] == digitString)
                        {
                            value = (j + 1) * 10;
                            goto firstDigitFound;
                        }
                    }
                }
            }
            firstDigitFound:

            // Get the last digit from the back
            for (int i = line.Length - 1; i >= 0; i--)
            {
                var character = line[i];
                if (char.IsDigit(character))
                {
                    value += Helpers.CharToInt(character);
                    goto secondDigitFound;
                }
                else
                {
                    var lineSpan = line.AsSpan();
                    for (int j = 0; j < digitStrings.Length; j++)
                    {
                        var digitString = digitStrings[j];

                        var candidateStartIndex = i - (digitString.Length - 1);
                        var candidateEndIndex = i + 1;

                        if (candidateStartIndex >= 0 &&
                            line[candidateStartIndex..candidateEndIndex] == digitString)
                        {
                            value += (j + 1);
                            goto secondDigitFound;
                        }
                    }
                }
            }
            secondDigitFound:

            // Sum
            sum += value;
        }

        return sum.ToString();
    }
}

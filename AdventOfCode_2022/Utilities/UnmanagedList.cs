﻿using System.Runtime.InteropServices;

namespace AdventOfCode_2022;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public unsafe struct UnmanagedList<T> where T : unmanaged
{
    private readonly T* array;
    private readonly int arrayLength;

    public T this[int i]
    {
        get => array[i];
    }

    public int Count;

    public UnmanagedList(T* array, int arrayLength)
    {
        this.array = array;
        this.arrayLength = arrayLength;

        Count = 0;
    }

    public void Add(T item)
    {
        array[Count++] = item;
    }

    public void Clear()
    {
        Count = 0;
    }
}
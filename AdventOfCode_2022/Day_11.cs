﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2022;

[Solution(11)]
[SolutionInput("Inputs/Day_11_Test.txt", Problem1Solution = "10605", Problem2Solution = "2713310158")]
[SolutionInput("Inputs/Day_11.txt", Problem1Solution = "107822", Problem2Solution = "27267163742", Benchmark = true)]
public unsafe class Day_11 : Solution
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct UnmanagedMonkey
    {
        public readonly uint OperationParameter;
        public readonly uint TestDivisor;
        public readonly int TestResultTrueMonkeyIndex;
        public readonly int TestResultFalseMonkeyIndex;
        public readonly delegate*<ulong, ulong, ulong> Operation;

        // List must not be readonly otherwise defensive copy will destroy state
        public UnmanagedList<ulong> ItemsList;
        public int InspectionCount;

        public UnmanagedMonkey(
            UnmanagedList<ulong> itemsList, 
            char operationType, 
            uint operationParameter, 
            uint testDivisor, 
            int testResultTrueMonkeyIndex, 
            int testResultFalseMonkeyIndex)
        {
            ItemsList = itemsList;
            OperationParameter = operationParameter;
            TestDivisor = testDivisor;
            TestResultTrueMonkeyIndex = testResultTrueMonkeyIndex;
            TestResultFalseMonkeyIndex = testResultFalseMonkeyIndex;

            InspectionCount = 0;

            // Set the operation function
            if (operationType == '+')
            {
                if (operationParameter == OperationParameterOld)
                {
                    Operation = &AdditionWithSelf;
                }
                else
                {
                    Operation = &AdditionWithParameter;
                }
            }
            else if (operationType == '*')
            {
                if (operationParameter == OperationParameterOld)
                {
                    Operation = &MultiplicationWithSelf;
                }
                else
                {
                    Operation = &MultiplicationWithParameter;
                }
            }
            else
            {
                throw new Exception($"Unsupported operation type: {operationType}");
            }
        }

        private static ulong AdditionWithParameter(ulong item, ulong parameter)
        {
            return item + parameter;
        }
        private static ulong AdditionWithSelf(ulong item, ulong _)
        {
            return item + item;
        }
        private static ulong MultiplicationWithParameter(ulong item, ulong parameter)
        {
            return item * parameter;
        }
        private static ulong MultiplicationWithSelf(ulong item, ulong _)
        {
            return item * item;
        }
    }

    private const int NumberOfInputLinesPerMonkey = 7;
    private static readonly int OperationParameterOld = Helpers.SubstringToUnsignedInt("old", 0, 3);

    public Day_11(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Allocate the required amount of memory
        var numberOfMonkeys = ((inputLines.Length + 1) / NumberOfInputLinesPerMonkey);
        var expectedMaximumNumberOfItemsPerMonkey = numberOfMonkeys * 10;

        // Get the memory for the monkeys to use then set them up
        var memoryRequirements = GetMemoryRequirements(expectedMaximumNumberOfItemsPerMonkey);
        var memory = stackalloc byte[memoryRequirements.Total];
        var monkeyArray = SetUpMonkeyArrayFromInput(memory, memoryRequirements.Total, expectedMaximumNumberOfItemsPerMonkey);

        // Run for the specified number of rounds
        var numberOfRoundsToRunFor = 20;
        for (int roundNumber = 0; roundNumber < numberOfRoundsToRunFor; roundNumber++)
        {
            // Visit each monkey in turn
            for (int monkeyIndex = 0; monkeyIndex < numberOfMonkeys; monkeyIndex++)
            {
                var monkey = monkeyArray + monkeyIndex;

                // Inspect each of the monkey's items
                for (int itemIndex = 0; itemIndex < monkey->ItemsList.Count; itemIndex++)
                {
                    var item = monkey->ItemsList[itemIndex];

                    // Run the item through the monkey's operation
                    item = monkey->Operation(item, monkey->OperationParameter);

                    // Divide the item by three to get its decreased worry level
                    item /= 3;

                    // Find which monkey he passes the item to next
                    var indexOfMonkeyToPassTo = monkey->TestResultFalseMonkeyIndex;
                    if (item % monkey->TestDivisor == 0)
                    {
                        indexOfMonkeyToPassTo = monkey->TestResultTrueMonkeyIndex;
                    }

                    // Increment the inspection count
                    monkey->InspectionCount++;

                    // Pass the item on
                    (monkeyArray + indexOfMonkeyToPassTo)->ItemsList.Add(item);
                }

                // Now clear this monkey's items
                monkey->ItemsList.Clear();
            }
        }

        // Return the level of monkey business
        return GetLevelOfMonkeyBusiness(monkeyArray, numberOfMonkeys).ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        // Allocate the required amount of memory
        var numberOfMonkeys = ((inputLines.Length + 1) / NumberOfInputLinesPerMonkey);
        var expectedMaximumNumberOfItemsPerMonkey = numberOfMonkeys * 10;

        // Get the memory for the monkeys to use then set them up
        var memoryRequirements = GetMemoryRequirements(expectedMaximumNumberOfItemsPerMonkey);
        var memory = stackalloc byte[memoryRequirements.Total];
        var monkeyArray = SetUpMonkeyArrayFromInput(memory, memoryRequirements.Total, expectedMaximumNumberOfItemsPerMonkey);

        // Get the highest common factor of the monkeys' divisors
        var highestDivisor = monkeyArray[0].TestDivisor;
        for (int i = 1; i < numberOfMonkeys; i++)
        {
            var divisor = monkeyArray[i].TestDivisor;
            if (divisor > highestDivisor)
            {
                highestDivisor = divisor;
            }
        }
        bool foundLowestCommonMultiple;
        var lowestCommonMultiple = highestDivisor;
        do
        {
            lowestCommonMultiple += highestDivisor;
            foundLowestCommonMultiple = true;
            for (int i = 0; i < numberOfMonkeys & foundLowestCommonMultiple; i++)
            {
                foundLowestCommonMultiple &= lowestCommonMultiple % monkeyArray[i].TestDivisor == 0;
            }
        }
        while (!foundLowestCommonMultiple);

        // Run for the specified number of rounds
        var numberOfRoundsToRunFor = 10_000;
        for (int roundNumber = 0; roundNumber < numberOfRoundsToRunFor; roundNumber++)
        {
            // Visit each monkey in turn
            for (int monkeyIndex = 0; monkeyIndex < numberOfMonkeys; monkeyIndex++)
            {
                var monkey = monkeyArray + monkeyIndex;

                // Inspect each of the monkey's items
                for (int itemIndex = 0; itemIndex < monkey->ItemsList.Count; itemIndex++)
                {
                    var item = monkey->ItemsList[itemIndex];

                    // Run the item through the monkey's operation
                    item = monkey->Operation(item, monkey->OperationParameter);

                    // Normalise the item worry level while preserving any divisibility
                    item = (item % lowestCommonMultiple);

                    // Find which monkey he passes the item to next
                    var indexOfMonkeyToPassTo = monkey->TestResultFalseMonkeyIndex;
                    if (item % monkey->TestDivisor == 0)
                    {
                        indexOfMonkeyToPassTo = monkey->TestResultTrueMonkeyIndex;
                    }

                    // Increment the inspection count
                    monkey->InspectionCount++;

                    // Pass the item on
                    (monkeyArray + indexOfMonkeyToPassTo)->ItemsList.Add(item);
                }

                // Now clear this monkey's items
                monkey->ItemsList.Clear();
            }
        }

        // Return the level of monkey business
        return GetLevelOfMonkeyBusiness(monkeyArray, numberOfMonkeys).ToString();
    }

    /// <summary>
    /// Get the level of monkey business from the given monkeys.
    /// </summary>
    private static ulong GetLevelOfMonkeyBusiness(UnmanagedMonkey* monkeyArray, int numberOfMonkeys)
    {
        // Get the level of monkey business (the two highest inspection counts multiplied together)
        var highestInspectionCount = int.MinValue;
        var secondHighestInspectionCount = int.MinValue;
        for (int monkeyIndex = 0; monkeyIndex < numberOfMonkeys; monkeyIndex++)
        {
            var inspectionCount = (monkeyArray + monkeyIndex)->InspectionCount;
            if (inspectionCount > highestInspectionCount)
            {
                secondHighestInspectionCount = highestInspectionCount;
                highestInspectionCount = inspectionCount;
            }
            else if (inspectionCount > secondHighestInspectionCount)
            {
                secondHighestInspectionCount = inspectionCount;
            }
        }
        var levelOfMonkeyBusiness = (ulong)highestInspectionCount * (ulong)secondHighestInspectionCount;
        return levelOfMonkeyBusiness;
    }

    /// <summary>
    /// Set up the monkeys using the given memory, verifying that we've been given enough.
    /// </summary>
    private UnmanagedMonkey* SetUpMonkeyArrayFromInput(byte* memory, int memoryLength, int maximumNumberOfItemsPerMonkey)
    {
        var inputLines = Input.Lines;

        // Verify we have enough memory
        var memoryRequirements = GetMemoryRequirements(maximumNumberOfItemsPerMonkey);
        if (memoryLength < memoryRequirements.Total)
        {
            throw new Exception($"Not enough memory to set up monkeys, requires {memoryRequirements.Total}, has {memoryLength}.");
        }

        var monkeyArray = (UnmanagedMonkey*)memory;
        var listMemory = (ulong*)(memory + memoryRequirements.ForMonkeys);

        // Read the input to build up the starting states for the monkeys
        for (int i = 0; i < inputLines.Length; i += NumberOfInputLinesPerMonkey)
        {
            var line = inputLines[i];

            var monkeyIndex = i / 7;

            // Set up the items list for this monkey
            var itemsList = new UnmanagedList<ulong>(
                listMemory + (maximumNumberOfItemsPerMonkey * monkeyIndex),
                maximumNumberOfItemsPerMonkey);

            // Add the starting items (first item starts at index 18, each item is separated by
            // a comma and a space, so they start at 4 character intervals)
            var startingItemsLine = inputLines[i + 1];
            for (int startingItemsIndex = 18;
                startingItemsIndex < startingItemsLine.Length;
                startingItemsIndex += 4)
            {
                var startingItem = (uint)Helpers.SubstringToUnsignedInt(startingItemsLine, startingItemsIndex, 2);
                itemsList.Add(startingItem);
            }

            // Get the operation
            var operationLine = inputLines[i + 2];
            var operationType = operationLine[23];
            var operationParameter = (uint)Helpers.SubstringToUnsignedInt(operationLine, 25, operationLine.Length - 25);

            // Get the test divisor
            var testDivisorLine = inputLines[i + 3];
            var testDivisor = (uint)Helpers.SubstringToUnsignedInt(testDivisorLine, 21, testDivisorLine.Length - 21);

            // Get the monkey indices for true and false results
            var testResultTrueLine = inputLines[i + 4];
            var testResultFalseLine = inputLines[i + 5];
            var testResultTrueMonkeyIndex = Helpers.CharToInt(testResultTrueLine[testResultTrueLine.Length - 1]);
            var testResultFalseMonkeyIndex = Helpers.CharToInt(testResultFalseLine[testResultFalseLine.Length - 1]);

            // Create and store this monkey
            var monkeyMemory = monkeyArray + monkeyIndex;
            *monkeyMemory = new UnmanagedMonkey(
                itemsList,
                operationType,
                operationParameter,
                testDivisor,
                testResultTrueMonkeyIndex,
                testResultFalseMonkeyIndex);
        }

        // Return the set up monkeys
        return monkeyArray;
    }

    /// <summary>
    /// Get the memory required for the monkeys and their lists.
    /// </summary>
    private (int ForMonkeys, int ForLists, int Total) GetMemoryRequirements(int maximumNumberOfItemsPerMonkey)
    {
        var numberOfMonkeys = ((Input.Lines.Length + 1) / NumberOfInputLinesPerMonkey);
        var memoryRequiredForMonkeys = (numberOfMonkeys * sizeof(UnmanagedMonkey));
        var memoryRequiredForLists = (maximumNumberOfItemsPerMonkey * numberOfMonkeys) * sizeof(ulong);

        return (
            memoryRequiredForMonkeys,
            memoryRequiredForLists,
            memoryRequiredForMonkeys + memoryRequiredForLists);
    }
}

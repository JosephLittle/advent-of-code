﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

/// <summary>
/// https://adventofcode.com/2022/day/1
/// </summary>
[Solution(1)]
[SolutionInput("Inputs/Day_01_Test.txt", Problem1Solution = "24000", Problem2Solution = "45000")]
[SolutionInput("Inputs/Day_01.txt", Problem1Solution = "74198", Problem2Solution = "209914", Benchmark = true)]
public unsafe class Day_01 : Solution
{
    public Day_01(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        const int IndexOfHighestSum = 1;
        var valueStore = new long[2];

        long currentValue = 0;
        long currentSum = 0;

        var characterLookback1 = '\r';
        var characterLookback2 = '\r';

        var input = Input.Raw;
        for (int i = 0; i < input.Length; i++)
        {
            var currentCharacter = input[i];

            // Check for breaks
            var isValueBreak = currentCharacter == '\r' | currentCharacter == '\n';
            var isListBreak = currentCharacter == '\r' & characterLookback2 == '\r';

            // If we're on a value break add the current value to the current sum
            currentSum += currentValue * Helpers.BoolToInt(isValueBreak);

            // If we're on a list break reset the current sum
            currentSum *= Helpers.BoolToInt(!isListBreak);

            // Parse the current character into the current value
            currentValue *= 10;
            currentValue += Helpers.CharToInt(currentCharacter);

            // Reset the value if we're on a value break
            currentValue *= Helpers.BoolToInt(!isValueBreak);

            // Check if the current sum is higher than the highest sum
            var isHigher = currentSum > valueStore[IndexOfHighestSum];

            // Set the new highest sum if we're higher
            valueStore[IndexOfHighestSum * Helpers.BoolToInt(isHigher)] = currentSum;

            characterLookback2 = characterLookback1;
            characterLookback1 = currentCharacter;
        }

        return valueStore[IndexOfHighestSum].ToString();
    }

    protected override string? Problem2()
    {
        const int IndexOfHighestSum1 = 1;
        const int IndexOfHighestSum2 = 2;
        const int IndexOfHighestSum3 = 3;
        var valueStore = stackalloc long[4];

        long currentValue = 0;
        long currentSum = 0;

        var characterLookback1 = '\r';
        var characterLookback2 = '\r';

        var input = Input.Raw;
        for (int i = 0; i < input.Length; i++)
        {
            var currentCharacter = input[i];

            // Check for breaks
            var isValueBreak = currentCharacter == '\r' | currentCharacter == '\n';
            var isListBreak = currentCharacter == '\r' & characterLookback2 == '\r';

            // If we're on a value break add the current value to the current sum
            currentSum += currentValue * Helpers.BoolToInt(isValueBreak);

            // If we're on a list break reset the current sum
            var currentSumForComparisons = currentSum * Helpers.BoolToInt(isListBreak);
            currentSum *= Helpers.BoolToInt(!isListBreak);

            // Parse the current character into the current value
            currentValue *= 10;
            currentValue += Helpers.CharToInt(currentCharacter);

            // Reset the value if we're on a value break
            currentValue *= Helpers.BoolToInt(!isValueBreak);

            // Check if the current sum is higher than the third highest sum
            var isHigher = currentSumForComparisons > valueStore[IndexOfHighestSum3];

            // Set the new highest sum if we're higher
            valueStore[IndexOfHighestSum3 * Helpers.BoolToInt(isHigher)] = currentSumForComparisons;

            // Swap the third and the second highest if the third is higher
            var isThirdHigher = valueStore[IndexOfHighestSum3] > valueStore[IndexOfHighestSum2];
            if (isThirdHigher)
            {
                Helpers.SwapValues(
                    valueStore + IndexOfHighestSum3,
                    valueStore + IndexOfHighestSum2);
            }

            // Swap the second and the first highest if the second is higher
            var isSecondHigher = valueStore[IndexOfHighestSum2] > valueStore[IndexOfHighestSum1];
            if (isSecondHigher)
            {
                Helpers.SwapValues(
                    valueStore + IndexOfHighestSum2,
                    valueStore + IndexOfHighestSum1);
            }

            characterLookback2 = characterLookback1;
            characterLookback1 = currentCharacter;
        }

        var highestSum1 = valueStore[IndexOfHighestSum1];
        var highestSum2 = valueStore[IndexOfHighestSum2];
        var highestSum3 = valueStore[IndexOfHighestSum3];
        return (highestSum1 + highestSum2 + highestSum3).ToString();
    }
}

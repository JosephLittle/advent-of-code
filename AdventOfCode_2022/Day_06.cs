﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(6)]
[SolutionInput("Inputs/Day_06_Test1.txt", Problem1Solution = "7", Problem2Solution = "19")]
[SolutionInput("Inputs/Day_06_Test2.txt", Problem1Solution = "5", Problem2Solution = "23")]
[SolutionInput("Inputs/Day_06_Test3.txt", Problem1Solution = "6", Problem2Solution = "23")]
[SolutionInput("Inputs/Day_06_Test4.txt", Problem1Solution = "10", Problem2Solution = "29")]
[SolutionInput("Inputs/Day_06_Test5.txt", Problem1Solution = "11", Problem2Solution = "26")]
[SolutionInput("Inputs/Day_06.txt", Problem1Solution = "1235", Problem2Solution = "3051", Benchmark = true)]
public unsafe class Day_06 : Solution
{
    public Day_06(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var input = Input.Raw;

        // Iterate over the rest of the characters, bailing out when we have a 4 character
        // sequence without any duplicates
        var i = 3;
        var length = input.Length;
        var indexOfLastDuplicate = -1;
        while (i < length)
        {
            var currentCharacter = input[i];
            var lookbackCharacter1 = input[i - 1];
            var lookbackCharacter2 = input[i - 2];
            var lookbackCharacter3 = input[i - 3];

            // Compare with the third lookback character
            if (currentCharacter == lookbackCharacter3 |
                lookbackCharacter1 == lookbackCharacter3 |
                lookbackCharacter2 == lookbackCharacter3)
            {
                indexOfLastDuplicate = i - 3;
            }

            // Compare with the second lookback character
            if (currentCharacter == lookbackCharacter2 |
                lookbackCharacter1 == lookbackCharacter2)
            {
                indexOfLastDuplicate = i - 2;
            }

            // Compare with the first lookback character
            if (currentCharacter == lookbackCharacter1)
            {
                indexOfLastDuplicate = i - 1;
            }

            // Stop iterating if the index of the last duplicate was more
            // than three characters away
            var noDuplicatesInLast4Characters = indexOfLastDuplicate < i - 3;
            if (noDuplicatesInLast4Characters)
            {
                break;
            }

            // Jump 4 characters from the index of the last duplicated character
            i = indexOfLastDuplicate + 4;
        }

        return (i + 1).ToString();
    }

    protected override string? Problem2()
    {
        // We want to look 14 characters back
        const int LookbackRange = 14;

        var input = Input.Raw;

        // Character index lookup array contains last index character was seen at
        var characterIndexLookup = new int['z' + 1];
        Array.Fill(characterIndexLookup, -1);

        // Track the current character index and the start of the range of characters we're looking at
        var i = LookbackRange - 1;
        var lookbackRangeStart = i - LookbackRange;

        // Iterate backwards from our current position to the range start, if we encounter a character
        // that we've already seen in this range, then jump forward from that position
        while (i > lookbackRangeStart)
        {
            var character = input[i];

            // Get the last index we saw this character at, then store the new index
            var characterLastIndexSeen = characterIndexLookup[character];
            characterIndexLookup[character] = i;

            // Check whether we have already seen this character
            if (characterLastIndexSeen > i)
            {
                // Jump forward from this index by the lookback range
                lookbackRangeStart = i;
                i += LookbackRange;
            }
            else
            {
                // Check the previous character
                i--;
            }
        }

        return (i + LookbackRange + 1).ToString();
    }
}

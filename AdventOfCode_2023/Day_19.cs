﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(19)]
[SolutionInput("Inputs/Day_19_Test.txt", Problem1Solution = "19114", Problem2Solution = "")]
[SolutionInput("Inputs/Day_19.txt", Problem1Solution = "386787", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_19 : Solution
{
    private readonly record struct Part(long X, long M, long A, long S);

    public Day_19(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        var acceptedParts = new List<Part>();
        var rejectedParts = new List<Part>();

        var ruleDictionary = new Dictionary<string, List<RuleBase>>();
        ruleDictionary.Add("A", new List<RuleBase>() { new StorageRule(acceptedParts) });
        ruleDictionary.Add("R", new List<RuleBase>() { new StorageRule(rejectedParts) });

        // Process workflows until an empty line
        string line;
        var i = 0;
        while ((line = Input.Lines[i++]).Length > 0)
        {
            // Parse the label
            var labelEndIndex = line.IndexOf('{');
            var label = line[..labelEndIndex];

            // Add the rule list
            var ruleList = new List<RuleBase>();
            ruleDictionary.Add(label, ruleList);

            // Parse the rules
            var startOfRuleStringIndex = labelEndIndex + 1;
            int endOfRuleStringIndex;
            while ((endOfRuleStringIndex = line.IndexOf(',', startOfRuleStringIndex)) != -1)
            {
                var ruleString = line[startOfRuleStringIndex..endOfRuleStringIndex];

                // Parse the fields
                var category = ruleString[0];
                var operation = ruleString[1];
                var valueStartIndex = 2;
                var value = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(ruleString, ref valueStartIndex, ':');
                var destination = ruleString[(ruleString.IndexOf(':') + 1)..];

                // Add the rule
                ruleList.Add(new CategoryCheckRule(category, operation, value, destination));

                startOfRuleStringIndex = endOfRuleStringIndex + 1;
            }

            // Get the final destination
            var finalDestination = line[startOfRuleStringIndex..^1];

            // Add the final destination rule
            ruleList.Add(new DestinationRule(finalDestination));
        }

        // Process the parts
        for (; i < Input.Lines.Length; i++)
        {
            line = Input.Lines[i];

            // Parse the different fields
            var xStartIndex = line.IndexOf("x=") + 2;
            var x = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref xStartIndex, ',');
            var mStartIndex = line.IndexOf("m=") + 2;
            var m = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref mStartIndex, ',');
            var aStartIndex = line.IndexOf("a=") + 2;
            var a = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref aStartIndex, ',');
            var sStartIndex = line.IndexOf("s=") + 2;
            var s = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref sStartIndex, '}');

            // Process the part
            var part = new Part(x, m, a, s);

            var destination = "in";
            while (destination != string.Empty)
            {
                var ruleList = ruleDictionary[destination];
                foreach (var rule in ruleList)
                {
                    if (rule.TryProcessPart(part, out destination))
                    {
                        break;
                    }
                }
            }
        }

        // Sum the categories of the accepted parts
        long sum = 0;
        foreach (var part in acceptedParts)
        {
            sum += part.X;
            sum += part.M;
            sum += part.A;
            sum += part.S;
        }

        return sum.ToString();
    }

    private abstract class RuleBase
    {
        public abstract bool TryProcessPart(Part part, out string destination);
    }

    private class CategoryCheckRule : RuleBase
    {
        private char category;
        private char operation;
        private long value;
        private string destination;

        public CategoryCheckRule(char category, char operation, long value, string destination)
        {
            this.category = category;
            this.operation = operation;
            this.value = value;
            this.destination = destination;
        }

        public override bool TryProcessPart(Part part, out string destination)
        {
            long categoryValue;
            switch (category)
            {
                case 'x':
                    categoryValue = part.X;
                    break;
                case 'm':
                    categoryValue = part.M;
                    break;
                case 'a':
                    categoryValue = part.A;
                    break;
                case 's':
                    categoryValue = part.S;
                    break;
                default:
                    throw new NotImplementedException();
            }

            if (operation == '>')
            {
                if (categoryValue > value)
                {
                    destination = this.destination;
                    return true;
                }
            }
            else if (operation == '<')
            {
                if (categoryValue < value)
                {
                    destination = this.destination;
                    return true;
                }
            }
            else
            {
                throw new NotImplementedException();
            }

            destination = null;
            return false;
        }
    }

    private class DestinationRule : RuleBase
    {
        private string destination;

        public DestinationRule(string finalDestination)
        {
            this.destination = finalDestination;
        }

        public override bool TryProcessPart(Part part, out string destination)
        {
            destination = this.destination;
            return true;
        }
    }

    private class StorageRule : RuleBase
    {
        private readonly List<Part> storageList;

        public StorageRule(List<Part> storageList)
        {
            this.storageList = storageList;
        }

        public override bool TryProcessPart(Part part, out string destination)
        {
            storageList.Add(part);
            destination = string.Empty;
            return true;
        }
    }

    protected override string? Problem2()
    {
        // Build a tree and run things backwards through it to get the sets of rules that
        // will be accepted.
        // Can test this by hand first.
        return null;
    }
}

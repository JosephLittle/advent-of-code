﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public unsafe class Day_21
    {
        public static void PartOne()
        {
            var data = Data;

            // Parse the starting positions
            var lines = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var playerPositions = lines.Select(l => int.Parse(l.Split(':')[1].Trim())).ToArray();

            // Define the deterministic dice
            var numberOfDiceRolls = 0;
            var maximumDiceRoll = 100;
            int GetNextDiceRoll()
            {
                return (numberOfDiceRolls++ % maximumDiceRoll) + 1;
            }

            // Define the board
            var winningScore = 1_000;
            var numberOfPositions = 10;
            var playerPositionsAndScores = playerPositions.Select(pp => (PositionIndex: pp - 1, Score: 0)).ToArray();
            var turnCounter = 0;

            // Play until a player has won
            var playerHasWon = false;
            var winningPlayerIndex = -1;
            while (!playerHasWon)
            {
                // Get the player's position and score
                var playerIndex = turnCounter++ % playerPositionsAndScores.Length;
                var positionAndScore = playerPositionsAndScores[playerIndex];

                // Get the dice roll
                var diceRoll = GetNextDiceRoll();
                diceRoll += GetNextDiceRoll();
                diceRoll += GetNextDiceRoll();

                // Move the player
                positionAndScore.PositionIndex += diceRoll;
                positionAndScore.PositionIndex = positionAndScore.PositionIndex % numberOfPositions;

                // Increase the score
                positionAndScore.Score += positionAndScore.PositionIndex + 1;

                // Store the updated values
                playerPositionsAndScores[playerIndex] = positionAndScore;

                // Check whether the player has won
                playerHasWon = positionAndScore.Score >= winningScore;
                winningPlayerIndex = playerIndex;

                Console.WriteLine("Player {0} rolls {1} and moves to space {2} for a total score of {3}",
                    playerIndex + 1,
                    diceRoll,
                    positionAndScore.PositionIndex + 1,
                    positionAndScore.Score);
            }

            // Get the losing score and multiply it by the number of dice rolls
            var losingScore = playerPositionsAndScores[(winningPlayerIndex + 1) % playerPositionsAndScores.Length].Score;
            var answer = losingScore * numberOfDiceRolls;

            Console.WriteLine("Losing Score: {0}, Number Of Dice Rolls: {1}, Answer: {2}",
                losingScore,
                numberOfDiceRolls,
                answer);
        }

        public static void PartTwo()
        {
            var data = Data;

            // Parse the starting positions
            var lines = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var playerPositions = lines.Select(l => int.Parse(l.Split(':')[1].Trim())).ToArray();

            var scoreToWin = 21;

            var scoreIndices = 22;
            var positionIndices = 10;

            // Create the possibilities arrays [score1][score2][position1][position2]
            var possibilities = new ulong[scoreIndices, scoreIndices, positionIndices, positionIndices];

            var position1IndexStart = playerPositions[0] - 1;
            var position2IndexStart = playerPositions[1] - 1;
            possibilities[0, 0, position1IndexStart, position2IndexStart] = 1;

            // Execute a turn
            byte player1TurnMask = byte.MaxValue;
            byte player2TurnMask = 0;
            ulong possibilitiesEncounteredOnTurn = 1;
            while (possibilitiesEncounteredOnTurn > 0)
            {
                possibilitiesEncounteredOnTurn = 0;

                //PrintPossibilitiesGridAndWaitForReadLine();

                for (int score1 = scoreToWin - 1; score1 >= 0; score1--)
                {
                    for (int score2 = scoreToWin - 1; score2 >= 0; score2--)
                    {
                        for (int position1Index = 0; position1Index < positionIndices; position1Index++)
                        {
                            for (int position2Index = 0; position2Index < positionIndices; position2Index++)
                            {
                                // Get the number of possibilities, then zero it
                                var numberOfPossibilties = possibilities[score1, score2, position1Index, position2Index];
                                possibilities[score1, score2, position1Index, position2Index] = 0;

                                if (numberOfPossibilties == 0)
                                {
                                    continue;
                                }

                                possibilitiesEncounteredOnTurn += numberOfPossibilties;

                                // Roll the die three times
                                for (int roll1 = 1; roll1 <= 3; roll1++)
                                    for (int roll2 = 1; roll2 <= 3; roll2++)
                                        for (int roll3 = 1; roll3 <= 3; roll3++)
                                            Move((byte)(roll1 + roll2 + roll3));

                                // Dice roll 1, 2, and 3
                                void Move(byte moveDistance)
                                {
                                    var position1IndexAfterMove = (position1Index + (moveDistance & player1TurnMask)) % positionIndices;
                                    var position2IndexAfterMove = (position2Index + (moveDistance & player2TurnMask)) % positionIndices;

                                    var score1AfterMove = Math.Min(scoreToWin, score1 + ((position1IndexAfterMove + 1) & player1TurnMask));
                                    var score2AfterMove = Math.Min(scoreToWin, score2 + ((position2IndexAfterMove + 1) & player2TurnMask));

                                    possibilities[
                                        score1AfterMove, 
                                        score2AfterMove, 
                                        position1IndexAfterMove, 
                                        position2IndexAfterMove] 
                                        += numberOfPossibilties;
                                }
                            }
                        }
                    }
                }

                // Swap the turn masks
                var swap = player1TurnMask;
                player1TurnMask = player2TurnMask;
                player2TurnMask = swap;
            }

            // Count winning player 1
            ulong player1PossibilitiesSum = 0;
            for (int score2 = 0; score2 < scoreIndices; score2++)
            {
                for (int position1Index = 0; position1Index < positionIndices; position1Index++)
                {
                    for (int position2Index = 0; position2Index < positionIndices; position2Index++)
                    {
                        player1PossibilitiesSum += possibilities[scoreToWin, score2, position1Index, position2Index];
                    }
                }
            }
            ulong player2PossibilitiesSum = 0;
            for (int score1 = 0; score1 < scoreIndices; score1++)
            {
                for (int position1Index = 0; position1Index < positionIndices; position1Index++)
                {
                    for (int position2Index = 0; position2Index < positionIndices; position2Index++)
                    {
                        player2PossibilitiesSum += possibilities[score1, scoreToWin, position1Index, position2Index];
                    }
                }
            }
            var sum = player1PossibilitiesSum + player2PossibilitiesSum;

            Console.WriteLine("Player 1 winning universes: {0}", player1PossibilitiesSum);
            Console.WriteLine("Player 2 winning universes: {0}", player2PossibilitiesSum);
        }

        private struct PlayerWinCounts
        {
            public ulong Player1WinCount;
            public ulong Player2WinCount;
        }

        private struct PlayerData
        {
            public int PositionIndex;
            public int Score;
        }

        void PrintPossibilitiesGridAndWaitForReadLine(ulong[,,,] possibilities, int scoreIndices, int positionIndices)
        {
            var sb = new StringBuilder();
            for (int score1 = 0; score1 < scoreIndices; score1++)
            {
                for (int score2 = 0; score2 < scoreIndices; score2++)
                {
                    ulong possibilitiesSum = 0;

                    for (int position1Index = 0; position1Index < positionIndices; position1Index++)
                    {
                        for (int position2Index = 0; position2Index < positionIndices; position2Index++)
                        {
                            possibilitiesSum += possibilities[score1, score2, position1Index, position2Index];
                        }
                    }

                    if (possibilitiesSum > 0)
                    {
                        sb.Append("#");
                    }
                    else
                    {
                        sb.Append(".");
                    }
                }
                sb.AppendLine();
            }
            Console.WriteLine(sb.ToString());
            Console.ReadKey();
        }

        private static readonly string DataTest = @"Player 1 starting position: 4
Player 2 starting position: 8";

        private static readonly string Data = @"Player 1 starting position: 4
Player 2 starting position: 5";
    }
}

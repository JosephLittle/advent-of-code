﻿using AdventOfCode.Framework;
using System.Diagnostics;

namespace AdventOfCode_2022;

[Solution(17)]
[SolutionInput("Inputs/Day_17_Test.txt", Problem1Solution = "3068", Problem2Solution = "1514285714288")]
[SolutionInput("Inputs/Day_17.txt", Problem1Solution = "3109", Problem2Solution = "1541449275365", Benchmark = true)]
public unsafe class Day_17 : Solution
{
    private const int WorldWidth = 7;

    private const int NumberOfRockShapes = 5;

    private struct WorldRow
    {
        public byte Cells;
        public ulong NumberOfRocksPlaced;
    }

    private struct RockShape
    {
        public readonly int Width;
        public readonly int Height;
        public readonly delegate*<WorldRow*, int, bool> IsCollidingFunction;
        public readonly delegate*<WorldRow*, int, void> OccupySpaceFunction;

        public RockShape(
            int Width,
            int Height,
            delegate*<WorldRow*, int, bool> IsCollidingFunction,
            delegate*<WorldRow*, int, void> OccupySpaceFunction)
        {
            this.Width = Width;
            this.Height = Height;
            this.IsCollidingFunction = IsCollidingFunction;
            this.OccupySpaceFunction = OccupySpaceFunction;
        }
    }

    public Day_17(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        return SolveForNumberOfRocks(2022).ToString();
    }

    protected override string? Problem2()
    {
        return SolveForNumberOfRocks(1_000_000_000_000).ToString();
    }

    private ulong SolveForNumberOfRocks(ulong numberOfRocksToDrop)
    {
        // Set up the shapes
        var rockShapes = stackalloc RockShape[NumberOfRockShapes];
        rockShapes[0] = new RockShape(4, 1, &IsCollidingShape1, &OccupySpaceShape1);
        rockShapes[1] = new RockShape(3, 3, &IsCollidingShape2, &OccupySpaceShape2);
        rockShapes[2] = new RockShape(3, 3, &IsCollidingShape3, &OccupySpaceShape3);
        rockShapes[3] = new RockShape(1, 4, &IsCollidingShape4, &OccupySpaceShape4);
        rockShapes[4] = new RockShape(2, 2, &IsCollidingShape5, &OccupySpaceShape5);
        var nextRockShapeIndex = 0;
        RockShape GetNextRockShape()
        {
            return rockShapes[nextRockShapeIndex++ % NumberOfRockShapes];
        }

        // Set up the air jets
        var airJets = Input.Raw;
        var nextAirJetIndex = 0;
        char GetNextAirJet()
        {
            return airJets[nextAirJetIndex++ % airJets.Length];
        }
        int GetNextAirJetAsOffset()
        {
            return GetNextAirJet() - 61;
        }

        // Set up the world grid
        const int WorldHeight = 10_000;
        var worldRows = stackalloc WorldRow[WorldHeight];

        // Set the bottom row as occupied
        var bottomRow = worldRows + (WorldHeight - 1);
        bottomRow->Cells = byte.MaxValue;

        // Set up the repeated section variables
        var hasFoundRepeatingSection = false;
        ulong numberOfRowsForRepeatingSection = 0;

        // Drop each rock
        var highestRockRowIndex = WorldHeight - 1;
        ulong numberOfRocksRemaining = numberOfRocksToDrop;
        while (numberOfRocksRemaining-- > 0)
        {
            // Get the rock to drop
            var currentRock = GetNextRockShape();

            // Get the starting position
            var currentX = 2;
            var currentY = highestRockRowIndex - 4 - (currentRock.Height - 1);

            // First let the rock fall three cells and be blown by air jets three times
            ShiftStartingXByAirJet();
            ShiftStartingXByAirJet();
            ShiftStartingXByAirJet();
            void ShiftStartingXByAirJet()
            {
                var offset = GetNextAirJetAsOffset();
                currentX += offset;
                if (!(currentX >= 0 & currentX <= WorldWidth - currentRock.Width))
                {
                    currentX -= offset;
                }
            }

            // Move the starting position down by 3
            currentY += 3;

            // Get the pointer to the current row
            var currentRow = worldRows + currentY;

            // Loop shifting and dropping until the rock drops on something solid
            while (true)
            {
                // Get the next shift offset
                var shiftOffset = GetNextAirJetAsOffset();

                // Check that it doesn't shift us into a wall
                var xAfterShift = currentX + shiftOffset;
                if (xAfterShift >= 0 & xAfterShift <= WorldWidth - currentRock.Width)
                {
                    // Check that it doesn't collide with anything
                    if (!currentRock.IsCollidingFunction(currentRow, xAfterShift))
                    {
                        // Allow the shift to happen
                        currentX += shiftOffset;
                    }
                }

                // Now check to see if the move down will collide with anything
                if (currentRock.IsCollidingFunction(currentRow + 1, currentX))
                {
                    // Stop moving this rock
                    break;
                }
                else
                {
                    // No collision, so safely move down
                    currentRow++;
                    currentY++;
                }
            }

            // Stopped moving the rock, so occupy the current space
            currentRock.OccupySpaceFunction(currentRow, currentX);

            // Set the new highest rock row index
            if (currentY < highestRockRowIndex)
            {
                highestRockRowIndex = currentY;

                // Set the number of rocks dropped when this row was occupied
                currentRow->NumberOfRocksPlaced = numberOfRocksToDrop - numberOfRocksRemaining;

                // Look back and try to find a recurring pattern
                if (!hasFoundRepeatingSection)
                {
                    var rowToCompareWith1 = currentRow;
                    while (++rowToCompareWith1 < bottomRow)
                    {
                        // Ignore if this row has different cells or doesn't have a rock count
                        if (rowToCompareWith1->Cells != currentRow->Cells |
                            rowToCompareWith1->NumberOfRocksPlaced == 0)
                        {
                            continue;
                        }

                        // Get the number of rows between the rows
                        var numberOfRowsBetween = (rowToCompareWith1 - currentRow);

                        // Check the next row after the number of rows
                        var rowToCompareWith2 = (rowToCompareWith1 + numberOfRowsBetween);
                        if (rowToCompareWith2 < bottomRow &&
                            (rowToCompareWith2->Cells == currentRow->Cells & 
                            rowToCompareWith2->NumberOfRocksPlaced > 0))
                        {
                            // We have found three identical rows which have equal numbers of rows
                            // between them. Now check whether they have an equal number of rocks
                            // between them
                            var numberOfRocksBetween = currentRow->NumberOfRocksPlaced - rowToCompareWith1->NumberOfRocksPlaced;
                            if (numberOfRocksBetween == (rowToCompareWith1->NumberOfRocksPlaced - rowToCompareWith2->NumberOfRocksPlaced))
                            {
                                // Now check whether those rows between them are equal
                                var comparisonRow1 = currentRow + 1;
                                var comparisonRow2 = currentRow + 1 + numberOfRowsBetween;
                                var areEqual = true;
                                while ((areEqual &= ((comparisonRow1++)->Cells == (comparisonRow2++)->Cells)) &
                                    comparisonRow1 < rowToCompareWith1)
                                { }
                                if (areEqual)
                                {
                                    // We have found a repeating section!
                                    var numberOfRowsInSection = numberOfRowsBetween;
                                    var numberOfRocksInSection = numberOfRocksBetween;

                                    // Repeat the section
                                    var numberOfTimesSectionCanBeRepeated = numberOfRocksRemaining / numberOfRocksInSection;
                                    numberOfRowsForRepeatingSection = (ulong)numberOfRowsInSection * numberOfTimesSectionCanBeRepeated;
                                    numberOfRocksRemaining %= numberOfRocksInSection;

                                    hasFoundRepeatingSection = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return numberOfRowsForRepeatingSection + (ulong)(WorldHeight - highestRockRowIndex - 1);
    }

    private static void PrintRows(WorldRow* worldRows, int startIndex, int endIndex)
    {
        Console.WriteLine();
        for (int i = startIndex; i <= endIndex; i++)
        {
            var worldRow = worldRows[i];

            Console.Write(((worldRow.Cells & 0b1000_0000) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0100_0000) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0010_0000) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0001_0000) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0000_1000) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0000_0100) != 0) ? '#' : '.');
            Console.Write(((worldRow.Cells & 0b0000_0010) != 0) ? '#' : '.');
            Console.WriteLine();
        }
        Console.ReadKey();
    }

    // Shape 1
    // ####
    private static bool IsCollidingShape1(WorldRow* currentRow, int currentX)
    {
        return (currentRow[0].Cells & (0b1111_0000 >> currentX)) != 0;
    }
    private static void OccupySpaceShape1(WorldRow* currentRow, int currentX)
    {
        currentRow[0].Cells |= (byte)(0b1111_0000 >> currentX);
    }

    // Shape 2
    // .#.
    // ###
    // .#.
    private static bool IsCollidingShape2(WorldRow* currentRow, int currentX)
    {
        return (currentRow[0].Cells & (0b0100_0000 >> currentX)) != 0 |
            (currentRow[1].Cells & (0b1110_0000 >> currentX)) != 0 |
            (currentRow[2].Cells & (0b0100_0000 >> currentX)) != 0;
    }
    private static void OccupySpaceShape2(WorldRow* currentRow, int currentX)
    {
        currentRow[0].Cells |= (byte)(0b0100_0000 >> currentX);
        currentRow[1].Cells |= (byte)(0b1110_0000 >> currentX);
        currentRow[2].Cells |= (byte)(0b0100_0000 >> currentX);
    }

    // Shape 3
    // ..#
    // ..#
    // ###
    private static bool IsCollidingShape3(WorldRow* currentRow, int currentX)
    {
        return (currentRow[0].Cells & (0b0010_0000 >> currentX)) != 0 |
            (currentRow[1].Cells & (0b0010_0000 >> currentX)) != 0 |
            (currentRow[2].Cells & (0b1110_0000 >> currentX)) != 0;
    }
    private static void OccupySpaceShape3(WorldRow* currentRow, int currentX)
    {
        currentRow[0].Cells |= (byte)(0b0010_0000 >> currentX);
        currentRow[1].Cells |= (byte)(0b0010_0000 >> currentX);
        currentRow[2].Cells |= (byte)(0b1110_0000 >> currentX);
    }

    // Shape 4
    // #
    // #
    // #
    // #
    private static bool IsCollidingShape4(WorldRow* currentRow, int currentX)
    {
        return (currentRow[0].Cells & (0b1000_0000 >> currentX)) != 0 |
            (currentRow[1].Cells & (0b1000_0000 >> currentX)) != 0 |
            (currentRow[2].Cells & (0b1000_0000 >> currentX)) != 0 |
            (currentRow[3].Cells & (0b1000_0000 >> currentX)) != 0;
    }
    private static void OccupySpaceShape4(WorldRow* currentRow, int currentX)
    {
        currentRow[0].Cells |= (byte)(0b1000_0000 >> currentX);
        currentRow[1].Cells |= (byte)(0b1000_0000 >> currentX);
        currentRow[2].Cells |= (byte)(0b1000_0000 >> currentX);
        currentRow[3].Cells |= (byte)(0b1000_0000 >> currentX);
    }

    // Shape 5
    // ##
    // ##
    private static bool IsCollidingShape5(WorldRow* currentRow, int currentX)
    {
        return (currentRow[0].Cells & (0b1100_0000 >> currentX)) != 0 |
            (currentRow[1].Cells & (0b1100_0000 >> currentX)) != 0;
    }
    private static void OccupySpaceShape5(WorldRow* currentRow, int currentX)
    {
        currentRow[0].Cells |= (byte)(0b1100_0000 >> currentX);
        currentRow[1].Cells |= (byte)(0b1100_0000 >> currentX);
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(6)]
[SolutionInput("Inputs/Day_06_Test.txt", Problem1Solution = "288", Problem2Solution = "71503")]
[SolutionInput("Inputs/Day_06.txt", Problem1Solution = "2344708", Problem2Solution = "30125202", Benchmark = true)]
public unsafe class Day_06 : Solution
{
    public Day_06(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Get the times and distances
        var times = Input.Lines[0].
            Split(' ', StringSplitOptions.RemoveEmptyEntries).
            Skip(1).
            Select(int.Parse).
            ToArray();
        var distances = Input.Lines[1].
            Split(' ', StringSplitOptions.RemoveEmptyEntries).
            Skip(1).
            Select(int.Parse).
            ToArray();

        // Track the result
        var result = 1;

        // Process each time and distance
        for (int i = 0; i < times.Length; i++)
        {
            var time = times[i];
            var distance = distances[i];

            var numberOfWinningOptions = 0;

            // Count the number of winning options
            var speed = 0;
            while (time > 0)
            {
                var isWinnOption = speed * time > distance;
                numberOfWinningOptions += Helpers.BoolToInt(isWinnOption);

                speed++;
                time--;
            }

            // Multiple the number of winning options by the current result
            result *= numberOfWinningOptions;
        }

        return result.ToString();
    }

    protected override string? Problem2()
    {
        // Get the time and distance
        var time = ulong.Parse(string.Join("", Input.Lines[0].
            Split(' ', StringSplitOptions.RemoveEmptyEntries).
            Skip(1)));
        var distance = ulong.Parse(string.Join("", Input.Lines[1].
            Split(' ', StringSplitOptions.RemoveEmptyEntries).
            Skip(1)));

        // Get the number of winning options of this one race
        var numberOfWinningOptions = 0;
        ulong speed = 0;
        while (time > 0)
        {
            var isWinnOption = speed * time > distance;
            numberOfWinningOptions += Helpers.BoolToInt(isWinnOption);

            speed++;
            time--;
        }

        return numberOfWinningOptions.ToString();
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(2)]
[SolutionInput("Inputs/Day_02_Test.txt", Problem1Solution = "15", Problem2Solution = "12")]
[SolutionInput("Inputs/Day_02.txt", Problem1Solution = "10624", Problem2Solution = "14060", Benchmark = true)]
public class Day_02 : Solution
{
    public Day_02(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // A or X = Rock
        // B or Y = Paper
        // C or Z = Scissors
        // Win = 6 points
        // Draw = 3 points
        // Lose = 0 points
        // Rock = 1 point
        // Paper = 2 points
        // Scissors = 3 points

        int GetPairKey(char a, char b)
        {
            return
                (a - 'A') +
                ((b - 'X') * 3);
        }

        // Create the lookup array
        var pointsLookup = new long[GetPairKey('C', 'Z') + 1];

        // Add draw points
        pointsLookup[GetPairKey('A', 'X')] = 3;
        pointsLookup[GetPairKey('B', 'Y')] = 3;
        pointsLookup[GetPairKey('C', 'Z')] = 3;

        // Add win points
        pointsLookup[GetPairKey('A', 'Y')] = 6;
        pointsLookup[GetPairKey('B', 'Z')] = 6;
        pointsLookup[GetPairKey('C', 'X')] = 6;

        // Iterate over the lines, summing the points
        long pointsSum = 0;
        var inputLines = Input.Lines;
        for (int i = 0; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            var selectionOpponent = line[0];
            var selectionMine = line[2];

            // Add the points for win or draw
            pointsSum += pointsLookup[GetPairKey(selectionOpponent, selectionMine)];

            // Add the points for my selection (x = 1, y = 2, z = 3)
            pointsSum += selectionMine - 'W';
        }

        return pointsSum.ToString();
    }

    protected override string? Problem2()
    {
        // A or X = Rock
        // B or Y = Paper
        // C or Z = Scissors
        // Win = 6 points
        // Draw = 3 points
        // Lose = 0 points
        // Rock = 1 point
        // Paper = 2 points
        // Scissors = 3 points

        int GetPairKey(char a, char b)
        {
            return
                (a - 'A') +
                ((b - 'X') * 3);
        }

        // Create the lookup array
        var pointsLookup = new long[GetPairKey('C', 'Z') + 1];

        // Add draw points
        pointsLookup[GetPairKey('A', 'X')] = 3;
        pointsLookup[GetPairKey('B', 'Y')] = 3;
        pointsLookup[GetPairKey('C', 'Z')] = 3;

        // Add win points
        pointsLookup[GetPairKey('A', 'Y')] = 6;
        pointsLookup[GetPairKey('B', 'Z')] = 6;
        pointsLookup[GetPairKey('C', 'X')] = 6;

        // Create the result lookup array
        // X = lose
        // Y = draw
        // Z = loss
        var resultLookup = new char[GetPairKey('C', 'Z') + 1];
        resultLookup[GetPairKey('A', 'X')] = 'Z';
        resultLookup[GetPairKey('A', 'Y')] = 'X';
        resultLookup[GetPairKey('A', 'Z')] = 'Y';
        resultLookup[GetPairKey('B', 'X')] = 'X';
        resultLookup[GetPairKey('B', 'Y')] = 'Y';
        resultLookup[GetPairKey('B', 'Z')] = 'Z';
        resultLookup[GetPairKey('C', 'X')] = 'Y';
        resultLookup[GetPairKey('C', 'Y')] = 'Z';
        resultLookup[GetPairKey('C', 'Z')] = 'X';

        // Iterate over the lines, summing the points
        long pointsSum = 0;
        var inputLines = Input.Lines;
        for (int i = 0; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            var selectionOpponent = line[0];
            var desiredResult = line[2];

            // Get the correct selection for the desire result
            var selectionMine = resultLookup[GetPairKey(selectionOpponent, desiredResult)];

            // Add the points for win or draw
            pointsSum += pointsLookup[GetPairKey(selectionOpponent, selectionMine)];

            // Add the points for my selection (x = 1, y = 2, z = 3)
            pointsSum += selectionMine - 'W';
        }

        return pointsSum.ToString();
    }
}
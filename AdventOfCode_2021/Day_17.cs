﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Day_17
    {
        public static void PartOneAndTwo()
        {
            var data = Data;

            // Use a regex to extract the values
            var regex = new Regex(@"x=(-?\d+)..(-?\d+).*y=(-?\d+)..(-?\d+)");
            var match = regex.Match(data);
            if (!match.Success)
            {
                throw new Exception($"No regex match on data: {data}");
            }

            // Get the x and y regions
            var xStart = int.Parse(match.Groups[1].Value);
            var xEnd   = int.Parse(match.Groups[2].Value);
            var yStart = int.Parse(match.Groups[3].Value);
            var yEnd   = int.Parse(match.Groups[4].Value);

            // Ensure start is less than end
            if (xStart > xEnd)
            {
                Helpers.Swap(ref xStart, ref xEnd);
            }
            if (yStart > yEnd)
            {
                Helpers.Swap(ref yStart, ref yEnd);
            }

            // Get the x velocities that will land us in the correct region
            var xStartingVelocitiesInsideBounds = new List<(int xStartingVelocity, int stepsRequired, bool inMotion)>(256);
            for (int x = 0; x <= xEnd; x++)
            {
                var xPosition = 0;
                var xVelocity = x;

                // Step until we've passed the xEnd or out velocity is 0
                var steps = 1;
                do
                {
                    // Move the position and decrement the velocity
                    xPosition += xVelocity;
                    xVelocity--;

                    // Check whether we're in bounds
                    if (xPosition >= xStart & xPosition <= xEnd)
                    {
                        xStartingVelocitiesInsideBounds.Add((x, steps, xVelocity > 0));
                    }

                    // Increment the step
                    steps++;
                }
                while (xPosition <= xEnd & xVelocity > 0);
            }

            // For each starting x velocity and number of steps find the highest valid starting y velocity
            var validStartingVelocities = new List<(int xStartingVelocity, int yStartingVelocity, int yPositionMaximum)>(256);
            for (int i = 0; i < xStartingVelocitiesInsideBounds.Count; i++)
            {
                var item = xStartingVelocitiesInsideBounds[i];
                var xStartingVelocity = item.xStartingVelocity;
                var stepsMinimum = item.stepsRequired;
                var stepsMaximum = item.inMotion ? stepsMinimum : int.MaxValue;

                // Test each starting y velocity, starting at the highest possible (y start)
                var yStartingVelocity = Math.Abs(yStart);
                while (yStartingVelocity >= yStart)
                {
                    var yPosition = 0;
                    var yVelocity = yStartingVelocity;
                    var yPositionMaximum = int.MinValue;

                    // Simulate until we've gone past the y start
                    var steps = 1;
                    do
                    {
                        // Move the position and decrement the velocity
                        yPosition += yVelocity;
                        yVelocity--;

                        // Set the highest y position so far
                        if (yPosition > yPositionMaximum)
                        {
                            yPositionMaximum = yPosition;
                        }

                        // Check whether we're in bounds and have passed the required number of steps
                        if (yPosition >= yStart & yPosition <= yEnd & 
                            steps >= stepsMinimum & steps <= stepsMaximum)
                        {
                            validStartingVelocities.Add((xStartingVelocity, yStartingVelocity, yPositionMaximum));
                            break; 
                        }

                        // Increment the step
                        steps++;
                    }
                    while (yPosition >= yStart);

                    // Decrement the starting velocity
                    yStartingVelocity--;
                }
            }

            var startingVelocityWithHighestYPosition = validStartingVelocities.MaxBy((sv) => sv.yPositionMaximum);
            Console.WriteLine(startingVelocityWithHighestYPosition);
            Console.WriteLine(validStartingVelocities.DistinctBy((sv) => (sv.xStartingVelocity, sv.yStartingVelocity)).Count());
        }

        private static readonly string DataTest = @"target area: x=20..30, y=-10..-5";

        private static readonly string Data = "@target area: x=155..182, y=-117..-67";
    }
}

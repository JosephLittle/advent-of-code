﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(13)]
[SolutionInput("Inputs/Day_13_Test.txt", Problem1Solution = "405", Problem2Solution = "400")]
[SolutionInput("Inputs/Day_13.txt", Problem1Solution = "33735", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_13 : Solution
{
    public Day_13(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        ulong summarySum = 0;

        // Process blocks of patterns
        var startLineIndex = 0;
        for (int i = 0; i < Input.Lines.Length; i++)
        {
            if (Input.Lines[i].Length == 0)
            {
                // Process this pattern block
                summarySum += GetPatternSummary(startLineIndex, i, false);

                // Set the next start index
                startLineIndex = ++i;
            }
        }

        // Process the final pattern block
        summarySum += GetPatternSummary(startLineIndex, Input.Lines.Length - 1, false);

        return summarySum.ToString();
    }

    protected override string? Problem2()
    {
        ulong summarySum = 0;

        // Process blocks of patterns
        var startLineIndex = 0;
        for (int i = 0; i < Input.Lines.Length; i++)
        {
            if (Input.Lines[i].Length == 0)
            {
                // Process this pattern block
                summarySum += GetPatternSummary(startLineIndex, i, true);

                // Set the next start index
                startLineIndex = ++i;
            }
        }

        // Process the final pattern block
        summarySum += GetPatternSummary(startLineIndex, Input.Lines.Length - 1, true);

        return summarySum.ToString();
    }

    private ulong GetPatternSummary(int startLineIndex, int endLineIndex, bool checkSmudge)
    {
        // First check the rows
        var rows = Input.Lines[startLineIndex..endLineIndex];
        if (CheckForMirror(rows, out ulong patternSummary, checkSmudge))
        {
            // Found mirror in rows
            return patternSummary * 100;
        }

        // No mirror in rows, so build up columns
        var columns = new string[rows[0].Length];
        for (int c = 0; c < columns.Length; c++)
        {
            for (int r = 0; r < rows.Length; r++)
            {
                columns[c] += rows[r][c];
            }
        }
        
        // Check for columns
        if (CheckForMirror(columns, out patternSummary, checkSmudge))
        {
            // Found mirror in columns
            return patternSummary;
        }
        else
        {
            throw new Exception("No mirror found!");
        }

        // Local method to check for a mirror
        static bool CheckForMirror(string[] lines, out ulong patternSummary, bool checkSmudge)
        {
            // Check pairs of lines
            for (int lineIndex = 1; lineIndex < lines.Length; lineIndex++)
            {
                var leftIndex = lineIndex - 1;
                var rightIndex = lineIndex;

                var hasRemovedSmudge = false;

                // Look outwards from the left and right lines
                var isMirror = true;
                while (isMirror & leftIndex >= 0 & rightIndex < lines.Length)
                {
                    var left = lines[leftIndex];
                    var right = lines[rightIndex];
                    isMirror = left == right;

                    // Check if we can attempt to remove a smudge
                    if (!isMirror & checkSmudge & !hasRemovedSmudge)
                    {
                        // Count the different characters
                        var differenceCount = 0;
                        for (int i = 0; i < left.Length; i++)
                        {
                            differenceCount += Helpers.BoolToInt(!(left[i] == right[i]));
                        }

                        // If they're different by only 1 character, then allow it
                        isMirror = differenceCount == 1;
                        hasRemovedSmudge = true;
                    }

                    leftIndex--;
                    rightIndex++;
                }

                // Return if a mirror was found
                var hasCheckedSmudgeIfRequired = !checkSmudge | hasRemovedSmudge;
                if (isMirror & hasCheckedSmudgeIfRequired)
                {
                    patternSummary = (ulong)lineIndex;
                    return true;
                }
            }

            patternSummary = 0;
            return false;
        }
    }
}

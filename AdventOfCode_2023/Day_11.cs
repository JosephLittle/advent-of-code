﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2023;

[Solution(11)]
[SolutionInput("Inputs/Day_11_Test.txt", Problem1Solution = "374", Problem2Solution = "82000210")]
[SolutionInput("Inputs/Day_11.txt", Problem1Solution = "9536038", Problem2Solution = "447744640566", Benchmark = true)]
public unsafe class Day_11 : Solution
{
    private readonly record struct Coordinates(byte X, byte Y);

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct WorldCell
    {
        public byte X;
        public byte Y;

        public ulong ShortestPath;
        public ulong Cost;

        public bool IsVisited;
        public bool HasBeenAddedToUnvisitedSet;
    }

    public Day_11(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        return Solution(Input, 2).ToString();
    }

    protected override string? Problem2()
    {
        return Solution(Input, 1_000_000).ToString();
    }

    private ulong Solution(Input input, ulong emptyLineCost)
    {
        const byte CostDefault = 1;

        // Create the world map
        var height = Input.Lines.Length;
        var width = Input.Lines[0].Length;
        WorldCell* worldMap = stackalloc WorldCell[width * height];
        WorldCell* GetWorldCell(int x, int y) => worldMap + (width * y) + x;
        bool TryGetWorldCell(int x, int y, out WorldCell* worldCell)
        {
            if (x >= 0 & x < width &
                y >= 0 & y < height)
            {
                worldCell = GetWorldCell(x, y);
                return true;
            }
            else
            {
                worldCell = null;
                return false;
            }
        }

        // Gather the galaxies
        var galaxyCoordinates = new List<Coordinates>(32);
        for (int y = 0; y < height; y++)
        {
            var line = Input.Lines[y];
            var galaxyFound = false;
            for (int x = 0; x < width; x++)
            {
                if (line[x] == '#')
                {
                    galaxyCoordinates.Add(new Coordinates((byte)x, (byte)y));
                    galaxyFound = true;
                }

                // Set this cell
                *GetWorldCell(x, y) = new WorldCell()
                {
                    X = (byte)x,
                    Y = (byte)y,
                    Cost = CostDefault,
                    ShortestPath = ulong.MaxValue
                };
            }

            // Set the cost of this row
            if (!galaxyFound)
            {
                for (int x = 0; x < line.Length; x++)
                {
                    GetWorldCell(x, y)->Cost = emptyLineCost;
                }
            }
        }

        // Check the columns for galaxies
        for (int x = 0; x < width; x++)
        {
            var galaxyFound = false;
            for (int y = 0; y < height & !galaxyFound; y++)
            {
                galaxyFound = Input.Lines[y][x] == '#';
            }

            // Set the cost of this row
            if (!galaxyFound)
            {
                for (int y = 0; y < height & !galaxyFound; y++)
                {
                    GetWorldCell(x, y)->Cost = emptyLineCost;
                }
            }
        }

        // Create a clone world map to use to reset our working one
        Span<WorldCell> worldMapClone = stackalloc WorldCell[width * height];
        var worldMapSpan = new Span<WorldCell>(worldMap, width * height);
        worldMapSpan.CopyTo(worldMapClone);

        // Perform dijkstra for each galaxy
        ulong shortestPathsSum = 0;
        var unvisitedCells = new List<IntPtr>(64);
        for (int galaxyIndex = 0; galaxyIndex < galaxyCoordinates.Count - 1; galaxyIndex++)
        {
            var startCoordinates = galaxyCoordinates[galaxyIndex];

            // Reset state
            unvisitedCells.Clear();
            worldMapClone.CopyTo(worldMapSpan);

            // Start with galaxy coordinates
            var startCell = GetWorldCell(startCoordinates.X, startCoordinates.Y);
            unvisitedCells.Add((IntPtr)startCell);
            startCell->HasBeenAddedToUnvisitedSet = true;
            startCell->ShortestPath = 0;

            // Perform dijkstra
            while (unvisitedCells.Count > 0)
            {
                // Get the next cell to check with the shortest path
                var currentCellIndex = 0;
                var currentCell = (WorldCell*)unvisitedCells[0];
                for (int candidateCellIndex = 1; candidateCellIndex < unvisitedCells.Count; candidateCellIndex++)
                {
                    var candidateCell = (WorldCell*)unvisitedCells[candidateCellIndex];
                    if (candidateCell->ShortestPath < currentCell->ShortestPath)
                    {
                        currentCellIndex = candidateCellIndex;
                        currentCell = candidateCell;
                    }
                }
                unvisitedCells.RemoveAt(currentCellIndex);

                // Mark this cell as visited
                currentCell->IsVisited = true;

                // Process each neighbour
                WorldCell* cell;
                if (TryGetWorldCell(currentCell->X + 1, currentCell->Y, out cell))
                {
                    ProcessNeighbour(cell);
                }
                if (TryGetWorldCell(currentCell->X - 1, currentCell->Y, out cell))
                {
                    ProcessNeighbour(cell);
                }
                if (TryGetWorldCell(currentCell->X, currentCell->Y + 1, out cell))
                {
                    ProcessNeighbour(cell);
                }
                if (TryGetWorldCell(currentCell->X, currentCell->Y - 1, out cell))
                {
                    ProcessNeighbour(cell);
                }

                // Local method to process each neighbour
                void ProcessNeighbour(WorldCell* neighbourCell)
                {
                    // Ignore if we've already visited it
                    if (neighbourCell->IsVisited)
                    {
                        return;
                    }

                    // Get the length of the path to this cell from our previous
                    var pathLength = (currentCell->ShortestPath + neighbourCell->Cost);

                    // If the new path length is shorter then update it
                    if (neighbourCell->ShortestPath > pathLength)
                    {
                        neighbourCell->ShortestPath = pathLength;
                    }

                    // If it's not already in the unvisited list then add it now
                    if (!neighbourCell->HasBeenAddedToUnvisitedSet)
                    {
                        unvisitedCells.Add((IntPtr)neighbourCell);
                        neighbourCell->HasBeenAddedToUnvisitedSet = true;
                    }
                }
            }

            // Sum the shortest paths from this galaxy onwards
            for (int otherGalaxyIndex = galaxyIndex + 1; otherGalaxyIndex < galaxyCoordinates.Count; otherGalaxyIndex++)
            {
                var otherGalaxy = galaxyCoordinates[otherGalaxyIndex];

                var shortestPath = GetWorldCell(otherGalaxy.X, otherGalaxy.Y)->ShortestPath;

                shortestPathsSum += shortestPath;
            }
        }

        return shortestPathsSum;
    }
}

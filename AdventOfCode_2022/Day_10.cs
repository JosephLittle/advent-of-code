﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(10)]
[SolutionInput("Inputs/Day_10_Test.txt", Problem1Solution = "13140", Problem2Solution =
    "##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....")]
[SolutionInput("Inputs/Day_10.txt", Problem1Solution = "14160", Problem2Solution =
    "###....##.####.###..###..####.####..##..\n#..#....#.#....#..#.#..#.#....#....#..#.\n#..#....#.###..#..#.#..#.###..###..#....\n###.....#.#....###..###..#....#....#....\n#.#..#..#.#....#.#..#....#....#....#..#.\n#..#..##..####.#..#.#....####.#.....##..", Benchmark = true)]
public unsafe class Day_10 : Solution
{
    public Day_10(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        var cycleToXValue = stackalloc short[240];
        var cycleCounter = 0;
        short currentXValue = 1;

        var indexOfCycleToSetValuesFrom = 0;

        // Process each instruction
        for (int i = 0; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            var command = line[0];
            if (command == 'n')
            {
                // No-op instruction, so just increment cycle
                cycleCounter += 1;
            }
            else
            {
                // Add instruction, so increment cycles by 2
                cycleCounter += 2;

                // Set the value for the cycles that haven't been set yet
                var spanToSet = new Span<short>(
                    cycleToXValue + indexOfCycleToSetValuesFrom,
                    cycleCounter - indexOfCycleToSetValuesFrom);
                spanToSet.Fill(currentXValue);

                // Parse and add the value
                var value = Helpers.SubstringToSignedInt(line, 5, line.Length - 5);
                currentXValue += (short)value;

                // Set the new index to set values from
                indexOfCycleToSetValuesFrom = cycleCounter;
            }
        }

        // Set the value for the final cycles that havn't been set yet
        var finalSpanToSet = new Span<short>(
            cycleToXValue + indexOfCycleToSetValuesFrom,
            cycleCounter - indexOfCycleToSetValuesFrom);
        finalSpanToSet.Fill(currentXValue);

        // Return the sum
        return ((cycleToXValue[19] * 20) +
            (cycleToXValue[59] * 60) +
            (cycleToXValue[99] * 100) +
            (cycleToXValue[139] * 140) +
            (cycleToXValue[179] * 180) +
            (cycleToXValue[219] * 220)).ToString();
    }

    protected override string? Problem2()
    {
        const int ScreenWidth = 40;

        var inputLines = Input.Lines;

        // Build up the output in a screen array, using 240 elements for the pixels
        // and 5 for the new line characters. This will be directly converted into
        // a string for the result
        var outputBuffer = stackalloc char[240 + 5];
        outputBuffer[40] = '\n';
        outputBuffer[81] = '\n';
        outputBuffer[122] = '\n';
        outputBuffer[163] = '\n';
        outputBuffer[204] = '\n';

        var cycleCounter = 0;
        short spriteStartIndex = 0;

        // Process each instruction
        for (int i = 0; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            var command = line[0];
            if (command == 'n')
            {
                // No-op instruction, so just increment cycle
                SetPixelAndIncrementCycle();
            }
            else
            {
                // Add instruction, so increment cycles by 2
                SetPixelAndIncrementCycle();
                SetPixelAndIncrementCycle();

                // Parse and add the value
                var value = Helpers.SubstringToSignedInt(line, 5, line.Length - 5);
                spriteStartIndex += (short)value;
            }
        }

        // Local method to set the current pixel and then increment the cycle
        void SetPixelAndIncrementCycle()
        {
            // Get the indices of the pixel we're drawing
            var pixelIndexInArray = cycleCounter + (cycleCounter / ScreenWidth);
            var pixelIndexInRow = cycleCounter % ScreenWidth;

            // Get whether the pixel is within the sprite's lit pixels
            var isPixelWithinSprite =
                pixelIndexInRow >= spriteStartIndex &
                pixelIndexInRow <= spriteStartIndex + 2;

            // Set the pixel in the output
            outputBuffer[pixelIndexInArray] = isPixelWithinSprite ? '#' : '.';

            // Increment the cycle
            cycleCounter++;
        }

        // Create the output string from the output buffer
        return new string(outputBuffer, 0, 245);
    }
}

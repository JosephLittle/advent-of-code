﻿namespace AdventOfCode_2023;

public unsafe static class Helpers
{
    /// <summary>
    /// Swap the values at the given pointers only if the condition is true without branching.
    /// </summary>
    public static void SwapValuesWithoutBranching(long* a, long* b, bool condition)
    {
        var aValue = *a;
        var bValue = *b;
        var conditionMultiplier = BoolToInt(condition);

        aValue = aValue ^ (bValue * conditionMultiplier);
        bValue = (aValue * conditionMultiplier) ^ bValue;
        aValue = aValue ^ (bValue * conditionMultiplier);

        *a = aValue;
        *b = bValue;
    }

    /// <summary>
    /// Set the value at the destination to the given value based on the condition without branching.
    /// </summary>
    public static void SetValueWithoutBranching<T>(T* destination, T value, bool condition) 
        where T : unmanaged
    {
        var valueStore = stackalloc T[2];
        valueStore[1] = *destination;

        valueStore[BoolToInt(condition)] = value;

        *destination = valueStore[1];
    }

    /// <summary>
    /// Get the integer value of the given char.
    /// </summary>
    public static int CharToInt(char c)
    {
        return c - 48;
    }

    /// <summary>
    /// Get the integer value of the ascii number at the given substring.
    /// </summary>
    public static int SubstringToUnsignedInt(string s, int indexStart, int characterCount)
    {
        var result = CharToInt(s[indexStart]);
        for (int i = indexStart + 1, end = indexStart + characterCount; i < end; i++)
        {
            result *= 10;
            result += CharToInt(s[i]);
        }
        return result;
    }

    /// <summary>
    /// Get the integer value of the ascii number at the given substring.
    /// </summary>
    public static int SubstringToSignedInt(string s, int indexStart, int characterCount)
    {
        var indexEnd = indexStart + characterCount;

        var firstCharacter = s[indexStart];
        var signMultiplier = 1;
        if (firstCharacter == '-' |
            firstCharacter == '+')
        {
            signMultiplier = (-firstCharacter) + 44;
            firstCharacter = s[++indexStart];
        }

        var result = CharToInt(firstCharacter);
        for (int i = indexStart + 1; i < indexEnd; i++)
        {
            result *= 10;
            result += CharToInt(s[i]);
        }

        return result * signMultiplier;
    }

    /// <summary>
    /// Get the integer value of the given bool, 1 for true, and 0 for false.
    /// </summary>
    public static int BoolToInt(bool b)
    {
        return *(byte*)&b;
    }

    /// <summary>
    /// Parse the integer starting at the given index until we reach the given delimiter.
    /// </summary>
    public static ulong ParseIntegerUntilDelimiter(string line, int charIndex, char delimiter)
    {
        var currentChar = line[charIndex];
        var integer = (ulong)(currentChar - '0');

        while ((currentChar = line[++charIndex]) != delimiter)
        {
            integer *= 10;
            integer += (ulong)(currentChar - '0');
        }

        return integer;
    }

    /// <summary>
    /// Parse the integer starting at the given index until we reach the given delimiter or end of string.
    /// </summary>
    public static ulong ParseUnsignedIntegerUntilDelimiterOrEnd(string line, ref int charIndex, char delimiter)
    {
        var currentChar = line[charIndex];
        var integer = (ulong)(currentChar - '0');

        while (
            ++charIndex < line.Length &&
            (currentChar = line[charIndex]) != delimiter)
        {
            integer *= 10;
            integer += (ulong)(currentChar - '0');
        }

        return integer;
    }

    /// <summary>
    /// Parse the integer starting at the given index until we reach the given delimiter or end of string.
    /// </summary>
    public static long ParseSignedIntegerUntilDelimiterOrEnd(string line, ref int charIndex, char delimiter)
    {
        var firstCharacter = line[charIndex];
        var signMultiplier = 1;
        if (firstCharacter == '-' |
            firstCharacter == '+')
        {
            signMultiplier = (-firstCharacter) + 44;
            firstCharacter = line[++charIndex];
        }

        var currentChar = firstCharacter;
        var integer = (currentChar - '0');

        while (
            ++charIndex < line.Length &&
            (currentChar = line[charIndex]) != delimiter)
        {
            integer *= 10;
            integer += (currentChar - '0');
        }

        return integer * signMultiplier;
    }

    /// <summary>
    /// Swap method for pointer values.
    /// </summary>
    public static void SwapValues<T>(T* a, T* b)
        where T : unmanaged
    {
        var temp = *a;
        *a = *b;
        *b = temp;
    }

    /// <summary>
    /// Get the least common multiple of the two integers. Taken from:
    /// https://stackoverflow.com/questions/13569810/least-common-multiple
    /// </summary>
    public static int LeastCommonMultiple(int a, int b)
    {
        var a2 = a;
        var b2 = b;
        while (b2 != 0)
        {
            int temp = b2;
            b2 = a2 % b2;
            a2 = temp;
        }
        return (a / a2) * b;
    }

    /// <summary>
    /// Get the least common multiple of the two ulongs. Taken from:
    /// https://stackoverflow.com/questions/13569810/least-common-multiple
    /// </summary>
    public static ulong LeastCommonMultiple(ulong a, ulong b)
    {
        var a2 = a;
        var b2 = b;
        while (b2 != 0)
        {
            ulong temp = b2;
            b2 = a2 % b2;
            a2 = temp;
        }
        return (a / a2) * b;
    }

    /// <summary>
    /// Simple method for getting the number of digits required for an unsigned integer.
    /// </summary>
    public static int GetNumberOfDigitsSimple(uint a)
    {
        if (a < 10)
        {
            return 1;
        }
        else if (a < 100)
        {
            return 2;
        }
        else if (a < 1_000)
        {
            return 3;
        }
        else if (a < 10_000)
        {
            return 4;
        }
        else if (a < 100_000)
        {
            return 5;
        }
        else if (a < 1_000_000)
        {
            return 6;
        }
        else if (a < 10_000_000)
        {
            return 7;
        }
        else if (a < 100_000_000)
        {
            return 8;
        }
        else if (a < 1_000_000_000)
        {
            return 9;
        }
        else
        {
            return 10;
        }
    }
}
﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2022;

[Solution(9)]
[SolutionInput("Inputs/Day_09_Test1.txt", Problem1Solution = "13", Problem2Solution = "1")]
[SolutionInput("Inputs/Day_09_Test2.txt", Problem1Solution = "88", Problem2Solution = "36")]
[SolutionInput("Inputs/Day_09.txt", Problem1Solution = "6314", Problem2Solution = "2504", Benchmark = true)]
public unsafe class Day_09 : Solution
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct Position
    {
        public int X;
        public int Y;
    }

    public Day_09(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        return Solve(2);
    }

    protected override string? Problem2()
    {
        return Solve(10);
    }

    private string? Solve(int numberOfKnots)
    {
        var inputLines = Input.Lines;

        // Create a grid for the expected size of the world, this will track where we've been
        // TODO: Attempt to stack alloc this
        var expectedWorldSideLength = 1000;
        var worldGrid = new bool[expectedWorldSideLength][];
        for (int i = 0; i < expectedWorldSideLength; i++)
        {
            worldGrid[i] = new bool[expectedWorldSideLength];
        }

        // Start the positions in the middle of the world
        var knotPositions = stackalloc Position[numberOfKnots];
        for (int i = 0; i < numberOfKnots; i++)
        {
            knotPositions[i] = new Position
            {
                X = expectedWorldSideLength / 2,
                Y = expectedWorldSideLength / 2
            };
        }

        var headKnot = knotPositions;
        var tailKnot = knotPositions + numberOfKnots - 1;
        var countOfPositionsVisitedAtLeastOnce = 1;

        // Iterate over each movement command
        for (int i = 0; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            var command = line[0];
            var movementDistance = Helpers.SubstringToUnsignedInt(line, 2, line.Length - 2);

            // Move one step at a time
            while (--movementDistance >= 0)
            {
                // Move the head
                switch (command)
                {
                    case 'R':
                        headKnot->X += 1;
                        break;
                    case 'L':
                        headKnot->X -= 1;
                        break;
                    case 'U':
                        headKnot->Y += 1;
                        break;
                    case 'D':
                        headKnot->Y -= 1;
                        break;
                }

                // Try to move each knot towards the knot in front, starting with the first
                // knot behind the head
                bool tailMoved;
                var currentTail = knotPositions + 1;
                var guardian = knotPositions + numberOfKnots;
                do
                {
                    var currentHead = currentTail - 1;

                    // Get the distance from the head to the tail
                    var xDifference = currentHead->X - currentTail->X;
                    var yDifference = currentHead->Y - currentTail->Y;

                    // If either of these differences are greater than 1 then
                    // the tail needs to move to catch up
                    if (xDifference > 1 | xDifference < -1 |
                        yDifference > 1 | yDifference < -1)
                    {
                        // If there is any difference between the tail and head on either axis
                        // then we want to move 1 in that direction
                        currentTail->X += Math.Sign(xDifference);
                        currentTail->Y += Math.Sign(yDifference);

                        tailMoved = true;
                    }
                    else
                    {
                        tailMoved = false;
                    }
                }
                while (tailMoved & ++currentTail < guardian);

                // If we moved the last knot, then update the positions count
                if (tailMoved & currentTail == guardian)
                {
                    // Set this as a visited place
                    var wasVisited = worldGrid[tailKnot->X][tailKnot->Y];
                    worldGrid[tailKnot->X][tailKnot->Y] = true;

                    // Count the uniquely visited places
                    countOfPositionsVisitedAtLeastOnce += Helpers.BoolToInt(!wasVisited);
                }
            }
        }

        return countOfPositionsVisitedAtLeastOnce.ToString();
    }
}

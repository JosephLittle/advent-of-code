﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(14)]
[SolutionInput("Inputs/Day_14_Test.txt", Problem1Solution = "24", Problem2Solution = "93")]
[SolutionInput("Inputs/Day_14.txt", Problem1Solution = "715", Problem2Solution = "25248", Benchmark = true)]
public unsafe class Day_14 : Solution
{
    public const byte IsOccupied = 1;
    public const byte IsBottom = 2;

    public Day_14(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Set up the grid from unmanaged memory
        var gridWidth = 180;
        var gridHeight = 200;
        var sizeOfGrid = (gridWidth * gridHeight) * sizeof(byte);
        var gridMemory = stackalloc byte[sizeOfGrid];
        var gridRows = stackalloc byte*[gridHeight];
        for (int y = 0; y < gridHeight; y++)
        {
            gridRows[y] = gridMemory + (gridWidth * y);
        }

        // Use a stack to keep track of the previous positions of the sand so that we can start
        // the next grain at the previous free position
        var previousPositionsStack = stackalloc byte*[gridHeight];
        var previousPositionsStackCount = 0;

        // Actual coordinates can range from roughly X 400 -> 600 and Y 0 -> 200
        // so modify the X coordinates when we read them in to fit our grid
        var xModifier = -(500 - (gridWidth / 2));
        var xEntryPoint = 500 + xModifier;

        // Track the lowest point that we've set a wall at so we know that once a grain
        // of sand goes past that then it will fall forever
        var lowestY = int.MinValue;

        // Read the input lines
        for (int i = 0; i < inputLines.Length; i++)
        {
            var inputLine = inputLines[i];

            var characterIndex = 0;

            // Parse the first coordinate pair
            var xFrom = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ',');
            characterIndex++;
            var yFrom = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ' ');

            // Modify the x
            xFrom += xModifier;

            // Parse subsequent coordinate pairs
            while (characterIndex < inputLine.Length)
            {
                // Skip forward 4 until the next pair
                characterIndex += 4;

                // Parse the next coordinate pair
                var xTo = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ',');
                characterIndex++;
                var yTo = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ' ');

                // Modify the x
                xTo += xModifier;

                // Apply this line
                if (xFrom != xTo)
                {
                    // Horizontal line
                    var gridRow = gridRows[yFrom];
                    var step = Math.Sign(xTo - xFrom);
                    for (int x = xFrom; x != (xTo + step); x += step)
                    {
                        gridRow[x] = IsOccupied;
                    }
                }
                else
                {
                    // Vertical line
                    var step = Math.Sign(yTo - yFrom);
                    for (int y = yFrom; y != (yTo + step); y += step)
                    {
                        gridRows[y][xFrom] = IsOccupied;
                    }
                }

                // Check for lowest y
                lowestY = yFrom > lowestY ? yFrom : lowestY;

                // Prepare for next coordinates
                xFrom = xTo;
                yFrom = yTo;
            }

            // Check for lowest y
            lowestY = yFrom > lowestY ? yFrom : lowestY;
        }

        // Set the lowest y with the bottom value
        new Span<byte>(gridRows[lowestY + 1], gridWidth).Fill(IsBottom);

        // Add the grains of sand until we add one which will fall forever
        var start = gridRows[0] + xEntryPoint;
        var current = start;
        var hasReachedBottom = false;
        var numberOfGrainsOfSandThatHaveComeToRest = 0;
        while (!hasReachedBottom)
        {
            // Loop while the sand falls
            while (!hasReachedBottom)
            {
                // Check directly below
                var currentBelow = current + gridWidth;
                if (*currentBelow != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelow;

                    // Check whether we've reached the bottom
                    hasReachedBottom = (*currentBelow == IsBottom);
                    continue;
                }

                // Check below and to the left
                var currentBelowLeft = currentBelow - 1;
                if (*currentBelowLeft != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelowLeft;
                    continue;
                }

                // Check below and to the right
                var currentBelowRight = currentBelow + 1;
                if (*currentBelowRight != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelowRight;
                    continue;
                }

                // No free spaces so the sand somes to a stop
                *current = IsOccupied;
                numberOfGrainsOfSandThatHaveComeToRest++;

                // Start the next grain of sand at the previous position
                var poppedPrevious = previousPositionsStack[--previousPositionsStackCount];
                current = poppedPrevious;

                // Break out of this loop since this grain is no longer falling
                break;
            }
        }

        //// Print the grid
        //for (int y = 0; y < gridHeight; y++)
        //{
        //    for (int x = 0; x < gridWidth; x++)
        //    {
        //        if (gridRows[y][x] == IsOccupied)
        //        {
        //            Console.Write('%');
        //        }
        //        else
        //        {
        //            Console.Write('.');
        //        }
        //    }
        //    Console.WriteLine();
        //}

        return numberOfGrainsOfSandThatHaveComeToRest.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        // Set up the grid from unmanaged memory
        var gridWidth = 120;
        var gridHeight = 200;
        var sizeOfGrid = (gridWidth * gridHeight) * sizeof(byte);
        var gridMemory = stackalloc byte[sizeOfGrid];
        var gridRows = stackalloc byte*[gridHeight];
        for (int y = 0; y < gridHeight; y++)
        {
            gridRows[y] = gridMemory + (gridWidth * y);
        }

        // Use a stack to keep track of the previous positions of the sand so that we can start
        // the next grain at the previous free position
        var previousPositionsStack = stackalloc byte*[gridHeight];
        var previousPositionsStackCount = 0;

        // Actual coordinates can range from roughly X 400 -> 600 and Y 0 -> 200
        // so modify the X coordinates when we read them in to fit our grid
        var xModifier = -(500 - (gridWidth / 2));
        var xEntryPoint = 500 + xModifier;

        // Track the lowest point that we've set a wall at so we know that once a grain
        // of sand goes past that then it will fall forever
        var lowestY = int.MinValue;
        var leftmostX = int.MaxValue;
        var rightmostX = int.MinValue;

        // Read the input lines
        for (int i = 0; i < inputLines.Length; i++)
        {
            var inputLine = inputLines[i];

            var characterIndex = 0;

            // Parse the first coordinate pair
            var xFrom = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ',');
            characterIndex++;
            var yFrom = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ' ');

            // Modify the x
            xFrom += xModifier;

            // Parse subsequent coordinate pairs
            while (characterIndex < inputLine.Length)
            {
                // Skip forward 4 until the next pair
                characterIndex += 4;

                // Parse the next coordinate pair
                var xTo = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ',');
                characterIndex++;
                var yTo = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref characterIndex, ' ');

                // Modify the x
                xTo += xModifier;

                // Apply this line
                if (xFrom != xTo)
                {
                    // Horizontal line
                    var gridRow = gridRows[yFrom];
                    var step = Math.Sign(xTo - xFrom);
                    for (int x = xFrom; x != (xTo + step); x += step)
                    {
                        gridRow[x] = IsOccupied;
                    }
                }
                else
                {
                    // Vertical line
                    var step = Math.Sign(yTo - yFrom);
                    for (int y = yFrom; y != (yTo + step); y += step)
                    {
                        gridRows[y][xFrom] = IsOccupied;
                    }
                }

                // Check for bounds
                lowestY = yFrom > lowestY ? yFrom : lowestY;
                leftmostX = xFrom < leftmostX ? xFrom : leftmostX;
                rightmostX = xFrom > rightmostX ? xFrom : rightmostX;

                // Prepare for next coordinates
                xFrom = xTo;
                yFrom = yTo;
            }

            // Check for lowest y
            lowestY = yFrom > lowestY ? yFrom : lowestY;
            leftmostX = xFrom < leftmostX ? xFrom : leftmostX;
            rightmostX = xFrom > rightmostX ? xFrom : rightmostX;
        }

        // Set the lowest y + 2 plane as solid floor
        var floorY = lowestY + 2;
        new Span<byte>(gridRows[floorY], gridWidth).Fill(IsOccupied);

        // Set walls at either side of the X boundaries
        var leftWallX = leftmostX - 2;
        var rightWallX = rightmostX + 2;
        for (int y = 0; y < floorY; y++)
        {
            var gridRow = gridRows[y];
            gridRow[leftWallX] = IsOccupied;
            gridRow[rightWallX] = IsOccupied;
        }

        // Add the grains of sand until we add one which will fall forever
        var start = gridRows[0] + xEntryPoint;
        var current = start;
        var hasSandReachedStart = false;
        var numberOfGrainsOfSandThatHaveComeToRest = 0;
        previousPositionsStack[previousPositionsStackCount++] = start;
        while (!hasSandReachedStart)
        {
            // Loop while the sand falls
            while (!hasSandReachedStart)
            {
                // Check directly below
                var currentBelow = current + gridWidth;
                if (*currentBelow != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelow;
                    continue;
                }

                // Check below and to the left
                var currentBelowLeft = currentBelow - 1;
                if (*currentBelowLeft != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelowLeft;
                    continue;
                }

                // Check below and to the right
                var currentBelowRight = currentBelow + 1;
                if (*currentBelowRight != IsOccupied)
                {
                    previousPositionsStack[previousPositionsStackCount++] = current;
                    current = currentBelowRight;
                    continue;
                }

                // No free spaces so the sand somes to a stop
                *current = IsOccupied;
                numberOfGrainsOfSandThatHaveComeToRest++;

                // Check whether the sand has reached the start
                hasSandReachedStart = current == start;

                // Start the next grain of sand at the previous position
                var poppedPrevious = previousPositionsStack[--previousPositionsStackCount];
                current = poppedPrevious;

                // Break out of this loop since this grain is no longer falling
                break;
            }
        }

        // Find how high the sand has reached on either wall
        var leftmostHighestPoint = 0;
        var rightmostHighestPoint = 0;
        for (int y = 0; y < floorY; y++)
        {
            var gridRow = gridRows[y];

            if (gridRow[leftWallX + 1] == IsOccupied &
                leftmostHighestPoint == 0)
            {
                leftmostHighestPoint = y;
            }
            if (gridRow[rightWallX - 1] == IsOccupied &
                rightmostHighestPoint == 0)
            {
                rightmostHighestPoint = y;
            }
            if (leftmostHighestPoint != 0 &
                rightmostHighestPoint != 0)
            {
                break;
            }
        }

        // Use these high points to calculate how much sand would have piled up on either side
        leftmostHighestPoint++;
        var heightOfSand = floorY - leftmostHighestPoint;
        numberOfGrainsOfSandThatHaveComeToRest +=
            (heightOfSand * (heightOfSand + 1)) / 2;
        rightmostHighestPoint++;
        heightOfSand = floorY - rightmostHighestPoint;
        numberOfGrainsOfSandThatHaveComeToRest +=
            (heightOfSand * (heightOfSand + 1)) / 2;

        //// Print the grid
        //for (int y = 0; y < gridHeight; y++)
        //{
        //    for (int x = 0; x < gridWidth; x++)
        //    {
        //        if (gridRows[y][x] == IsOccupied)
        //        {
        //            Console.Write('%');
        //        }
        //        else
        //        {
        //            Console.Write('.');
        //        }
        //    }
        //    Console.WriteLine();
        //}

        return numberOfGrainsOfSandThatHaveComeToRest.ToString();
    }
}

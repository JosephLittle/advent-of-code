﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(20)]
[SolutionInput("Inputs/Day_20_Test.txt", Problem1Solution = "32000000", Problem2Solution = "")]
[SolutionInput("Inputs/Day_20.txt", Problem1Solution = "825896364", Problem2Solution = "243566897206981", Benchmark = true)]
public unsafe class Day_20 : Solution
{
    public Day_20(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        (var labelsToNodes, string finalNodeLabel) = SetUpNodes();

        // Press the button 1000 times
        ulong lowPulseCount = 0;
        ulong highPulseCount = 0;
        for (int i = 0; i < 1000; i++)
        {
            // Start with a low pulse to the broadcast node
            var pulsesToProcess = new Queue<SentPulse>();
            pulsesToProcess.Enqueue(new SentPulse("button", "roadcaster", Pulse.Low));

            while (pulsesToProcess.TryDequeue(out var sentPulse))
            {
                // Count it
                if (sentPulse.Pulse == Pulse.Low)
                {
                    lowPulseCount++;
                }
                else
                {
                    highPulseCount++;
                }

                // Check for the final node
                if (sentPulse.DestinationNode == finalNodeLabel)
                {
                    continue;
                }

                // Get the node
                var node = labelsToNodes[sentPulse.DestinationNode];

                // Process the pulse
                node.ProcessPulse(sentPulse.SourceNode, sentPulse.Pulse, pulsesToProcess);
            }
        }

        return (lowPulseCount * highPulseCount).ToString();
    }


    protected override string? Problem2()
    {
        (var labelsToNodes, string finalNodeLabel) = SetUpNodes();

        // Do nothing if we have no final node
        if (string.IsNullOrEmpty(finalNodeLabel))
        {
            return null;
        }

        var nodesToLastPulse = new Dictionary<string, List<(int, Pulse)>>();
        nodesToLastPulse.Add("fv", new List<(int, Pulse)>());
        nodesToLastPulse.Add("jd", new List<(int, Pulse)>());
        nodesToLastPulse.Add("lm", new List<(int, Pulse)>());
        nodesToLastPulse.Add("vm", new List<(int, Pulse)>());
        var finalConjunctionThing = new List<(ulong, Pulse, Pulse, Pulse, Pulse)>();

        // Press the button 1000 times
        for (int i = 0; i < 1_000_000; i++)
        {
            // Start with a low pulse to the broadcast node
            var pulsesToProcess = new Queue<SentPulse>();
            pulsesToProcess.Enqueue(new SentPulse("button", "roadcaster", Pulse.Low));

            while (pulsesToProcess.TryDequeue(out var sentPulse))
            {
                // Check for the final node
                if (sentPulse.DestinationNode == finalNodeLabel)
                {
                    continue;
                }

                // Get the node
                var node = labelsToNodes[sentPulse.DestinationNode];

                // Process the pulse
                node.ProcessPulse(sentPulse.SourceNode, sentPulse.Pulse, pulsesToProcess);

                if (node.LastPulseSent == Pulse.High && nodesToLastPulse.ContainsKey(sentPulse.DestinationNode))
                {
                    nodesToLastPulse[sentPulse.DestinationNode].Add((i, node.LastPulseSent));
                }
            }
        }

        // Worked out by hand after looking at the nodes that feed into the final node, then seeing after how
        // many button presses each of them give a high signal, then getting the least common multiple of those four values
        var answer = 243566897206981;

        return answer.ToString();
    }

    private (Dictionary<string, NodeBase> Nodes, string FinalNodeLabel) SetUpNodes()
    {
        var labelsToNodes = new Dictionary<string, NodeBase>(Input.Lines.Length);
        string finalNodeLabel = null;

        // Process each line
        foreach (var line in Input.Lines)
        {
            // Get the output nodes
            var outputNodeLabels = line[(line.IndexOf("->") + 2)..].Split(',', StringSplitOptions.TrimEntries);

            // Get the label
            var nodeLabel = line[1..line.IndexOf(' ')];

            // Get the node type
            NodeBase node;
            switch (line[0])
            {
                case 'b':
                    // Broadcaster
                    node = new BroadcastNode(nodeLabel, outputNodeLabels);
                    break;
                case '%':
                    // Flip flop
                    node = new FlipFlopNode(nodeLabel, outputNodeLabels);
                    break;
                case '&':
                    // Conjunction
                    node = new ConjunctionNode(nodeLabel, outputNodeLabels);
                    break;
                default:
                    throw new NotImplementedException();
            }

            // Add to the dictionary with its label
            labelsToNodes.Add(nodeLabel, node);
        }

        // Link the nodes
        foreach (var node in labelsToNodes.Values)
        {
            foreach (var outputNodeLabel in node.OutputNodeLabels)
            {
                if (labelsToNodes.TryGetValue(outputNodeLabel, out var outputNode))
                {
                    outputNode.AddInputNode(node.NodeLabel);
                }
                else if (finalNodeLabel is null)
                {
                    finalNodeLabel = outputNodeLabel;
                }
                else
                {
                    throw new Exception($"Could not find output node: {outputNodeLabel}");
                }
            }
        }

        return (labelsToNodes, finalNodeLabel);
    }

    private enum Pulse : byte
    {
        Low,
        High
    }

    private readonly record struct SentPulse(string SourceNode, string DestinationNode, Pulse Pulse);

    private abstract record NodeBase(string NodeLabel, string[] OutputNodeLabels)
    {
        public Pulse LastPulseSent;

        public virtual void AddInputNode(string inputNodeLabel) { }

        public abstract void ProcessPulse(string inputNodeLabel, Pulse pulse, Queue<SentPulse> pulsedNodesOutputBuffer);

        public void SendToAllOutputs(Pulse pulseToSend, Queue<SentPulse> pulsedNodesOutputBuffer)
        {
            foreach (var outputNode in OutputNodeLabels)
            {
                pulsedNodesOutputBuffer.Enqueue(new SentPulse(NodeLabel, outputNode, pulseToSend));
            }
            LastPulseSent = pulseToSend;
        }
    }

    private record FlipFlopNode(string NodeLabel, string[] OutputNodeLabels) : NodeBase(NodeLabel, OutputNodeLabels)
    {
        private bool isOn;

        public override void ProcessPulse(string inputNodeLabel, Pulse pulse, Queue<SentPulse> pulsedNodesOutputBuffer)
        {
            // Only process low pulses
            if (pulse == Pulse.Low)
            {
                // Flip
                isOn = !isOn;

                // Send the pulse
                var pulseToSend = isOn ? Pulse.High : Pulse.Low;
                SendToAllOutputs(pulseToSend, pulsedNodesOutputBuffer);
            }
        }
    }

    private record ConjunctionNode(string NodeLabel, string[] OutputNodeLabels) : NodeBase(NodeLabel, OutputNodeLabels)
    {
        private Dictionary<string, Pulse> inputNodeLastPulses = new Dictionary<string, Pulse>();

        public override void AddInputNode(string inputNodeLabel)
        {
            inputNodeLastPulses.Add(inputNodeLabel, Pulse.Low);
        }

        public override void ProcessPulse(string inputNodeLabel, Pulse pulse, Queue<SentPulse> pulsedNodesOutputBuffer)
        {
            // Update the last value for this input node
            inputNodeLastPulses[inputNodeLabel] = pulse;

            // Check if all input nodes remember a high pulse
            var allInputNodesRememberHighPulse = true;
            foreach (var inputNodeLastPulse in inputNodeLastPulses)
            {
                if (inputNodeLastPulse.Value != Pulse.High)
                {
                    allInputNodesRememberHighPulse = false;
                    break;
                }
            }

            // Send the pulse to all outputs
            var pulseToSend = allInputNodesRememberHighPulse ? Pulse.Low : Pulse.High;
            SendToAllOutputs(pulseToSend, pulsedNodesOutputBuffer);
        }
    }

    private record BroadcastNode(string NodeLabel, string[] OutputNodeLabels) : NodeBase(NodeLabel, OutputNodeLabels)
    {
        public override void ProcessPulse(string inputNodeLabel, Pulse pulse, Queue<SentPulse> pulsedNodesOutputBuffer)
        {
            SendToAllOutputs(pulse, pulsedNodesOutputBuffer);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Day_18
    {
        public static void PartOne()
        {
            var data = Data;
            var parser = new TreeParser();

            // Parse each line
            Node currentTree = null;
            foreach (var line in data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var node = parser.Parse(line);

                // Add the node to the tree
                if (currentTree == null)
                {
                    currentTree = node;

                    Console.WriteLine("\t{0}", currentTree);
                }
                else
                {
                    currentTree = Node.Add(currentTree, node);

                    Console.WriteLine("+\t{0}", node);
                    Console.WriteLine("=\t{0}", currentTree);
                }

                // Now reduce the tree if needed
                if (TryReduceTree(currentTree))
                {
                    Console.WriteLine("=\t{0}", currentTree);
                }

                // Write the current magnitude
                Console.WriteLine("=\t{0}", currentTree.GetMagnitude());

                Console.WriteLine();
            }
        }

        public static void PartTwo()
        {
            var data = Data;
            var parser = new TreeParser();

            // Parse each line
            var numbers = new List<Node>();
            foreach (var line in data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
            {
                numbers.Add(parser.Parse(line));
            }

            // Sum each pair and track the largest magnitude
            var largestMagnitude = int.MinValue;
            for (int leftIndex = 0; leftIndex < numbers.Count; leftIndex++)
            {
                var leftNumber = numbers[leftIndex];

                for (int rightIndex = 0; rightIndex < numbers.Count; rightIndex++)
                {
                    // Don't sum this number with itself
                    if (leftIndex == rightIndex)
                    {
                        continue;
                    }

                    var rightNumber = numbers[rightIndex];

                    // Add and reduce (Cloning before adding so as to preserve the original numbers)
                    var sum = Node.Add(leftNumber.Clone(), rightNumber.Clone());
                    TryReduceTree(sum);

                    // Get the magnitude and track if it's the largest
                    var magnitude = sum.GetMagnitude();
                    if (magnitude > largestMagnitude)
                    {
                        largestMagnitude = magnitude;
                    }
                }
            }

            Console.WriteLine(largestMagnitude);
        }

        /// <summary>
        /// Reduces the given tree using the following rules:
        /// - If any pair is nested inside four pairs, the leftmost such pair explodes.
        /// - If any value is 10 or greater, the leftmost such regular number splits.
        /// 
        /// During reduction, at most one action applies, after which the process returns 
        /// to the top of the list of actions. For example, if split produces a pair that 
        /// meets the explode criteria, that pair explodes before other splits occur.
        /// </summary>
        private static bool TryReduceTree(Node currentTree)
        {
            // Loop while we're still performing actions
            var actionPerformed = false;
            var actionPerformedThisIteration = false;
            do
            {
                actionPerformedThisIteration = 
                    TryExplodeValidNodes(currentTree) ||
                    TrySplitValidNodes(currentTree);

                actionPerformed |= actionPerformedThisIteration;
            }
            while (actionPerformedThisIteration);

            return actionPerformed;
        }

        /// <summary>
        /// Explodes a single node pair in the given tree, then returns.
        /// 
        /// To explode a pair, the pair's left value is added to the first regular number 
        /// to the left of the exploding pair (if any), and the pair's right value is 
        /// added to the first regular number to the right of the exploding pair (if any).
        /// Exploding pairs will always consist of two regular numbers. Then, the entire 
        /// exploding pair is replaced with the regular number 0.
        /// </summary>
        private static bool TryExplodeValidNodes(Node currentTree)
        {
            // Find a node pair at depth 4
            if (!TryFindNodePairAtDepthRecursive(currentTree, 4, out var nodePairToExplode))
            {
                return false;
            }

            // Add the pair's left node value to the leaf node to the left (if there is one)
            if (!nodePairToExplode.ChildLeft.IsLeaf)
            {
                throw new Exception("Node pair to explode's left node is not a leaf.");
            }
            if (TryFindNodeLeafNeighbour(nodePairToExplode, true, out var nodeNeighbourLeft))
            {
                nodeNeighbourLeft.Value += nodePairToExplode.ChildLeft.Value;
            }

            // Add the pair's right node value to the leaf node to the right (if there is one)
            if (!nodePairToExplode.ChildRight.IsLeaf)
            {
                throw new Exception("Node pair to explode's right node is not a leaf.");
            }
            if (TryFindNodeLeafNeighbour(nodePairToExplode, false, out var nodeNeighbourRight))
            {
                nodeNeighbourRight.Value += nodePairToExplode.ChildRight.Value;
            }

            // Replace the exploded node with a 0 value leaf node
            nodePairToExplode.Parent.ReplaceChild(nodePairToExplode, new Node(0));
            return true;

            // Local method to find the leaf node immediately to the left, if any
            bool TryFindNodeLeafNeighbour(Node node, bool findLeftNeighbour, out Node nodeLeaf)
            {
                // Find the first node on the neighbour's side
                var currentNode = node;
                Node firstNeighbourOnSide = null;
                while (currentNode.HasParent)
                {
                    var parent = currentNode.Parent;

                    // Get the neighbour on the side to check
                    var nodeOnNeighbourSide = parent.ChildRight;
                    if (findLeftNeighbour)
                    {
                        nodeOnNeighbourSide = parent.ChildLeft;
                    }

                    // If we're not the node on the neighbour side, we've found the first node on that side
                    if (nodeOnNeighbourSide != currentNode)
                    {
                        firstNeighbourOnSide = nodeOnNeighbourSide;
                        break;
                    }
                    else
                    {
                        currentNode = parent;
                    }
                }
                if (firstNeighbourOnSide == null)
                {
                    nodeLeaf = default;
                    return false;
                }

                // Find the first leaf, checking the opposite neighbour side
                currentNode = firstNeighbourOnSide;
                while (currentNode.IsPair)
                {
                    if (findLeftNeighbour)
                    {
                        currentNode = currentNode.ChildRight;
                    }
                    else
                    {
                        currentNode = currentNode.ChildLeft;
                    }
                }

                nodeLeaf = currentNode;
                return true;
            }

            // Local method to find the leftmost pair at a depth of 4
            bool TryFindNodePairAtDepthRecursive(Node currentNode, int depth, out Node nodePairAtDepth)
            {
                // Ignore leaves
                if (currentNode.IsLeaf)
                {
                    nodePairAtDepth = default;
                    return false;
                }

                // We're found the node pair
                if (depth == 0)
                {
                    nodePairAtDepth = currentNode;
                    return true;
                }

                // Decrement the depth
                depth--;

                // Check children, left first
                if (TryFindNodePairAtDepthRecursive(currentNode.ChildLeft, depth, out nodePairAtDepth) ||
                    TryFindNodePairAtDepthRecursive(currentNode.ChildRight, depth, out nodePairAtDepth))
                {
                    return true;
                }
                else
                {
                    nodePairAtDepth = default;
                    return false;
                }
            }
        }

        /// <summary>
        /// Splits a single node with a value of 10 or greater in the given tree, then returns.
        /// 
        /// To split a regular number, replace it with a pair; the left element of the 
        /// pair should be the regular number divided by two and rounded down, while the 
        /// right element of the pair should be the regular number divided by two and 
        /// rounded up. For example, 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6],
        /// and so on.
        /// </summary>
        private static bool TrySplitValidNodes(Node currentTree)
        {
            const int SplitThreshold = 10;

            // Try to find a splittable node
            if (TryFindSplittableNode(currentTree, out var nodeToSplit))
            {
                nodeToSplit.Split();
                return true;
            }
            else
            {
                return false;
            }

            // Local method to find the leftmost leaf node with a splittable value
            bool TryFindSplittableNode(Node currentNode, out Node splittableNode)
            {
                if (currentNode.IsLeaf)
                {
                    if (currentNode.Value >= SplitThreshold)
                    {
                        splittableNode = currentNode;
                        return true;
                    }
                    else
                    {
                        splittableNode = default;
                        return false;
                    }
                }

                // Check chidlren, left first
                if (TryFindSplittableNode(currentNode.ChildLeft, out splittableNode) ||
                    TryFindSplittableNode(currentNode.ChildRight, out splittableNode))
                {
                    return true;
                }
                else
                {
                    splittableNode = default;
                    return false;
                }
            }
        }

        /// <summary>
        /// Parses node tress from text.
        /// </summary>
        private class TreeParser
        {
            private const char NodeOpener = '[';
            private const char NodeCloser = ']';
            private const char ChildSeparator = ',';

            // The current character index.
            private int characterIndex;

            // The current text to parse
            private string text;

            /// <summary>
            /// Parse the given text.
            /// </summary>
            public Node Parse(string text)
            {
                this.text = text;

                // Ensure we start on a node opener
                if (text[0] != NodeOpener)
                {
                    throw new Exception(string.Format(
                        "Text does not start with node opener '{0}'. Text:\n{1}",
                        NodeOpener,
                        text));
                }

                // Parse recursively
                characterIndex = 1;
                return ParseNodeRecursive();
            }

            /// <summary>
            /// Recursively parses the current node. <see cref="characterIndex"/> must be on
            /// the character after the <see cref="NodeOpener"/> for the current node.
            /// </summary>
            public Node ParseNodeRecursive()
            {
                Node childNodeLeft = null;
                Node childNodeRight = null;

                Node currentNode = null;
                var hasCurrentNode = false;

                int currentInteger = 0;
                var hasCurrentInteger = false;

                // Parse the characters for this node
                while (characterIndex < text.Length)
                {
                    var character = text[characterIndex++];

                    // Check for a child node
                    if (character == NodeOpener)
                    {
                        currentNode = ParseNodeRecursive();
                        hasCurrentNode = true;
                    }

                    // Check for a digit
                    else if (char.IsDigit(character))
                    {
                        var digitToAdd = Helpers.CharToInt(character);
                        currentInteger = (currentInteger * 10) + digitToAdd;
                        hasCurrentInteger = true;
                    }

                    // Check for a child separator
                    else if (character == ChildSeparator)
                    {
                        if (hasCurrentNode)
                        {
                            childNodeLeft = currentNode;
                        }
                        else if (hasCurrentInteger)
                        {
                            childNodeLeft = new Node(currentInteger);
                        }
                        else
                        {
                            throw new Exception(string.Format(
                                "Found child separator without parsing child for node. Index: {0}, Text:\n{1}",
                                characterIndex - 1,
                                text));
                        }

                        hasCurrentNode = false;
                        hasCurrentInteger = false;
                        currentNode = null;
                        currentInteger = 0;
                    }
                    
                    // Check for the end of this node
                    else if (character == NodeCloser)
                    {
                        if (hasCurrentNode)
                        {
                            childNodeRight = currentNode;
                        }
                        else if (hasCurrentInteger)
                        {
                            childNodeRight = new Node(currentInteger);
                        }
                        else
                        {
                            throw new Exception(string.Format(
                                "Found node end without parsing right child for node. Index: {0}, Text:\n{1}",
                                characterIndex - 1,
                                text));
                        }

                        // Return the node
                        return new Node(childNodeLeft, childNodeRight);
                    }
                    else
                    {
                        throw new Exception(string.Format(
                            "Unexpected character '{0}' when parsing node. Index: {1}, Text:\n{2}",
                            character,
                            characterIndex - 1,
                            text));
                    }
                }

                // Did not finish node
                throw new Exception(string.Format(
                    "Text ended before node was closed:\n{0}",
                    text));
            }
        }

        /// <summary>
        /// A node in the tree. Holds either a value 
        /// </summary>
        public class Node
        {
            // Property backing fields
            private Node childLeftBacking;
            private Node childRightBacking;
            private int valueBacking;

            /// <summary>
            /// This node's left child, null if <see cref="IsLeaf"/>.
            /// </summary>
            public Node ChildLeft
            {
                get
                {
                    if (IsLeaf)
                        throw new Exception("Cannot get child of leaf node.");
                    return childLeftBacking;
                }
                set 
                {
                    if (IsLeaf)
                        throw new Exception("Cannot set child on leaf node.");
                    childLeftBacking = value;
                    childLeftBacking.Parent = this;
                }
            }

            /// <summary>
            /// This node's left child, null if <see cref="IsLeaf"/>.
            /// </summary>
            public Node ChildRight
            {
                get
                {
                    if (IsLeaf)
                        throw new Exception("Cannot get child of leaf node.");
                    return childRightBacking;
                }
                set
                {
                    if (IsLeaf)
                        throw new Exception("Cannot set child on leaf node.");
                    childRightBacking = value;
                    childRightBacking.Parent = this;
                }
            }

            /// <summary>
            /// This node's parent, if any.
            /// </summary>
            public Node Parent;

            /// <summary>
            /// The value on this node, if <see cref="IsLeaf"/> is true.
            /// </summary>
            public int Value
            {
                get
                {
                    if (IsPair)
                        throw new Exception("Cannot get value of a pair node.");
                    return valueBacking;
                }
                set
                {
                    if (IsPair)
                        throw new Exception("Cannot set value on pair node.");
                    valueBacking = value;
                }
            }

            /// <summary>
            /// Whether this is a leaf node.
            /// </summary>
            public bool IsLeaf { get; private set; }

            /// <summary>
            /// Whether this is a pair node (has children).
            /// </summary>
            public bool IsPair => !IsLeaf;

            /// <summary>
            /// Whether this node has a parent node.
            /// </summary>
            public bool HasParent => Parent != null;

            /// <summary>
            /// Create a new leaf node with the given value.
            /// </summary>
            public Node(int value)
            {
                IsLeaf = true;
                Value = value;
            }

            /// <summary>
            /// Create a new pair node with the given children.
            /// </summary>
            public Node(Node childLeft, Node childRight)
            {
                IsLeaf = false;
                ChildLeft = childLeft;
                ChildRight = childRight;
            }

            /// <summary>
            /// Recursively clone this node and any children.
            /// </summary>
            public Node Clone()
            {
                if (IsLeaf)
                {
                    return new Node(Value);
                }
                else
                {
                    return new Node(ChildLeft.Clone(), ChildRight.Clone());
                }
            }

            /// <summary>
            /// Replace a child on this node with another child.
            /// </summary>
            public void ReplaceChild(Node childToReplace, Node childToReplaceWith)
            {
                if (childToReplaceWith == this)
                {
                    throw new InvalidOperationException("Attempted to create self referencing node.");
                }

                if (ChildLeft == childToReplace)
                {
                    ChildLeft = childToReplaceWith;
                }
                else if (ChildRight == childToReplace)
                {
                    ChildRight = childToReplaceWith;
                }
                else
                {
                    throw new InvalidOperationException("Cannot replace child as it does not belong to this node.");
                }
            }

            /// <summary>
            /// Split this leaf node into a pair node.
            /// The left node of the pair will be the leaf value divided by two and rounded down, 
            /// while the right node of the pair will be the leaf value divided by two and rounded 
            /// up. For example, 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6], and so on.
            /// </summary>
            public void Split()
            {
                // First cache the value
                var valueAsDouble = (double)Value;

                // Change this leaf node into a pair node
                IsLeaf = false;

                // Create the children with the split values
                var leftValue = (int)Math.Round(valueAsDouble / 2.0, MidpointRounding.ToZero);
                ChildLeft = new Node(leftValue);
                var rightValue = (int)Math.Round(valueAsDouble / 2.0, MidpointRounding.AwayFromZero);
                ChildRight = new Node(rightValue);
            }

            /// <summary>
            /// Get the magnitude of this node.
            /// The magnitude of a leaf node is its <see cref="Value"/>, the magnitude
            /// of a pair node is its left node's magnitude multiplied by 3, plus its
            /// right node's magnitude multiplied by 2.
            /// </summary>
            public int GetMagnitude()
            {
                if (IsLeaf)
                {
                    return Value;
                }
                else
                {
                    return ChildLeft.GetMagnitude() * 3 +
                        ChildRight.GetMagnitude() * 2;
                }
            }

            /// <summary>
            /// Add the two nodes together to create a new node tree.
            /// </summary>
            public static Node Add(Node left, Node right)
            {
                return new Node(left, right);
            }

            /// <summary>
            /// Convert this node to a string, using the given string builder.
            /// </summary>
            public void ToString(StringBuilder stringBuilder)
            {
                if (IsLeaf)
                {
                    stringBuilder.Append(Value);
                }
                else
                {
                    stringBuilder.Append('[');
                    ChildLeft.ToString(stringBuilder);
                    stringBuilder.Append(',');
                    ChildRight.ToString(stringBuilder);
                    stringBuilder.Append(']');
                }
            }

            /// <summary>
            /// Convert this node to a string.
            /// </summary>
            public override string ToString()
            {
                var sb = new StringBuilder();
                ToString(sb);
                return sb.ToString();
            }
        }

        private static readonly string DataTest1 = @"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

        private static readonly string DataTest2 = @"[1,1]
[2,2]
[3,3]
[4,4]";
        private static readonly string DataTest3 = @"[1,1]
[2,2]
[3,3]
[4,4]
[5,5]";
        private static readonly string DataTest4 = @"[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]";

        private static readonly string Data = @"[[8,[6,[0,1]]],[8,[2,[1,9]]]]
[[[5,[1,7]],9],[4,[[9,4],4]]]
[[[1,[6,3]],8],[[3,[5,3]],[1,6]]]
[0,[[1,[7,4]],[6,[7,3]]]]
[[[7,6],2],[[[9,5],9],[[5,5],[6,3]]]]
[[3,[[2,3],0]],0]
[4,[2,[4,[2,6]]]]
[[2,[8,[4,5]]],[[[7,7],7],1]]
[[[[6,7],6],[9,8]],[[0,[7,3]],[[9,1],[2,0]]]]
[0,[[[8,4],[4,9]],5]]
[[0,7],[8,5]]
[6,[[9,[0,7]],[[0,0],[8,1]]]]
[[[[8,3],[1,9]],[[9,3],[6,5]]],4]
[[[6,6],[[1,2],[1,7]]],[[8,8],[3,2]]]
[[[6,4],[[0,3],1]],[[6,2],[4,[0,3]]]]
[[[2,9],[[2,1],1]],[[6,[1,4]],[6,[0,3]]]]
[9,[[7,4],[1,9]]]
[[[[1,2],[7,8]],[[9,6],[1,3]]],[[0,6],[[3,6],6]]]
[[[1,[8,6]],[2,[3,4]]],[[0,4],[5,[5,7]]]]
[[[5,9],[[1,0],[4,3]]],[[7,[6,7]],[1,[1,5]]]]
[[[[6,8],[5,9]],[5,[4,5]]],4]
[[[[3,4],4],7],[[5,[3,7]],7]]
[[[[3,3],[7,9]],[1,[4,8]]],0]
[[[3,[9,4]],[1,[3,7]]],[[[1,8],1],[6,1]]]
[3,[[[7,5],[4,8]],[7,8]]]
[[7,[2,[2,4]]],[0,[8,[0,3]]]]
[[[[8,5],3],[3,[8,3]]],[1,[0,[7,4]]]]
[[[[7,1],3],[3,4]],[[3,7],[[1,8],[4,8]]]]
[[[3,[9,9]],4],[[4,2],[[4,2],4]]]
[[[5,[9,1]],[[3,5],[1,9]]],7]
[[[[0,8],5],[9,[5,1]]],[[7,0],1]]
[[[0,2],[[1,9],7]],[[0,3],[[0,3],[4,8]]]]
[[[[1,8],0],[[8,6],[7,6]]],[[[1,8],4],[[0,4],[8,3]]]]
[[[1,[2,7]],[[5,4],[5,0]]],[5,[8,[8,4]]]]
[[[[4,4],[7,3]],[4,[2,3]]],[[[6,5],[1,5]],[5,[8,6]]]]
[[[[7,8],4],[9,[4,2]]],[[[1,4],2],[0,7]]]
[[8,4],[1,[2,5]]]
[[[[2,5],4],[7,[0,2]]],[5,3]]
[[3,[[7,4],3]],3]
[[[3,5],[3,[1,4]]],[[[0,8],1],8]]
[[[[1,9],5],[2,[4,8]]],[[[9,2],[0,1]],1]]
[[[6,[1,5]],[[2,2],6]],[[1,[2,6]],5]]
[[[3,2],[9,3]],[[2,1],[4,8]]]
[[[[9,2],7],[[5,9],[1,2]]],[[[3,0],[2,8]],0]]
[[[6,5],[[9,4],3]],[[[6,2],1],[0,7]]]
[[[8,6],1],[9,[1,[0,1]]]]
[[[[5,1],4],[8,[6,8]]],[4,[[1,8],9]]]
[[[[1,1],[8,9]],[2,[0,6]]],3]
[[[1,[8,3]],[[4,3],1]],[[[4,1],[8,6]],8]]
[[8,[[6,2],8]],[[[4,0],8],6]]
[[[[2,2],7],[[9,0],[3,3]]],[[[4,4],0],2]]
[8,[[3,[9,1]],[0,[9,1]]]]
[[[0,[4,2]],[[2,2],[8,7]]],[[6,[4,2]],[1,6]]]
[[3,2],[4,[[6,2],2]]]
[[6,[3,[2,9]]],[[9,[1,5]],[4,4]]]
[[[[7,5],5],8],[1,[0,[2,7]]]]
[[2,[[2,9],[1,6]]],[[[0,1],[0,2]],[4,[3,4]]]]
[[[[8,9],[7,4]],[8,[6,5]]],1]
[[8,9],[[2,[6,9]],[2,8]]]
[[5,1],8]
[[[8,[4,2]],[5,[1,8]]],[[0,[0,6]],[[6,7],9]]]
[[[8,[8,0]],[[8,0],8]],[[[9,9],9],[9,[5,4]]]]
[[[[3,3],5],[5,[9,0]]],[[2,6],[[3,8],[7,1]]]]
[3,[[[1,5],8],5]]
[[[9,8],[4,3]],5]
[[[5,7],[[2,1],6]],[[4,2],[1,[0,2]]]]
[[[[9,3],[9,8]],[[1,0],6]],[[[6,5],2],[[0,3],6]]]
[8,[[[9,8],[2,8]],[1,0]]]
[[8,[5,9]],[[[4,3],6],[[5,1],4]]]
[[0,8],[1,[4,[6,3]]]]
[3,[3,[6,[5,6]]]]
[0,[0,[[8,0],8]]]
[[0,4],[[7,4],[[0,7],1]]]
[7,[[[6,3],[4,0]],1]]
[9,[5,[[5,3],[2,8]]]]
[[7,[[8,3],[1,7]]],[[[2,7],1],[[9,4],[7,1]]]]
[[[0,[7,3]],3],2]
[[1,[[9,0],2]],3]
[[1,[7,[0,1]]],[[1,8],5]]
[3,[5,[4,1]]]
[3,[[[9,8],4],[4,[9,7]]]]
[[2,9],[0,9]]
[[[[7,1],[9,3]],[1,[1,8]]],9]
[[[9,8],[[7,8],3]],[[1,[6,3]],[2,[7,3]]]]
[[[7,3],[1,[5,5]]],[[4,8],[8,[2,5]]]]
[[2,[[6,5],[4,6]]],[[0,3],7]]
[[[4,[9,7]],[[6,1],6]],[[[8,1],6],[[2,5],9]]]
[[[6,0],0],[9,9]]
[[[[1,0],0],[[5,7],9]],[[[7,2],0],[9,6]]]
[[[[5,0],[2,0]],[0,[7,5]]],[[[7,7],[2,4]],8]]
[0,[[9,[3,4]],[[3,4],6]]]
[[[0,8],[[1,5],[3,4]]],[[5,[6,4]],[[2,5],[2,5]]]]
[[8,0],[[2,[7,9]],9]]
[[[3,[7,0]],[3,[8,4]]],2]
[[8,1],[[[8,9],[1,0]],3]]
[[[8,3],[[4,8],4]],[[8,[8,8]],[0,2]]]
[[0,[9,4]],[[6,8],[[7,1],9]]]
[[[[5,3],[2,8]],[8,7]],[9,[[5,9],[5,2]]]]
[2,[4,[[4,3],8]]]
[[[[7,2],[6,4]],7],8]";
    }
}

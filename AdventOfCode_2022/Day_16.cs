﻿using AdventOfCode.Framework;
using System.Diagnostics;

namespace AdventOfCode_2022;

[Solution(16)]
[SolutionInput("Inputs/Day_16_Test.txt", Problem1Solution = "1651", Problem2Solution = "1707")]
[SolutionInput("Inputs/Day_16.txt", Problem1Solution = "1944", Problem2Solution = "2679", Benchmark = true)]
public unsafe class Day_16 : Solution
{
    private class Valve
    {
        public readonly ValveName Name;
        public readonly long FlowRate;
        public readonly Dictionary<ValveName, LinkedValve> LinkedValves;

        public long Score;

        public Valve(ValveName name, long flowRate)
        {
            Name = name;
            FlowRate = flowRate;
            LinkedValves = new Dictionary<ValveName, LinkedValve>(6);

            Score = 0;
        }

        public override string ToString() =>
            $"{Name}, FlowRate: {FlowRate}";
    }
    private record LinkedValve(Valve Valve, long UseCost);
    private readonly record struct ValveName(char First, char Second);

    public Day_16(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;
        var startingValveName = new ValveName('A', 'A');

        var valvesByName = GetNormalisedValveGraph(inputLines, startingValveName);

        // Perform the search
        var openedValves = new HashSet<Valve>(valvesByName.Count);
        var highestPressureReleased = long.MinValue;
        RecursiveSearch(valvesByName[startingValveName], 30, 0);

        // Local method to recursively search for the highest possible pressure released
        void RecursiveSearch(Valve currentValve, long minutesRemaining, long pressureReleased)
        {
            // Check whether we have enough time remaining to use this valve
            if (minutesRemaining <= 0)
            {
                return;
            }

            // Check for a new highest pressure released
            if (pressureReleased > highestPressureReleased)
            {
                highestPressureReleased = pressureReleased;
            }

            // Add this valve to the opened valves
            openedValves.Add(currentValve);

            // Recurse into each linked valve
            foreach (var linkedValve in currentValve.LinkedValves)
            {
                if (openedValves.Contains(linkedValve.Value.Valve))
                {
                    continue;
                }

                var minutesRemainingAfterUse = minutesRemaining - linkedValve.Value.UseCost;
                RecursiveSearch(
                    linkedValve.Value.Valve,
                    minutesRemainingAfterUse,
                    pressureReleased + (minutesRemainingAfterUse * linkedValve.Value.Valve.FlowRate));
            }

            // Remove this valve from the opened valves
            openedValves.Remove(currentValve);
        }

        // Return the highest pressure
        return highestPressureReleased.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;
        var startingValveName = new ValveName('A', 'A');

        var valvesByName = GetNormalisedValveGraph(inputLines, startingValveName);

        // Get the flow rate sum
        var flowRateSum = (long)valvesByName.Values.Sum(v => v.FlowRate);

        // Perform the search
        var openedValves = new HashSet<Valve>(valvesByName.Count);
        var highestPressureReleased = long.MinValue;

        var unopenedValves = new Queue<Valve>(valvesByName.Values.Where(v => v.Name != startingValveName));
        RecursiveSearchNew(valvesByName[startingValveName], valvesByName[startingValveName], 26, 26, 0, flowRateSum);

        void RecursiveSearchNew(
            Valve currentValveA, Valve currentValveB,
            long minutesRemainingA, long minutesRemainingB,
            long pressureReleased, long flowRateSum)
        {
            // Check for a new highest pressure released
            if (pressureReleased > highestPressureReleased)
            {
                highestPressureReleased = pressureReleased;
            }

            // Stop if we have no more unopened valves
            // or the maximum potential pressure released is less than the highest pressure released
            var maximumPotentialPressureReleased = pressureReleased + (flowRateSum * (minutesRemainingA > minutesRemainingB ? minutesRemainingA : minutesRemainingB));
            if (unopenedValves.Count == 0 |
                maximumPotentialPressureReleased <= highestPressureReleased)
            {
                return;
            }

            // Work through the unopened valves
            var numberOfUnopenedValvesToVisit = unopenedValves.Count;
            while (numberOfUnopenedValvesToVisit-- > 0)
            {
                // Pop an unopened valve
                var unopenedValve = unopenedValves.Dequeue();

                // Check for A
                var valveLinkA = currentValveA.LinkedValves[unopenedValve.Name];
                var minutesRemainingAfterUseA = minutesRemainingA - valveLinkA.UseCost;
                if (minutesRemainingAfterUseA > 0)
                {
                    RecursiveSearchNew(
                        valveLinkA.Valve,
                        currentValveB,
                        minutesRemainingAfterUseA,
                        minutesRemainingB,
                        pressureReleased + (minutesRemainingAfterUseA * valveLinkA.Valve.FlowRate),
                        flowRateSum - valveLinkA.Valve.FlowRate);
                }

                // Check for B
                var valveLinkB = currentValveB.LinkedValves[unopenedValve.Name];
                var minutesRemainingAfterUseB = minutesRemainingB - valveLinkB.UseCost;
                if (minutesRemainingAfterUseB > 0)
                {
                    RecursiveSearchNew(
                        currentValveA,
                        valveLinkB.Valve,
                        minutesRemainingA,
                        minutesRemainingAfterUseB,
                        pressureReleased + (minutesRemainingAfterUseB * valveLinkB.Valve.FlowRate),
                        flowRateSum - valveLinkB.Valve.FlowRate);
                }

                // Push the unopened valve back on
                unopenedValves.Enqueue(unopenedValve);
            }
        }

        // Return the highest pressure
        return highestPressureReleased.ToString();
    }

    private static Dictionary<ValveName, Valve> GetNormalisedValveGraph(string[] inputLines, ValveName startingValveName)
    {
        var valvesByName = new Dictionary<ValveName, Valve>(inputLines.Length);
        var valvesWithZeroFlow = new HashSet<ValveName>(inputLines.Length);

        // First pass, read the valves
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var inputLine = inputLines[lineIndex];

            // Parse and add the valve
            var valve = new Valve(
                    new ValveName(inputLine[6], inputLine[7]),
                    (long)Helpers.ParseIntegerUntilDelimiter(inputLine, 23, ';'));
            valvesByName.Add(valve.Name, valve);

            // Track the valves with zero flow so we can remove them later
            if (valve.FlowRate == 0)
            {
                valvesWithZeroFlow.Add(valve.Name);
            }
        }

        // Second pass, read the links
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var inputLine = inputLines[lineIndex];

            // Parse the valve name
            var valveName = new ValveName(inputLine[6], inputLine[7]);
            var valve = valvesByName[valveName];

            // Parse linked valves
            for (int characterIndex = inputLine.Length - 2; characterIndex >= 0; characterIndex -= 4)
            {
                var firstCharacter = inputLine[characterIndex];

                // If this is not an upper case character then we're finished parsing linked valves
                if (!char.IsUpper(firstCharacter))
                {
                    break;
                }

                // Add to the valve's linked valves
                var linkedValveName = new ValveName(firstCharacter, inputLine[characterIndex + 1]);
                valve.LinkedValves.Add(
                    linkedValveName,
                    new LinkedValve(
                        valvesByName[linkedValveName],
                        1));
            }
        }

        // Find the shortest paths for each valve
        var unvisitedValves = new HashSet<Valve>();
        var visitedValves = new HashSet<Valve>();
        var valveNamesToNewLinks = new Dictionary<ValveName, List<LinkedValve>>(inputLines.Length);
        foreach (var valveToCalculateShortestPathsFor in valvesByName)
        {
            // Ignore this valve if it has no flow and it's not the starting valve
            if (valveToCalculateShortestPathsFor.Value.Name != startingValveName &
                valveToCalculateShortestPathsFor.Value.FlowRate == 0)
            {
                continue;
            }

            unvisitedValves.Clear();
            visitedValves.Clear();

            // Reset shortest paths
            foreach (var valveToReset in valvesByName)
            {
                valveToReset.Value.Score = int.MaxValue;
            }

            // Start with the valve
            var startingValve = valveToCalculateShortestPathsFor.Value;
            startingValve.Score = 0;
            unvisitedValves.Add(startingValve);

            // Perform Dijkstra to get the shortest path to each other valve
            while (unvisitedValves.Count > 0)
            {
                // Get the valve with the shortest path
                var enumerator = unvisitedValves.GetEnumerator();
                enumerator.MoveNext();
                var currentValve = enumerator.Current;
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Score < currentValve.Score)
                    {
                        currentValve = enumerator.Current;
                    }
                }
                unvisitedValves.Remove(currentValve);
                visitedValves.Add(currentValve);

                foreach (var kv in currentValve.LinkedValves)
                {
                    var linkedValve = kv.Value.Valve;

                    // Ignore already visited valves
                    if (visitedValves.Contains(linkedValve))
                    {
                        continue;
                    }

                    // Get the shortest path to this valve
                    var shortestPath = currentValve.Score + 1;

                    // Set the new shortest path if this is shorter
                    if (linkedValve.Score > shortestPath)
                    {
                        linkedValve.Score = shortestPath;
                    }

                    // Add to the unvisited valves
                    unvisitedValves.Add(linkedValve);
                }
            }

            // Now that we have the shortest paths, get the links for this valve to all other valves
            var newLinks = new List<LinkedValve>(valvesByName.Count - 1);
            foreach (var valveToLinkTo in valvesByName)
            {
                // Ignore the current valve and valves with no flow
                if (valveToCalculateShortestPathsFor.Value == valveToLinkTo.Value |
                    valvesWithZeroFlow.Contains(valveToLinkTo.Key))
                {
                    continue;
                }

                // Add the new link with the use cost as its shortest path + 1 for the activate cost
                newLinks.Add(new LinkedValve(
                    valveToLinkTo.Value,
                    (valveToLinkTo.Value.Score + 1)));
            }
            valveNamesToNewLinks.Add(
                valveToCalculateShortestPathsFor.Key,
                newLinks);
        }

        // Set the new links and reset score
        foreach (var valveNameAndNewLinks in valveNamesToNewLinks)
        {
            var valve = valvesByName[valveNameAndNewLinks.Key];
            valve.LinkedValves.Clear();
            foreach (var linkedValve in valveNameAndNewLinks.Value)
            {
                valve.LinkedValves.Add(
                    linkedValve.Valve.Name,
                    linkedValve);
            }
            valve.Score = 0;
        }

        // Remove the valves with no flow
        foreach (var valveWithZeroFlow in valvesWithZeroFlow)
        {
            // Only remove if not the starting valve
            if (valveWithZeroFlow != startingValveName)
            {
                valvesByName.Remove(valveWithZeroFlow);
            }
        }

        return valvesByName;
    }
}

﻿using System.Diagnostics;
using System.Text;

namespace AdventOfCode
{
    public class Day_23
    {
        public static void PartOne()
        {
            // Set up the nodes
            // 00 01 02 03 04 05 06 07 08 09 10
            //       11    13    15    17
            //       12    14    16    18
            var nodes = new Node[19];
            nodes[00] = new Node(00, true, true);
            nodes[01] = new Node(01, true, true);
            nodes[02] = new Node(02, true, false);
            nodes[03] = new Node(03, true, true);
            nodes[04] = new Node(04, true, false);
            nodes[05] = new Node(05, true, true);
            nodes[06] = new Node(06, true, false);
            nodes[07] = new Node(07, true, true);
            nodes[08] = new Node(08, true, false);
            nodes[09] = new Node(09, true, true);
            nodes[10] = new Node(10, true, true);
            nodes[11] = new Node(11, (sbyte)'A');
            nodes[12] = new Node(12, (sbyte)'A');
            nodes[13] = new Node(13, (sbyte)'B');
            nodes[14] = new Node(14, (sbyte)'B');
            nodes[15] = new Node(15, (sbyte)'C');
            nodes[16] = new Node(16, (sbyte)'C');
            nodes[17] = new Node(17, (sbyte)'D');
            nodes[18] = new Node(18, (sbyte)'D');
            nodes[00].SetNeighbours(nodes[01]);
            nodes[01].SetNeighbours(nodes[00], nodes[02]);
            nodes[02].SetNeighbours(nodes[01], nodes[03], nodes[11]);
            nodes[03].SetNeighbours(nodes[02], nodes[04]);
            nodes[04].SetNeighbours(nodes[03], nodes[05], nodes[13]);
            nodes[05].SetNeighbours(nodes[04], nodes[06]);
            nodes[06].SetNeighbours(nodes[05], nodes[07], nodes[15]);
            nodes[07].SetNeighbours(nodes[06], nodes[08]);
            nodes[08].SetNeighbours(nodes[07], nodes[09], nodes[17]);
            nodes[09].SetNeighbours(nodes[08], nodes[10]);
            nodes[10].SetNeighbours(nodes[09]);
            nodes[11].SetNeighbours(nodes[02], nodes[12]);
            nodes[12].SetNeighbours(nodes[11]);
            nodes[13].SetNeighbours(nodes[04], nodes[14]);
            nodes[14].SetNeighbours(nodes[13]);
            nodes[15].SetNeighbours(nodes[06], nodes[16]);
            nodes[16].SetNeighbours(nodes[15]);
            nodes[17].SetNeighbours(nodes[08], nodes[18]);
            nodes[18].SetNeighbours(nodes[17]);

            // Map destination nodes to their destination pair
            var destinationNodesToPairedDestinationNode = new Dictionary<Node, Node>();
            destinationNodesToPairedDestinationNode.Add(nodes[11], nodes[12]);
            destinationNodesToPairedDestinationNode.Add(nodes[12], nodes[11]);
            destinationNodesToPairedDestinationNode.Add(nodes[13], nodes[14]);
            destinationNodesToPairedDestinationNode.Add(nodes[14], nodes[13]);
            destinationNodesToPairedDestinationNode.Add(nodes[15], nodes[16]);
            destinationNodesToPairedDestinationNode.Add(nodes[16], nodes[15]);
            destinationNodesToPairedDestinationNode.Add(nodes[17], nodes[18]);
            destinationNodesToPairedDestinationNode.Add(nodes[18], nodes[17]);

            // TEST
            // #############
            // #...........#
            // ###B#C#B#D###
            //   #A#D#C#A#
            //   #########
            // Add the actors to their nodes
            //nodes[11].OccupierActorId = (sbyte)'B';
            //nodes[12].OccupierActorId = (sbyte)'A';
            //nodes[13].OccupierActorId = (sbyte)'C';
            //nodes[14].OccupierActorId = (sbyte)'D';
            //nodes[15].OccupierActorId = (sbyte)'B';
            //nodes[16].OccupierActorId = (sbyte)'C';
            //nodes[17].OccupierActorId = (sbyte)'D';
            //nodes[18].OccupierActorId = (sbyte)'A';

            // INPUT
            // #############
            // #...........#
            // ###C#A#B#D###
            //   #C#A#D#B#
            //   #########
            nodes[11].OccupierActorId = (sbyte)'C';
            nodes[12].OccupierActorId = (sbyte)'C';
            nodes[13].OccupierActorId = (sbyte)'A';
            nodes[14].OccupierActorId = (sbyte)'A';
            nodes[15].OccupierActorId = (sbyte)'B';
            nodes[16].OccupierActorId = (sbyte)'D';
            nodes[17].OccupierActorId = (sbyte)'D';
            nodes[18].OccupierActorId = (sbyte)'B';

            // Set the movement costs
            var baseMovementCosts = new int[sbyte.MaxValue];
            baseMovementCosts[(sbyte)'A'] = 1;
            baseMovementCosts[(sbyte)'B'] = 10;
            baseMovementCosts[(sbyte)'C'] = 100;
            baseMovementCosts[(sbyte)'D'] = 1000;

            // Set which nodes have actors
            var nodesWithActors = new List<Node>()
            {
                nodes[11],
                nodes[12],
                nodes[13],
                nodes[14],
                nodes[15],
                nodes[16],
                nodes[17],
                nodes[18],
            };

            // Remove any actors that are already at their dead-end destinations
            for (int i = nodesWithActors.Count - 1; i >= 0; i--)
            {
                if (nodesWithActors[i].IsOccupiedByActorAtTheirDestination &
                    nodesWithActors[i].IsDeadEnd)
                {
                    nodesWithActors.RemoveAt(i);
                }
            }

            // Do the search
            var visitedNodes = new HashSet<Node>();
            var minimumCost = int.MaxValue;
            var movesStack = new Stack<string>();
            RecursiveSearch(0);

            // Not 11352
            Console.WriteLine("Minimum Cost: {0}", minimumCost);

            void RecursiveSearch(int currentCost)
            {
                // Check if all actors are accounted for
                if (nodesWithActors.Count == 0)
                {
                    if (currentCost < minimumCost)
                    {
                        minimumCost = currentCost;

                        // Print the state
                        var sb = new StringBuilder($"State for cost: {minimumCost}\n");
                        for (int i = 0; i < nodes.Length; i++)
                        {
                            sb.AppendLine($"Node {i}, Occupier: {nodes[i].OccupierActorId}");
                        }
                        foreach (var move in movesStack.Reverse())
                        {
                            sb.AppendLine(move);
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    return;
                }
                else if (currentCost >= minimumCost)
                {
                    // More expensive than current minimum cost, so ignore
                    return;
                }

                // Find a node with an actor to move
                for (int i = nodesWithActors.Count - 1; i >= 0; i--)
                {
                    var node = nodesWithActors[i];

                    // Find a node for the actor to move to
                    var reachableNodes = GetReachableNodesForActor(node);

                    // Remove from the current node
                    var actorId = node.OccupierActorId;
                    node.OccupierActorId = sbyte.MinValue;
                    nodesWithActors.RemoveAt(i);

                    // Visit each reachable node
                    foreach (var reachableNodeAndDistance in reachableNodes)
                    {
                        var reachableNode = reachableNodeAndDistance.node;
                        var distance = reachableNodeAndDistance.distance;

                        // Add the actor to this node
                        reachableNode.OccupierActorId = actorId;

                        // Add to nodes with actors if it hasn't reached its destination
                        var wasAddedToNodesWithActors = false;
                        if (!reachableNode.IsOccupiedByActorAtTheirDestination)
                        {
                            nodesWithActors.Add(reachableNode);
                            wasAddedToNodesWithActors = true;
                        }

                        // Get the cost of moving to this node
                        var movementCost = distance * baseMovementCosts[actorId];

                        movesStack.Push($"{(char)actorId} from {node.Id} to {reachableNode.Id} (costs {movementCost})");

                        // Recurse to the next step
                        RecursiveSearch(currentCost + movementCost);

                        movesStack.Pop();

                        // Remove the actor from this node
                        reachableNode.OccupierActorId = sbyte.MinValue;

                        // Remove from nodes with actors if it was added
                        if (wasAddedToNodesWithActors)
                        {
                            nodesWithActors.Remove(reachableNode);
                        }
                    }

                    // Add back to the current node for the recursive search
                    node.OccupierActorId = actorId;
                    nodesWithActors.Add(node);
                }
            }

            List<(Node node, int distance)> GetReachableNodesForActor(Node node)
            {
                var reachableNodesBuffer = new List<(Node node, int distance)>();
                GetReachableNodesForActorRecursive(
                    node,
                    node.OccupierActorId, node.IsHallway,
                    0,
                    reachableNodesBuffer);
                return reachableNodesBuffer;
            }
            void GetReachableNodesForActorRecursive(
                Node node,
                sbyte actorId, bool isActorInHallway,
                int distance, List<(Node node, int distance)> reachableNodesBuffer)
            {
                // Increase the distance
                distance++;

                // Add as a visited node
                visitedNodes.Add(node);

                // Check each neighbour
                foreach (var neighbourNode in node.Neighbours)
                {
                    // Ignore if this node is occupied or is the previous node
                    if (neighbourNode.IsOccupied | visitedNodes.Contains(neighbourNode))
                    {
                        continue;
                    }

                    // Add if a valid destination node
                    if (neighbourNode.IsDestination &
                        neighbourNode.DestinationActorId == actorId)
                    {
                        // Only add this destination if it is the deadend or
                        // the other paired destination is an occupied dead end
                        var otherDestinationNode = destinationNodesToPairedDestinationNode[neighbourNode];
                        if (neighbourNode.IsDeadEnd | otherDestinationNode.IsOccupied)
                        {
                            reachableNodesBuffer.Add((neighbourNode, distance));
                        }
                    }
                    // Add if a valid hallway
                    else if (neighbourNode.IsHallway & neighbourNode.CanStop & !isActorInHallway)
                    {
                        reachableNodesBuffer.Add((neighbourNode, distance));
                    }

                    // Visit this neighbour
                    GetReachableNodesForActorRecursive(neighbourNode,
                        actorId, isActorInHallway,
                        distance,
                        reachableNodesBuffer);
                }

                // Remove from visited nodes
                visitedNodes.Remove(node);
            }
        }

        public static void PartTwo()
        {
            // Set up the nodes
            // 00 01 02 03 04 05 06 07 08 09 10
            //       11    15    19    23
            //       12    16    20    24
            //       13    17    21    25
            //       14    18    22    26
            var nodes = new Node[27];
            nodes[00] = new Node(00, true, true);
            nodes[01] = new Node(01, true, true);
            nodes[02] = new Node(02, true, false);
            nodes[03] = new Node(03, true, true);
            nodes[04] = new Node(04, true, false);
            nodes[05] = new Node(05, true, true);
            nodes[06] = new Node(06, true, false);
            nodes[07] = new Node(07, true, true);
            nodes[08] = new Node(08, true, false);
            nodes[09] = new Node(09, true, true);
            nodes[10] = new Node(10, true, true);
            nodes[11] = new Node(11, (sbyte)'A');
            nodes[12] = new Node(12, (sbyte)'A');
            nodes[13] = new Node(13, (sbyte)'A');
            nodes[14] = new Node(14, (sbyte)'A');
            nodes[15] = new Node(15, (sbyte)'B');
            nodes[16] = new Node(16, (sbyte)'B');
            nodes[17] = new Node(17, (sbyte)'B');
            nodes[18] = new Node(18, (sbyte)'B');
            nodes[19] = new Node(19, (sbyte)'C');
            nodes[20] = new Node(20, (sbyte)'C');
            nodes[21] = new Node(21, (sbyte)'C');
            nodes[22] = new Node(22, (sbyte)'C');
            nodes[23] = new Node(23, (sbyte)'D');
            nodes[24] = new Node(24, (sbyte)'D');
            nodes[25] = new Node(25, (sbyte)'D');
            nodes[26] = new Node(26, (sbyte)'D');
            nodes[00].SetNeighbours(nodes[01]);
            nodes[01].SetNeighbours(nodes[00], nodes[02]);
            nodes[02].SetNeighbours(nodes[01], nodes[03], nodes[11]);
            nodes[03].SetNeighbours(nodes[02], nodes[04]);
            nodes[04].SetNeighbours(nodes[03], nodes[05], nodes[15]);
            nodes[05].SetNeighbours(nodes[04], nodes[06]);
            nodes[06].SetNeighbours(nodes[05], nodes[07], nodes[19]);
            nodes[07].SetNeighbours(nodes[06], nodes[08]);
            nodes[08].SetNeighbours(nodes[07], nodes[09], nodes[23]);
            nodes[09].SetNeighbours(nodes[08], nodes[10]);
            nodes[10].SetNeighbours(nodes[09]);
            nodes[11].SetNeighbours(nodes[02], nodes[12]);
            nodes[12].SetNeighbours(nodes[11], nodes[13]);
            nodes[13].SetNeighbours(nodes[12], nodes[14]);
            nodes[14].SetNeighbours(nodes[13]);
            nodes[15].SetNeighbours(nodes[04], nodes[16]);
            nodes[16].SetNeighbours(nodes[15], nodes[17]);
            nodes[17].SetNeighbours(nodes[16], nodes[18]);
            nodes[18].SetNeighbours(nodes[17]);
            nodes[19].SetNeighbours(nodes[06], nodes[20]);
            nodes[20].SetNeighbours(nodes[19], nodes[21]);
            nodes[21].SetNeighbours(nodes[20], nodes[22]);
            nodes[22].SetNeighbours(nodes[21]);
            nodes[23].SetNeighbours(nodes[08], nodes[24]);
            nodes[24].SetNeighbours(nodes[23], nodes[25]);
            nodes[25].SetNeighbours(nodes[24], nodes[26]);
            nodes[26].SetNeighbours(nodes[25]);

            var shouldDestinationNodeBeFilled = new Dictionary<Node, Func<bool>>();
            shouldDestinationNodeBeFilled.Add(nodes[11], () => 
                nodes[12].IsOccupiedByActorAtTheirDestination & 
                nodes[13].IsOccupiedByActorAtTheirDestination & 
                nodes[14].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[12], () => 
                nodes[13].IsOccupiedByActorAtTheirDestination & 
                nodes[14].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[13], () => 
                nodes[14].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[14], () => true);

            shouldDestinationNodeBeFilled.Add(nodes[15], () => 
                nodes[16].IsOccupiedByActorAtTheirDestination & 
                nodes[17].IsOccupiedByActorAtTheirDestination & 
                nodes[18].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[16], () => 
                nodes[17].IsOccupiedByActorAtTheirDestination & 
                nodes[18].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[17], () => 
                nodes[18].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[18], () => true);

            shouldDestinationNodeBeFilled.Add(nodes[19], () => 
                nodes[20].IsOccupiedByActorAtTheirDestination & 
                nodes[21].IsOccupiedByActorAtTheirDestination & 
                nodes[22].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[20], () => 
                nodes[21].IsOccupiedByActorAtTheirDestination & 
                nodes[22].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[21], () => 
                nodes[22].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[22], () => true);

            shouldDestinationNodeBeFilled.Add(nodes[23], () => 
                nodes[24].IsOccupiedByActorAtTheirDestination & 
                nodes[25].IsOccupiedByActorAtTheirDestination & 
                nodes[26].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[24], () => 
                nodes[25].IsOccupiedByActorAtTheirDestination & 
                nodes[26].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[25], () => 
                nodes[26].IsOccupiedByActorAtTheirDestination);
            shouldDestinationNodeBeFilled.Add(nodes[26], () => true);

            if (true)
            {
                // TEST
                // #############
                // #...........#
                // ###B#C#B#D###
                //   #D#C#B#A#
                //   #D#B#A#C#
                //   #A#D#C#A#
                //   #########
                // Add the actors to their nodes
                nodes[11].OccupierActorId = (sbyte)'B';
                nodes[12].OccupierActorId = (sbyte)'D';
                nodes[13].OccupierActorId = (sbyte)'D';
                nodes[14].OccupierActorId = (sbyte)'A';
                nodes[15].OccupierActorId = (sbyte)'C';
                nodes[16].OccupierActorId = (sbyte)'C';
                nodes[17].OccupierActorId = (sbyte)'B';
                nodes[18].OccupierActorId = (sbyte)'D';
                nodes[19].OccupierActorId = (sbyte)'B';
                nodes[20].OccupierActorId = (sbyte)'B';
                nodes[21].OccupierActorId = (sbyte)'A';
                nodes[22].OccupierActorId = (sbyte)'C';
                nodes[23].OccupierActorId = (sbyte)'D';
                nodes[24].OccupierActorId = (sbyte)'A';
                nodes[25].OccupierActorId = (sbyte)'C';
                nodes[26].OccupierActorId = (sbyte)'A';

            }
            else
            {
                // INPUT
                // #############
                //#...........#
                //###C#A#B#D###
                //  #D#C#B#A#
                //  #D#B#A#C#
                //  #C#A#D#B#
                //  #########
                nodes[11].OccupierActorId = (sbyte)'C';
                nodes[12].OccupierActorId = (sbyte)'D';
                nodes[13].OccupierActorId = (sbyte)'D';
                nodes[14].OccupierActorId = (sbyte)'C';
                nodes[15].OccupierActorId = (sbyte)'A';
                nodes[16].OccupierActorId = (sbyte)'C';
                nodes[17].OccupierActorId = (sbyte)'B';
                nodes[18].OccupierActorId = (sbyte)'A';
                nodes[19].OccupierActorId = (sbyte)'B';
                nodes[20].OccupierActorId = (sbyte)'B';
                nodes[21].OccupierActorId = (sbyte)'A';
                nodes[22].OccupierActorId = (sbyte)'D';
                nodes[23].OccupierActorId = (sbyte)'D';
                nodes[24].OccupierActorId = (sbyte)'A';
                nodes[25].OccupierActorId = (sbyte)'C';
                nodes[26].OccupierActorId = (sbyte)'B';

                // 55136?
            }

            // Set the movement costs
            var baseMovementCosts = new int[sbyte.MaxValue];
            baseMovementCosts[(sbyte)'A'] = 1;
            baseMovementCosts[(sbyte)'B'] = 10;
            baseMovementCosts[(sbyte)'C'] = 100;
            baseMovementCosts[(sbyte)'D'] = 1000;

            // Do the search
            var visitedNodes = new HashSet<Node>();
            var minimumCost = int.MaxValue;
            var movesStack = new Stack<string>();
            RecursiveSearch(0);

            // Not 11352
            Console.WriteLine("Minimum Cost: {0}", minimumCost);

            void RecursiveSearch(int currentCost)
            {
                // Check if all actors are accounted for
                if (!nodes.Any(n => n.IsOccupied & !n.IsOccupiedByActorAtTheirDestination))
                {
                    if (currentCost < minimumCost)
                    {
                        minimumCost = currentCost;

                        // Print the state
                        var sb = new StringBuilder($"State for cost: {minimumCost}\n");
                        for (int i = 0; i < nodes.Length; i++)
                        {
                            sb.AppendLine($"Node {i}, Occupier: {nodes[i].OccupierActorId}");
                        }
                        foreach (var move in movesStack.Reverse())
                        {
                            sb.AppendLine(move);
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    return;
                }
                else if (currentCost >= minimumCost)
                {
                    // More expensive than current minimum cost, so ignore
                    return;
                }

                var nodesToMove = GetNodesWithActorsThatNeedToBeMoved();

                // Find a node with an actor to move
                foreach (var node in nodesToMove)
                {
                    // Find a node for the actor to move to
                    var reachableNodes = GetReachableNodesForActor(node);

                    // Remove from the current node
                    var actorId = node.OccupierActorId;
                    node.OccupierActorId = sbyte.MinValue;

                    // Visit each reachable node
                    foreach (var reachableNodeAndDistance in reachableNodes)
                    {
                        var reachableNode = reachableNodeAndDistance.node;
                        var distance = reachableNodeAndDistance.distance;

                        // Add the actor to this node
                        reachableNode.OccupierActorId = actorId;

                        // Get the cost of moving to this node
                        var movementCost = distance * baseMovementCosts[actorId];

                        movesStack.Push($"{(char)actorId} from {node.Id} to {reachableNode.Id} (costs {movementCost})");

                        // Recurse to the next step
                        RecursiveSearch(currentCost + movementCost);

                        movesStack.Pop();

                        // Remove the actor from this node
                        reachableNode.OccupierActorId = sbyte.MinValue;

                    }

                    // Add back to the current node for the recursive search
                    node.OccupierActorId = actorId;
                }
            }

            List<Node> GetNodesWithActorsThatNeedToBeMoved()
            {
                var nodesWithActors = new List<Node>();
                foreach (var node in nodes)
                {
                    if (!node.IsOccupied)
                    {
                        continue;
                    }

                    var nodeDoesntNeedToBeMoved =
                        node.IsOccupiedByActorAtTheirDestination &&
                        shouldDestinationNodeBeFilled[node]();
                    if (!nodeDoesntNeedToBeMoved)
                    {
                        nodesWithActors.Add(node);
                    }
                }
                return nodesWithActors;
            }

            List<(Node node, int distance)> GetReachableNodesForActor(Node node)
            {
                visitedNodes.Clear();
                var reachableNodesBuffer = new List<(Node node, int distance)>();
                GetReachableNodesForActorRecursive(
                    node,
                    node.OccupierActorId, node.IsHallway,
                    0,
                    reachableNodesBuffer);
                return reachableNodesBuffer;
            }
            void GetReachableNodesForActorRecursive(
                Node node,
                sbyte actorId, bool isActorInHallway,
                int distance, List<(Node node, int distance)> reachableNodesBuffer)
            {
                // Increase the distance
                distance++;

                // Add as a visited node
                visitedNodes.Add(node);

                // Check each neighbour
                foreach (var neighbourNode in node.Neighbours)
                {
                    // Ignore if this node is occupied or is the previous node
                    if (neighbourNode.IsOccupied | visitedNodes.Contains(neighbourNode))
                    {
                        continue;
                    }

                    // Add if a valid destination node
                    if (neighbourNode.IsDestination &
                        neighbourNode.DestinationActorId == actorId)
                    {
                        if (shouldDestinationNodeBeFilled[neighbourNode]())
                        {
                            reachableNodesBuffer.Add((neighbourNode, distance));
                        }
                    }
                    // Add if a valid hallway
                    else if (neighbourNode.IsHallway & neighbourNode.CanStop & !isActorInHallway)
                    {
                        reachableNodesBuffer.Add((neighbourNode, distance));
                    }

                    // Visit this neighbour
                    GetReachableNodesForActorRecursive(neighbourNode,
                        actorId, isActorInHallway,
                        distance,
                        reachableNodesBuffer);
                }

                // Remove from visited nodes
                visitedNodes.Remove(node);
            }
        }

        private class Node
        {
            public readonly int Id;

            public readonly bool IsHallway;
            public readonly bool CanStop;
            public readonly sbyte DestinationActorId;

            public sbyte OccupierActorId;

            public bool IsOccupied => OccupierActorId != sbyte.MinValue;

            public bool IsDestination => !IsHallway;

            public bool IsDeadEnd => Neighbours.Length == 1;

            public bool IsOccupiedByActorAtTheirDestination =>
                IsOccupied &
                IsDestination &
                OccupierActorId == DestinationActorId;

            public Node[] Neighbours { get; private set; }

            public Node(int id, bool isHallway, bool canStop)
            {
                Id = id;
                IsHallway = isHallway;
                CanStop = canStop;
                DestinationActorId = sbyte.MinValue;

                OccupierActorId = sbyte.MinValue;
            }

            public Node(int id, sbyte destinationId)
            {
                Id = id;
                DestinationActorId = destinationId;
                IsHallway = false;
                CanStop = true;

                OccupierActorId = sbyte.MinValue;
            }

            public void SetNeighbours(params Node[] neighbours)
            {
                Neighbours = neighbours;
            }
        }
    }
}
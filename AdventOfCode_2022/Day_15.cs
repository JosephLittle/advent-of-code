﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(15)]
[SolutionInput("Inputs/Day_15_Test.txt", Problem1Solution = "26", Problem2Solution = "56000011")]
[SolutionInput("Inputs/Day_15.txt", Problem1Solution = "5147333", Problem2Solution = "13734006908372", Benchmark = true)]
public unsafe class Day_15 : Solution
{
    private readonly record struct Range(int Start, int End)
    {
        public int Width => (End - Start) + 1;

        public bool IntersectsWith(Range other)
        {
            var thisWidth = this.Width;
            var otherWidth = other.Width;
            return (Math.Abs(
                (this.Start + (thisWidth / 2)) - 
                (other.Start + (otherWidth / 2))) * 2) < (thisWidth + otherWidth);
        }
    }

    private readonly record struct Sensor(int X, int Y, int Distance);

    public Day_15(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Chose the sample row based on the input (test input has a length of 750)
        var sampleRow = Input.Raw.Length == 750 ? 10 : 2000000;

        var sensorXRangesOnSampleRow = new List<Range>(inputLines.Length);
        var beaconXPositionsOnSampleRow = new HashSet<int>(inputLines.Length);

        // Loop over every input line
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var line = inputLines[lineIndex];
            var characterIndex = 12;

            // Parse the sensor coordinates
            var sensorX = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ',');
            characterIndex += 4;
            var sensorY = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ':');

            // Parse the beacon coordinates
            characterIndex += 25;
            var beaconX = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ',');
            characterIndex += 4;
            var beaconY = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ' ');

            // Get the manhatten distance to the beacon
            var manhattenY = Math.Abs(sensorY - beaconY);
            var manhattenX = Math.Abs(sensorX - beaconX);
            var manhattenDistance = manhattenX + manhattenY;
            AddOrIntersectSensorRangeForRow(
                sampleRow, 
                new Sensor(sensorX, sensorY, manhattenDistance),
                sensorXRangesOnSampleRow);

            // Check to see whether we need to add the beacon position
            if (beaconY == sampleRow)
            {
                beaconXPositionsOnSampleRow.Add(beaconX);
            }
        }

        // Sum the widths of the ranges
        var sensorXRangesSum = sensorXRangesOnSampleRow.Sum(range => range.Width);

        // Remove the beacons on the sample row
        foreach (var beaconX in beaconXPositionsOnSampleRow)
        {
            foreach (var range in sensorXRangesOnSampleRow)
            {
                if (beaconX >= range.Start & beaconX <= range.End)
                {
                    sensorXRangesSum--;
                    break;
                }
            }
        }

        return sensorXRangesSum.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        // Chose the search area based on the input (test input has a length of 750)
        var searchAreaWidth = 4_000_000;
        var searchAreaHeight = 4_000_000;
        if (Input.Raw.Length == 750)
        {
            searchAreaWidth = 20;
            searchAreaHeight = 20;
        }

        var sensors = new List<Sensor>(inputLines.Length);

        // Loop over every input line
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var line = inputLines[lineIndex];
            var characterIndex = 12;

            // Parse the sensor coordinates
            var sensorX = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ',');
            characterIndex += 4;
            var sensorY = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ':');

            // Parse the beacon coordinates
            characterIndex += 25;
            var beaconX = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ',');
            characterIndex += 4;
            var beaconY = (int)Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ' ');

            // Get the manhatten distance to the beacon
            var manhattenY = Math.Abs(sensorY - beaconY);
            var manhattenX = Math.Abs(sensorX - beaconX);
            var manhattenDistance = manhattenX + manhattenY;

            // Add to sensors
            sensors.Add(new Sensor(sensorX, sensorY, manhattenDistance));
        }

        // Loop over each row in the search area, looking for the row that our sensors don't fully cover
        var sensorXRangesForRow = new List<Range>(inputLines.Length);
        long missingBeaconX = 0;
        long missingBeaconY = 0;
        for (int y = 0; y < searchAreaHeight; y++)
        {
            sensorXRangesForRow.Clear();

            // Add the ranges for each sensor
            for (int i = 0; i < sensors.Count; i++)
            {
                AddOrIntersectSensorRangeForRow(y, sensors[i], sensorXRangesForRow);
            }

            // Check whether there are any ranges that do not cover the full search area
            for (int i = 0; i < sensorXRangesForRow.Count; i++)
            {
                var range = sensorXRangesForRow[i];
                if (range.Start > 0 | range.End < searchAreaWidth)
                {
                    // Range doesn't cover the full search area, so we know it contains our missing beacon
                    missingBeaconY = y;

                    // Find the x position of the beacon
                    if (range.Start <= 0)
                    {
                        missingBeaconX = range.End + 1;
                    }
                    else
                    {
                        missingBeaconX = range.Start - 1;
                    }

                    // Goto the 
                    goto SearchEnd;
                }
            }
        }
        SearchEnd:

        // Return the tuning frequency
        return ((missingBeaconX * 4_000_000) + missingBeaconY).ToString();
    }

    private static void AddOrIntersectSensorRangeForRow(
        int rowIndex,
        Sensor sensor,
        List<Range> sensorXRangesForRow)
    {
        // Check whether this sensor's information covers our sample row
        var sensorIsAboveSampleRowAndCoversIt = sensor.Y <= rowIndex & sensor.Y + sensor.Distance >= rowIndex;
        var sensorIsBelowSampleRowAndCoversIt = sensor.Y >= rowIndex & sensor.Y - sensor.Distance <= rowIndex;
        if (!(sensorIsAboveSampleRowAndCoversIt | sensorIsBelowSampleRowAndCoversIt))
        {
            return;
        }

        // Get the distance to the sample row from the edge so we can calculate how many cells are on it
        var distanceToSampleRowFromEdge = sensorIsAboveSampleRowAndCoversIt ?
            sensor.Distance - (rowIndex - sensor.Y) :
            sensor.Distance - (sensor.Y - rowIndex);

        // Get the range of cells
        var sensorXStart = sensor.X - distanceToSampleRowFromEdge;
        var sensorXEnd = sensor.X + distanceToSampleRowFromEdge;
        var range = new Range(sensorXStart, sensorXEnd);

        // Loop over the existing ranges and intersect with them
        for (int i = sensorXRangesForRow.Count - 1; i >= 0; i--)
        {
            var existingRange = sensorXRangesForRow[i];

            // Check for an intersection
            if (range.IntersectsWith(existingRange))
            {
                // Create a new range which is a union of the two
                var unionStart = range.Start < existingRange.Start ? range.Start : existingRange.Start;
                var unionEnd = range.End > existingRange.End ? range.End : existingRange.End;
                range = new Range(unionStart, unionEnd);

                // Remove the range that we intersected with
                sensorXRangesForRow.RemoveAt(i);
            }
        }

        // Add the new range
        sensorXRangesForRow.Add(range);
    }
}

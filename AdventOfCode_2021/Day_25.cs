﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public class Day_25
    {
        private const int Cell_Empty = 0;
        private const int Cell_Right = 1;
        private const int Cell_Down = 2;

        public static void PartOne()
        {
            // Process the input
            var data = Data;

            // Split by lines
            var lines = data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            // Get the width and height
            var height = lines.Length;
            var width = lines[0].Length;

            // Create the world and populate it, adding each mover to the list of previous (inital) movers
            var previousMovers = new List<(int X, int Y)>();
            var world = new byte[height][];
            for (int y = 0; y < height; y++)
            {
                var line = lines[y];
                var row = new byte[width];
                world[y] = row;

                for (int x = 0; x < width; x++)
                {
                    var cell = line[x];
                    if (cell == '>')
                    {
                        row[x] = Cell_Right;
                        previousMovers.Add((x, y));
                    }
                    else if (cell == 'v')
                    {
                        row[x] = Cell_Down;
                        previousMovers.Add((x, y));
                    }
                }
            }
            int GetXToRight(int x) => (x + 1) % width;
            int GetXToLeft(int x) => (x + (width - 1)) % width;
            int GetYBelow(int y) => (y + 1) % height;
            int GetYAbove(int y) => (y + (height - 1)) % height;

            var validRightMovers = new Queue<(int X, int Y)>(previousMovers.Count);
            var validDownMovers = new Queue<(int X, int Y)>(previousMovers.Count);
            var validMoversAddedThisStep = new HashSet<(int X, int Y)>(previousMovers.Count);

            // Local methods to add valid movers
            void AddValidRightMover((int X, int Y) cell)
            {
                if (validMoversAddedThisStep.Add(cell))
                {
                    validRightMovers.Enqueue(cell);

                    // If the cell above contains a down mover, then it will also be valid to move
                    if (world[GetYAbove(cell.Y)][cell.X] == Cell_Down)
                    {
                        AddValidDownMover((cell.X, GetYAbove(cell.Y)));
                    }
                }
            }
            void AddValidDownMover((int X, int Y) cell)
            {
                if (validMoversAddedThisStep.Add(cell))
                {
                    validDownMovers.Enqueue(cell);
                }
            }

            // Run until there are no more valid movers
            var stepCounter = 0;
            do
            {
#if DEBUG
                Console.WriteLine($"Press any key to execute step {stepCounter}");
                Console.ReadKey();
#endif
                stepCounter++;

                // Get the counts to process before we add any more
                var rightMoversToMove = validRightMovers.Count;
                var downMoversToMove = validDownMovers.Count;

                // Clear the movers that were added this step
                validMoversAddedThisStep.Clear();

                // Move the right movers
                while (rightMoversToMove-- > 0)
                {
                    var moveFrom = validRightMovers.Dequeue();

                    var row = world[moveFrom.Y];
                    var rowAbove = world[GetYAbove(moveFrom.Y)];

                    // Empty the cell
                    Debug.Assert(row[moveFrom.X] != Cell_Empty);
                    row[moveFrom.X] = Cell_Empty;

                    // Add the mover to the right cell
                    var moveTo = (X: GetXToRight(moveFrom.X), Y: moveFrom.Y);
                    row[moveTo.X] = Cell_Right;
                    previousMovers.Add(moveTo);

                    // If the cell to the left has a right mover, and the cell above doesn't have
                    // a down mover, then add that right mover
                    if (row[GetXToLeft(moveFrom.X)] == Cell_Right &
                        rowAbove[moveFrom.X] != Cell_Down)
                    {
                        AddValidRightMover((GetXToLeft(moveFrom.X), moveFrom.Y));
                    }
                }

                // Move the down movers
                while (downMoversToMove-- > 0)
                {
                    var moveFrom = validDownMovers.Dequeue();

                    // Empty the cell
                    var row = world[moveFrom.Y];
                    Debug.Assert(row[moveFrom.X] != Cell_Empty);
                    row[moveFrom.X] = Cell_Empty;

                    // Add the mover to the below cell 
                    var moveTo = (X: moveFrom.X, Y: GetYBelow(moveFrom.Y));
                    world[moveTo.Y][moveTo.X] = Cell_Down;
                    previousMovers.Add(moveTo);

                    // If the cell to the left has a right mover then add that as a valid right mover...
                    if (row[GetXToLeft(moveFrom.X)] == Cell_Right)
                    {
                        AddValidRightMover((GetXToLeft(moveFrom.X), moveFrom.Y));
                    }

                    // ...or if the cell above has a down mover, and the cell to the left doesn't have
                    // a right mover, then add that down mover
                    else if (world[GetYAbove(moveFrom.Y)][moveFrom.X] == Cell_Down &
                        row[GetXToLeft(moveFrom.X)] != Cell_Right)
                    {
                        AddValidDownMover((moveFrom.X, GetYAbove(moveFrom.Y)));
                    }
                }

                // Check the previous movers to see if they can move again
                for (int i = 0; i < previousMovers.Count; i++)
                {
                    var previousMover = previousMovers[i];

                    var row = world[previousMover.Y];
                    var rowBelow = world[GetYBelow(previousMover.Y)];
                    var cell = row[previousMover.X];

                    if (cell == Cell_Right)
                    {
                        // Right movers only need the cell to the right to be empty
                        if (row[GetXToRight(previousMover.X)] == Cell_Empty)
                        {
                            AddValidRightMover(previousMover);
                        }
                    }
                    else if (cell == Cell_Down)
                    {
                        // Down movers need the cell below to be empty and below-left to not have a right mover...
                        var cellBelow = rowBelow[previousMover.X];
                        if (cellBelow == Cell_Empty &
                            rowBelow[GetXToLeft(previousMover.X)] != Cell_Right)
                        {
                            AddValidDownMover(previousMover);
                        }

                        // ...or if the cell below has a right mover, the cell below-right must be empty...
                        else if (cellBelow == Cell_Right &
                            rowBelow[GetXToRight(previousMover.X)] == Cell_Empty)
                        {
                            AddValidDownMover(previousMover);
                        }
                    }
                }
                previousMovers.Clear();

                // Print the world after this step
                PrintWorld(world, validRightMovers, validDownMovers);
            }
            while (validRightMovers.Count > 0 | validDownMovers.Count > 0);

            Console.WriteLine($"No more movers after step {stepCounter}");
        }

        [Conditional("DEBUG")]
        private static void PrintWorld(
            byte[][] world, 
            Queue<(int X, int Y)> validRightMovers, 
            Queue<(int X, int Y)> validDownMovers)
        {
            Console.WriteLine(" " + string.Join("", Enumerable.Range(0, world[0].Length)));
            for (int y = 0; y < world.Length; y++)
            {
                Console.Write(y);
                for (int x = 0; x < world[0].Length; x++)
                {
                    var cell = world[y][x];
                    Console.Write(
                        cell == Cell_Empty ? "." : 
                        cell == Cell_Right ? 
                        validRightMovers.Contains((x, y)) ? ">" : ">" : 
                        validDownMovers.Contains((x, y)) ? "v" : "v");
                }
                Console.WriteLine();
            }
        }

        private static readonly string DataTest = @"
v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>";

        private static readonly string Data = @"v>...>>>.>v.v..v.>.>.>>>..v..v.>.v.vv>.....>>>>v...>>.>..v>.>vv.>.v.>vv..v.v.v.>.v.....v..v>v>.v....vvvv>v.v.v>v.v>vv..>.v...>>>vv.v.v.>>..
v>.>.v....>...v..v.>.>..>.v.>..>vv.....>..>..vv..vvvvvv.>v>.>.>.>vv....>.v.vv...>..v.....v......v>.v..>v.v>..>.>.v.vvvv>..vv.>v>v.v.vv.v.vv
...vv...vv.>.v..v..>.>....v.>..>...v>...v>...vv>......>>vvv>.>v>.>>vvvv>...v..>>.v..v.>vv..........>v>v>...>.>v.>v>.v..>....v..>>>>...vv..>
>>...>.>....vv.v..>>..v...>>...>.v.>vvv...>v..>>v.vv>..>...v..v.v>vv..>vv..>.v..v..>v.v..>........>.v>v..vvv.v...vvv.>..vvv.>>.v..v.....vv.
>v.>.vv>>...>>v...>..>.>.v.v>.v..v.>>.v.>.v..>.v.v.vv>.v.vv>>vv..>..>..>v.v.....>.v..v>>.v.>vv...>.>.>>>.vv>>>>...v.>..vv.v...v......v...v>
.v...v>.>v.v..v.v...v>v.v>vvv>..>..vv.vv>v.v.....vvv....vv>....>.....v>v.>.v>..v...>>v>v....>>v.>.>v..v>.v.v.vv>v.>v.>v..vv..>v.....>..>..v
.vv.>.>>....>v>v.......>..>.>>......v.v..>>...v.>v>..v>.>..>.>>vv>>...>>..v.v.v....v.>..v..>....>.........vvv...>.>.>..v>>v>..v>.v..v>.v>>.
.v>...vv>v.>v...>vv.v.>.>v.v..>.v.>>>..v...>>>.>.v...........>v..>v>v.>vv....vvv.v.v>..>v>.>>..vv.>..v>>v.v......>.>>vv>.v>v.v.>>..>v>>>v.v
.v......v>>>v>v..v.>>v>.v..v>.>>>.v.>>>....v.....v..v..>.>>>v.>v......v....>v.>...v>.v.>.>v..>>.v.>v>v>...>vvvv...>v>..>vv.>....>.v.v.v....
v....>.>v>.>..>...>v..v.>v..vv.>.>v>vv..>vv>>>>>v.v>..>v..>.>...v>v..>>>>.>.v.v..>.>..v.v>...>..>.>>v..>v..>vv>.v>>...>>>.>vv>v...>...>>>>.
>.>v.v.vv>v..vv.v..v>v..v>......>v.>>>.>>>v..v.v.v>..v.vv....v..>v.v>v>.>.>v.v>>.>.v....v.....>v.>v.vv.>......v...v...>...>vvv.......>>.v..
.v..v.v.>>v...v>>...>vv.>.vvv>.v>vv.>vv>.>v.v>>>>>....>v.v>vvv>v.v>.>..>...>..>.vv..v>>.>...v>v..v...v.v>v.>>.>.v>....v.v.v..>>>.vvv>.vv>>>
.vvv>...vv.v>.>>v.>v>>>v>vv........>v..v.>..>...v..v..v..v>...v.>......>>v>>.>v..>>vv>....>.>vvv.vvv...v.>..v.>>.>>.>...>.vv.....v..>v>.v>>
>>>.v.v.v.v>.v>.>.>v.>.>.....>.v>.>..>.>..v.>>v..>...v...>>.....>v.>..vvv..>v..>...vv>..v>.v.v>>v.vv>.>v..>..>>.v>.v..>..v.>v>v.>v..>>>vv.v
.v.....>..>v>>vv...>.v.....vvv>.v>..vv.....>..>.............>..>.v>v>vv.>.>.>.v.>...>>.>>.>.>v..>>v.>...>v...v>.v..vv..>>v>.>..v>.vvv...v.v
v.>v..>v>.>v>v...vv.vvvvv.>.vvv...v..>>v.......>vv.v.vv.v.v.vv>..>>>v>>.v>.>>>.>vv.v>>v.vv..>>.v>.v>.v...v...v..>..v...v>v>.v...v.>>vv.v.>>
v>.v.v.>.>...>v.v..v..vv..vv>.v>.vv..>......>>vv>>..>..>v>.>>v.....vv>.>.>v>..>v.>.>>.....>.v>..v....v....>v.v>....v.>.v.v.vvv>.....>v.>.v.
....>>>v.>>.v.>..v...>.v.>..>.>v.>....>v..v.>....v...>...v....>.v>>v.>>v>vv..v>v....>v...>>v.v>>....>..>.vv.v.vv>v>>>>>.v.>v>v.v..>..>v....
...>>v>v.>>...>..vvvvv>.v.vv.>.v.>>v>..v.vv>v.>vv.....>.vvv>>>v.v..vvvvvvv.>>vvv>v...>..>.v>>v>...vv>..>..>......v..>.v..>v>>.>>>>..v.>>v.v
..vvvv..v.>>.>.>>>.v.v..v.>v.>.v>v.v>..>.v.v.v.>..v>>.>...vvv.v..>...v....v.v>vv>...v.vv>..>vv.v>v>v...>.vv....>.v>...>v...>>>>..>>....vvvv
v..v.>>>>v..>vvvv>.>v.>v...>>..>v>v...v.>......>>>.......vv.vv>..>...v..>vvv.>.>.v>v.v>.v>......>...v.>..>.v.>.>v>..>....vv.>....vvv...v...
.v.v>....>.>..>...v>..v>>..>>.v.v>v.>.>..>.>.v....>v......vvv>..>>.....>.v..v>.v>v..>.v..v..vvv.v>..>.>.v...>..vv.>v.>v>..v>..>...>.>>.vv.>
v.vv>v.vv...>..>..>..>..>vv>v.>..>.>..>v..>>v>.vvvv>v.>v>vv.>v...vv.v.v.v...vv>.>.....vvvv..>v..v..vv.>v>v.v.>..>.v..>.....vv>>.....vv>...v
..v..v.>>.>v..v>..>...v...>v>>.vv>.>>.v..v>...>..>>..vv.>.v.>>..v>vv>..vv>vv>.v>...vv...v.>>.v..v>.v>>.v.....v..vv>>v>>v>...>v...>.....>.>>
..v....>vv>v.vv..........vv..>>.vv......v..vvv..v>>>v.v.>>.v>>..v..>..vv>v..>>>..>......>v>vv>.>>v....v.v>>.v.>....>>..>v>>.>...>..>vv..>v>
>...>.>>>.>.v..>v>>..v.....>>......v>>..v....>.>v>.>v.>>>vv....>v.v...v..>.>.v.v..>vv>.v>>..v.>vv>.>>...v..>vv.v>.>>.....>.v.v..v...v..>v>>
v....v>.>>>vv>..v>>v....>>>..vvv.>.>..vv...v..>>v.>...>>....>>>.>.>.v.v.......>.v..vv>vv...>v.>v>v.v>v.>>v>v>v>.....>.v>..>...>v>v>...>.v.v
v...>.>.....>.v>vv>>.v..vv..v.v.v.vvvvv.v...v>>.v>...v.v....vv....v>v.>v.>v..>..v.v>>..v>vv>vv.v.>>>.>...v.v..>.v>v>v..v>....v>>..vv>.>>.>v
.v>>..v.vvv......vv...v>.>>v....v....v...>>.>v.>...v.v>>..>>.>v..>.v.........vv.>>>>vv.>..vvv...v....>.>v..v.....v.v..v>.v.............vv>v
v..v.vv..>>....v.>...vvvvvv.v>.....>..v.>>>.v.v>..v.>vv..>.....>v.>v.v.v>......v..>>.>v...vvv>.v>v..>>>>v.>...>...v>.>v.>.>>.....vvv....vv.
....>>..>...>v.>>...v..v.>..>vvv>>v.v>>.vv>...v>....>vv>.vv.>v>>>v..>>>..>.>v..>>v.>vv.>......v>>v.vvv.v.v>.v.v...vv>>vv..>....v>....vv..vv
.>vv>.....vv>.v...>>..>>vv.>>v>...>v..>>.v>v....>....v......>v..v>...v.vv.vv..>v.>.v.>>...>.v..v.v.vv..>.>>vv.>>...>.>..>.v>..v>>v.vv.v.vvv
v.vv.v>>vv>vvv..v.......>>....>v..v>..v.v>v>.>v.v..>v>.v>v.v...>...vv...>.>.>>......v...>...v>..>.v>..vvv>.vvv>vv>>>>...v.vv.>.v.v..v.v>.>v
..>>>..>>v.v...>..>..vvv.v.>>.>.>.v.v..vv>>..>>.v.v.......vvv.>.v..>v>........>.v.>v...>..v>v...>.......>>..>v>.....>vv.v>v.v..>>...v....>.
v.>>>>..>..>>>.vv..>.>..vv..vv..v...>v>...v.>v.>.vv>..v>.....>.>v>.vvv.>.>>v.>..v..v.v...vv.>...>..>>>.>..>.v.>.>..v.v..v>>.v...>v>>.>.v..v
>v.v>v>.v>>v>....v.v>v.>.v..>>....v>v.>>>.vv>.vv....>..v>.vv.>.vvv.v..>....v.......vvv.v>v>v..v..v....v>v>v>.v..v.>..>....>>>..>..v.v.>...v
>>...v>vv>>...vv>>..>.vvvvv>.>.vv.>v>.>.>>>v..>>v..>vv.>>>>..>v...v.v>.......>vv>>...v>.v..>>.>.>v>..>>.....>.>..>vv..>.>..>vvv>.vv.vv>v.>.
...>.>v.>>vvv>vv>>...v.>v...vv>.>.>v.>....>...v.>v.vv.>vv>.>.>.>...>...>>>.v......>.....v.vv.....v.>>..>..v...vvv.>...v..vv>v>..v.>..v..>.v
v>v>>..v>v...>.>vv.>...>>v.>vvv..vv>.>v.....v>v.>v..>v...>..>.>.>v.>.....>..>vv>v..>...v.vv>.v>>v.>v....v>.....v......>..>.........>>...>v.
v.>.>>>.>v>.......v..>v>v....>..v....v.>.....vv.>.>.>>>v..>v..>.v.vv>...v>vv.>v.>...>>..>>>>v..>v.....>.>>>v..>.>>v.>>>......v.>.>vv..vv.>.
.v..v>vv..>..>.vv..v>.>..>>...>>v>..v.....v....v..v.>.v....vv>.v>...v>.vv.>..v>..>vvv.v>.>.v>..>.>...>..v>.vv.>.>.v>v>v..>.vv.>.>v..v>.>>>.
v....>>.>.vv..>v>.v.>..v>.>vv..>.>>.vv.>.v...>v.>.>v....v...vv>..>.>.vv>vv..>...v.>.....>.>..v...>>..vv......>v>vv>v>v..>v>>.>v..v...v..>..
>...>.>v>...>v.>vv.v>.>v.....v.>.>...v>v.v....>.v>..>..v.v>.....vv.vv.>...>vv.>..>.v>.v.....>>.>v.vv>>..v>..>...>v>vv.>v>.>..v>v...>>v.v..v
v>>v>>.vv..vv>vvv.>vv.>.v..vvvv.v>v>..v..vvv>.....>..>.>v..>....>.v>v..>>>..vv.v>v..>v.>>>>.>.vv.v..vvv>>.vv...>.>.>....>>v..>.v>..vvvv>..v
v..>>>.vv....v.v........v>>>..>>..v>..v...>..v>v>.>v......v..vv.v...v.v..>vv>>vv>v>.>>v>.>>>>.v>.....vv..v..>v..>..>vv>>.vv....>>.vv>v>....
.>...vvv......vvv....v...>....v.v.>>.v...>>v>...v>v>v>v.>>v>v>vv.>v.>>>...>.>v>v.v..v.vvv..>vvv..v.>>..vv.......>vvv.vv.vv....v.vvv.>v>..>v
..>.vv>...>>.v.v.v.>.>>..v..v.>..>v.v.v..v>>.>.vv>...vv>>vv.....>vv.>>>vv>>>>>.>.v>........vv.v>>..v.v>..v....>>....>....>>....>v..v>v>>vv.
.....v.v..v....>v...v.v>>.>...>.>>v.>..>>.>>...>>>v.v...v>>>..>v.v...vv.vv..>.>.>>>.v>..>>v>v>........>.vv.>.>v..>>>..>>.>v..>...vv.>..>v..
.v...v>..>.>v.v.v>>.v..>v>>v>>vv.>vv.v.v>v.v...>>.>.>.>>.>v.>..>>>v..v>>.......v>...vv.....>.>v.>v>v...v.>...>>>.v....>.>v....>.>..>..>>.v.
>>v>vv.>.v>.>v.vv>....v>>...v.vv..v....vv>.v....v>.>...v>.vv.v.>.v.>>>.vv.v>>..v.>.....>.>.v>vvv>>vv.v...v...v.>.v..>.v..>.>...v.>......>vv
.v..v..>.vv>..>vv..v>>...v>v.>.>..v>vv.v>vv.vvv.v>...v>v>...v...>v>......>.>v>>v..v.v.>v..vv......v.>.>....v.>.v..v>..>..>.vv..v..>>...>.vv
v>.v...>.v..>.>>.v..>v>.v.>v.v.>>>.v>.v>v.vv.v.>.>.>.v..v.v....v..>.v.v..v..>>.>v.>...v....>.vv..vv.v.>.>..vv>..v.v..v.>.>.v>.v.vvvvv..v.v>
...>.vvvv..v.>v...>v.>v.>...>>>.v>.>.>v>...>>>>>>.v.>>.......v>>v..v.vv.vvv.>.vvvv.vv>.>v>..v.>...vv.vvvv>>>v.>..>v.>...v>vvv.v.....v.vvvv.
>..>..v......>.>>...>v.>v....>.v>..>...>.>...>.>.vv.>>.v.>>.>>>>.vv.vv....>..vvvv...v>.>.>>.>>.>>.v..v>.>..>.vv...>....v>v..>.....v>..v>..>
>..>....v.....v..v.....v...v.>..v....v......>v>..v>.....>>>>..vv.v...>>>....v.>>v....v.v.>>...>>.>>...v..>>>.v..>>.>..>.v.>.vv..v..>.v>...>
.v>>.>>..>vvv.v..>v>..>.v.v..>v......v>>.>>v>>>...>..>>v.vvvv.>.....v..vv.v...>vv>>>v.v..v>..>.v>.>...>>....v>.>..>>>>>.v>>>vv..>>....v.v.>
..v.vv...>.vvv..v.>...>v.......>..v>>>>>.v.>......>v.v>..v.>.>.>>>v......>.v>..v.v>.>.v..v>>..>>>v>>.v..>..v.>...v.....>.vv>...v...>...v...
.v>>v>vv>...>..>.v>.v....v>.vv.v...>.>..>v.....>...>>.>...>>v>vvv.v>>..>..v>>v>..>..vv..>..>v...v...v>..>>..v.v.>vvv....v.v>vvv..>>vv...v.v
.>v>..v.>>.vv....v>..>>>>>>.>.>>..v>.v>..>.....v>>v..v..>..v.v>>vvv..>..>.>.>.vvv.>..vvv...vv>>>..>>..v>>>>>vvv.v.>>...>....vv..vv.v>..vvv>
..v..>.v>>...>.>>>.>v>>v.v.>..>>..>>>v.....>.>>>v>.v.>.v....>v.>>v...>v.v.>..>.>.v>vvvv...vvvvv...v..>>.v..v>......>.>>>...v.>.v...>.....vv
vvvv.v...>v....>>>>.....vv>>>>..vvv.v..v.vv....>v.v..vvv.v..>v>>..v...v.v.>v.vv.v.>>vv>..>..v..>>...v>..v>>>.vv.v.vv..v..vv......v>v.>vv...
.v>>>..vv..>..>..v..>>>vv.>.>.v.>>v..........vv.>..>.>>.v.....>v>v.v.....>.v.v..vvvv>>...>>..v.v>v.v.v...v.....>>>..v.vv...>>...v>v.>>>>.v.
..vvv.>...vv..>vvv.v>>.vv>vv.v..vv....>...>v>>.>.>...v>..>.vv...>>...>v.>...v.>v......vv...v>>>.>.>...>v....vvv>..>>>>v>v>vv>vv..>.v>.v>>..
v>.>v......v...>v.>>v.v>>...>v>>..>.>...>..v.>.v..v.>.>v>vv>>.v..v.>...>v..v.....v>v...>vv.>....>v....v..>v.>vv..>>v..>>.v..>>.v>..>v..v>..
v>.vv.....v>>vv.>>...v..>..>>.vv.v..vv.>vvvv...v.vvv>v>>.....>vvv..v>.v.>.......>>vv>...vvv>v>vv..>vvv...>.vv>>.v.>>.v..>>.....v....>.>>..v
>..>.>v.v..>...v..>>..>.v.>v..v.>.v>>.>.>v>vv>.v.>>..vv>v>..>vv.......>......v>.>>>v...>..>.v>.>v.v..v.>v....>.>.v>.v>v>>v.v.v>>>.vv...vvv>
......v.v...>...v.>>.>v..>.>v....>.>>>...v>v...v.vv.vv>.v..v>>.>v..vv...>.vvvv...v.>>>.v>>.v..>.v>v..>..>.>.vv..v>.>.v.v>>>v>.>..>.>..>>>..
v....v.vv..>>>>..vv>>v...v>.>vv.>v>>.>..>.>>>...v..v.v.>v....>>v.vvv.v.>>...v.v>.vv..v.v.v.>.>>....vv>v.>>.>.>.v>.v>>>.....>.v>.>v>...>.>..
...>>..vv.>.v.v....>>..>.v..>v>.v...>...>v>v>v.>.v.vv>v....>v....>>>v..>>..>....>.>>>v.>.>...v..>>>.>vv..v>v....>....>..vvv>v>v.v>v..vv....
>vv>..>.vv.>>.v...>.>.>>.>v..v>vv...>v....v.vv>....v>...v.vv>...>>>.vv...>...v....>.v....>...v.vv..>.>v..>...>>>..>.v.v.v>>...>..>..>..v.>>
..vv>v>v>>>vvv>.vv>>vv..>>v>>..>v.>>.......>vv.v.vv....>v>..>.v>vv>>.vv.>>..vv.v>>>v.>...>....>>v>.......v>>>>.v>>>>>v.>>...>v.>vv.>.vv.>.v
>v>.v.>.>..>>>.v..>.v..>v..vvv.>..v>.>>.....v....vv.>..>.v.vv...v.v>.>.vv.>....>v..v>.v..>..>....>.v..v..>v>.>>..vv...>..v....>>v..>.v>>.v>
.>..>.....>vvv>.>..v>v.v..>vvv....>v>>>...>..>.>vvv..v.vv.>...>>v.....v.v..v>>>.v.>>vv..v.v..v......vv>v.>>.v.>v.>>>>..>v..v>.....v>.>v.v..
vv>.....>v>>v.>.>.>vv....v>..v.>.v.>vv>v......v.>>..>>.v>>>>.v>....v..>..v>.v.....>>.>....v.>...>v>>>>>.v.>.......>>v>...v..>>vv..>..>v.>vv
>>vv>..v.....v.v>vv.>..v>..vv>...>>>....>..v>v...>...vv.>>.>v>v>>..v.>...v...>.vvv.>>.>.v>v>.>>>...>..v.v>.vv>v.>.v..>...v..v..>..v..v.v>v>
v>....v.v>...vv...>v..v>v...>.>vvv....>.v...>>>v...>...v>>.v>v>>v>..>>>v.>..v.v.v>.v.vv>>..v.>.>..>>.v>v.>>>>...v>>.v.v>.>.v...>vv.vvvv.>v.
...vv...v.>.>.vvvv..v>.vv>.v.>.v>.>v>v......v...>v>>v...v>.>..>..>.>.>>>vv..>..>>.>v..>>>.>v>...>>>.>.>>v.v..v>...v>..v..v.>..vv.v.....>.>.
.>..>.>...........>.>.>v.>>>>v>.>v>.vv...v..>v.vv>..vv.>v.>>>..v.vv.....>>.v...>vv>.>>.v..v.....>v.....>vv.>vv...vv.vv>v.v....v......v..>vv
>v>..v>>>v>>vvv.....v..>>>v..>>>v..>>v>vv.>...v>.>>>.v.vv...v...v>>.....v.>vv..>..>>.vv.>..>......v.>....vv.>vv.....>>v...v>>v.vv>>..>.vv..
>.>v>>v..>.v..vv>.>....v..>>v....>...v>vv>.....vv.v...>v>.v>>..vvv......>...v...>v.v..>.>>.v>...>>>.vv.v>v...v.>...v..v.>v..>>v>>v..>...v..
>.>.>vvv>>.v...v..>v..>>.....>v.....>>vv.v>..>v...>v...>.v.v>>.v>......>>v...>..>.>......>.>..>...vvv>vv>.v.v.v>..vvv>.>.......v.v.>>>v>v>.
...v.v.>.v.>.>.v.>.>>v>v..>>..>>.vv..>>.>>.v>v>>.>...>>>.v.vv>..vv>...>>vv....v.>.v...>>..vv>.>..v>v...>..v..>>....>>>v.vv>>v..>.v....>.>.v
>>v.>>.v>>....>..vvv>v>>v..>vv....>.....vv.vvv>........>v.v.vv>>.v..>v.vv>vv.>>...>..>.v>.>.>>.v.v>v>...vv..v.v>..v>.v>....>vv>>....vv>...v
v.v...v.v.vvv>vv>.vvv.vv.>v>v.v....>.....>>..>.vv.>.>..v....v..v.v...vv.vvvvv......>v.vv..v>v>vv.v...v.>...v>>>v.v>v>.>..vvv.>.v.>v.>.>>v..
...>>>>.v..v.>..v.>v.v>..vv.>.>vv.>>.v>.>....v..v...>.>..vv.>..v.>.v>>...>.>...v.>>.>.vv>v>.>.>..>vv.>vv>...>.v.>.v..>>..>..v>vv.v.v.v.....
.>..vv...vvv>.....vvv>.>v.>vv.v..>.>vv.v.v..>>>..>v.v..vv..>..>v.v>>v...>>.>>.v>>v>..>...>>vv>>..v>......v>.>.v.v...v..v.v.v.v...vv..v>..v.
...v......vv>v.vv>>>.v.>.v.v.....v..>>v.>v.vv>v>>....>......v...>>..>....>>vvv.>v..>....>..v>>>>.>>v>v..>>>.>>v>>v....v.>v.vv>.>....v.>...>
..>>>>v>...v.v..vv>...v>>>>.>..v>.........v.>.>...v.>>...>..v>.>...v..vv.v>>.v..>.....>..>vv.v.v.>.>v>..v>>v.v>v>v>.....>vvv.>>>>>>vvv>.v.>
.v..v....v.vv.v.v..vv>..>>v....v.vv...>.>...>>.v.vv....v>.v.v.>>v>....vv>>..v...v....>.>>.>v......>>.v>v..>..v>vv.>..>>>....v..>...vvv.>>v>
..v>.v....v.>.>v>.>.v...vv>>..>.v...>>v.v...>>..vv>.....v.vv.>>>v>.>..v.>>v......vv>>v.>v..v>>..>..>>..>...>vv....v.vv..v>v.vv...>...>.>v>>
>.vvv.......>.>>...v>v..>.>>v..v.vvv..v.v..>..>>vv>.vv>..v..>...v.v....v.v........>v>>>.vv..v.....>.>v.>v.>v.v........>>v>.v>..>.>..>v>v...
....>vv...v.>>...vv>.....v....v>v.>..vv>.v.vv>>..v>v.v.v.v>vv..v..>v>.v....>v>...>v.....>v...v..v.>.>v>....>....v.v..>>v>>v..>>>>v>>>.>>...
.....v>v>.>.v...v.>>..v>>>>vvv.vv...vv>>.>..v..>v>..>..>..>vv>v....>>v..vv>..v>..>.v..>>vv>>v.v>...>>>.vv.>>.>..vvv>>v>..>v..v.>v.>..v.>...
v..>.>v>.v....v>>>.>...v...v.>>>>..vv...>.>>.>>v.>.>v..v>..>v>....>v>.v>>v.v..v>>.v.>...>..>v...>>>....>vv..v>>v.>>...>v.v.>v...>....>.>v..
...v>>.v.v.....v......>v.....vv>>.>.v.v>.>>>.vv>>.vv..>...>....v>..v>>>>.v..v>>>v>.v>>..>.>...>v...vvvv.>.v...>.>v.>v.>..vv.....>>>>..v..v.
..>>.>>..v>..>vv>>.>.v.vv....>v.v.>>>v>v.v>vv>>>vv...vv.>>...>.v.....v.>.>>.>.vv.v.>>v...vv..vvvv.>....v..>>.>>v.>.v>v.>...v.>.vv.v>>...v>v
..vvvv.v..>v.....>..v.>.>>v.v...>>..>.>.vv...v>v..>.>....>.v.v..v>.....vv...v....>....>...>v..vv>vv...v...>.>..>....>.v.>>v.v.>v.v>.vv>>vv>
.v..>vv>>>.v.>v..>v.>>.v.v...>.v..v.v.....>vv.v.>>vv...v>.>.>....vv.>..vvv.>...v>v>>.>...>>>>..v..v..vv...>.>.v..vv>.>v..>..v.....v.>...v.v
>.v...v.vv>>......>>>>v....>v.>vv>.>>.>v>v..v...>>v..>....v.v.....>...>v....vv.v>....>.>..v.vv..v>v>.>v....vv..v..>v........>.v>.v.........
v.>..vv>...>>.v.vv..>.>v>....v..>.vv..>..>>.>..>vv...v..vvvv>....v..>v.v.>.>.v>....v..>>v>>...v.>>v.v..v.v...>v.v>..v..v>.......>.v...v.vv.
v.>v>v>v>>>...v>.>>..v.>vv>.v..>.vv.>>v>v..>>vvv>.v.....>>.>v.v..>....v.>v.>v.....v>.v>>...v>..>..v.v....v..v...>.v..v....>>.>vvv>..>>.vv..
>v>>.>..v....>v.v...>>>v.>.>..v.>.vvv>v..vv.>vvv.v.>>>v>vvv.>>.>.v.......>>.>.>v>..vv>......v.>>>..v>vv..>..>>..v>vv>>vv.vv.vv.v...v...v.>>
.>.v..v.vv>.>.v..........vv...vvv.v.v.>........>....v>v.vvv.v..v>.....vvv>..v...>..>v>v>>v.>v>v..>.>v>v..v.>.v.>v.>..vvv>.>>>>.vv>>..>v>>.v
.....>vvvv..vv.vvv...vvv...vv>vv.v>>>.v>..v>.v...vv.vv>vv>.>vvvv...>vv...vv>.vv>>>..>.>>>.>vv..>...v.>...vv.>>>...v...vv.>vv.>vvv....>v>>.v
v>..>..v>>>v.>>v.vv>v>..vv>.>....>>>......>..>>..v.>..vv.v...v.>.vv.>>.>>.>..v.......>v.v.>.v.v>.v.v>>.v..vv>.vv.>>>v.v>...>...vv..v.>v>.v.
v.vvvvv..v.>.>.v.>..>>>..vv.>>>vv.......vv..v.....v.vv...v.v>.....>>.vv..>.>>.v..>v.v.v>.>>..v.>........>.v>..>v>>>>>>v.>..>..>v>.vv.vvv...
>..v>v.>>v>v.>..>>vvv>..>v>>v>..>.v.>>.vvv>.>vv.>vv.v........v>v.v..>...>v....v...>.v.>...>vv...v..vv........>...>v.v>..v..v>>..v.>>.v.vv>.
..v.v>.v.v.v>v....>.>>>.>.v>>..vvv>v>.v.>vv.>..>.v>v>v.>vv.vv.v.v.vv..v>.vv.vv.vv.>>...vvv.>..>.>v>......v.>.v>vv.>.v..vv>.v>.v>>.....v.>.>
v.v....>>..v...v>v>vvv....>>....>v>vv.vv>v.>>.vv..>.v.>....>>v..>>>vv..>.......>>....vv.>v>v.v.....v.>.>.>v>>>>..>>..v.>...>>>...vv>v>..>v.
.>.>..vv>>>...vvv.v>..>>v>v.v.v>vv..vv>v>>.vv....v>>v....v>>.vv>v...v..>v>v>.>.>.>>>....>.>.>..vv>v.v.>>...>v>..>.>vv.>..vv..>v..>v.v.....v
>v........>>v....v..>v.v..>.vv>..>v.v.v>>>>v>>vvv>.v>v.>.>.v.v.vvv..vv>..>..>v>.v>..>v.v...>.>.v>..>vvv.....v......>>>.>.v>.>vvv..v>.>v>vv>
..v.>>>>.>v.>>>.....>v>.>..vv..>>v..v.>.....>vv...>>..v..v.v..v>.>..>vv.>...v.v....v.>v.>.>...vvv.vv>.....v.....>.v>.v>.>..>....vv.>v.v....
.v>>.......v.v.v..>>..>v..>vv....>vvvvv.>.>..>...vv.>>v..vv....>v>.>>.>.vvv.>.v....vv>v.vv.>vv......>.>>>>.>.>vvv>vv.v.v..>.v..>..>.......>
.vv..>>..>....vv.vv..>.>.....>>.>.>..vv...>v>.vv>..>v.v>.>...>..v.vv..>.>v>>...>v..v...v...v>.>>>>..>.>>v....>.v..v.v.....>>v.....v.vv....v
..vv>vv...v.>>>v....v>.vv..v>v.>.v>.>v>>.>.v..>.vv...>.v.>.>.v..v.>..v.>>>..>..vv.>..>..>v>..>.>v...v>...>>>....>v...v.vv.>>.vv.>vv>v>>>.>>
>..vv>>vvv..>vvv..v>v.>...>..v>>vv.>v..>.v>.>.>.>v.>vvv>>.>.......v...>>v..>...vvvv>>v>.v..>>vvvvv.v..v>.>..>v....>..v>...>vv>>v.>vvv>...>.
v.v..>..>v...v..>>>v..v>.vvv....>v.v...v.v.>...vv.>v>.v>...>vv>vv.v>vv.v.v..vvv>..>>v.v>v>..>.>v..v..>vv>v.......>.>.v.>..vv.v.v>>.>.>...vv
...>>>v..>v..>>v.>.vvv.v.>>..>..vv>v.......>v.>.>....>v.vv.>..v>...v..>..v>...v>v.....v..v.>>>>>.>.>vv..>vv>vvv.>>>>..>.>v.v.v.>>>.v...>v>.
..vvv..>vv>.vv>v>.>>.>v.>vvv......>..>v...v.v.>.v..>vv>.vv.vvv>.......>.>...v>v.>v..v.>v.>v>vv...v>.>v.>v.v.....v.>>>v.>.v.v>.v..v>.vv....>
>v..v.v..v>>.>..>>...v.>.>......>..>.v.vvvv>vv>v..v>vvvv.>v..v>.....>..>>>...>.v..vv>v>.>..>v..v..v>>.....>.>>>...v>......>.vv..vvv>>......
.>>.v........vv.>.>.v>....v......v.>.>>..>v.v..>....>>.>.....>vv..>.>>.>v.vv..vv....>>..v.vv.>..>.>...>vv..v....>>>...>.v..v..v.>>...v>v...
>v..v.>>>>>....v...>..vv>.>.>>..>vv>vv.>>..>.>..v.vv>v.>>.>..v..v..>>vv.v>...v>v>v..>.>v.vvv...>>.v..>.>>.>>.....>.v>.>v.vv..vvv>......>v>.
....>.>>>>v>>.v>>...v..>>>.vv..v>>..>>.v.v>.v.v.>...v.>>...>v..v....vv.v.....v....>......>.....vv.>.vvvvv>....>vv..>>>v>vv>>v.....>>...v...
.>.....>v>...>v.>>..>v.>v>....vv..v>>..>.vv.v...v>..>..vv.v>..v..>v...>>v..v..v>v.v>.>v>v....v>.v>.vv..v....v.>>..>v..vv.v..>v>>.>>>..vvv.>
>v>>>v...>.>>vvv....v>vv...>...>v..>...>.>....v.v..>>..v>..vv>>>.........>v.v...>v......>>.>vv.......>.>.>..>..>>v>v.v..vv...>>v..>v>.>vv>>
..>v>v>v.vvv.v>.v>>>..>>v>....>.v.>...v>...>v.vv.vv>v..>.>v>vv.v.v.>...vv...v>>>>>vvv.>.v>.vv...>v....v.>...v..>..v..v.v...v>v.v.>v.>v..v..
.v.v.v.v>>.>.>.>>..v.v..v..v.v.>.>.>v>>.v....>>>....>>v>..>v....v......vv.>>..>...>...>>>.>.>>..v.>..>....>..v..vv.vvv>vvv...>>v>>.vv>v>v..
..>.v.>v.>>>>>>....vvv.v>.v.vv.>..v>v.........vv>.>v.v>v.vvv....>v>vv.>>..............v.v>>>..v>..>.>..vv....>.v>...>...v.....>>.vv>.v>..vv
.v...v.>.>.>>vv..>v>.>..>..vv>.>v...v>v.v..>v>.>>...v>v>...v...>..v>>........>v.>..v>..vv.v>v.>.vv>>.>vvv>v.>>>>>.v.v........>>>>..>.vv>v.v
>..v>v...>........v..>.>......v..v.>v.vv>>>>.>.>v.v>..>v.>>>vv....>....>v..>.>.>.v.vvv..>vv>..>v.......v>>>..vv..v..>v>>>...>v.>v.v>...vv>>
>.>....vv.vv.vv.vvv...v>>..>..>.>v.v>>.v.>v.v..>..>....>v..>>>>v.>..v..>>>v.v..v>>....v....v..>>>.>.v.>..>v..>...v...>v..v.>.v......v>.v.v.
.v.v..>....v..>v>vv.v>v>.v>.vvv......>.v..v.v.....>>.v...>..>..vv.>>.>.v..>....>.v>vv>..v>...>.>>vv>.vvvv.v.v>...>....>...>v.v...vvvv>....>
......v>v.vv..>v...vv..v>v....>.>v.>....>....>.>.>.>>...>>.v.>.>.>>v.v>.vv.v>>..>.>.v..>..>.v.....v.v.vv>.vvv....v.>..>.v.......v>.>..vv.v.
>>>...>.>vv>>>>>.>..........>vv>>...v....v.....v>vv.vv>..v>>..v...v>v.....vv>...vv.v..>.v..vv.v>.v.v.>.vvv.vv.>.>...>v>v..>.v>...>v...v.>.v
..>vv.....v.>.>>....vv........vv..>v..v.>.>.....>v>v>>.v.v.>>..v..vv.>v.>vv>v.>.>.vvv.>vv..v.vv.>.>>v....v..v.....v.v>v...v.>.vvv..v..>.>>.
v>vv>>v>.....v..v>v>v...v..>v..v>>.>v.vv>v>>.>..v.v...v>...>v.v.vv.>.v>v>.v...v.>vv>..>..>v.....v.>>...v>vvv>vv....>.v..>v....>..v>>v.>.>.>
.>.>.>.>>.>v.>.v.v..v.v..>v>>..v...>....v>v...v.v..v>.v...v>>v>>.vv...>>.>>>.>...v>vv>....>>..v.>v.v..>.....>>..>>>....v.>>..v..v.v.>..vv..";
    }
}

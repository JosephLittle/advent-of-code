﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Day_12
    {
        public static void Redo()
        {
            var data = Data;
            var isPartOne = false;

            var split = data.Split(new char[] { '\n', '\r', '-' }, StringSplitOptions.RemoveEmptyEntries);

            var pessimisticNodeCount = split.Length / 2;

            var indexToNodeName = new List<string>(pessimisticNodeCount);
            var indexToNeighbours = new List<List<int>>(pessimisticNodeCount);
            var indexToSmallNode = new List<bool>(pessimisticNodeCount);

            // Add the start node
            var startNodeIndex = LocalGetOrAddNodeIndex("start");

            // Add the end node
            var endNodeIndex = LocalGetOrAddNodeIndex("end");

            // Local method to get or add the node with the given name's index
            int LocalGetOrAddNodeIndex(string nodeName)
            {
                for (int nodeIndex = 0; nodeIndex < indexToNodeName.Count; nodeIndex++)
                {
                    if (indexToNodeName[nodeIndex] == nodeName)
                    {
                        return nodeIndex;
                    }
                }

                indexToNodeName.Add(nodeName);
                indexToNeighbours.Add(new List<int>(pessimisticNodeCount));
                indexToSmallNode.Add(!char.IsUpper(nodeName[0]));

                return indexToNodeName.Count - 1;
            }

            // Build up the node mappings
            for (int i = 0; i < split.Length; i += 2)
            {
                var leftNodeName = split[i];
                var rightNodeName = split[i + 1];

                var leftNodeIndex = LocalGetOrAddNodeIndex(leftNodeName);
                var rightNodeIndex = LocalGetOrAddNodeIndex(rightNodeName);

                // Don't map back to the start
                if (leftNodeIndex != startNodeIndex)
                {
                    indexToNeighbours[rightNodeIndex].Add(leftNodeIndex);
                }
                if (rightNodeIndex != startNodeIndex)
                {
                    indexToNeighbours[leftNodeIndex].Add(rightNodeIndex);
                }
            }

            var smallNodesVisited = new HashSet<int>(pessimisticNodeCount);
            var hasUsedSmallNodePass = isPartOne;

            // Traverse the nodes
            var numberOfRoutes = 0;
            LocalTraverseNodesRecursive(startNodeIndex);
            void LocalTraverseNodesRecursive(int currentNodeIndex)
            {
                // End the route at the end index
                if (currentNodeIndex == endNodeIndex)
                {
                    numberOfRoutes++;
                    return;
                }

                // Stop here if we've already visited this small node and have already used the pass
                var useSmallNodePass = smallNodesVisited.Contains(currentNodeIndex);
                if (useSmallNodePass & hasUsedSmallNodePass)
                {
                    return;
                }
                hasUsedSmallNodePass ^= useSmallNodePass;

                // Add this node to the visited small nodes
                if (indexToSmallNode[currentNodeIndex])
                {
                    smallNodesVisited.Add(currentNodeIndex);
                }

                // Traverse this node's neighbours
                var neighbours = indexToNeighbours[currentNodeIndex];
                for (int i = 0; i < neighbours.Count; i++)
                {
                    LocalTraverseNodesRecursive(neighbours[i]);
                }

                // Remove this node as a visited small node if we didn't use the pass
                if (!useSmallNodePass)
                {
                    smallNodesVisited.Remove(currentNodeIndex);
                }

                // Reset the small node pass to what it was before this iteration
                hasUsedSmallNodePass ^= useSmallNodePass;
            }

            Console.WriteLine(numberOfRoutes);
        }

        public static void PartOne()
        {
            var rootNode = ProcessData(Data);

            var answer = TraverseNodesRecursive(rootNode, new HashSet<Node>(), 0, true);

            // 3510
            Console.WriteLine(answer);
        }

        public static void PartTwo()
        {
            var rootNode = ProcessData(Data);

            var answer = TraverseNodesRecursive(rootNode, new HashSet<Node>(), 0, false);

            // 122880
            Console.WriteLine(answer);
        }

        private static int TraverseNodesRecursive(Node currentNode, HashSet<Node> restrictedNodes, int pathCount, bool hasUsedRestrictedNodePassPrevious)
        {
            var hasUsedRestrictedNodePass = hasUsedRestrictedNodePassPrevious;
            if (currentNode.Type == NodeType.End)
            {
                return pathCount + 1;
            }
            else if (restrictedNodes.Contains(currentNode))
            {
                if (hasUsedRestrictedNodePass)
                {
                    return pathCount;
                }
                hasUsedRestrictedNodePass = true;
            }
            else if (currentNode.Type == NodeType.Small)
            {
                restrictedNodes.Add(currentNode);
            }

            for (int i = 0; i < currentNode.Neighbours.Count; i++)
            {
                pathCount = TraverseNodesRecursive(currentNode.Neighbours[i], restrictedNodes, pathCount, hasUsedRestrictedNodePass);
            }

            if (hasUsedRestrictedNodePass == hasUsedRestrictedNodePassPrevious)
            {
                restrictedNodes.Remove(currentNode);
            }

            return pathCount;
        }

        private enum NodeType : byte
        {
            Start,
            End,
            Small,
            Large
        }

        private class Node
        {
            const string NodeNameStart = "start";
            const string NodeNameEnd = "end";

            public readonly string Name;
            public readonly NodeType Type;
            public readonly List<Node> Neighbours;

            public Node(string name)
            {
                Name = name;

                if (name.Equals(NodeNameStart))
                {
                    Type = NodeType.Start;
                }
                else if (name.Equals(NodeNameEnd))
                {
                    Type = NodeType.End;
                }
                else if (char.IsUpper(name[0]))
                {
                    Type = NodeType.Large;
                }
                else
                {
                    Type = NodeType.Small;
                }

                Neighbours = new List<Node>(32);
            }
        }

        private static Node ProcessData(string data)
        {
            var namesToNodes = new Dictionary<string, Node>();
            Node rootNode = null;

            var lines = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < lines.Length; i++)
            {
                var nodesPair = lines[i].Split('-');

                var nodeNameLeft = nodesPair[0];
                if (!namesToNodes.TryGetValue(nodeNameLeft, out var nodeLeft))
                {
                    nodeLeft = new Node(nodeNameLeft);
                    namesToNodes.Add(nodeNameLeft, nodeLeft);
                }

                var nodeNameRight = nodesPair[1];
                if (!namesToNodes.TryGetValue(nodeNameRight, out var nodeRight))
                {
                    nodeRight = new Node(nodeNameRight);
                    namesToNodes.Add(nodeNameRight, nodeRight);
                }

                if (nodeLeft.Type == NodeType.Start)
                {
                    rootNode = nodeLeft;

                    // Don't map back to the start
                    nodeLeft.Neighbours.Add(nodeRight);
                }
                else if (nodeRight.Type == NodeType.Start)
                {
                    rootNode = nodeRight;

                    // Don't map back to the start
                    nodeRight.Neighbours.Add(nodeLeft);
                }
                else
                {
                    nodeLeft.Neighbours.Add(nodeRight);
                    nodeRight.Neighbours.Add(nodeLeft);
                }
            }

            return rootNode;
        }

        private static readonly string DataTest1 = @"start-A
start-b
A-c
A-b
b-d
A-end
b-end";

        private static readonly string DataTest2 = @"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

        private static readonly string DataTest3 = @"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";

        private static readonly string Data = @"CI-hb
IK-lr
vr-tf
lr-end
XP-tf
start-vr
lr-io
hb-qi
end-CI
tf-YK
end-YK
XP-lr
XP-vr
lr-EU
tf-CI
EU-vr
start-tf
YK-hb
YK-vr
start-EU
lr-CI
hb-XP
XP-io
tf-EU";
    }
}

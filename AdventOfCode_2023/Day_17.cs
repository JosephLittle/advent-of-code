﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2023;

[Solution(17)]
[SolutionInput("Inputs/Day_17_Test.txt", Problem1Solution = "102", Problem2Solution = "94")]
[SolutionInput("Inputs/Day_17.txt", Problem1Solution = "665", Problem2Solution = "809", Benchmark = true)]
public unsafe class Day_17 : Solution
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct WorldCell
    {
        public byte X;
        public byte Y;

        public Direction LastMoveDirection;
        public byte NumberOfMovesInDirection;

        public ushort ShortestPath;
        public byte Cost;

        public bool IsVisited;
        public bool HasBeenAddedToUnvisitedSet;
    }

    private enum Direction : byte
    {
        Up,
        Down,
        Left,
        Right
    }

    public Day_17(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        return Solve(1, 3).ToString();
    }

    protected override string? Problem2()
    {
        return Solve(4, 10).ToString();
    }

    private ushort Solve(int minimumMovesBeforeTurningOrStopping, int maximumMovesInDirection)
    {
        int GetDirectionAndMovesIndex(Direction direction, int numberOfMovesInDirection)
        {
            return ((int)direction * maximumMovesInDirection) + (numberOfMovesInDirection - 1);
        }

        // Create the world map
        var height = Input.Lines.Length;
        var width = Input.Lines[0].Length;
        var dimensions = width * height;
        var maximumDirectionAndMovesIndex = 4 * maximumMovesInDirection;
        WorldCell* worldMap = (WorldCell*)Marshal.AllocHGlobal((dimensions * maximumDirectionAndMovesIndex) * sizeof(WorldCell));
        WorldCell* GetWorldCell(int x, int y, Direction direction, int numberOfMovesInDirection)
            => worldMap +
            // Z (direcion and moves dimension)
            (dimensions * GetDirectionAndMovesIndex(direction, numberOfMovesInDirection)) +
            // Y
            (width * y) +
            // X
            x;
        bool TryGetWorldCell(int x, int y, Direction direction, int numberOfMovesInDirection, out WorldCell* worldCell)
        {
            if (x >= 0 & x < width &
                y >= 0 & y < height &
                numberOfMovesInDirection <= maximumMovesInDirection)
            {
                worldCell = GetWorldCell(x, y, direction, numberOfMovesInDirection);
                return true;
            }
            else
            {
                worldCell = null;
                return false;
            }
        }

        // Initialise the world map
        for (int y = 0; y < height; y++)
        {
            var line = Input.Lines[y];
            for (int x = 0; x < width; x++)
            {
                var cellCost = Helpers.CharToInt(line[x]);

                for (int directionIndex = 0; directionIndex < 4; directionIndex++)
                {
                    var direction = (Direction)directionIndex;

                    for (int numberOfMovesInDirection = 1; numberOfMovesInDirection <= maximumMovesInDirection; numberOfMovesInDirection++)
                    {
                        TryGetWorldCell(x, y, direction, numberOfMovesInDirection, out var worldCell);

                        *worldCell = new WorldCell()
                        {
                            X = (byte)x,
                            Y = (byte)y,

                            LastMoveDirection = direction,
                            NumberOfMovesInDirection = (byte)numberOfMovesInDirection,

                            ShortestPath = ushort.MaxValue,
                            Cost = (byte)cellCost
                        };
                    }
                }
            }
        }

        // Start with the top left (going downwards and rightwardswith a modified starting number of moves)
        TryGetWorldCell(0, 0, Direction.Down, 1, out var startingCellDownwards);
        startingCellDownwards->HasBeenAddedToUnvisitedSet = true;
        startingCellDownwards->NumberOfMovesInDirection = 0;
        startingCellDownwards->ShortestPath = 0;
        TryGetWorldCell(0, 0, Direction.Right, 1, out var startingCellRightwards);
        startingCellRightwards->HasBeenAddedToUnvisitedSet = true;
        startingCellRightwards->NumberOfMovesInDirection = 0;
        startingCellRightwards->ShortestPath = 0;
        var unvisitedCells = new List<IntPtr>(1_024);
        unvisitedCells.Add((IntPtr)startingCellDownwards);
        unvisitedCells.Add((IntPtr)startingCellRightwards);

        // Perform dijkstra
        WorldCell endCell = default;
        while (unvisitedCells.Count > 0)
        {
            // Get the next cell to check with the shortest path
            var currentCellIndex = 0;
            var currentCell = (WorldCell*)unvisitedCells[0];
            for (int candidateCellIndex = 1; candidateCellIndex < unvisitedCells.Count; candidateCellIndex++)
            {
                var candidateCell = (WorldCell*)unvisitedCells[candidateCellIndex];
                if (candidateCell->ShortestPath < currentCell->ShortestPath)
                {
                    currentCellIndex = candidateCellIndex;
                    currentCell = candidateCell;
                }
            }
            unvisitedCells.RemoveAt(currentCellIndex);

            // Mark this cell as visited
            currentCell->IsVisited = true;

            // Check whether this is the end cell
            if (currentCell->X == width - 1 &
                currentCell->Y == height - 1 &
                currentCell->NumberOfMovesInDirection >= minimumMovesBeforeTurningOrStopping)
            {
                endCell = *currentCell;
                break;
            }

            // Process each neighbour
            WorldCell* cell;
            var canTurn = currentCell->NumberOfMovesInDirection >= minimumMovesBeforeTurningOrStopping;

            // Up
            if ((currentCell->LastMoveDirection == Direction.Up | canTurn) &
                TryGetWorldCell(
                currentCell->X,
                currentCell->Y - 1,
                Direction.Up,
                currentCell->LastMoveDirection == Direction.Up ? currentCell->NumberOfMovesInDirection + 1 : 1,
                out cell))
            {
                ProcessNeighbour(cell);
            }

            // Down
            if ((currentCell->LastMoveDirection == Direction.Down | canTurn) &
                TryGetWorldCell(
                currentCell->X,
                currentCell->Y + 1,
                Direction.Down,
                currentCell->LastMoveDirection == Direction.Down ? currentCell->NumberOfMovesInDirection + 1 : 1,
                out cell))
            {
                ProcessNeighbour(cell);
            }

            // Left
            if ((currentCell->LastMoveDirection == Direction.Left | canTurn) &
                TryGetWorldCell(
                currentCell->X - 1,
                currentCell->Y,
                Direction.Left,
                currentCell->LastMoveDirection == Direction.Left ? currentCell->NumberOfMovesInDirection + 1 : 1,
                out cell))
            {
                ProcessNeighbour(cell);
            }

            // Right
            if ((currentCell->LastMoveDirection == Direction.Right | canTurn) &
                TryGetWorldCell(
                currentCell->X + 1,
                currentCell->Y,
                Direction.Right,
                currentCell->LastMoveDirection == Direction.Right ? currentCell->NumberOfMovesInDirection + 1 : 1,
                out cell))
            {
                ProcessNeighbour(cell);
            }

            // Local method to process each neighbour
            void ProcessNeighbour(WorldCell* neighbourCell)
            {
                // Ignore if we've already visited it or it's a reversal
                var isReversal = currentCell->LastMoveDirection switch
                {
                    Direction.Up => neighbourCell->LastMoveDirection == Direction.Down,
                    Direction.Down => neighbourCell->LastMoveDirection == Direction.Up,
                    Direction.Left => neighbourCell->LastMoveDirection == Direction.Right,
                    Direction.Right => neighbourCell->LastMoveDirection == Direction.Left,
                    _ => throw new NotImplementedException(),
                };
                if (neighbourCell->IsVisited | isReversal)
                {
                    return;
                }

                // Get the length of the path to this cell from our previous
                var pathLength = (ushort)(currentCell->ShortestPath + neighbourCell->Cost);

                // If the new path length is shorter then update it
                if (neighbourCell->ShortestPath > pathLength)
                {
                    neighbourCell->ShortestPath = pathLength;
                }

                // If it's not already in the unvisited list then add it now
                if (!neighbourCell->HasBeenAddedToUnvisitedSet)
                {
                    unvisitedCells.Add((IntPtr)neighbourCell);
                    neighbourCell->HasBeenAddedToUnvisitedSet = true;
                }
            }
        }

        // Free up map memory
        Marshal.FreeHGlobal((IntPtr)worldMap);

        // Found the shortest path
        return endCell.ShortestPath;
    }
}

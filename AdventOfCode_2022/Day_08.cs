﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(8)]
[SolutionInput("Inputs/Day_08_Test.txt", Problem1Solution = "21", Problem2Solution = "8")]
[SolutionInput("Inputs/Day_08.txt", Problem1Solution = "1843", Problem2Solution = "180000", Benchmark = true)]
public unsafe class Day_08 : Solution
{
    public Day_08(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Create the outputs array grid
        var width = inputLines[0].Length;
        var height = inputLines.Length;
        var outputGrid = new byte[height][];
        for (int y = 0; y < height; y++)
        {
            outputGrid[y] = new byte[width];
        }

        // Get indices for the start and end excluding borders
        var xStart = 1;
        var yStart = 1;
        var xEnd = width - 1;
        var yEnd = height - 1;

        // Fill the top and bottom lines of the output grid with the starting heights
        // We will use these to store the highest top and bottom cells
        var inputLineTop = inputLines[0];
        var inputLineBottom = inputLines[yEnd];
        var outputGridTop = outputGrid[0];
        var outputGridBottom = outputGrid[yEnd];
        for (int x = xStart; x < xEnd; x++)
        {
            outputGridTop[x] = (byte)inputLineTop[x];
            outputGridBottom[x] = (byte)inputLineBottom[x];
        }

        // Iterate over the input going left-to-right and top-to-bottom
        for (int y = yStart; y < yEnd; y++)
        {
            var inputLine = inputLines[y];
            var outputGridLine = outputGrid[y];

            var highestLeftCell = inputLine[0];

            for (int x = xStart; x < xEnd; x++)
            {
                var cell = inputLine[x];

                // Get the visibility
                var isVisibleFromLeft = cell > highestLeftCell;
                var isVisibleFromTop = cell > outputGridTop[x];

                // Store the visibility in the output grid
                outputGridLine[x] = (byte)(
                    Helpers.BoolToInt(!isVisibleFromLeft) +
                    Helpers.BoolToInt(!isVisibleFromTop));

                // Update the highest cell heights if needed
                if (isVisibleFromLeft)
                {
                    highestLeftCell = cell;
                }
                if (isVisibleFromTop)
                {
                    outputGridTop[x] = (byte)cell;
                }
            }
        }

        // Iterate over the input going right-to-left and bottom-to-top, counting the cells that aren't visible
        var notVisibleCellCount = 0;
        for (int y = yEnd - 1; y >= yStart; y--)
        {
            var inputLine = inputLines[y];
            var outputGridLine = outputGrid[y];

            var highestRightCell = inputLine[xEnd];

            for (int x = xEnd - 1; x >= xStart; x--)
            {
                var cell = inputLine[x];

                // Get the visibility
                var isVisibleFromRight = cell > highestRightCell;
                var isVisibleFromBottom = cell > outputGridBottom[x];

                // Check whether this cell is visible at all
                var visibilityScore = outputGridLine[x] + (byte)(
                    Helpers.BoolToInt(!isVisibleFromRight) +
                    Helpers.BoolToInt(!isVisibleFromBottom));

                // If the visibility score is 4, this indicates it is not visible from any direction
                notVisibleCellCount += Helpers.BoolToInt(visibilityScore == 4);

                // Update the highest cell heights if needed
                if (isVisibleFromRight)
                {
                    highestRightCell = cell;
                }
                if (isVisibleFromBottom)
                {
                    outputGridBottom[x] = (byte)cell;
                }
            }
        }

        var totalCells = width * height;
        var totalVisibleCells = totalCells - notVisibleCellCount;
        return totalVisibleCells.ToString();
    }

    protected override string? Problem2()
    {
        // TODO: Attempt different solution where we iterate over the whole thing once 
        // but look outwards from each cell as we visit it

        var inputLines = Input.Lines;

        // Create the outputs array grid
        var width = inputLines[0].Length;
        var height = inputLines.Length;
        var outputGrid = new ulong[height][];
        for (int y = 0; y < height; y++)
        {
            outputGrid[y] = new ulong[width];
        }

        // Get indices for the start and end excluding borders
        var xStart = 1;
        var yStart = 1;
        var xEnd = width - 1;
        var yEnd = height - 1;

        // Approach is to keep an array where the key is the cell height and 
        // the value is the last index this height was seen at. Using this
        // we can do a lookup using the value of the cell we're checking
        var rowHeightToIndexLookup = new byte[10];
        var columnHeightToIndexLookups = new byte[width][];
        for (int x = xStart; x < xEnd; x++)
        {
            columnHeightToIndexLookups[x] = new byte[10];
        }

        // Iterate over the input going left-to-right and top-to-bottom
        for (int y = yStart; y < yEnd; y++)
        {
            var inputLine = inputLines[y];
            var outputGridLine = outputGrid[y];

            // Reset the row lookup array with the furthest left index (0)
            Array.Fill(rowHeightToIndexLookup, (byte)0);

            // Iterate left to right
            for (int x = xStart; x < xEnd; x++)
            {
                var cell = inputLine[x];
                var cellValue = Helpers.CharToInt(cell);
                var columnHeightToIndexLookup = columnHeightToIndexLookups[x];

                // Get the number of cells visible from this cell
                var numberOfCellsVisibleLeftwards = x - rowHeightToIndexLookup[cellValue];
                var numberOfCellsVisibleUpwards = y - columnHeightToIndexLookup[cellValue];

                // Store the product in the output grid
                outputGridLine[x] = (ulong)(numberOfCellsVisibleLeftwards * numberOfCellsVisibleUpwards);

                // Store this cell's index in the height to index lookup
                SetHeightToIndexLookupInfo(cellValue, x, y, rowHeightToIndexLookup, columnHeightToIndexLookup);
            }
        }

        // Reset the column lookup arrays with the furthest downward index (the height)
        for (int x = xStart; x < xEnd; x++)
        {
            Array.Fill(columnHeightToIndexLookups[x], (byte)(height - 1));
        }

        // Iterate over the input going right-to-left and bottom-to-top, counting the cells that aren't visible
        ulong highestScenicScore = 0;
        for (int y = yEnd - 1; y >= yStart; y--)
        {
            var inputLine = inputLines[y];
            var outputGridLine = outputGrid[y];

            // Reset the row lookup array with the furthest right index (the width)
            Array.Fill(rowHeightToIndexLookup, (byte)(width - 1));

            // Iterate right to left
            for (int x = xEnd - 1; x >= xStart; x--)
            {
                var cell = inputLine[x];
                var cellValue = Helpers.CharToInt(cell);
                var columnHeightToIndexLookup = columnHeightToIndexLookups[x];

                // Get the number of cells visible from this cell
                var numberOfCellsVisibleRightwards = (ulong)(rowHeightToIndexLookup[cellValue] - x);
                var numberOfCellsVisibleDownwards = (ulong)(columnHeightToIndexLookup[cellValue] - y);

                // Get of rightwards and downwards and multiply by the previously stored product
                var scenicScore =
                    outputGridLine[x] *
                    numberOfCellsVisibleRightwards *
                    numberOfCellsVisibleDownwards;

                // Store this if it's higher
                if (scenicScore > highestScenicScore)
                {
                    highestScenicScore = scenicScore;
                }

                // Store this cell's index in the height to index lookup
                SetHeightToIndexLookupInfo(cellValue, x, y, rowHeightToIndexLookup, columnHeightToIndexLookup);
            }
        }

        // Local method to store height to index lookup info
        static void SetHeightToIndexLookupInfo(int cellValue, int xIndex, int yIndex, byte[] rowLookup, byte[] columnLookup)
        {
            for (int i = cellValue; i >= 0; i--)
            {
                rowLookup[i] = (byte)xIndex;
                columnLookup[i] = (byte)yIndex;
            }
        }

        return highestScenicScore.ToString();
    }
}

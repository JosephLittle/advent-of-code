﻿using AdventOfCode.Framework;
using System.Runtime.InteropServices;

namespace AdventOfCode_2023;

[Solution(14)]
[SolutionInput("Inputs/Day_14_Test.txt", Problem1Solution = "136", Problem2Solution = "64")]
[SolutionInput("Inputs/Day_14.txt", Problem1Solution = "109466", Problem2Solution = "94585", Benchmark = true)]
public unsafe class Day_14 : Solution
{
    private readonly record struct Position(int X, int Y);

    public Day_14(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Create the map to write out input to.
        // Add an artificial row of stationary rocks at the top to stop the boulders
        var width = Input.Lines[0].Length;
        var height = Input.Lines.Length;
        Span<bool> map = stackalloc bool[(width * (height + 1))];
        ref bool GetMapCell(Span<bool> mapMemory, int x, int y)
        {
            // Plus 1 to normalise and allow indexing into artificial -1 row
            y += 1;
            return ref mapMemory[(width * y) + x];
        }

        // Set the artificial -1 row to stationary rocks
        map[..width].Fill(true);

        // Process each cell from the input, summing the load of boulders after they stop
        ulong loadSum = 0;
        for (int y = 0; y < height; y++)
        {
            var line = Input.Lines[y];
            for (int x = 0; x < width; x++)
            {
                var cell = line[x];
                if (cell == '#')
                {
                    // Set stationary rock
                    GetMapCell(map, x, y) = true;
                }
                else if (cell =='O')
                {
                    // Found a boulder, roll it upwards until it stops
                    var currentY = y;
                    bool isCellOccupied;
                    while (!(isCellOccupied = GetMapCell(map, x, --currentY)))
                    { }

                    // Set the cell below the occupied one as occupied, this is where our boulder stops
                    var stoppingY = ++currentY;
                    GetMapCell(map, x, stoppingY) = true;

                    // Add to the load sum
                    loadSum += (ulong)(height - stoppingY);
                }
            }
        }

        return loadSum.ToString();
    }

    protected override string? Problem2()
    {
        const int NumberOfCycles = 1_000_000_000;

        // Create the map with an artificial border of 1 on each side
        var width = Input.Lines[0].Length;
        var height = Input.Lines.Length;
        var widthWithBorder = width + 2;
        var heightWithBorder = height + 2;
        Span<bool> map = stackalloc bool[(widthWithBorder * heightWithBorder)];
        ref bool GetMapCell(Span<bool> mapMemory, int x, int y)
        {
            // Plus 1 to normalise and allow indexing into artificial -1 row
            y += 1;
            x += 1;
            return ref mapMemory[(widthWithBorder * y) + x];
        }

        // Set the artificial borders
        map[..widthWithBorder].Fill(true);
        map[(heightWithBorder * (heightWithBorder - 1))..].Fill(true);
        for (int y = 0; y < heightWithBorder; y++)
        {
            map[(widthWithBorder * y)] = true;
            map[(widthWithBorder * y) + (widthWithBorder - 1)] = true;
        }

        // Create row/column boulder maps
        var bouldersByRow = new List<Position>[height];
        var bouldersByColumn = new List<Position>[width];
        for (int i = 0; i < bouldersByRow.Length; i++)
        {
            bouldersByRow[i] = new List<Position>(width);
        }
        for (int i = 0; i < bouldersByColumn.Length; i++)
        {
            bouldersByColumn[i] = new List<Position>(height);
        }

        // Get the boulder positions and populate the map
        for (int y = 0; y < height; y++)
        {
            var line = Input.Lines[y];
            for (int x = 0; x < width; x++)
            {
                var cell = line[x];
                if (cell == '#')
                {
                    // Set map cell for stationary rock
                    GetMapCell(map, x, y) = true;
                }
                else if (cell == 'O')
                {
                    var boulderPosition = new Position(x, y);

                    // Set map cell
                    GetMapCell(map, x, y) = true;

                    // Add to row mapping (only row mapping is needed first)
                    bouldersByRow[y].Add(boulderPosition);
                }
            }
        }

        // Use a buffer to hold a copy of the map for comparisons
        Span<bool> mapSnapshot = stackalloc bool[map.Length];

        // Run cycles
        var cycleToTakeSnapshotAt = 8;
        for (int cycle = 0; cycle < NumberOfCycles; cycle++)
        {
            // Tilt up
            foreach (var bouldersOnRow in bouldersByRow)
            {
                foreach (var boulderPosition in bouldersOnRow)
                {
                    // Roll it upwards until it stops
                    var currentY = boulderPosition.Y;
                    bool isCellOccupied;
                    while (!(isCellOccupied = GetMapCell(map, boulderPosition.X, --currentY)))
                    { }

                    // Set the cell below the occupied one as occupied, this is where our boulder stops
                    var stoppingY = ++currentY;
                    GetMapCell(map, boulderPosition.X, boulderPosition.Y) = false;
                    GetMapCell(map, boulderPosition.X, stoppingY) = true;

                    // Add to boulders by column for next roll
                    bouldersByColumn[boulderPosition.X].Add(new Position(boulderPosition.X, stoppingY));
                }

                // Clear this row
                bouldersOnRow.Clear();
            }

            // Tilt left
            foreach (var bouldersOnColumn in bouldersByColumn)
            {
                foreach (var boulderPosition in bouldersOnColumn)
                {
                    // Roll it left until it stops
                    var currentX = boulderPosition.X;
                    bool isCellOccupied;
                    while (!(isCellOccupied = GetMapCell(map, --currentX, boulderPosition.Y)))
                    { }

                    // Set the cell below the occupied one as occupied, this is where our boulder stops
                    var stoppingX = ++currentX;
                    GetMapCell(map, boulderPosition.X, boulderPosition.Y) = false;
                    GetMapCell(map, stoppingX, boulderPosition.Y) = true;

                    // Add to boulders by row for next roll
                    bouldersByRow[boulderPosition.Y].Add(new Position(stoppingX, boulderPosition.Y));
                }

                // Clear this column
                bouldersOnColumn.Clear();
            }

            // Tilt down
            for (int i = bouldersByRow.Length - 1; i >= 0; i--)
            {
                foreach (var boulderPosition in bouldersByRow[i])
                {
                    // Roll it downwards until it stops
                    var currentY = boulderPosition.Y;
                    bool isCellOccupied;
                    while (!(isCellOccupied = GetMapCell(map, boulderPosition.X, ++currentY)))
                    { }

                    // Set the cell below the occupied one as occupied, this is where our boulder stops
                    var stoppingY = --currentY;
                    GetMapCell(map, boulderPosition.X, boulderPosition.Y) = false;
                    GetMapCell(map, boulderPosition.X, stoppingY) = true;

                    // Add to boulders by column for next roll
                    bouldersByColumn[boulderPosition.X].Add(new Position(boulderPosition.X, stoppingY));
                }

                // Clear this row
                bouldersByRow[i].Clear();
            }

            // Tilt right
            for (int i = bouldersByColumn.Length - 1; i >= 0; i--)
            {
                foreach (var boulderPosition in bouldersByColumn[i])
                {
                    // Roll it right until it stops
                    var currentX = boulderPosition.X;
                    bool isCellOccupied;
                    while (!(isCellOccupied = GetMapCell(map, ++currentX, boulderPosition.Y)))
                    { }

                    // Set the cell below the occupied one as occupied, this is where our boulder stops
                    var stoppingX = --currentX;
                    GetMapCell(map, boulderPosition.X, boulderPosition.Y) = false;
                    GetMapCell(map, stoppingX, boulderPosition.Y) = true;

                    // Add to boulders by row for next roll
                    bouldersByRow[boulderPosition.Y].Add(new Position(stoppingX, boulderPosition.Y));
                }

                // Clear this column
                bouldersByColumn[i].Clear();
            }

            // Take or compare the snapshot
            if (cycle == cycleToTakeSnapshotAt)
            {
                // Take the snapshot 
                map.CopyTo(mapSnapshot);
            }
            else if (cycle > cycleToTakeSnapshotAt)
            {
                // See if current map is the same as previous snapshot
                if (MemoryExtensions.SequenceEqual(map, mapSnapshot))
                {
                    // Snapshot has stabilised so work out how many cycles we can skip
                    var numberOfCyclesBeforeRepeating = cycle - cycleToTakeSnapshotAt;
                    var numberOfCyclesRemaining = NumberOfCycles - cycle;
                    var numberOfCyclesRemainingAfterSkipping = numberOfCyclesRemaining % numberOfCyclesBeforeRepeating;
                    cycle = NumberOfCycles - numberOfCyclesRemainingAfterSkipping;

                    // Don't need to compare anymore
                    cycleToTakeSnapshotAt = int.MaxValue;
                }
                // Check if we should take a new snapshot (pattern may not have settled yet
                else if (cycle >= cycleToTakeSnapshotAt * 2)
                {
                    // Take a new snapshot
                    map.CopyTo(mapSnapshot);

                    cycleToTakeSnapshotAt *= 2;
                }
            }
        }

        // Get the load on the support beam
        ulong loadSum = 0;
        foreach (var boulderPosition in bouldersByRow.SelectMany(l => l))
        {
            loadSum += (ulong)(height - boulderPosition.Y);
        }

        return loadSum.ToString();
    }

    private void PrintMap(Span<bool> mapMemory, int width, int height)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Console.Write(mapMemory[(width * y) + x] ? '#' : '.');
            }
            Console.WriteLine();
        }
    }
}

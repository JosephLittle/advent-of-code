﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(3)]
[SolutionInput("Inputs/Day_03_Test.txt", Problem1Solution = "4361", Problem2Solution = "467835")]
[SolutionInput("Inputs/Day_03.txt", Problem1Solution = "535235", Problem2Solution = "79844424", Benchmark = true)]
public unsafe class Day_03 : Solution
{
    public Day_03(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        var lines = Input.Lines;
        var sum = 0;

        // Create grid of the same size to store our info
        // (include border cells so we don't need to check bounds)
        var height = lines.Length;
        var width = lines[0].Length;
        var gridHeight = height + 2;
        var gridWidth = width + 2;
        var gridMemory = stackalloc char[
            gridWidth * 
            gridHeight];

        // Clear
        new Span<char>(gridMemory, gridWidth * gridHeight).Fill('.');

        Span<char> GetGridRow(int y)
        {
            return new Span<char>(gridMemory + (gridWidth * y), gridWidth);
        }

        // Copy to grid
        for (int y = 0; y < height; y++)
        {
            var toCopyFrom = lines[y].AsSpan();
            var toCopyTo = new Span<char>(gridMemory + (gridWidth * (y + 1)) + 1, width);
            toCopyFrom.CopyTo(toCopyTo);
        }

        // Scan the grid looking for symbols
        for (int y = 1; y < height; y++)
        {
            var rowAbove = GetGridRow(y - 1);
            var rowCurrent = GetGridRow(y);
            var rowBelow = GetGridRow(y + 1);

            for (int x = 1; x < width; x++)
            {
                var cell = rowCurrent[x];
                if ( // ASCII tricks to only check for symbols we care about
                    cell <  '.' |
                    cell == '/' |
                    cell >  '9')
                {
                    // Found a symbol, check surrounding cells for numbers
                    ParseAndSumAndWipeIntegerAtCell(rowAbove, x - 1, true);
                    ParseAndSumAndWipeIntegerAtCell(rowAbove, x);
                    ParseAndSumAndWipeIntegerAtCell(rowAbove, x + 1);

                    ParseAndSumAndWipeIntegerAtCell(rowCurrent, x - 1, true);
                    ParseAndSumAndWipeIntegerAtCell(rowCurrent, x + 1);

                    ParseAndSumAndWipeIntegerAtCell(rowBelow, x - 1, true);
                    ParseAndSumAndWipeIntegerAtCell(rowBelow, x);
                    ParseAndSumAndWipeIntegerAtCell(rowBelow, x + 1);
                }
            }
        }

        // If there is an integer at the cell, parse it, sum it, and wipe it from the grid
        void ParseAndSumAndWipeIntegerAtCell(Span<char> row, int x, bool shouldParseLeft = false)
        {
            var cell = row[x];
            if (!char.IsAsciiDigit(cell))
            {
                // Not digit
                return;
            }

            // Start with this value
            var value = Helpers.CharToInt(cell);

            // Wipe cell
            row[x] = '.';

            // Parse left
            if (shouldParseLeft)
            {
                var xLeft = x;
                var multiplier = 10;
                while (char.IsAsciiDigit((cell = row[--xLeft])))
                {
                    value += (Helpers.CharToInt(cell) * multiplier);
                    multiplier *= 10;

                    // Wipe cell
                    row[xLeft] = '.';
                }
            }

            // Parse right
            var xRight = x;
            while (char.IsAsciiDigit((cell = row[++xRight])))
            {
                value *= 10;
                value += Helpers.CharToInt(cell);

                // Wipe cell
                row[xRight] = '.';
            }

            // Sum
            sum += value;
        }

        //// Helper method to print the grid
        //void PrintGrid()
        //{
        //    for (int y = 0; y < gridHeight; y++)
        //    {
        //        var row = GetGridRow(y);
        //        for (int x = 0; x < gridWidth; x++)
        //        {
        //            Console.Write(row[x]);
        //        }
        //        Console.WriteLine();
        //    }
        //}

        // Return sum
        return sum.ToString();
    }

    protected override string? Problem2()
    {
        var lines = Input.Lines;
        var sum = 0;

        // Create grid of the same size to store our info
        // (include border cells so we don't need to check bounds)
        var height = lines.Length;
        var width = lines[0].Length;
        var gridHeight = height + 2;
        var gridWidth = width + 2;
        var gridMemory = stackalloc char[
            gridWidth *
            gridHeight];

        // Clear
        new Span<char>(gridMemory, gridWidth * gridHeight).Fill('.');

        Span<char> GetGridRow(int y)
        {
            return new Span<char>(gridMemory + (gridWidth * y), gridWidth);
        }

        // Copy to grid
        for (int y = 0; y < height; y++)
        {
            var toCopyFrom = lines[y].AsSpan();
            var toCopyTo = new Span<char>(gridMemory + (gridWidth * (y + 1)) + 1, width);
            toCopyFrom.CopyTo(toCopyTo);
        }

        // Scan the grid looking for symbols
        for (int y = 1; y < height; y++)
        {
            var rowAbove = GetGridRow(y - 1);
            var rowCurrent = GetGridRow(y);
            var rowBelow = GetGridRow(y + 1);

            for (int x = 1; x < width; x++)
            {
                var cell = rowCurrent[x];
                if (cell == '*')
                {
                    // Found a symbol, check surrounding cells for numbers
                    var gearRatio = 1;
                    var count = 0;

                    // Top row
                    var offset = -1;
                    do
                    {
                        gearRatio *= ParseIntegerAtCell(rowAbove, x, ref offset, ref count);
                    }
                    while (++offset < 2);

                    // Current row
                    offset = -1;
                    gearRatio *= ParseIntegerAtCell(rowCurrent, x, ref offset, ref count);
                    offset = 1;
                    gearRatio *= ParseIntegerAtCell(rowCurrent, x, ref offset, ref count);

                    // Below row
                    offset = -1;
                    do
                    {
                        gearRatio *= ParseIntegerAtCell(rowBelow, x, ref offset, ref count);
                    }
                    while (++offset < 2);

                    if (count == 2)
                    {
                        sum += gearRatio;
                    }
                }
            }
        }

        // If there is an integer at the cell, parse it, sum it, and wipe it from the grid
        int ParseIntegerAtCell(Span<char> row, int x, ref int offset, ref int count)
        {
            var cell = row[x + offset];
            if (!char.IsAsciiDigit(cell))
            {
                // Not digit
                return 1;
            }

            // Start with this value
            var value = Helpers.CharToInt(cell);

            // Parse left
            var xLeft = x + offset;
            var multiplier = 10;
            while (char.IsAsciiDigit((cell = row[--xLeft])))
            {
                value += (Helpers.CharToInt(cell) * multiplier);
                multiplier *= 10;
            }

            // Parse right
            var xRight = x + offset;
            while (char.IsAsciiDigit((cell = row[++xRight])))
            {
                value *= 10;
                value += Helpers.CharToInt(cell);
            }

            offset = xRight - x;
            count++;
            return value;
        }

        // Return sum
        return sum.ToString();
    }
}

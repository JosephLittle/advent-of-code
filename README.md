# Advent Of Code

C# projects containing my solutions for Advent Of Code puzzles for the years **2021**, and **2022**.

These solutions shouldn't be taken 100% seriously, I've messed around with different approaches such as branchless programming, using only stack-based memory, and at least one parallelised brute force approach (not efficient).

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(09)]
[SolutionInput("Inputs/Day_09_Test.txt", Problem1Solution = "114", Problem2Solution = "2")]
[SolutionInput("Inputs/Day_09.txt", Problem1Solution = "1882395907", Problem2Solution = "1005", Benchmark = true)]
public unsafe class Day_09 : Solution
{
    public Day_09(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        long sum = 0;

        // Process each line
        Span<long> sequenceBuffer = stackalloc long[30];
        foreach (var line in Input.Lines)
        {
            // Read each number into the buffer
            var characterIndex = 0;
            var numberOfNumbers = 0;
            while (characterIndex < line.Length)
            {
                var number = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ' ');
                characterIndex++;
                sequenceBuffer[numberOfNumbers++] = number;
            }

            // Process the sequence
            var sequence = sequenceBuffer[..numberOfNumbers];
            var nextNumberInSequence = GetNextNumberInSequence(sequence);

            // Sum its next sequence number
            sum += nextNumberInSequence;
        }

        return sum.ToString();

        // Local recursive function to get the next number in the sequence
        static long GetNextNumberInSequence(Span<long> sequence)
        {
            // Allocate the difference sequence
            Span<long> differenceSequence = stackalloc long[sequence.Length - 1];

            // Set the first difference
            var firstDifference = sequence[1] - sequence[0];
            differenceSequence[0] = firstDifference;

            // Track that they're all the same
            var allDifferencesAreSame = true;

            // Set the rest of the differences
            for (int i = 1; i < sequence.Length - 1; i++)
            {
                var difference = sequence[i + 1] - sequence[i];
                differenceSequence[i] = difference;

                allDifferencesAreSame &= difference == firstDifference;
            }

            // If all are the same then stop here, otherwise continue
            if (allDifferencesAreSame)
            {
                return sequence[^1] + differenceSequence[^1];
            }
            else
            {
                return sequence[^1] + GetNextNumberInSequence(differenceSequence);
            }
        }
    }

    protected override string? Problem2()
    {
        long sum = 0;

        // Process each line
        Span<long> sequenceBuffer = stackalloc long[30];
        foreach (var line in Input.Lines)
        {
            // Read each number into the buffer
            var characterIndex = 0;
            var numberOfNumbers = 0;
            while (characterIndex < line.Length)
            {
                var number = Helpers.ParseSignedIntegerUntilDelimiterOrEnd(line, ref characterIndex, ' ');
                characterIndex++;
                sequenceBuffer[numberOfNumbers++] = number;
            }

            // Process the sequence
            var sequence = sequenceBuffer[..numberOfNumbers];
            var nextNumberInSequence = GetNextNumberInSequence(sequence);

            // Sum its next sequence number
            sum += nextNumberInSequence;
        }

        return sum.ToString();

        // Local recursive function to get the next number in the sequence
        static long GetNextNumberInSequence(Span<long> sequence)
        {
            // Allocate the difference sequence
            Span<long> differenceSequence = stackalloc long[sequence.Length - 1];

            // Set the first difference
            var firstDifference = sequence[1] - sequence[0];
            differenceSequence[0] = firstDifference;

            // Track that they're all the same
            var allDifferencesAreSame = true;

            // Set the rest of the differences
            for (int i = 1; i < sequence.Length - 1; i++)
            {
                var difference = sequence[i + 1] - sequence[i];
                differenceSequence[i] = difference;

                allDifferencesAreSame &= difference == firstDifference;
            }

            // If all are the same then stop here, otherwise continue
            if (allDifferencesAreSame)
            {
                return sequence[0] - differenceSequence[0];
            }
            else
            {
                return sequence[0] - GetNextNumberInSequence(differenceSequence);
            }
        }
    }
}

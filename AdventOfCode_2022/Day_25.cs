﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(25)]
[SolutionInput("Inputs/Day_25_Test.txt", Problem1Solution = "2=-1=0", Problem2Solution = "")]
[SolutionInput("Inputs/Day_25.txt", Problem1Solution = "2---1010-0=1220-=010", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_25 : Solution
{
    private readonly int[] snafuToIntLookup;
    private readonly char[] intToSnafuLookup;

    public Day_25(Input input) 
        : base(input)
    {
        snafuToIntLookup = new int[255];
        snafuToIntLookup['='] = -2;
        snafuToIntLookup['-'] = -1;
        snafuToIntLookup['0'] = 0;
        snafuToIntLookup['1'] = 1;
        snafuToIntLookup['2'] = 2;

        intToSnafuLookup = new char[5];
        intToSnafuLookup[0] = '=';
        intToSnafuLookup[1] = '-';
        intToSnafuLookup[2] = '0';
        intToSnafuLookup[3] = '1';
        intToSnafuLookup[4] = '2';
    }

    protected override string? Problem1()
    {
        var currentSum = new List<int>(20);
        var otherSnafu = new List<int>(20);

        // Start with the first number and use it to store the running sum
        ParseSnafu(Input.Lines[0], currentSum);

        // Process each number
        for (int i = 1; i < Input.Lines.Length; i++)
        {
            // Parse the next number
            ParseSnafu(Input.Lines[i], otherSnafu);

            // Add it to the current sum
            AddSnafu(otherSnafu, currentSum);
        }

        // Return the resulting snafu
        return SnafuToString(currentSum);
    }

    protected override string? Problem2()
    {
        return null;
    }

    /// <summary>
    /// Parse the given string into a snafu number.
    /// </summary>
    private void ParseSnafu(string snafuString, List<int> snafuBuffer)
    {
        snafuBuffer.Clear();
        for (int i = snafuString.Length - 1; i >= 0; i--)
        {
            snafuBuffer.Add(snafuToIntLookup[snafuString[i]]);
        }
    }

    /// <summary>
    /// Get the string representation of the given snafu number.
    /// </summary>
    private string SnafuToString(List<int> snafu)
    {
        var stringBuffer = stackalloc char[snafu.Count];
        for (int i = 0; i < snafu.Count; i++)
        {
            stringBuffer[snafu.Count - 1 -i] = intToSnafuLookup[snafu[i] + 2];
        }
        return new string(stringBuffer);
    }

    /// <summary>
    /// Adds snafu a to b, mutating snafu b.
    /// </summary>
    private void AddSnafu(List<int> snafuA, List<int> snafuB)
    {
        for (int i = 0; i < snafuA.Count; i++)
        {
            AddToIndex(snafuA[i], snafuB, i);

            static void AddToIndex(int numberToAdd, List<int> snafuToAddTo, int indexToAddTo)
            {
                // Fill in significant places
                while (indexToAddTo >= snafuToAddTo.Count)
                {
                    snafuToAddTo.Add(0);
                }

                // Get the sum and perform the operation, carrying if needed
                var sum = numberToAdd + snafuToAddTo[indexToAddTo];
                switch (sum)
                {
                    case -4:
                        AddToIndex(-1, snafuToAddTo, indexToAddTo + 1);
                        snafuToAddTo[indexToAddTo] = 1;
                        break;
                    case -3:
                        AddToIndex(-1, snafuToAddTo, indexToAddTo + 1);
                        snafuToAddTo[indexToAddTo] = 2;
                        break;
                    case 3:
                        AddToIndex(1, snafuToAddTo, indexToAddTo + 1);
                        snafuToAddTo[indexToAddTo] = -2;
                        break;
                    case 4:
                        AddToIndex(1, snafuToAddTo, indexToAddTo + 1);
                        snafuToAddTo[indexToAddTo] = -1;
                        break;
                    default:
                        snafuToAddTo[indexToAddTo] = sum;
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Day_14
    {
        public static void PartOneAndTwo()
        {
            var data = Data;
            var splitByNewLine = data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            // Create the production mapping
            var productionMap = new char[ushort.MaxValue];
            for (int i = 1; i < splitByNewLine.Length; i++)
            {
                var production = splitByNewLine[i].Split(" -> ");

                var produceFrom = production[0];
                var productionKey = GetProductionKey(produceFrom[0], produceFrom[1]);

                var product = production[1][0];

                productionMap[productionKey] = product;
            }

            // Set up the parameters
            var numberOfSteps = 40;
            var inputCharacters = splitByNewLine[0];
            var numberOfThreads = 16;

            // Set up the pairs as a job queue and count the initial characters
            var numberOfCharacters = ('Z' - 'A') + 1;
            var characterCountsSum = new ulong[numberOfCharacters];
            var jobQueue = new Queue<(char left, char right)>();
            for (int i = 0; i < inputCharacters.Length - 1; i++)
            {
                jobQueue.Enqueue((inputCharacters[i], inputCharacters[i + 1]));

                characterCountsSum[inputCharacters[i] - 65]++;
            }
            characterCountsSum[inputCharacters[inputCharacters.Length - 1] - 65]++;

            // Create the character counts and start the threads
            var threadIndexToCharacterCounts = new ulong[numberOfThreads][];
            var threadIndexToThread = new Thread[numberOfThreads];
            for (int threadIndex = 0; threadIndex < numberOfThreads; threadIndex++)
            {
                // Create the character counts for this thread
                var characterCountsForThread = new ulong[numberOfCharacters];
                threadIndexToCharacterCounts[threadIndex] = characterCountsForThread;

                // Create and start this thread
                var threadId = threadIndex;
                threadIndexToThread[threadIndex] = new Thread(() =>
                {
                    ThreadMain(threadId, jobQueue, numberOfSteps, characterCountsForThread, productionMap);
                });
                threadIndexToThread[threadIndex].Start();
            }

            // Wait for all threads to exit and sum their character counts
            for (int threadIndex = 0; threadIndex < numberOfThreads; threadIndex++)
            {
                threadIndexToThread[threadIndex].Join();

                var characterCountsForThread = threadIndexToCharacterCounts[threadIndex];
                for (int i = 0; i < numberOfCharacters; i++)
                {
                    characterCountsSum[i] += characterCountsForThread[i];
                }
            }
            Console.WriteLine("{0}: All threads exited.", DateTime.UtcNow.TimeOfDay);

            // Sort the counts and get the answer
            Array.Sort(characterCountsSum);
            for (int i = 0; i < characterCountsSum.Length; i++)
            {
                var count = characterCountsSum[i];

                // Use the first non-zero count (this is the least commom character count)
                if (count != 0)
                {
                    var answer = characterCountsSum[characterCountsSum.Length - 1] - count;
                    Console.WriteLine("Answer: " + answer);
                    break;
                }
            }
        }

        static void ThreadMain(int threadId, Queue<(char left, char right)> jobQueue, int depth, ulong[] characterCounts, char[] productionMap)
        {
            while (true)
            {
                // Try to dequeue a job
                char left;
                char right;
                lock (jobQueue)
                {
                    if (!jobQueue.TryDequeue(out var job))
                    {
                        // No more jobs so exit
                        Console.WriteLine("{0}: Thread {1} exiting...", DateTime.UtcNow.TimeOfDay, threadId);
                        return;
                    }

                    left = job.left;
                    right = job.right;
                }

                // Run the job
                Console.WriteLine("{0}: Thread {1} starting pair {2} {3}...", DateTime.UtcNow.TimeOfDay, threadId, left, right);
                VisitPairsRecursive(left, right, depth, characterCounts, productionMap);
                Console.WriteLine("{0}: Thread {1} finished pair {2} {3}.", DateTime.UtcNow.TimeOfDay, threadId, left, right);
            }
        }

        static void VisitPairsRecursive(char left, char right, int depth, ulong[] characterCounts, char[] productionMap)
        {
            // Get the product
            var product = productionMap[GetProductionKey(left, right)];

            // Add to the count
            characterCounts[product - 65]++;

            // Return now if we've run out of depth
            if (--depth == 0)
            {
                return;
            }

            // Visit the left resulting pair first
            VisitPairsRecursive(left, product, depth, characterCounts, productionMap);

            // Visist the right resulting pair next
            VisitPairsRecursive(product, right, depth, characterCounts, productionMap);
        }

        private static int GetProductionKey(char left, char right)
        {
            return ((left - 65) << 8) + (right - 65);
        }


        private static readonly string DataTest = @"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

        private static readonly string Data = @"BNSOSBBKPCSCPKPOPNNK

HH -> N
CO -> F
BC -> O
HN -> V
SV -> S
FS -> F
CV -> F
KN -> F
OP -> H
VN -> P
PF -> P
HP -> H
FK -> K
BS -> F
FP -> H
FN -> V
VV -> O
PS -> S
SK -> N
FF -> K
PK -> V
OF -> N
VP -> K
KB -> H
OV -> B
CH -> F
SF -> F
NH -> O
NC -> N
SP -> N
NN -> F
OK -> S
BB -> S
NK -> S
FH -> P
FC -> S
OB -> P
VS -> P
BF -> S
HC -> V
CK -> O
NP -> K
KV -> S
OS -> V
CF -> V
FB -> C
HO -> S
BV -> V
KS -> C
HB -> S
SO -> N
PH -> C
PN -> F
OC -> F
KO -> F
VF -> V
CS -> O
VK -> O
FV -> N
OO -> K
NS -> S
KK -> C
FO -> S
PV -> S
CN -> O
VC -> P
SS -> C
PO -> P
BN -> N
PB -> N
PC -> H
SH -> K
BH -> F
HK -> O
VB -> P
NV -> O
NB -> C
CP -> H
NO -> K
PP -> N
CC -> S
CB -> K
VH -> H
SC -> C
KC -> N
SB -> B
BP -> P
KP -> K
SN -> H
KF -> K
KH -> B
HV -> V
HS -> K
NF -> B
ON -> H
BO -> P
VO -> K
OH -> C
HF -> O
BK -> H
";
    }
}

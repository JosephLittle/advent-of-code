﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Day_11
    {
        public static void PartOneAndTwo()
        {
            var data = ProcessData(Data);
            var height = data.Length;
            var width = data[0].Length;

            var flashThreshold = 10;
            var coordinatesToFlash = new Stack<(int X, int Y)>(64);
            var coordinatesFlashed = new List<(int X, int Y)>(64);

            var flashCount = 0;

            void IncreaseOnRow(int[] row, int xIndex, int yIndex)
            {
                if (++row[xIndex] == flashThreshold)
                {
                    coordinatesToFlash.Push((xIndex, yIndex));
                }
            }

            // Run for each step
            var maximumNumberOfSteps = 1000;
            for (int step = 0; step < maximumNumberOfSteps; step++)
            {
                // Reset any flashed coordinates
                for (int i = 0; i < coordinatesFlashed.Count; i++)
                {
                    var coordinates = coordinatesFlashed[i];
                    data[coordinates.Y][coordinates.X] = 0;
                }
                coordinatesFlashed.Clear();

                // Gather the initial flashes this step
                for (int y = 0; y < width; y++)
                {
                    var row = data[y];
                    for (int x = 0; x < row.Length; x++)
                    {
                        if (++row[x] == flashThreshold)
                        {
                            coordinatesToFlash.Push((x, y));
                        }
                    }
                }

                // While we still have coordinates to flash, increase their neighbours and gather new flashing coordinates
                while (coordinatesToFlash.TryPop(out var coordinates))
                {
                    flashCount++;
                    coordinatesFlashed.Add(coordinates);

                    var xHasLowerBound = coordinates.X - 1 >= 0;
                    var xHasUpperBound = coordinates.X + 1 < width;
                    var yHasLowerBound = coordinates.Y - 1 >= 0;
                    var yHasUpperBound = coordinates.Y + 1 < height;

                    // Top row
                    if (yHasLowerBound)
                    {
                        var y = coordinates.Y - 1;
                        var row = data[y];

                        IncreaseOnRow(row, coordinates.X, y); // Top middle
                        if (xHasLowerBound)
                        {
                            IncreaseOnRow(row, coordinates.X - 1, y); // Top left
                        }
                        if (xHasUpperBound)
                        {
                            IncreaseOnRow(row, coordinates.X + 1, y); // Top right
                        }
                    }

                    // Middle row
                    {
                        var row = data[coordinates.Y];
                        if (xHasLowerBound)
                        {
                            IncreaseOnRow(row, coordinates.X - 1, coordinates.Y); // Middle left
                        }
                        if (xHasUpperBound)
                        {
                            IncreaseOnRow(row, coordinates.X + 1, coordinates.Y); // Middle right
                        }
                    }

                    // Bottom row
                    if (yHasUpperBound)
                    {
                        var y = coordinates.Y + 1;
                        var row = data[y];

                        IncreaseOnRow(row, coordinates.X, y); // Bottom middle
                        if (xHasLowerBound)
                        {
                            IncreaseOnRow(row, coordinates.X - 1, y); // Bottom left
                        }
                        if (xHasUpperBound)
                        {
                            IncreaseOnRow(row, coordinates.X + 1, y); // Bottom right
                        }
                    }
                }

                // Print the number of flashes at the end of this step
                Console.WriteLine($"Step {step + 1} number of flashes: {flashCount}");

                // Check whether all coordinates have flashed
                if (coordinatesFlashed.Count == (width * height))
                {
                    Console.WriteLine($"All {(width * height)} coordinates have flashed!");
                    break;
                }
            }
        }

        private static int[][] ProcessData(string data)
        {
            var splitByNewlines = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            return splitByNewlines.Select(line => line.Select(character => Helpers.CharToInt(character)).ToArray()).ToArray();
        }

        private static readonly string DataTest = @"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

        private static readonly string Data = @"8448854321
4447645251
6542573645
4725275268
6442514153
4515734868
5513676158
3257376185
2172424467
6775163586";
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(20)]
[SolutionInput("Inputs/Day_20_Test.txt", Problem1Solution = "3", Problem2Solution = "1623178306")]
[SolutionInput("Inputs/Day_20.txt", Problem1Solution = "18257", Problem2Solution = "4148032160983", Benchmark = true)]
public unsafe class Day_20 : Solution
{
    public struct Node
    {
        public short Value;

        public short MilestoneIndex;

        public Node* Previous;
        public Node* Next;

        public bool IsMilestone => MilestoneIndex != -1;
    }

    public Day_20(Input input) 
        : base(input)
    {
        // TODO: Solution can be improved with a maintained list of milestones
    }

    protected override string? Problem1()
    {
        var numberOfNodes = Input.Lines.Length;
        var nodeArray = stackalloc Node[numberOfNodes];
        Node* zeroNode = null;

        // Build linked list of input numbers
        var previousNode = nodeArray + (numberOfNodes - 1);
        for (int i = 0; i < numberOfNodes; i++)
        {
            var node = nodeArray + i;

            // Parse the value
            var inputLine = Input.Lines[i];
            node->Value = (short)Helpers.SubstringToSignedInt(inputLine, 0, inputLine.Length);

            // Link with previous node
            previousNode->Next = node;
            node->Previous = previousNode;

            // Set as previous
            previousNode = node;

            // Check for the zero node
            if (node->Value == 0)
            {
                zeroNode = node;
            }
        }

        // Link last node with first
        var firstNode = nodeArray;
        var lastNode = nodeArray + (numberOfNodes - 1);
        firstNode->Previous = lastNode;
        lastNode->Next = firstNode;

        // Walk through the node array, moving them by their own offsets
        for (int i = 0; i < numberOfNodes; i++)
        {
            var node = nodeArray + i;

            // Remove from current position
            node->Previous->Next = node->Next;
            node->Next->Previous = node->Previous;

            // Track from the new next node
            var newNextNode = node->Next;

            // Get the remaining moves
            var remainingMoves = node->Value;
            remainingMoves += (short)(node->Value / numberOfNodes);
            remainingMoves %= (short)numberOfNodes;

            // Move through the linked list until we find the new position
            if (remainingMoves > 0)
            {
                while (--remainingMoves >= 0)
                {
                    newNextNode = newNextNode->Next;
                }
            }
            else if (remainingMoves < 0)
            {
                while (++remainingMoves <= 0)
                {
                    newNextNode = newNextNode->Previous;
                }
            }

            // Insert the node here
            var newPreviousNode = newNextNode->Previous;
            newPreviousNode->Next = node;
            node->Previous = newPreviousNode;
            newNextNode->Previous = node;
            node->Next = newNextNode;
        }

        // Get the 1000th, 2000th, and 3000th number after the zero node
        var node1000 = zeroNode;
        for (int i = 0; i < 1_000; i++)
        {
            node1000 = node1000->Next;
        }
        var node2000 = node1000;
        for (int i = 0; i < 1_000; i++)
        {
            node2000 = node2000->Next;
        }
        var node3000 = node2000;
        for (int i = 0; i < 1_000; i++)
        {
            node3000 = node3000->Next;
        }

        // Sum these nodes for the final answer
        return (
            node1000->Value +
            node2000->Value +
            node3000->Value).ToString();
    }

    protected override string? Problem2()
    {
        //return null;
        const long DecryptionKey = 811589153;

        var numberOfNodes = Input.Lines.Length;
        var nodeArray = stackalloc Node[numberOfNodes];
        Node* zeroNode = null;

        // Build linked list of input numbers
        var previousNode = nodeArray + (numberOfNodes - 1);
        for (int i = 0; i < numberOfNodes; i++)
        {
            var node = nodeArray + i;

            // Parse the value
            var inputLine = Input.Lines[i];
            node->Value = (short)Helpers.SubstringToSignedInt(inputLine, 0, inputLine.Length);

            // Link with previous node
            previousNode->Next = node;
            node->Previous = previousNode;

            // Set as previous
            previousNode = node;

            // Check for the zero node
            if (node->Value == 0)
            {
                zeroNode = node;
            }
        }

        // Link last node with first
        var firstNode = nodeArray;
        var lastNode = nodeArray + (numberOfNodes - 1);
        firstNode->Previous = lastNode;
        lastNode->Next = firstNode;

        // Walk through the node array, moving them by their own offsets
        // Do this 10 times
        for (int iteration = 0; iteration < 10; iteration++)
        {
            for (int i = 0; i < numberOfNodes; i++)
            {
                var node = nodeArray + i;

                // Remove from current position
                node->Previous->Next = node->Next;
                node->Next->Previous = node->Previous;

                // Track from the new next node
                var newNextNode = node->Next;

                // Get the remaining moves
                var valueMultipliedByDecryptionKey = node->Value * DecryptionKey;
                var remainingMoves = valueMultipliedByDecryptionKey % numberOfNodes;
                var additionalMoves = valueMultipliedByDecryptionKey / numberOfNodes;

                // Whittle down the additional moves
                for (int w = 0; w < 10; w++)
                {
                    var reduced = (additionalMoves % numberOfNodes) + (additionalMoves / numberOfNodes);
                    additionalMoves = reduced;
                }
                remainingMoves += additionalMoves;

                // Move through the linked list until we find the new position
                if (remainingMoves > 0)
                {
                    while (--remainingMoves >= 0)
                    {
                        newNextNode = newNextNode->Next;
                    }
                }
                else if (remainingMoves < 0)
                {
                    while (++remainingMoves <= 0)
                    {
                        newNextNode = newNextNode->Previous;
                    }
                }

                // Insert the node here
                var newPreviousNode = newNextNode->Previous;
                newPreviousNode->Next = node;
                node->Previous = newPreviousNode;
                newNextNode->Previous = node;
                node->Next = newNextNode;
            }

            Console.WriteLine("Interation: " + iteration);
        }

        // Get the 1000th, 2000th, and 3000th number after the zero node
        var node1000 = zeroNode;
        for (int i = 0; i < 1_000; i++)
        {
            node1000 = node1000->Next;
        }
        var node2000 = node1000;
        for (int i = 0; i < 1_000; i++)
        {
            node2000 = node2000->Next;
        }
        var node3000 = node2000;
        for (int i = 0; i < 1_000; i++)
        {
            node3000 = node3000->Next;
        }

        // Sum these nodes for the final answer
        return (
            (node1000->Value * DecryptionKey) +
            (node2000->Value * DecryptionKey) +
            (node3000->Value * DecryptionKey)).ToString();
    }

    private void PrintList(Node* startNode, int numberOfNodes)
    {
        Console.WriteLine();

        var currentNode = startNode;
        while(--numberOfNodes >= 0)
        {
            Console.WriteLine(currentNode->Value);
            currentNode = currentNode->Next;
        }
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(13)]
[SolutionInput("Inputs/Day_13_Test.txt", Problem1Solution = "13", Problem2Solution = "140")]
[SolutionInput("Inputs/Day_13.txt", Problem1Solution = "5825", Problem2Solution = "24477", Benchmark = true)]
public unsafe class Day_13 : Solution
{
    public readonly record struct PacketValue(byte Value)
    {
        public bool IsListStart => Value == '[' - 48;
        public bool IsListEnd => Value == ']' - 48;
    }

    public Day_13(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        var sumOfIndicesOfPairsInCorrectOrder = 0;

        // Iterate over the pairs of packets
        var pairIndex = 1;
        for (int i = 0; i < inputLines.Length; i += 3)
        {
            var lineLeft = inputLines[i];
            var lineRight = inputLines[i + 1];
            var arePacketsInOrder = ArePacketsInOrder(lineLeft, lineRight);
            
            // Sum this pair index if the left is smaller (means it was in the right order)
            sumOfIndicesOfPairsInCorrectOrder += pairIndex * Helpers.BoolToInt(arePacketsInOrder);

            pairIndex++;
        }

        return sumOfIndicesOfPairsInCorrectOrder.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        const string DividerPacket1 = "[[2]]";
        const string DividerPacket2 = "[[6]]";

        int dividerPacket1Index = 1;
        int dividerPacket2Index = 2;

        for (int i = 0; i < inputLines.Length; i += 3)
        {
            var packetA = inputLines[i];
            var packetB = inputLines[i + 1];

            dividerPacket1Index += Helpers.BoolToInt(ArePacketsInOrder(packetA, DividerPacket1));
            dividerPacket1Index += Helpers.BoolToInt(ArePacketsInOrder(packetB, DividerPacket1));

            dividerPacket2Index += Helpers.BoolToInt(ArePacketsInOrder(packetA, DividerPacket2));
            dividerPacket2Index += Helpers.BoolToInt(ArePacketsInOrder(packetB, DividerPacket2));
        }

        return (dividerPacket1Index * dividerPacket2Index).ToString();
    }

    private static bool ArePacketsInOrder(string lineLeft, string lineRight)
    {
        // Skip the first character, always a '['
        var lineLeftIndex = 1;
        var lineRightIndex = 1;

        var result = 0;

        while (lineLeftIndex < lineLeft.Length & lineRightIndex < lineRight.Length & result == 0)
        {
            result = RecursiveCompare(
                GetNextCharacterSkippingCommas(lineLeft, ref lineLeftIndex),
                GetNextCharacterSkippingCommas(lineRight, ref lineRightIndex));

            // Local method to get the next non-comma character
            static char GetNextCharacterSkippingCommas(string line, ref int lineIndex)
            {
                char c;
                while ((c = line[lineIndex++]) == ',')
                { }
                if (c == '1' && line[lineIndex] == '0')
                {
                    lineIndex++;
                    c = (char)('9' + 1);
                }
                return c;
            }

            // Local method which allows for recursive comparisons
            int RecursiveCompare(char leftValue, char rightValue)
            {
                var areValuesDifferent = leftValue != rightValue;
                if ((leftValue == ']' | rightValue == ']') & areValuesDifferent)
                {
                    // One list ended before the other
                    return (leftValue == ']' & rightValue != ']') ? -1 : 1;
                }
                else if ((leftValue == '[' | rightValue == '[') & areValuesDifferent)
                {
                    // One line started a list before the other
                    if (leftValue == '[')
                    {
                        // Get the first value in the list
                        leftValue = GetNextCharacterSkippingCommas(lineLeft, ref lineLeftIndex);

                        // Compare with current integer value from other list
                        var result = RecursiveCompare(leftValue, rightValue);
                        if (result != 0)
                        {
                            return result;
                        }

                        // Insert artificial end of list
                        return RecursiveCompare(
                            GetNextCharacterSkippingCommas(lineLeft, ref lineLeftIndex),
                            ']');
                    }
                    else
                    {
                        // Get the first value in the list
                        rightValue = GetNextCharacterSkippingCommas(lineRight, ref lineRightIndex);

                        // Compare with current integer value from other list
                        var result = RecursiveCompare(leftValue, rightValue);
                        if (result != 0)
                        {
                            return result;
                        }

                        // Insert artificial end of list
                        return RecursiveCompare(
                            ']',
                            GetNextCharacterSkippingCommas(lineRight, ref lineRightIndex));
                    }
                }
                else
                {
                    // Compare the values
                    return Math.Sign(leftValue - rightValue);
                }
            }
        }

        // Check which is smaller if we reached the end of the line
        if (lineLeftIndex >= lineLeft.Length | lineRightIndex >= lineRight.Length)
        {
            if (lineLeftIndex >= lineLeft.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return result == -1;
        }
    }
}

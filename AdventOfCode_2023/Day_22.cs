﻿using AdventOfCode.Framework;
using System.Reflection;
using System.Text;

namespace AdventOfCode_2023;

[Solution(22)]
[SolutionInput("Inputs/Day_22_Test.txt", Problem1Solution = "5", Problem2Solution = "")]
[SolutionInput("Inputs/Day_22.txt", Problem1Solution = "", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_22 : Solution
{
    private readonly record struct Position(int X, int Y, int Z);
    private readonly record struct Brick(Position Start, Position End)
    {
        public int Width => End.X - Start.X;
        public int Depth => End.Y - Start.Y;
        public int Height => End.Z - Start.Z;

        public Brick GetCopyMovedDown() =>
            new Brick(
                Start with { Z = Start.Z - 1 },
                End with { Z = End.Z - 1 });

        public bool IsSpaceOccupied(bool[,,] world)
        {
            foreach (var position in GetBottomPositions())
            {
                if (world[position.X, position.Y, position.Z] |
                    position.Z == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<Position> GetContainedPositions()
        {
            for (int x = Start.X; x <= End.X; x++)
            {
                for (int y = Start.Y; y <= End.Y; y++)
                {
                    for (int z = Start.Z; z <= End.Z; z++)
                    {
                        yield return new Position(x, y, z);
                    }
                }
            }
        }

        public IEnumerable<Position> GetBottomPositions()
        {
            for (int x = Start.X; x <= End.X; x++)
            {
                for (int y = Start.Y; y <= End.Y; y++)
                {
                    yield return new Position(x, y, Start.Z);
                }
            }
        }
    }

    public Day_22(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // All bricks are sticks (only one axis greater than 1 in length)

        var bricks = new List<Brick>(Input.Lines.Length);
        var xMin = int.MaxValue;
        var xMax = int.MinValue;
        var yMin = int.MaxValue;
        var yMax = int.MinValue;
        var zMin = int.MaxValue;
        var zMax = int.MinValue;

        // Parse lines into bricks
        foreach (var line in Input.Lines)
        {
            var splitStartAndEnd = line.Split("~");

            var splitStart = splitStartAndEnd[0].Split(",").Select(int.Parse).ToArray();
            var splitEnd = splitStartAndEnd[1].Split(",").Select(int.Parse).ToArray();

            var brick = new Brick(
                new Position(splitStart[0], splitStart[1], splitStart[2]),
                new Position(splitEnd[0], splitEnd[1], splitEnd[2]));
            bricks.Add(brick);

            xMin = int.Min(xMin, brick.Start.X);
            xMax = int.Max(xMax, brick.End.X);
            yMin = int.Min(yMin, brick.Start.Y);
            yMax = int.Max(yMax, brick.End.Y);
            zMin = int.Min(zMin, brick.Start.Z);
            zMax = int.Max(zMax, brick.End.Z);
            if (brick.Start.X > brick.End.X |
                brick.Start.Y > brick.End.Y |
                brick.Start.Z > brick.End.Z |
                xMin < 0 |
                yMin < 0 |
                zMin < 1)
            {
                throw new NotImplementedException();
            }
        }

        // Build the world
        var world = new bool[xMax + 1, yMax + 1, zMax + 1];

        // Sort the bricks from lowest to highest
        bricks.Sort((b1, b2) => b1.Start.Z.CompareTo(b2.Start.Z));
        
        // Fill in the world
        for (int b = 0; b < bricks.Count; b++)
        {
            var lastValidBrick = bricks[b];
            
            // Move the brick down until it enters an occupied space
            for (int z = lastValidBrick.Start.Z - 1; z > 0; z--)
            {
                var candidateBrick = lastValidBrick.GetCopyMovedDown();

                // Check if it uses an occupied space
                if (candidateBrick.IsSpaceOccupied(world))
                {
                    break;
                }

                lastValidBrick = candidateBrick;
            }

            // Set the last valid brick location as occupied
            foreach (var position in lastValidBrick.GetContainedPositions())
            {
                world[position.X, position.Y, position.Z] = true;
            }

            // Update the brick in the list
            bricks[b] = lastValidBrick;
        }

        PrintWorld(world);

        // Sort the bricks again from lowest to highest
        bricks.Sort((b1, b2) => b1.Start.Z.CompareTo(b2.Start.Z));

        // Check each one to see whether it can be removed without affecting others
        var numberOfBricksThatCanBeRemovedWithoutDisruption = 0;
        for (int i = 0; i < bricks.Count; i++)
        {
            // Remove the brick
            var brickToRemove = bricks[i];
            foreach (var position in brickToRemove.GetContainedPositions())
            {
                world[position.X, position.Y, position.Z] = false;
            }

            // Check whether any other bricks can now move
            var areOtherBricksDisturbed = false;
            for (int j = i + 1; j < bricks.Count; j++)
            {
                var brickToCheck = bricks[j];
                brickToCheck = brickToCheck.GetCopyMovedDown();
                if (!brickToCheck.IsSpaceOccupied(world))
                {
                    areOtherBricksDisturbed = true;
                    break;
                }
            }

            // Add to the count if other bricks are disturbed
            if (!areOtherBricksDisturbed)
            {
                numberOfBricksThatCanBeRemovedWithoutDisruption++;
            }

            // Add the brick back in
            foreach (var position in brickToRemove.GetContainedPositions())
            {
                world[position.X, position.Y, position.Z] = true;
            }
        }

        return numberOfBricksThatCanBeRemovedWithoutDisruption.ToString();
    }

    private void PrintWorld(bool[,,] world)
    {
        Console.WriteLine("");
        for (int z = world.GetLength(2) - 1; z >= 0; z--)
        {
            Console.Write($"{z:00}: ");

            // X
            for (int x = 0; x < world.GetLength(0); x++)
            {
                var occupied = false;
                for (int y = 0; y < world.GetLength(1); y++)
                {
                    occupied |= world[x, y, z];
                }

                Console.Write(occupied ? '#' : '.');
            }

            Console.Write("  ");

            // Y
            for (int y = 0; y < world.GetLength(1); y++)
            {
                var occupied = false;
                for (int x = 0; x < world.GetLength(0); x++)
                {
                    occupied |= world[x, y, z];
                }

                Console.Write(occupied ? '#' : '.');
            }

            Console.WriteLine();
        }
    }

    protected override string? Problem2()
    {
        return null;
    }
}

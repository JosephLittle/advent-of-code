﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(22)]
[SolutionInput("Inputs/Day_22_Test.txt", Problem1Solution = "6032", Problem2Solution = "5031")]
[SolutionInput("Inputs/Day_22.txt", Problem1Solution = "191010", Problem2Solution = "55364", Benchmark = true)]
public unsafe class Day_22 : Solution
{
    private readonly record struct Row(int StartingX, List<bool> Cells);
    private readonly record struct Column(int StartingY, List<bool> Cells);
    private enum Direction : byte
    {
        East = 0,
        South = 1,
        West = 2,
        North = 3
    }

    public Day_22(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        int numberOfRows = 300;
        int numberOfColumns = 300;

        // Initialise the rows and columns
        var rows = new Row[numberOfRows];
        var columns = new Column[numberOfColumns];

        // Process the input into the jagged lists of rows and columns
        for (int y = 0; y < Input.Lines.Length - 2; y++)
        {
            var inputLine = Input.Lines[y];
            for (int x = 0; x < inputLine.Length; x++)
            {
                var cell = inputLine[x];

                // Ignore if empty
                if (cell == ' ')
                {
                    continue;
                }

                // Get whether this is a wall
                var isWall = cell == '#';

                // Get and populate the row
                var row = rows[y];
                if (row == default)
                {
                    rows[y] = row = new Row(x, new List<bool>(numberOfColumns));
                }
                row.Cells.Add(isWall);

                // Get and populate the column
                var column = columns[x];
                if (column == default)
                {
                    columns[x] = column = new Column(y, new List<bool>(numberOfRows));
                }
                column.Cells.Add(isWall);
            }
        }

        // Start by facing east
        var direction = Direction.East;
        var currentY = 0;
        var currentX = rows[0].StartingX;

        // Process the instruction line 
        var instructionLine = Input.Lines[Input.Lines.Length - 1];
        var instructionLineIndex = 0;
        while (instructionLineIndex < instructionLine.Length)
        {
            // Get the next instruction
            var instructionCharacter = instructionLine[instructionLineIndex];
            if (instructionCharacter == 'R')
            {
                // Rotate right
                direction = (Direction)(((int)direction + 1) % 4);
                instructionLineIndex++;
            }
            else if (instructionCharacter == 'L')
            {
                // Rotate left
                var newDirectionIndex = (int)direction - 1;
                direction = (Direction)(newDirectionIndex < 0 ? 3 : newDirectionIndex);
                instructionLineIndex++;
            }
            else
            {
                // Character is digit, so find the end of the number or instruction string
                var endOfNumberIndex = instructionLineIndex + 1;
                while (
                    endOfNumberIndex < instructionLine.Length &&
                    char.IsDigit(instructionLine[endOfNumberIndex]))
                {
                    endOfNumberIndex++;
                }

                // Parse the number of steps
                var numberOfStepsRemaining = Helpers.SubstringToUnsignedInt(instructionLine,
                    instructionLineIndex,
                    endOfNumberIndex - instructionLineIndex);

                // Move the index along
                instructionLineIndex = endOfNumberIndex;

                // Move the given number of steps
                if (direction == Direction.East | direction == Direction.West)
                {
                    // Get the row we're moving on
                    var row = rows[currentY];

                    // Get the current x within the row
                    var xWithinRow = currentX - row.StartingX;

                    // Move until we hit a wall or run out of moves
                    if (direction == Direction.East)
                    {
                        while (numberOfStepsRemaining-- > 0)
                        {
                            var xWithinRowNext = (xWithinRow + 1) % row.Cells.Count;
                            if (row.Cells[xWithinRowNext])
                            {
                                break;
                            }
                            else
                            {
                                xWithinRow = xWithinRowNext;
                            }
                        }
                    }
                    else
                    {
                        while (numberOfStepsRemaining-- > 0)
                        {
                            var xWithinRowNext = xWithinRow - 1;
                            if (xWithinRowNext < 0)
                            {
                                xWithinRowNext = row.Cells.Count - 1;
                            }
                            if (row.Cells[xWithinRowNext])
                            {
                                break;
                            }
                            else
                            {
                                xWithinRow = xWithinRowNext;
                            }
                        }
                    }

                    // Set the current x
                    currentX = row.StartingX + xWithinRow;
                }
                else
                {
                    // Get the column we're moving on
                    var column = columns[currentX];

                    // Get the current y within the column
                    var yWithinColumn = currentY - column.StartingY;

                    // Move until we hit a wall or run out of moves
                    if (direction == Direction.South)
                    {
                        while (numberOfStepsRemaining-- > 0)
                        {
                            var yWithinRowNext = (yWithinColumn + 1) % column.Cells.Count;
                            if (column.Cells[yWithinRowNext])
                            {
                                break;
                            }
                            else
                            {
                                yWithinColumn = yWithinRowNext;
                            }
                        }
                    }
                    else
                    {
                        while (numberOfStepsRemaining-- > 0)
                        {
                            var yWithinRowNext = yWithinColumn - 1;
                            if (yWithinRowNext < 0)
                            {
                                yWithinRowNext = column.Cells.Count - 1;
                            }
                            if (column.Cells[yWithinRowNext])
                            {
                                break;
                            }
                            else
                            {
                                yWithinColumn = yWithinRowNext;
                            }
                        }
                    }

                    // Set the current y
                    currentY = column.StartingY + yWithinColumn;
                }
            }
        }

        // Return the password
        return (
            ((currentY + 1) * 1000) +
            ((currentX + 1) * 4) +
            (int)direction).ToString();
    }

    private class Face
    {
        public readonly int StartingX;
        public readonly int StartingY;
        public readonly bool[][] Cells;

        public readonly FaceLink[] LeavingDirectionsToFaceLinks;

        public Face(int startingX, int startingY, bool[][] cells)
        {
            StartingX = startingX;
            StartingY = startingY;
            Cells = cells;

            LeavingDirectionsToFaceLinks = new FaceLink[4];
        }
    }
    private readonly record struct FaceLink(
        Face Face,
        Direction EnteringDirection);

    protected override string? Problem2()
    {
        int sideLength;
        Face startingFace;

        // Check specifically for the test input
        if (Input.Raw.Length == 201)
        {
            sideLength = 4;

            // Create the faces
            var front = CreateFace(2, 0, sideLength, Input);
            var back  = CreateFace(2, 2, sideLength, Input);
            var up    = CreateFace(0, 1, sideLength, Input);
            var down  = CreateFace(2, 1, sideLength, Input);
            var left  = CreateFace(1, 1, sideLength, Input);
            var right = CreateFace(3, 2, sideLength, Input);

            // Link them (12 links in total)
            LinkFaces(front, down, Direction.South, Direction.South);
            LinkFaces(front, left, Direction.West, Direction.South);
            LinkFaces(front, up, Direction.North, Direction.South);
            LinkFaces(front, right, Direction.East, Direction.West);

            LinkFaces(down, back, Direction.South, Direction.South);
            LinkFaces(down, left, Direction.West, Direction.West);
            LinkFaces(down, right, Direction.East, Direction.South);

            LinkFaces(back, right, Direction.East, Direction.East);
            LinkFaces(back, left, Direction.West, Direction.North);
            LinkFaces(back, up, Direction.South, Direction.North);

            LinkFaces(left, up, Direction.West, Direction.West);

            LinkFaces(up, right, Direction.West, Direction.North);

            // Set the starting face
            startingFace = front;
        }
        else
        {
            sideLength = 50;

            // Create the faces
            var front = CreateFace(1, 0, sideLength, Input);
            var back = CreateFace(1, 2, sideLength, Input);
            var up = CreateFace(0, 3, sideLength, Input);
            var down = CreateFace(1, 1, sideLength, Input);
            var left = CreateFace(0, 2, sideLength, Input);
            var right = CreateFace(2, 0, sideLength, Input);

            // Link them (12 links in total)
            LinkFaces(front, down, Direction.South, Direction.South);
            LinkFaces(front, left, Direction.West, Direction.East);
            LinkFaces(front, up, Direction.North, Direction.East);
            LinkFaces(front, right, Direction.East, Direction.East);

            LinkFaces(down, back, Direction.South, Direction.South);
            LinkFaces(down, left, Direction.West, Direction.South);
            LinkFaces(down, right, Direction.East, Direction.North);

            LinkFaces(back, right, Direction.East, Direction.West);
            LinkFaces(back, left, Direction.West, Direction.West);
            LinkFaces(back, up, Direction.South, Direction.West);

            LinkFaces(left, up, Direction.South, Direction.South);

            LinkFaces(up, right, Direction.South, Direction.South);

            // Set the starting face
            startingFace = front;
        }

        // Create a lookup for the direction step values
        var directionToStepValues = new (int XStep, int YStep)[4];
        directionToStepValues[(int)Direction.East] = (1, 0);
        directionToStepValues[(int)Direction.South] = (0, 1);
        directionToStepValues[(int)Direction.West] = (-1, 0);
        directionToStepValues[(int)Direction.North] = (0, -1);

        var directionsFromAndToToTranslationFunctions = new Dictionary<(Direction From, Direction To), Func<int, int, int, (int, int)>>();
        directionsFromAndToToTranslationFunctions.Add((Direction.East, Direction.East), (oldX, oldY, upperBound) => (0, oldY));
        directionsFromAndToToTranslationFunctions.Add((Direction.East, Direction.South), (oldX, oldY, upperBound) => (upperBound - oldY, 0));
        directionsFromAndToToTranslationFunctions.Add((Direction.East, Direction.West), (oldX, oldY, upperBound) => (upperBound, upperBound - oldY));
        directionsFromAndToToTranslationFunctions.Add((Direction.East, Direction.North), (oldX, oldY, upperBound) => (oldY, upperBound));

        directionsFromAndToToTranslationFunctions.Add((Direction.South, Direction.East), (oldX, oldY, upperBound) => (0, upperBound - oldX));
        directionsFromAndToToTranslationFunctions.Add((Direction.South, Direction.South), (oldX, oldY, upperBound) => (oldX, 0));
        directionsFromAndToToTranslationFunctions.Add((Direction.South, Direction.West), (oldX, oldY, upperBound) => (upperBound, oldX));
        directionsFromAndToToTranslationFunctions.Add((Direction.South, Direction.North), (oldX, oldY, upperBound) => (upperBound - oldX, upperBound));

        directionsFromAndToToTranslationFunctions.Add((Direction.West, Direction.East), (oldX, oldY, upperBound) => (0, upperBound - oldY));
        directionsFromAndToToTranslationFunctions.Add((Direction.West, Direction.South), (oldX, oldY, upperBound) => (oldY, 0));
        directionsFromAndToToTranslationFunctions.Add((Direction.West, Direction.West), (oldX, oldY, upperBound) => (upperBound, oldY));
        directionsFromAndToToTranslationFunctions.Add((Direction.West, Direction.North), (oldX, oldY, upperBound) => (upperBound - oldY, upperBound));

        directionsFromAndToToTranslationFunctions.Add((Direction.North, Direction.East), (oldX, oldY, upperBound) => (0, oldX));
        directionsFromAndToToTranslationFunctions.Add((Direction.North, Direction.South), (oldX, oldY, upperBound) => (upperBound - oldX, 0));
        directionsFromAndToToTranslationFunctions.Add((Direction.North, Direction.West), (oldX, oldY, upperBound) => (upperBound, upperBound - oldX));
        directionsFromAndToToTranslationFunctions.Add((Direction.North, Direction.North), (oldX, oldY, upperBound) => (oldX, upperBound));

        // Start by facing east
        var currentDirection = Direction.East;
        var currentDirectionStepValues = directionToStepValues[(int)currentDirection];
        var currentFace = startingFace;
        var currentY = 0;
        var currentX = 0;

        // Process the instruction line 
        var instructionLine = Input.Lines[Input.Lines.Length - 1];
        var instructionLineIndex = 0;
        while (instructionLineIndex < instructionLine.Length)
        {
            // Get the next instruction
            var instructionCharacter = instructionLine[instructionLineIndex];
            if (instructionCharacter == 'R')
            {
                // Rotate right
                currentDirection = (Direction)(((int)currentDirection + 1) % 4);
                currentDirectionStepValues = directionToStepValues[(int)currentDirection];
                instructionLineIndex++;
            }
            else if (instructionCharacter == 'L')
            {
                // Rotate left
                var newDirectionIndex = (int)currentDirection - 1;
                currentDirection = (Direction)(newDirectionIndex < 0 ? 3 : newDirectionIndex);
                currentDirectionStepValues = directionToStepValues[(int)currentDirection];
                instructionLineIndex++;
            }
            else
            {
                // Character is digit, so find the end of the number or instruction string
                var endOfNumberIndex = instructionLineIndex + 1;
                while (
                    endOfNumberIndex < instructionLine.Length &&
                    char.IsDigit(instructionLine[endOfNumberIndex]))
                {
                    endOfNumberIndex++;
                }

                // Parse the number of steps
                var numberOfStepsRemaining = Helpers.SubstringToUnsignedInt(instructionLine,
                    instructionLineIndex,
                    endOfNumberIndex - instructionLineIndex);

                // Move the index along
                instructionLineIndex = endOfNumberIndex;

                // Move each step until we hit a wall
                while (numberOfStepsRemaining-- > 0)
                {
                    // Get the next x and y
                    var nextX = currentX + currentDirectionStepValues.XStep;
                    var nextY = currentY + currentDirectionStepValues.YStep;

                    // If either of them are out of bounds, then we need to move to a new face
                    if (nextX < 0 | nextX >= sideLength |
                        nextY < 0 | nextY >= sideLength)
                    {
                        // Get the link to the new face we're moving to
                        var faceLink = currentFace.LeavingDirectionsToFaceLinks[(int)currentDirection];

                        // Translate the next x and y to the new face
                        var translationFunction = directionsFromAndToToTranslationFunctions[(currentDirection, faceLink.EnteringDirection)];
                        (var nextXTranslated, var nextYTranslated) = translationFunction(nextX, nextY, sideLength - 1);

                        // Check for a wall on the new face
                        if (faceLink.Face.Cells[nextYTranslated][nextXTranslated])
                        {
                            // Hit a wall, so stop
                            break;
                        }
                        else
                        {
                            // No wall, continue with new face, x and y, and direction
                            currentFace = faceLink.Face;
                            currentDirection = faceLink.EnteringDirection;
                            currentDirectionStepValues = directionToStepValues[(int)currentDirection];
                            currentX = nextXTranslated;
                            currentY = nextYTranslated;
                        }
                    }
                    else
                    {
                        // Check for a wall on the current face
                        if (currentFace.Cells[nextY][nextX])
                        {
                            // Hit a wall, so stop
                            break;
                        }
                        else
                        {
                            // No wall, continue
                            currentX = nextX;
                            currentY = nextY;
                        }
                    }
                }
            }
        }

        // Return the password
        return (
            ((currentFace.StartingY + currentY + 1) * 1000) +
            ((currentFace.StartingX + currentX + 1) * 4) +
            (int)currentDirection).ToString();
    }

    private static Face CreateFace(int faceX, int faceY, int sideLength, Input input)
    {
        var startingX = faceX * sideLength;
        var startingY = faceY * sideLength;
        var cells = new bool[sideLength][];
        for (int y = 0; y < sideLength; y++)
        {
            var inputLine = input.Lines[startingY + y];

            var cellRow = new bool[sideLength];
            cells[y] = cellRow;

            for (int x = 0; x < sideLength; x++)
            {
                var isWall = inputLine[startingX + x] == '#';
                cellRow[x] = isWall;
            }
        }

        return new Face(startingX, startingY, cells);
    }

    private static void LinkFaces(Face from, Face to, Direction leavingDirection, Direction enteringDirection)
    {
        from.LeavingDirectionsToFaceLinks[(int)leavingDirection] = new FaceLink(to, enteringDirection);
        to.LeavingDirectionsToFaceLinks[(int)ReverseDirection(enteringDirection)] = new FaceLink(from, ReverseDirection(leavingDirection));
    }

    private static Direction ReverseDirection(Direction direction)
    {
        return (Direction)(((int)direction + 2) % 4);
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(2)]
[SolutionInput("Inputs/Day_02_Test.txt", Problem1Solution = "8", Problem2Solution = "2286")]
[SolutionInput("Inputs/Day_02.txt", Problem1Solution = "2268", Problem2Solution = "", Benchmark = true)]
public unsafe class Day_02 : Solution
{
    public Day_02(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        const int maximumNumberOfReds = 12;
        const int maximumNumberOfGreens = 13;
        const int maximumNumberOfBlues = 14;

        var gameNumberSum = 0;

        var lines = Input.Lines;
        for (int i = 0; i < lines.Length; i++)
        {
            var line = lines[i];
            var gameNumber = (ushort)(i + 1);

            // Reset colour counts for this game
            var numberOfReds = 0;
            var numberOfGreens = 0;
            var numberOfBlues = 0;

            // Get the index of the first value
            var currentIndex = "Game ".Length + Helpers.GetNumberOfDigitsSimple(gameNumber);

            // Parse numbers and colours until the end of the line
            while (currentIndex < line.Length)
            {
                // Check for the end of a round
                if (line[currentIndex] == ';')
                {
                    numberOfReds = 0;
                    numberOfGreens = 0;
                    numberOfBlues = 0;
                }
                currentIndex += 2;

                // Parse the number of cubes
                var numberOfCubes = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref currentIndex, ' ');
                currentIndex++;

                // Parse the colour
                var colourOfCubes = line[currentIndex];

                // Set and check the counts for this colour, and jump forward
                switch (colourOfCubes)
                {
                    case 'r':
                        {
                            currentIndex += "red".Length;
                            numberOfReds += numberOfCubes;

                            if (numberOfReds > maximumNumberOfReds)
                            {
                                goto outerLoopEnd;
                            }
                        }
                        break;
                    case 'g':
                        {
                            currentIndex += "green".Length;
                            numberOfGreens += numberOfCubes;

                            if (numberOfGreens > maximumNumberOfGreens)
                            {
                                goto outerLoopEnd;
                            }
                        }
                        break;
                    case 'b':
                        {
                            currentIndex += "blue".Length;
                            numberOfBlues += numberOfCubes;

                            if (numberOfBlues > maximumNumberOfBlues)
                            {
                                goto outerLoopEnd;
                            }
                        }
                        break;
                    default:
                        throw new Exception($"Unrecognised colour '{colourOfCubes}'.");
                }
            }

            // If we got here then we have enough cubes for this game, so sum its number
            gameNumberSum += gameNumber;

            // Label to skip to loop end
            outerLoopEnd:;
        }

        return gameNumberSum.ToString();
    }

    protected override string? Problem2()
    {
        var powerSetSum = 0;

        var lines = Input.Lines;
        for (int i = 0; i < lines.Length; i++)
        {
            var line = lines[i];
            var gameNumber = (ushort)(i + 1);

            // Reset colour counts and maximum colour counts for this game
            var numberOfReds = 0;
            var numberOfGreens = 0;
            var numberOfBlues = 0;
            var maxNumberOfReds = 0;
            var maxNumberOfGreens = 0;
            var maxNumberOfBlues = 0;

            // Get the index of the first value
            var currentIndex = "Game ".Length + Helpers.GetNumberOfDigitsSimple(gameNumber) + ": ".Length;

            // Parse numbers and colours until the end of the line
            while (currentIndex < line.Length)
            {
                // Parse the number of cubes
                var numberOfCubes = (int)Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref currentIndex, ' ');
                currentIndex++;

                // Parse the colour
                var colourOfCubes = line[currentIndex];

                // Set and check the counts for this colour, and jump forward
                switch (colourOfCubes)
                {
                    case 'r':
                        {
                            currentIndex += "red".Length;
                            numberOfReds += numberOfCubes;
                        }
                        break;
                    case 'g':
                        {
                            currentIndex += "green".Length;
                            numberOfGreens += numberOfCubes;
                        }
                        break;
                    case 'b':
                        {
                            currentIndex += "blue".Length;
                            numberOfBlues += numberOfCubes;
                        }
                        break;
                    default:
                        throw new Exception($"Unrecognised colour '{colourOfCubes}'.");
                }

                // Check for the end of a round
                if (currentIndex >= line.Length || line[currentIndex] == ';')
                {
                    maxNumberOfReds = int.Max(maxNumberOfReds, numberOfReds);
                    maxNumberOfGreens = int.Max(maxNumberOfGreens, numberOfGreens);
                    maxNumberOfBlues = int.Max(maxNumberOfBlues, numberOfBlues);

                    numberOfReds = 0;
                    numberOfGreens = 0;
                    numberOfBlues = 0;
                }
                currentIndex += 2;
            }

            // Sum the power set for this game with the others
            powerSetSum += maxNumberOfReds * maxNumberOfGreens * maxNumberOfBlues;
        }

        return powerSetSum.ToString();
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(7)]
[SolutionInput("Inputs/Day_07_Test.txt", Problem1Solution = "95437", Problem2Solution = "24933642")]
[SolutionInput("Inputs/Day_07.txt", Problem1Solution = "1325919", Problem2Solution = "2050735", Benchmark = true)]
public class Day_07 : Solution
{
    public Day_07(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var result = GetDirectorySizes();
        var directoryIdToSizeIncludingSubDirectories = result.DirectoryIdToDirectorySizeIncludingSubDirectories;
        var lastDirectoryId = result.lastDirectoryId;

        // Sum every directory size 100_000 and under
        ulong directorySizeSum = 0;
        for (int i = 0; i <= lastDirectoryId; i++)
        {
            var directorySize = directoryIdToSizeIncludingSubDirectories[i];
            if (directorySize <= 100_000)
            {
                directorySizeSum += directorySize;
            }
        }

        return directorySizeSum.ToString();
    }

    protected override string? Problem2()
    {
        const ulong TotalDiskSpace = 70_000_000;
        const ulong UnusedDiskSpaceRequired = 30_000_000;

        // Get the directory sizes
        var result = GetDirectorySizes();
        var directoryIdToSizeIncludingSubDirectories = result.DirectoryIdToDirectorySizeIncludingSubDirectories;
        var lastDirectoryId = result.lastDirectoryId;

        // Get the size of the root directory
        var usedDiskSpace = directoryIdToSizeIncludingSubDirectories[0];
        var sizeNeededToDelete = usedDiskSpace - (TotalDiskSpace - UnusedDiskSpaceRequired);

        // Find the size of the smallest directory we can delete to free up this must space
        var smallestDirectorySizeToDelete = ulong.MaxValue;
        for (int i = 0; i <= lastDirectoryId; i++)
        {
            var directorySize = directoryIdToSizeIncludingSubDirectories[i];
            if (directorySize > sizeNeededToDelete &
                directorySize < smallestDirectorySizeToDelete)
            {
                smallestDirectorySizeToDelete = directorySize;
            }
        }

        return smallestDirectorySizeToDelete.ToString();
    }

    /// <summary>
    /// Gets the directory sizes in an array keyed by directory Ids.
    /// </summary>
    private (ulong[] DirectoryIdToDirectorySizeIncludingSubDirectories, int lastDirectoryId) GetDirectorySizes()
    {
        var inputLines = Input.Lines;

        var directoryIdToParentDirectoryId = new int[inputLines.Length];
        var directoryIdToSize = new ulong[inputLines.Length];
        var directoryIdToSizeIncludingSubDirectories = new ulong[inputLines.Length];

        var directoryIdStack = new int[inputLines.Length];
        var directoryIdStackCount = 1;

        directoryIdToParentDirectoryId[0] = -1;

        var nextDirectoryId = 1;

        // Process each line
        // (Skip the first two instructions which are just root and ls commands)
        for (int i = 2; i < inputLines.Length; i++)
        {
            var line = inputLines[i];

            // Check for a command
            if (line[0] == '$')
            {
                // Check for pop directory
                if (line[5] == '.')
                {
                    // Pop a directory from the stack
                    directoryIdStackCount--;
                }
                // Must be a push directory
                else
                {
                    // Push the current directory
                    var directoryId = nextDirectoryId++;
                    directoryIdToParentDirectoryId[directoryId] = directoryIdStack[directoryIdStackCount - 1];
                    directoryIdStack[directoryIdStackCount++] = directoryId;

                    // Skip the next instruction which is always an 'ls' command
                    i++;
                }

            }
            // Check for a directory listing
            else if (line[0] == 'd')
            {
                // Do nothing
            }
            // Must be a file listing
            else
            {
                // Add this file size to the current directory
                var fileSize = Helpers.ParseIntegerUntilDelimiter(line, 0, ' ');
                directoryIdToSize[directoryIdStack[directoryIdStackCount - 1]] += fileSize;
            }
        }

        // Copy the directory sizes to the directory sizes including subdirectories
        Array.Copy(
            directoryIdToSize,
            directoryIdToSizeIncludingSubDirectories,
            directoryIdToSize.Length);

        // Add the subdirectory sizes to their parent directories
        for (int directoryId = nextDirectoryId - 1; directoryId > 0; directoryId--)
        {
            var subDirectoryId = directoryId;
            var parentDirectoryId = directoryIdToParentDirectoryId[subDirectoryId];
            var directorySize = directoryIdToSize[subDirectoryId];

            // Stop when we reach parent directory -1, the parent directory of the root
            while (parentDirectoryId != -1)
            {
                // Add this directory's size to the parent directory
                directoryIdToSizeIncludingSubDirectories[parentDirectoryId] += directorySize;

                // Set the next sub directory and parent directory
                subDirectoryId = parentDirectoryId;
                parentDirectoryId = directoryIdToParentDirectoryId[subDirectoryId];
            }
        }

        return (directoryIdToSizeIncludingSubDirectories, nextDirectoryId - 1);
    }
}

﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(08)]
[SolutionInput("Inputs/Day_08_Test1.txt", Problem1Solution = "6", Problem2Solution = null)]
[SolutionInput("Inputs/Day_08_Test2.txt", Problem1Solution = null, Problem2Solution = "6")]
[SolutionInput("Inputs/Day_08.txt", Problem1Solution = "16343", Problem2Solution = "15299095336639", Benchmark = true)]
public unsafe class Day_08 : Solution
{

    public Day_08(Input input)
        : base(input)
    { }

    private readonly record struct Destinations(ushort Left, ushort Right);

    protected override string? Problem1()
    {
        if (Input.Lines.Length == 10)
        {
            return null;
        }

        var start = GetKey("AAA");
        var end = GetKey("ZZZ");

        // Build up the mapping
        Span<Destinations> sourceDestinationMap = stackalloc Destinations[end + 1];
        for (int i = 2; i < Input.Lines.Length; i++)
        {
            var line = Input.Lines[i].AsSpan();

            var source = GetKey(line[..3]);

            var destinationLeft = GetKey(line[7..10]);
            var destinationRight = GetKey(line[12..15]);

            sourceDestinationMap[source] = new Destinations(destinationLeft, destinationRight);
        }

        // Process the directions until we reach the end
        var current = start;
        var numberOfSteps = 0;
        var directions = Input.Lines[0];
        while (true)
        {
            for (int i = 0; i < directions.Length; i++)
            {
                var direction = directions[i];
                var destinations = sourceDestinationMap[current];

                if (direction == 'L')
                {
                    // Go left
                    current = destinations.Left;
                }
                else
                {
                    // Go right
                    current = destinations.Right;
                }

                // Increment steps
                numberOfSteps++;

                // Check for end
                if (current == end)
                {
                    goto endLoop;
                }
            }
        }
        endLoop:

        return numberOfSteps.ToString();
    }
    private readonly record struct DestinationInfo(ushort Left, ushort Right, bool IsEnd);

    protected override string? Problem2()
    {
        var end = GetKey("ZZZ");
        Span<ushort> positions = stackalloc ushort[12];
        var numberOfStartingPositions = 0;

        // Build up the mapping
        Span<DestinationInfo> sourceDestinationMap = stackalloc DestinationInfo[end + 1];
        for (int i = 2; i < Input.Lines.Length; i++)
        {
            var line = Input.Lines[i].AsSpan();

            var source = GetKey(line[..3]);

            var destinationLeft = GetKey(line[7..10]);
            var destinationRight = GetKey(line[12..15]);

            sourceDestinationMap[source] = new DestinationInfo(destinationLeft, destinationRight, line[2] == 'Z');

            // Check for a starting position
            if (line[2] == 'A')
            {
                positions[numberOfStartingPositions++] = source;
            }
        }

        // Set the length of the positions
        positions = positions[..numberOfStartingPositions];

        // Store the number of steps required for each starting position
        Span<ulong> numberOfStepsToFinish = stackalloc ulong[numberOfStartingPositions];

        // Process the directions until we reach the end
        ulong numberOfSteps = 0;
        var directions = Input.Lines[0];
        while (true)
        {
            for (int directionIndex = 0; directionIndex < directions.Length; directionIndex++)
            {
                var direction = directions[directionIndex];

                // Process for each position
                var numberOfPositionsFinished = 0;
                for (int positionIndex = 0; positionIndex < positions.Length; positionIndex++)
                {
                    ref var position = ref positions[positionIndex];
                    var destinationInfo = sourceDestinationMap[position];

                    // Check whether we've reached the end
                    if (destinationInfo.IsEnd)
                    {
                        if (numberOfStepsToFinish[positionIndex] == 0)
                        {
                            numberOfStepsToFinish[positionIndex] = numberOfSteps;
                        }

                        numberOfPositionsFinished++;
                        continue;
                    }

                    // Move in the new direction
                    if (direction == 'L')
                    {
                        // Go left
                        position = destinationInfo.Left;
                    }
                    else
                    {
                        // Go right
                        position = destinationInfo.Right;
                    }
                }

                // Check whether we've finished collecting info for each starting position
                if (numberOfPositionsFinished == numberOfStartingPositions)
                {
                    goto endLoop;
                }

                // Increment steps
                numberOfSteps++;
            }
        }
    endLoop:

        // Get the number of steps required for all to finish
        // (take the LCM of each one in turn)
        var numberOfStepsRequiredForAll = numberOfStepsToFinish[0];
        for (int i = 1; i < numberOfStepsToFinish.Length; i++)
        {
            numberOfStepsRequiredForAll = Helpers.LeastCommonMultiple(
                numberOfStepsRequiredForAll,
                numberOfStepsToFinish[i]);
        }

        return numberOfStepsRequiredForAll.ToString();
    }

    private ushort GetKey(ReadOnlySpan<char> value)
    {
        const int Multiplier = 'Z' - 'A' + 1;
        const int Multiplier2 = Multiplier * Multiplier;

        return (ushort)(
            ((value[0] - 'A') * Multiplier2) +
            ((value[1] - 'A') * Multiplier) +
            (value[2] - 'A'));
    }
}

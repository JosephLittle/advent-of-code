﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(18)]
[SolutionInput("Inputs/Day_18_Test.txt", Problem1Solution = "64", Problem2Solution = "58")]
[SolutionInput("Inputs/Day_18.txt", Problem1Solution = "3396", Problem2Solution = "2044", Benchmark = true)]
public unsafe class Day_18 : Solution
{
    const int Width = 20;
    const int Height = 20;
    const int Depth = 20;

    const int BorderThickness = 2;
    const int WidthWithBorder = Width + (BorderThickness * 2);
    const int HeightWithBorder = Height + (BorderThickness * 2);
    const int DepthWithBorder = Depth + (BorderThickness * 2);

    const int XStep = 1;
    const int YStep = Width;
    const int ZStep = Width * Height;

    const int XStepWithBorder = 1;
    const int YStepWithBorder = WidthWithBorder;
    const int ZStepWithBorder = WidthWithBorder * HeightWithBorder;

    private struct WorldCell
    {
        public bool IsOccupied;
        public bool IsVisited;
    }

    public Day_18(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Create the 3d map with a border so we don't have to worry about bounds checking
        var worldMap = stackalloc bool[WidthWithBorder * HeightWithBorder * DepthWithBorder];

        // Get the 0, 0, 0 cell
        var zeroCell = GetCellWithBorder(worldMap, BorderThickness, BorderThickness, BorderThickness);

        // Track the surface area
        var surfaceAreaCount = 0;

        // Process the input
        for (int i = 0; i < Input.Lines.Length; i++)
        {
            var inputLine = Input.Lines[i];

            var charIndex = 0;
            var x = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');
            charIndex++;
            var y = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');
            charIndex++;
            var z = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');

            // Get the cell
            var currentCell = GetCell(zeroCell, (int)x, (int)y, (int)z);

            // Check adjacencies
            var occupiedCount = 
                Helpers.BoolToInt(*(currentCell - XStep)) +
                Helpers.BoolToInt(*(currentCell + XStep)) +
                Helpers.BoolToInt(*(currentCell - YStep)) +
                Helpers.BoolToInt(*(currentCell + YStep)) +
                Helpers.BoolToInt(*(currentCell - ZStep)) +
                Helpers.BoolToInt(*(currentCell + ZStep));
            var unoccupiedCount = 6 - occupiedCount;

            // Update the count
            surfaceAreaCount += unoccupiedCount;
            surfaceAreaCount -= occupiedCount;

            // Occupy the cell
            *currentCell = true;
        }

        return surfaceAreaCount.ToString();
    }

    protected override string? Problem2()
    {
        // Create the 3d map with a border so we don't have to worry about bounds checking
        var worldMap = stackalloc WorldCell[WidthWithBorder * HeightWithBorder * DepthWithBorder];

        // Set the border cells as occupied
        for (int x = 0; x < WidthWithBorder; x++)
        {
            for (int y = 0; y < HeightWithBorder; y++)
            {
                // Set front face
                GetCellWithBorder(worldMap, x, y, 0)->IsVisited = true;

                // Set back face
                GetCellWithBorder(worldMap, x, y, DepthWithBorder - 1)->IsVisited = true;
            }
        }
        for (int z = 0; z < DepthWithBorder; z++)
        {
            for (int y = 0; y < HeightWithBorder; y++)
            {
                // Set left face
                GetCellWithBorder(worldMap, 0, y, z)->IsVisited = true;

                // Set back face
                GetCellWithBorder(worldMap, WidthWithBorder - 1, y, z)->IsVisited = true;
            }
        }
        for (int z = 0; z < DepthWithBorder; z++)
        {
            for (int x = 0; x < WidthWithBorder; x++)
            {
                // Set top face
                GetCellWithBorder(worldMap, x, 0, z)->IsVisited = true;

                // Set bottom face
                GetCellWithBorder(worldMap, x, HeightWithBorder - 1, z)->IsVisited = true;
            }
        }

        // Process the input and occupy cells
        for (int i = 0; i < Input.Lines.Length; i++)
        {
            var inputLine = Input.Lines[i];

            var charIndex = 0;
            var x = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');
            charIndex++;
            var y = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');
            charIndex++;
            var z = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(inputLine, ref charIndex, ',');

            // Get the cell and occupy it
            var currentCell = GetCellWithBorder(worldMap, 
                (int)x + BorderThickness, 
                (int)y + BorderThickness, 
                (int)z + BorderThickness);
            currentCell->IsOccupied = true;
        }

        // Create a stack for the search
        var toVisitStackSize = WidthWithBorder * HeightWithBorder * DepthWithBorder;
        var toVisitStack = stackalloc WorldCell*[toVisitStackSize];
        var toVisitStackCount = 0;

        // Start at the 1,1,1
        toVisitStack[toVisitStackCount++] = GetCellWithBorder(worldMap, 1, 1, 1);

        // Perform a breadth first search, counting the surface area
        var surfaceAreaCount = 0;
        while (toVisitStackCount > 0)
        {
            var currentCell = toVisitStack[--toVisitStackCount];

            // Get adjacent cells
            var leftCell  = currentCell - XStepWithBorder;
            var rightCell = currentCell + XStepWithBorder;
            var upCell    = currentCell - YStepWithBorder;
            var downCell  = currentCell + YStepWithBorder;
            var frontCell = currentCell - ZStepWithBorder;
            var backCell  = currentCell + ZStepWithBorder;

            // Count the surface area
            surfaceAreaCount +=
                Helpers.BoolToInt(leftCell->IsOccupied) +
                Helpers.BoolToInt(rightCell->IsOccupied) +
                Helpers.BoolToInt(upCell->IsOccupied) +
                Helpers.BoolToInt(downCell->IsOccupied) +
                Helpers.BoolToInt(frontCell->IsOccupied) +
                Helpers.BoolToInt(backCell->IsOccupied);

            // Visit if not already
            if (!leftCell->IsOccupied & !leftCell->IsVisited)
            {
                leftCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = leftCell;
            }
            if (!rightCell->IsOccupied & !rightCell->IsVisited)
            {
                rightCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = rightCell;
            }
            if (!upCell->IsOccupied & !upCell->IsVisited)
            {
                upCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = upCell;
            }
            if (!downCell->IsOccupied & !downCell->IsVisited)
            {
                downCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = downCell;
            }
            if (!frontCell->IsOccupied & !frontCell->IsVisited)
            {
                frontCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = frontCell;
            }
            if (!backCell->IsOccupied & !backCell->IsVisited)
            {
                backCell->IsVisited = true;
                toVisitStack[toVisitStackCount++] = backCell;
            }
        }

        return surfaceAreaCount.ToString();
    }

    private void PrintWorldMap(WorldCell* worldMap)
    {
        for (int z = 0; z < DepthWithBorder; z++)
        {
            for (int y = 0; y < HeightWithBorder; y++)
            {
                for (int x = 0; x < WidthWithBorder; x++)
                {
                    var cell = GetCellWithBorder(worldMap, x, y, z);
                    if (cell->IsOccupied)
                    {
                        if (cell->IsVisited)
                        {
                            Console.Write('?');
                        }
                        else
                        {
                            Console.Write('#');
                        }
                    }
                    else
                    {
                        if (cell->IsVisited)
                        {
                            Console.Write(',');
                        }
                        else
                        {
                            Console.Write('.');
                        }
                    }
                    //Console.Write(cell->IsOccupied ? '#' : '.');
                }
                Console.WriteLine();
            }
            Console.WriteLine("Depth: " + z);
            Console.ReadKey();
        }
    }

    private T* GetCell<T>(T* startCell, int x, int y, int z) 
        where T : unmanaged
    {
        return startCell +
            (XStep * x) +
            (YStep * y) +
            (ZStep * z);
    }

    private T* GetCellWithBorder<T>(T* startCell, int x, int y, int z)
        where T : unmanaged
    {
        return startCell +
            (XStepWithBorder * x) +
            (YStepWithBorder * y) +
            (ZStepWithBorder * z);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public unsafe static class Helpers
    {
        public static int CharToInt(char c)
        {
            return (int)(c - 48);
        }

        public static int BoolToInt(bool b)
        {
            return *(int*)&b;
        }

        public static int BinaryStringToInt12Bit(string binaryString)
        {
            var result = 0;
            result += CharToInt(binaryString[11]) << 0;
            result += CharToInt(binaryString[10]) << 1;
            result += CharToInt(binaryString[9]) << 2;
            result += CharToInt(binaryString[8]) << 3;
            result += CharToInt(binaryString[7]) << 4;
            result += CharToInt(binaryString[6]) << 5;
            result += CharToInt(binaryString[5]) << 6;
            result += CharToInt(binaryString[4]) << 7;
            result += CharToInt(binaryString[3]) << 8;
            result += CharToInt(binaryString[2]) << 9;
            result += CharToInt(binaryString[1]) << 10;
            result += CharToInt(binaryString[0]) << 11;

            return result;
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            var temp = a;
            a = b;
            b = temp;
        }

        public static IEnumerable<(T, T)> AllPairs<T>(IEnumerable<T> source)
        {
            foreach (var left in source)
            {
                foreach (var right in source)
                {
                    if (left.Equals(right))
                    {
                        continue;
                    }

                    yield return (left, right);
                }
            }
        }
    }
}

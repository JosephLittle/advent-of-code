﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(3)]
[SolutionInput("Inputs/Day_03_Test.txt", Problem1Solution = "157", Problem2Solution = "70")]
[SolutionInput("Inputs/Day_03.txt", Problem1Solution = "7763", Problem2Solution = "2569", Benchmark = true)]
public unsafe class Day_03 : Solution
{
    public Day_03(Input input) 
        : base(input)
    { }

    protected override string? Problem1()
    {
        var inputLines = Input.Lines;

        // Create the item to value mapping
        var itemToValueMapping = new int[256];
        for (char c = 'a'; c <= 'z'; c++)
        {
            itemToValueMapping[c] = (c - 'a') + 1;
        }
        for (char c = 'A'; c <= 'Z'; c++)
        {
            itemToValueMapping[c] = (c - 'A') + 27;
        }

        var itemValueSum = 0;

        // Iterate over each line
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex++)
        {
            var line = inputLines[lineIndex];

            ulong itemsPresentAsBits = 0;

            // Iterate over the first half
            for (int itemIndex = 0; itemIndex < line.Length / 2; itemIndex++)
            {
                var item = line[itemIndex];

                // Add this item to thee items present
                itemsPresentAsBits |= 1ul << itemToValueMapping[item];
            }

            // Iterate over the second half
            for (int itemIndex = line.Length / 2; itemIndex < line.Length; itemIndex++)
            {
                var item = line[itemIndex];

                // Check whether this item is already present
                var itemValue = itemToValueMapping[item];
                var itemBit = 1ul << itemValue;
                var isItemPresent = (itemsPresentAsBits & itemBit) == itemBit;

                // Zero the item bit out so we don't double count
                itemsPresentAsBits &= ~itemBit;

                // Sum this item if it was already present
                var valueMultiplier = *(int*)(&isItemPresent);
                itemValueSum += itemValue * valueMultiplier;
            }
        }

        return itemValueSum.ToString();
    }

    protected override string? Problem2()
    {
        var inputLines = Input.Lines;

        // Create the item to value mapping
        var itemToValueMapping = new int[256];
        for (char c = 'a'; c <= 'z'; c++)
        {
            itemToValueMapping[c] = (c - 'a') + 1;
        }
        for (char c = 'A'; c <= 'Z'; c++)
        {
            itemToValueMapping[c] = (c - 'A') + 27;
        }

        ulong itemValueSum = 0;

        // Iterate over the lines in groups of 3
        for (int lineIndex = 0; lineIndex < inputLines.Length; lineIndex += 3)
        {
            var itemsForLine1 = GetItemsInLineAsBits(inputLines[lineIndex + 0]);
            var itemsForLine2 = GetItemsInLineAsBits(inputLines[lineIndex + 1]);
            var itemsForLine3 = GetItemsInLineAsBits(inputLines[lineIndex + 2]);

            var intersectionOfItems =
                itemsForLine1 &
                itemsForLine2 &
                itemsForLine3;

            var valueOfCommonItem = GetIndexOfBitSet(intersectionOfItems);
            itemValueSum += valueOfCommonItem;
        }

        ulong GetIndexOfBitSet(ulong n)
        {
            ulong i = 0;
            while (i < 64 & n != 1)
            {
                n >>= 1;
                i++;
            }

            return i;
        }

        ulong GetItemsInLineAsBits(string line)
        {
            ulong itemsPresentAsBits = 0;
            for (int itemIndex = 0; itemIndex < line.Length; itemIndex++)
            {
                var item = line[itemIndex];
                itemsPresentAsBits |= 1ul << itemToValueMapping[item];
            }
            return itemsPresentAsBits;
        }

        return itemValueSum.ToString();
    }
}
﻿using AdventOfCode.Framework;
using System.ComponentModel.DataAnnotations;

namespace AdventOfCode_2023;

[Solution(18)]
[SolutionInput("Inputs/Day_18_Test.txt", Problem1Solution = "62", Problem2Solution = "952408144115")]
[SolutionInput("Inputs/Day_18.txt", Problem1Solution = "52231", Problem2Solution = "57196493937398", Benchmark = true)]
public unsafe class Day_18 : Solution
{
    private readonly record struct Position(long X, long Y)
    {
        public static Position operator +(Position a, Position b) => new Position(a.X + b.X, a.Y + b.Y);
    }
    private readonly record struct DirectionAndDistance(Direction Direction, long Distance);

    private enum Direction : byte
    {
        Right = 0,
        Down  = 1,
        Left  = 2,
        Up    = 3,
    }

    public Day_18(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        // Parse the input into directions and distances
        var directionsAndDistances = new List<DirectionAndDistance>(Input.Lines.Length);
        foreach (var line in Input.Lines)
        {
            // Set the previous direction
            Direction direction = default;
            switch (line[0])
            {
                case 'U':
                    direction = Direction.Up;
                    break;
                case 'D':
                    direction = Direction.Down;
                    break;
                case 'L':
                    direction = Direction.Left;
                    break;
                case 'R':
                    direction = Direction.Right;
                    break;
            }

            // Get the distance
            var startIndex = 2;
            var distance = Helpers.ParseUnsignedIntegerUntilDelimiterOrEnd(line, ref startIndex, ' ');

            directionsAndDistances.Add(new DirectionAndDistance(direction, (long)distance));
        }

        return Solution(directionsAndDistances).ToString();
    }

    protected override string? Problem2()
    {
        // Parse the input into directions and distances
        var directionsAndDistances = new List<DirectionAndDistance>(Input.Lines.Length);
        foreach (var line in Input.Lines)
        {
            // Parse the first 5 hex digits as the distance
            var startOfHex = line.IndexOf('#');
            var distance = (long)Convert.ToUInt64(line[(startOfHex + 1)..^2], 16);

            // And the last as the direction
            var direction = (Direction)Helpers.CharToInt(line[^2]);

            directionsAndDistances.Add(new DirectionAndDistance(direction, distance));
        }

        return Solution(directionsAndDistances).ToString();
    }

    private static long Solution(List<DirectionAndDistance> directionsAndDistances)
    {
        // Work out whether the path we're following has the centre on the right side
        var previousDirection = directionsAndDistances[^1].Direction;
        var numberOfLeftTurns = 0;
        var numberOfRightTurns = 0;
        foreach (var (direction, _) in directionsAndDistances)
        {
            switch (direction)
            {
                case Direction.Up:
                    numberOfLeftTurns += Helpers.BoolToInt(previousDirection == Direction.Right);
                    numberOfRightTurns += Helpers.BoolToInt(previousDirection == Direction.Left);
                    break;
                case Direction.Down:
                    numberOfLeftTurns += Helpers.BoolToInt(previousDirection == Direction.Left);
                    numberOfRightTurns += Helpers.BoolToInt(previousDirection == Direction.Right);
                    break;
                case Direction.Left:
                    numberOfLeftTurns += Helpers.BoolToInt(previousDirection == Direction.Up);
                    numberOfRightTurns += Helpers.BoolToInt(previousDirection == Direction.Down);
                    break;
                case Direction.Right:
                    numberOfLeftTurns += Helpers.BoolToInt(previousDirection == Direction.Down);
                    numberOfRightTurns += Helpers.BoolToInt(previousDirection == Direction.Up);
                    break;
            }
            previousDirection = direction;
        }
        var isRightSideInsde = numberOfRightTurns > numberOfLeftTurns;

        // Now go through the path and create each side with its start and end and inward facing direction
        long currentX = 0;
        long currentY = 0;
        long previousX = 0;
        long previousY = 0;
        var positions = new List<Position>();
        foreach (var (direction, distance) in directionsAndDistances)
        {
            switch (direction)
            {
                case Direction.Up:
                    currentY -= distance;
                    break;
                case Direction.Down:
                    currentY += distance;
                    break;
                case Direction.Left:
                    currentX -= distance;
                    break;
                case Direction.Right:
                    currentX += distance;
                    break;
            }

            previousX = currentX;
            previousY = currentY;

            positions.Add(new Position(currentX, currentY));
        }

        positions.Add(positions[0]);
        var area = (long)Math.Abs(
            positions.Take(positions.Count - 1).Select((p, i) => (positions[i + 1].X - p.X) * (positions[i + 1].Y + p.Y)).Sum() / 2.0);
        var line = (directionsAndDistances.Sum(dd => dd.Distance) / 2) + 1;

        return area + line;
    }
}
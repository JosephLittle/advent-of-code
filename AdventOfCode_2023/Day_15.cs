﻿using AdventOfCode.Framework;

namespace AdventOfCode_2023;

[Solution(15)]
[SolutionInput("Inputs/Day_15_Test.txt", Problem1Solution = "1320", Problem2Solution = "145")]
[SolutionInput("Inputs/Day_15.txt", Problem1Solution = "517015", Problem2Solution = "286104", Benchmark = true)]
public unsafe class Day_15 : Solution
{
    public Day_15(Input input)
        : base(input)
    { }

    protected override string? Problem1()
    {
        var line = Input.Lines[0];

        ulong currentStepValue = 0;
        ulong sumStepValue = 0;

        // Process each step in the sequence
        for (int i = 0; i < line.Length; i++)
        {
            var character = line[i];

            if (character == ',')
            {
                // Delimiter, so sum and reset value
                sumStepValue += currentStepValue;
                currentStepValue = 0;
            }
            else
            {
                // Character, so add to step value
                currentStepValue += character;
                currentStepValue *= 17ul;
                currentStepValue %= 256;
            }
        }

        // Add the final step value
        sumStepValue += currentStepValue;

        return sumStepValue.ToString();
    }

    private readonly record struct Lens(string Label, ulong FocalLength);

    protected override string? Problem2()
    {
        var line = Input.Lines[0];

        // Initialise the lens boxes
        var lensBoxes = new List<Lens>[256];
        for (int i = 0; i < lensBoxes.Length; i++)
        {
            lensBoxes[i] = new List<Lens>(10);
        }

        ulong currentLabelValue = 0;
        int currentLabelStart = 0;

        // Process each step in the sequence
        for (int i = 0; i < line.Length; i++)
        {
            var character = line[i];

            // Parse until a - or =
            if (character == '-')
            {
                // Get the label
                var currentLabel = line[currentLabelStart..i];

                // Remove this lens from its box
                var lenses = lensBoxes[currentLabelValue];
                for (int lensIndex = lenses.Count - 1; lensIndex >= 0; lensIndex--)
                {
                    if (lenses[lensIndex].Label == currentLabel)
                    {
                        // Lens found, so remove
                        lenses.RemoveAt(lensIndex);
                    }
                }

                // Reset the label
                currentLabelValue = 0;

                // Skip to the delimiter
                i += 1;
                currentLabelStart = i + 1;
            }
            else if (character == '=')
            {
                // Get the focal length of this lens
                var focalLength = (ulong)Helpers.CharToInt(line[i + 1]);

                // Create the lens
                var currentLabel = line[currentLabelStart..i];
                var lens = new Lens(currentLabel, focalLength);

                // Update or add the lens in its box
                var lenses = lensBoxes[currentLabelValue];
                for (int lensIndex = 0; lensIndex < lenses.Count; lensIndex++)
                {
                    if (lenses[lensIndex].Label == currentLabel)
                    {
                        // Lens found, so update
                        lenses[lensIndex] = lens;
                        goto LensFound;
                    }
                }

                // Lens not found, so add to the end
                lenses.Add(lens);

                // Goto, used if lens is found
                LensFound:

                // Reset the label
                currentLabelValue = 0;

                // Skip to the delimiter
                i += 2;
                currentLabelStart = i + 1;
            }
            else
            {
                // Label character, so add to step value
                currentLabelValue += character;
                currentLabelValue *= 17ul;
                currentLabelValue %= 256;
            }
        }

        // Go through the lens boxes, adding up the focussing power
        ulong focussingPower = 0;
        for (int boxIndex = 0; boxIndex < lensBoxes.Length; boxIndex++)
        {
            var lensBox = lensBoxes[boxIndex];

            for (int lensIndex = 0; lensIndex < lensBox.Count; lensIndex++)
            {
                focussingPower +=
                    (ulong)(1 + boxIndex) *
                    (ulong)(1 + lensIndex) *
                    lensBox[lensIndex].FocalLength;

            }
        }

        return focussingPower.ToString();
    }
}

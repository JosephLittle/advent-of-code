﻿using System.Diagnostics;

namespace AdventOfCode
{
    public unsafe static class Program
    {
        public static void Main()
        {
            var sw = Stopwatch.StartNew();
            //Day_1.PartOne();
            //Day_1.PartTwo();
            //Day_2.PartOne();
            //Day_2.PartTwo();
            //Day_3.PartOne();
            //Day_3.PartTwo();
            //Day_4.PartOneAndTwo();
            //Day_5.PartOne();
            //Day_5.PartTwo();
            //Day_6.PartOne();
            //Day_6.PartTwo();
            //Day_7.PartOne();
            //Day_7.PartTwo();
            //Day_8.PartTwo();
            //Day_9.PartOne();
            //Day_9.PartTwo();
            //Day_10.PartOneAndTwo();
            //Day_11.PartOneAndTwo();
            //Day_12.PartOne();
            //Day_12.PartTwo();
            //Day_12.Redo();
            //Day_13.PartOneAndTwo();
            //Day_14.PartOneAndTwo();
            //Day_15.PartOneDijkstra();
            //Day_15.PartTwoDijkstra();
            //Day_16.PartOneAndTwo();
            //Day_17.PartOneAndTwo();
            //Day_18.PartOne();
            //Day_18.PartTwo();
            //Day_19.PartOneAndTwo();
            //Day_20.PartOneAndTwo();
            //Day_21.PartOne();
            //Day_21.PartTwo();
            //Day_22.PartOne();
            //Day_22.PartTwo();
            //Day_23.PartOne();
            //Day_23.PartTwo();
            //Day_24.PartOne();
            Day_25.PartOne();

            Console.WriteLine("Time Taken: {0}", sw.Elapsed);
        }
    }
}
﻿using AdventOfCode.Framework;

namespace AdventOfCode_2022;

[Solution(21)]
[SolutionInput("Inputs/Day_21_Test.txt", Problem1Solution = "152", Problem2Solution = "301")]
[SolutionInput("Inputs/Day_21.txt", Problem1Solution = "38914458159166", Problem2Solution = "3665520865940", Benchmark = true)]
public unsafe class Day_21 : Solution
{
    private struct Monkey
    {
        public int ParentNameKey;

        public long Value;

        public char Operation;

        public int OperationLhsParameterNameKey;
        public int OperationRhsParameterNameKey;
    }

    public Day_21(Input input) 
        : base(input)
    {
        // No monkey has more than one parent
    }

    protected override string? Problem1()
    {
        // Set up the monkeys
        (var keysToMonkeys, var leafKeys) = SetUpMonkeys();

        // Process the tree
        ProcessTree(keysToMonkeys, leafKeys);

        // Return the value of the root monkey
        return keysToMonkeys[NameStringToKey("root", 0)].Value.ToString();
    }

    protected override string? Problem2()
    {
        // Set up the monkeys
        (var keysToMonkeys, var leafKeys) = SetUpMonkeys();

        var rootKey = NameStringToKey("root", 0);
        var humanKey = NameStringToKey("humn", 0);

        // Get all the keys on the left hand side of the tree
        var rootMonkey = keysToMonkeys[rootKey];
        var lhsRootKey = rootMonkey.OperationLhsParameterNameKey;
        var leafKeysInLhsOfTree = new HashSet<int>(keysToMonkeys.Count);
        GetLeafKeysRecursive(lhsRootKey);
        void GetLeafKeysRecursive(int key)
        {
            // Get this monkey and check any of its children
            var monkey = keysToMonkeys[key];            
            if (monkey.Value < 0)
            {
                GetLeafKeysRecursive(monkey.OperationLhsParameterNameKey);
                GetLeafKeysRecursive(monkey.OperationRhsParameterNameKey);
            }
            else
            {
                // This monkey is a leaf, so store its key
                leafKeysInLhsOfTree.Add(key);
            }
        }

        // Check if the human is in th left side of the tree, then use this to get
        // the leaf keys for the side of the tree without the human
        var leafKeysForSingleSide = leafKeys;
        var leafKeysForHumanSide = leafKeys;
        int singleSideRootKey;
        int humanSideRootKey;
        if (leafKeysInLhsOfTree.Contains(humanKey))
        {
            leafKeysForSingleSide.ExceptWith(leafKeysInLhsOfTree);
            leafKeysForHumanSide = leafKeysInLhsOfTree;
            singleSideRootKey = rootMonkey.OperationRhsParameterNameKey;
            humanSideRootKey = rootMonkey.OperationLhsParameterNameKey;
        }
        else
        {
            leafKeysForSingleSide = leafKeysInLhsOfTree;
            leafKeysForHumanSide.ExceptWith(leafKeysInLhsOfTree);
            singleSideRootKey = rootMonkey.OperationLhsParameterNameKey;
            humanSideRootKey = rootMonkey.OperationRhsParameterNameKey;
        }

        // Disconnect the single side root from the root, so we can process it in isolation
        var singleSideRootMonkey = keysToMonkeys[singleSideRootKey];
        singleSideRootMonkey.ParentNameKey = -1;
        keysToMonkeys[singleSideRootKey] = singleSideRootMonkey;

        // Process the single side
        ProcessTree(keysToMonkeys, leafKeysForSingleSide);

        // Get the value of the single side, this is the value we need to run through the opposite
        // side of the tree to work out the value of the human node
        var singleSideValue = keysToMonkeys[singleSideRootKey].Value;

        // Clear the value of the human node so we can calculate it ourselves
        var humanNode = keysToMonkeys[humanKey];
        humanNode.Value = -1;
        keysToMonkeys[humanKey] = humanNode;

        // Process the human side as much as possible
        ProcessTree(keysToMonkeys, leafKeysForHumanSide);

        // Set the value of the human side root so we can process from the top down
        var humanSideRootMonkey = keysToMonkeys[humanSideRootKey];
        humanSideRootMonkey.Value = singleSideValue;
        keysToMonkeys[humanSideRootKey] = humanSideRootMonkey;

        // Now process the human side from the top down
        ProcessTreeTopDownRecursive(humanSideRootMonkey);
        void ProcessTreeTopDownRecursive(Monkey monkey)
        {
            // Return if this is a leaf
            if (monkey.OperationLhsParameterNameKey == -1 &
                monkey.OperationRhsParameterNameKey == -1)
            {
                return;
            }

            // Get the parameter monkeys
            var lhsMonkey = keysToMonkeys[monkey.OperationLhsParameterNameKey];
            var rhsMonkey = keysToMonkeys[monkey.OperationRhsParameterNameKey];

            // Check which one doesn't have a value (the value we need to calculate
            if (lhsMonkey.Value >= 0)
            {
                // Perform the opposite operation, setting the rhs monkey's value
                switch (monkey.Operation)
                {
                    case '+':
                        rhsMonkey.Value = monkey.Value - lhsMonkey.Value;
                        break;
                    case '-':
                        rhsMonkey.Value = lhsMonkey.Value - monkey.Value;
                        break;
                    case '/':
                        rhsMonkey.Value = lhsMonkey.Value / monkey.Value;
                        break;
                    case '*':
                        rhsMonkey.Value = monkey.Value / lhsMonkey.Value;
                        break;
                }

                // Store the result
                keysToMonkeys[monkey.OperationRhsParameterNameKey] = rhsMonkey;

                // Process this side
                ProcessTreeTopDownRecursive(rhsMonkey);
            }
            else if (rhsMonkey.Value >= 0)
            {
                // Perform the opposite operation, setting the lhs monkey's value
                switch (monkey.Operation)
                {
                    case '+':
                        lhsMonkey.Value = monkey.Value - rhsMonkey.Value;
                        break;
                    case '-':
                        lhsMonkey.Value = rhsMonkey.Value + monkey.Value;
                        break;
                    case '/':
                        lhsMonkey.Value = rhsMonkey.Value * monkey.Value;
                        break;
                    case '*':
                        lhsMonkey.Value = monkey.Value / rhsMonkey.Value;
                        break;
                }

                // Store the result
                keysToMonkeys[monkey.OperationLhsParameterNameKey] = lhsMonkey;

                // Process this side
                ProcessTreeTopDownRecursive(lhsMonkey);
            }
            else
            {
                throw new Exception("Neither parameter monkey has a value, shouldn't happen!");
            }
        }

        // Return the value of the human monkey
        return keysToMonkeys[humanKey].Value.ToString();
    }

    private (Dictionary<int, Monkey> keysToMonkeys, HashSet<int> leafKeys) SetUpMonkeys()
    {
        var inputLines = Input.Lines;
        var numberOfMonkeys = inputLines.Length;

        var keysToMonkeys = new Dictionary<int, Monkey>(numberOfMonkeys);
        var leafKeys = new HashSet<int>(numberOfMonkeys);

        // Process each input line
        for (int i = 0; i < inputLines.Length; i++)
        {
            var inputLine = inputLines[i];

            // Get the monkey's name key
            var key = NameStringToKey(inputLine, 0);

            // Set default values
            var operation = ' ';
            var lhsMonkeyKey = -1;
            var rhsMonkeyKey = -1;
            var value = -1;

            // If the line is 17 chars long, then it has an operation 6 11 13
            if (inputLine.Length == 17)
            {
                lhsMonkeyKey = NameStringToKey(inputLine, 6);
                rhsMonkeyKey = NameStringToKey(inputLine, 13);
                operation = inputLine[11];

                // Set the parents of these monkeys
                keysToMonkeys.TryGetValue(lhsMonkeyKey, out var lhsMonkey);
                keysToMonkeys.TryGetValue(rhsMonkeyKey, out var rhsMonkey);
                lhsMonkey.ParentNameKey = key;
                rhsMonkey.ParentNameKey = key;
                keysToMonkeys[lhsMonkeyKey] = lhsMonkey;
                keysToMonkeys[rhsMonkeyKey] = rhsMonkey;
            }
            else
            {
                value = Helpers.SubstringToSignedInt(inputLine, 6, inputLine.Length - 6);

                // Add to the leaf keys
                leafKeys.Add(key);
            }

            // Add or update the monkey info
            keysToMonkeys.TryGetValue(key, out var monkey);
            monkey.Operation = operation;
            monkey.OperationLhsParameterNameKey = lhsMonkeyKey;
            monkey.OperationRhsParameterNameKey = rhsMonkeyKey;
            monkey.Value = value;
            keysToMonkeys[key] = monkey;
        }

        return (keysToMonkeys, leafKeys);
    }

    private void ProcessTree(Dictionary<int, Monkey> keysToMonkeys, HashSet<int> leafKeys)
    {
        var keysToCheck = leafKeys;
        var keysToCheckNext = new HashSet<int>(keysToCheck.Count);

        // First get the parents of the inital keys to check (the value monkeys)
        foreach (var key in keysToCheck)
        {
            var monkey = keysToMonkeys[key];
            keysToCheckNext.Add(monkey.ParentNameKey);
        }

        // Run until we've processed all monkeys
        while (keysToCheck.Count > 0)
        {
            // Check whether we have the same keys to check next (indicates that we've
            // processed as much as we can and should bail out now)
            if (keysToCheck.SetEquals(keysToCheckNext))
            {
                break;
            }

            // Swap the keys to check
            (keysToCheck, keysToCheckNext) = (keysToCheckNext, keysToCheck);
            keysToCheckNext.Clear();

            // Process each key to check
            foreach (var key in keysToCheck)
            {
                var monkey = keysToMonkeys[key];

                // Get the parameter monkeys
                var lhsMonkey = keysToMonkeys[monkey.OperationLhsParameterNameKey];
                var rhsMonkey = keysToMonkeys[monkey.OperationRhsParameterNameKey];

                // Check next time if both parameter monkeys don't have values
                if (lhsMonkey.Value < 0 | rhsMonkey.Value < 0)
                {
                    keysToCheckNext.Add(key);
                }
                else
                {
                    // Perform the operation
                    switch (monkey.Operation)
                    {
                        case '+':
                            monkey.Value = lhsMonkey.Value + rhsMonkey.Value;
                            break;
                        case '-':
                            monkey.Value = lhsMonkey.Value - rhsMonkey.Value;
                            break;
                        case '/':
                            monkey.Value = lhsMonkey.Value / rhsMonkey.Value;
                            break;
                        case '*':
                            monkey.Value = lhsMonkey.Value * rhsMonkey.Value;
                            break;
                    }

                    // Store the new monkey info
                    keysToMonkeys[key] = monkey;

                    // Add the parent to the next keys to check
                    if (monkey.ParentNameKey > 0)
                    {
                        keysToCheckNext.Add(monkey.ParentNameKey);
                    }
                }

            }
        }
    }

    private int NameStringToKey(string line, int nameStartIndex)
    {
        return (
            ((line[nameStartIndex + 0] - 'a') * 100_00_00) +
            ((line[nameStartIndex + 1] - 'a') * 100_00) +
            ((line[nameStartIndex + 2] - 'a') * 100) +
             (line[nameStartIndex + 3] - 'a'));
    }

    private string NameKeyToString(int key)
    {
        var char4 = (char)((key % 100) + 'a');
        var char3 = (char)(((key / 100) % 100) + 'a');
        var char2 = (char)(((key / 100_00) % 100) + 'a');
        var char1 = (char)(((key / 100_00_00) % 100) + 'a');
        return new string(new[] { char1, char2, char3, char4 });
    }
}

﻿using System.Runtime.InteropServices;

namespace AdventOfCode
{
    public unsafe class Day_24
    {
        private const byte INSTRUCTION_READ_INPUT = 0;
        private const byte INSTRUCTION_ADD        = 1;
        private const byte INSTRUCTION_MULTIPLY   = 2;
        private const byte INSTRUCTION_DIVIDE     = 3;
        private const byte INSTRUCTION_MODULO     = 4;
        private const byte INSTRUCTION_EQUALITY   = 5;

        private const int MEMORY_INDEX_W = 0;
        private const int MEMORY_INDEX_X = 1;
        private const int MEMORY_INDEX_Y = 2;
        private const int MEMORY_INDEX_Z = 3;

        private const int MINIMUM_LINE_LENGTH = 5;

        private delegate void ProgramOperation(long* operandLeft, long* operandRight);

        public static void PartOne()
        {
            var programInput = new long[14];

            (var program, var memory) = CompileProgram(Data);

            var handle = GCHandle.Alloc(memory, GCHandleType.Pinned);
            var memoryPointer = (long*)handle.AddrOfPinnedObject();

            // Set the operations
            var operations = new ProgramOperation[6];
            operations[INSTRUCTION_READ_INPUT] = (l, r) => { *l = programInput[(int)*r]; };
            operations[INSTRUCTION_ADD]        = (l, r) => { *l += *r; };
            operations[INSTRUCTION_MULTIPLY]   = (l, r) => { *l *= *r; };
            operations[INSTRUCTION_DIVIDE]     = (l, r) => { *l /= *r; };
            operations[INSTRUCTION_MODULO]     = (l, r) => { *l %= *r; };
            operations[INSTRUCTION_EQUALITY]   = (l, r) => { *l = Helpers.BoolToInt(*l == *r); };

            var statesHashSet = new HashSet<ProgramState>();

            var validNumbers = new List<long>();

            void Recursive(long w, long x, long y, long z, int instructionPointer, long inputNumber)
            {
                if (!statesHashSet.Add(new ProgramState((int)w, (int)x, (int)y, (int)z, (ushort)instructionPointer)))
                {
                    return;
                }

                // Check the instruction pointer
                if (instructionPointer >= program.Length)
                {
                    // Add the valid number if Z is 0
                    if (z == 0)
                    {
                        Console.WriteLine("Found valid input: {0}", inputNumber);
                        validNumbers.Add(inputNumber);
                    }
                    return;
                }

                // Load the state
                memory[0] = w;
                memory[1] = x;
                memory[2] = y;
                memory[3] = z;

                // Get the instruction
                var instruction = program[instructionPointer];

                // Check for an input
                if (instruction.Instruction == INSTRUCTION_READ_INPUT)
                {
                    // Recurse
                    inputNumber *= 10;
                    Recursive(1, x, y, z, instructionPointer + 1, inputNumber + 1);
                    Recursive(2, x, y, z, instructionPointer + 1, inputNumber + 2);
                    Recursive(3, x, y, z, instructionPointer + 1, inputNumber + 3);
                    Recursive(4, x, y, z, instructionPointer + 1, inputNumber + 4);
                    Recursive(5, x, y, z, instructionPointer + 1, inputNumber + 5);
                    Recursive(6, x, y, z, instructionPointer + 1, inputNumber + 6);
                    Recursive(7, x, y, z, instructionPointer + 1, inputNumber + 7);
                    Recursive(8, x, y, z, instructionPointer + 1, inputNumber + 8);
                    Recursive(9, x, y, z, instructionPointer + 1, inputNumber + 9);
                }
                else
                {
                    // Execute the program with this state
                    operations[instruction.Instruction](
                        memoryPointer + instruction.OperandLeft,
                        memoryPointer + instruction.OperandRight);

                    // Move to the next state
                    Recursive(memory[0], memory[1], memory[2], memory[3], instructionPointer + 1, inputNumber);
                }
            }
            Recursive(0, 0, 0, 0, 0, 0);

            // Print the valid numbers
            //foreach (var validNumber in validNumbers)
            //{
            //    Console.WriteLine(validNumbers);
            //}
        }

        /// <summary>
        /// Compile the given program string into the program's instructions and memory.
        /// </summary>
        private static (InstructionAndOperands[] Program, long[] Memory) CompileProgram(string programString)
        {
            // Set up the program memory (first 4 cells are the registers, the remainder are constant values)
            var memory = new List<long>(64)
            {
                0, // w
                0, // x
                0, // y
                0, // z
            };
            int GetRegisterMemoryIndex(char register)
            {
                return register - 'w';
            }
            int GetOrAddConstantMemoryIndex(long constant)
            {
                var indexOfConstant = memory.IndexOf(constant, 4);
                if (indexOfConstant == -1)
                {
                    // Add the constant since it does not exist yet
                    indexOfConstant = memory.Count;
                    memory.Add(constant);
                }

                return indexOfConstant;
            }

            // Parse the program line by line
            var lines = programString.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var program = new List<InstructionAndOperands>(lines.Length);
            var inputCounter = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                if (line.Length < MINIMUM_LINE_LENGTH)
                {
                    throw new Exception($"Found program line less than minimum length ({MINIMUM_LINE_LENGTH}): {line}");
                }

                // Get the instruction
                var instructionString = line.Remove(3);
                var instructionValue = byte.MaxValue;
                switch (instructionString)
                {
                    case "inp":
                        instructionValue = INSTRUCTION_READ_INPUT;
                        break;
                    case "add":
                        instructionValue = INSTRUCTION_ADD;
                        break;
                    case "mul":
                        instructionValue = INSTRUCTION_MULTIPLY;
                        break;
                    case "div":
                        instructionValue = INSTRUCTION_DIVIDE;
                        break;
                    case "mod":
                        instructionValue = INSTRUCTION_MODULO;
                        break;
                    case "eql":
                        instructionValue = INSTRUCTION_EQUALITY;
                        break;
                    default:
                        throw new Exception($"Unrecognised instruction: {instructionString}");
                }

                // Read the left operand (should always be a register)
                var operandLeft = GetRegisterMemoryIndex(line[4]);

                // Read the right operand (should only have it for operations instructions)
                var operandRight = -1;
                if (instructionValue == INSTRUCTION_READ_INPUT)
                {
                    if (line.Length != MINIMUM_LINE_LENGTH)
                    {
                        throw new Exception($"Found read input instruction line not of the expected length ({MINIMUM_LINE_LENGTH}): {line}");
                    }

                    // Set the right operand to the input pointer
                    operandRight = GetOrAddConstantMemoryIndex(inputCounter++);
                }
                else
                {
                    if (line.Length <= MINIMUM_LINE_LENGTH)
                    {
                        throw new Exception($"Found instruction line not of expected length: {line}");
                    }

                    var operandRightString = line.Substring(MINIMUM_LINE_LENGTH + 1);

                    // Get for a register
                    if (operandRightString.Length == 1 & char.IsLetter(operandRightString[0]))
                    {
                        operandRight = GetRegisterMemoryIndex(operandRightString[0]);
                    }
                    // Not a register, so a constant value
                    else
                    {
                        if (!long.TryParse(operandRightString, out var constant))
                        {
                            throw new Exception($"Failed to parsed constant value: {operandRightString}");
                        }

                        operandRight = GetOrAddConstantMemoryIndex(constant);
                    }
                }

                // Store the instruction
                program.Add(new InstructionAndOperands(instructionValue, operandLeft, operandRight));
            }

            return (program.ToArray(), memory.ToArray());
        }

        private readonly record struct ProgramState(int W, int X, int Y, int Z, ushort InstructionPointer);

        private readonly struct InstructionAndOperands
        {
            public readonly int Instruction;
            public readonly int OperandLeft;
            public readonly int OperandRight;

            public InstructionAndOperands(byte instruction, int operandLeft, int operandRight)
            {
                Instruction = instruction;
                OperandLeft = operandLeft;
                OperandRight = operandRight;
            }
        }

        private static readonly string DataNegate = @"inp x
mul x -1";

        private static readonly string DataIsThreeTimesLarger = @"inp z
inp x
mul z 3
eql z x";

        private static readonly string Data4BitSeparator = @"inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2";

        private static readonly string Data = @"inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 14
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 15
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -14
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 1
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x 0
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -6
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 13
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 3
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -3
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 8
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 13
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 14
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 15
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 4
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -2
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 7
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -9
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 15
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -2
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 1
mul y x
add z y";
    }
}